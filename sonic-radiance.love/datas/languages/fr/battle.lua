return {
  ['attack']    = "Attaquer",
  ['skills']    = "Techniques",
  ['objects']   = "Objets",
  ['defend']    = "Défendre",
  ['flee']      = "Fuite",
  ['back']      = "Retour",
  ['heal']      = "Médecines",
  ['rings']     = "Anneaux",
  ['wisps']     = "Wisps",
  ['other']     = "Autres",
}
