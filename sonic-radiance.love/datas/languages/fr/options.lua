return {
  ["inputs"]  = "Controles",
  ["video"]   = "Affichage",
  ["audio"]   = "Sons",
  ["langs"]   = "Langues",
  ["resolution"]  = "Resolution",
  ["fullscreen"]  = "Plein Ecran",
  ["borders"]     = "Bordures",
  ["vsync"]       = "Sync Vert.",
  ["back"]        = "Retour",
}
