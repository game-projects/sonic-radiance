return {
  ['spinattack']    = "Atk Tourbillon",
  ['spinjump']      = "Saut Tourbillon",
  ['spindash']      = "Charge Tourbillon",
  ['hommingattack'] = "Atk Téléguidée",
  ['sonicflare']    = "Sonic Flare",
  ['bluetornado']   = "Tornade Bleu",
  ['soniccracker']  = "Sonic Cracker",
  ['boost']         = "Boost",
  ['lightspeedattack']  = "L. Speed Atk",
}
