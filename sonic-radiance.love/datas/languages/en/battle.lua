return {
  ['attack']    = "Attack",
  ['skills']    = "Skills",
  ['objects']   = "Objects",
  ['defend']    = "Defends",
  ['flee']      = "Flee",
  ['back']      = "Back",
  ['heal']      = "Healing",
  ['rings']     = "Rings",
  ['wisps']     = "Wisps",
  ['other']     = "Other",
}
