return {
  ['spinattack']    = "Spin Attack",
  ['spinjump']      = "Spin Jump",
  ['spindash']      = "Spin Dash",
  ['hommingattack'] = "Homming Attack",
  ['sonicflare']    = "Sonic Flare",
  ['bluetornado']   = "Blue Tornado",
  ['soniccracker']  = "Sonic Cracker",
  ['boost']         = "Boost",
  ['lightspeedattack']  = "L. Speed Atk",
}
