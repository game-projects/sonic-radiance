return {
    datapacks = {
        ["battles"] = {"battles", {"test"}, true, {}},
        ["items"] = {"items", {"medicines", "powerups", "gloves", "shoes", "accessories"}, true, {}},
        ["ennemies"] = {"ennemies", {"classics"}, false, {"stats", "skills"}},
        ["characters"] = {"characters", {}, false, {"stats", "skills", "inventory"}},
        ["skills"] = {"skills", {}, true, {}},
        ["badskills"] = {"ennemies.skills", {}, true, {}}
    },
    singlefile = {}
}