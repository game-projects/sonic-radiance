return {
    ["actions"] = {
        {"dialogBox", "", "Hi Big Blue. Let's test a bit the choice dialog.", "Rouge", ""},
        {"optionBox", "", "What do you want ?", "Rouge", "", "Ring", "A health fruit", "Battling badnics", "optionFlag"},
        {"dialogBox", {"optionFlag:1", "haveMoney:lt:300"}, "Oooh, in need of cash ?", "Rouge", ""},
        {"dialogBox", {"optionFlag:1", "haveMoney:gt:300"}, "You doesn't seem that poor to me.", "Rouge", ""},
        {"dialogBox", "optionFlag:2", "Did you failed your battle ? Well, you'll have to train more", "Rouge", ""},
        {"dialogBox", "optionFlag:3", "Let's see if you can keep it up", "Rouge", ""},
    }
}