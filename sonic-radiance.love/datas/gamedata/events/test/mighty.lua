return {
    ["actions"] = {
        {"dialogBox", "haveMoney:eq:0", "You don't have any rings !", "Mighty", ""},
        {"dialogBox", {"haveMoney:lt:300", "haveMoney:gt:0"}, "You seems to only have a few rings", "Mighty", ""},
        {"dialogBox", "haveMoney:ge:300", "Wow you're rich !", "Mighty", ""}
    }
}