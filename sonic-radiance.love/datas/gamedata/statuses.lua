return {
    ko = {
        temporary = false,
        replaceAll = true
    },
    hidden = {
        stats = {{"evasion", 30}},
    },
    focus = {
        stats = {{"critical", 30}},
        remove = "distracted"
    },
    shielded = {
        stats = {{"armor", 20}},
        remove = "vulnerable"
    },
    hyper = {
        stats = {
            {"damage", 25},
            {"critical", 20}
        },
        remove = "weakened"
    },
    lucky = {
        stats = {
            {"evasion", 20},
            {"luck", 20},
        },
        remove = "cursed"
    },
    regen = {
        stats = {{"hpregen", 8}}
    },
    poison = {
        stats = {{"hpregen", -15}}
    },
    weakened = {
        stats = {{"damage", -20}},
        remove = "hyper"
    },
    vulnerable = {
        stats = {{"armor", -20}}
    },
    cursed = {
        stats = {
            {"evasion", 0},
            {"luck", -30}
        },
        remove = "lucky"
    },
    distracted = {
        stats = {{"accuracy", -30}},
        remove = "focus"
    }
}