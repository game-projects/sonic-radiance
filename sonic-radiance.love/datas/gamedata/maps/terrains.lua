return {
    highpriority = {"ice"},
    list = {
        ["non-solid"] = {
            {
                forceAction = nil,
                level=0,
                height=0,
                canJump = true
            }
        },
        ice = {
            {
                forceAction = "slide",
                level=0,
                height=0,
                canJump = false,
                speedFactor = 1.2,
            }
        },
        water = {
            {
                level=-32,
                height=0,
                fallDamage = 10,
                speedFactor = 0,
                canJump = false,
                sound = "splash"
            },
            {
                forceAction = nil,
                level=-10,
                height=0,
                mustHaveAction = "swim",
                mustDoAction = nil,
                speedFactor = 0.5,
                canJump = true,
                damage = 0,
                sound = "splash"
            },
            {
                forceAction = nil,
                level=0,
                height=0,
                mustHaveAction = "run",
                mustDoAction = "run",
                speedFactor = 1,
                canJump = true,
                damage = 0,
            }
        },
        hole = {
            {
                level=-32,
                height=0,
                fallDamage = 10,
                speedFactor = 0,
                canJump = false,
                sound = "fall"
            },
        },
        lava = {
            {
                level=-32,
                height=0,
                fallDamage = 20,
                speedFactor = 0,
                canJump = false,
                sound = "splash"
            },
        },
        swamp = {
            {
                forceAction = nil,
                level=-8,
                height=0,
                speedFactor = 0.5,
                canJump = false,
            }
        },
        grass = {
            {
                forceAction = nil,
                level=0,
                height=8,
                speedFactor = 0.5,
                canJump = false,
            }
        }
    }
}