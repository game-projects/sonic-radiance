return {
    areaName = "testmap",
    canSave = true,
    color = {0.3, 0.3, 0.3},
    maps = {
        {
            name = "Test Plains Field",
            music = "testmap",
            folder = "plain",
            map = "test",
            x = 0,
            y = 0,
        },
        {
            name = "Test Road Zone",
            music = "testmap2",
            folder = "plain",
            map = "test2",
            x = 0,
            y = 640,
        }
    }
}