return {
    areaName = "testmap2",
    canSave = true,
    color = {0.3, 0.3, 0.3},
    maps = {
        {
            name = "Not Hidden Grotto",
            music = "testmap",
            folder = "test",
            map = "map",
            x = 0,
            y = 0,
        }
    },
    exitTo = {
        area = "test.plain",
        x = 21,
        y = 7,
        charDir = "down"
    }
}