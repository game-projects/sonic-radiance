return {
  name = "Chao Ruins",
  blocks    = {
    {  0,   0, 32, 32, "top1", "side1"},
    {224, 226, 32, 32, "top1", "side1"},
    {128,  32, 32, 64, "top2", "side1"},
    { 96, 160, 32, 64, "top2", "side1"},
    {  0,  96, 64, 32, "top3", "side2"},
    {192, 128, 64, 32, "top3", "side2"},
  },
  parallax  = {
    {0,    0,  .5,  0, true, false, "back1"},
    {0,   80,   1,   1, true, false, "back2"},
    {0,  -16,   1,   1, true, false, "back3"},
    --{256*4,  -16,   1,   1, false, false, "back3"},
  }
}
