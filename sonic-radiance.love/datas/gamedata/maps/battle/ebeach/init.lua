return {
  name = "Emerald Beach",
  blocks    = {
    {80,   64, 64, 32, "top1", "side2"},
    {96,   96, 32, 32, "top3", "side1"},
    {192,  32, 96, 32, "top2", "side3"},
    {224, 128, 32, 64, "top4", "side1"},
    {96,  176, 32, 64, "top4", "side1"},
    {192, 240, 64, 32, "top5", "side2"},
  },
  parallax  = {
    {0,  0,  .5,  0, true, false, "back1"},
    {0, 10,   1,   1, true, false, "back2"},
  }
}
