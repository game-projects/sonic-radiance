return {
  name = "Metal Depot",
  blocks    = {
    { 48,  0, 32, 96, "top1", "side"},
    {144, 32, 32, 64, "top2", "side"},
    {240, 32, 32, 96, "top1", "side"},
    {336, 32, 32, 64, "top2", "side"},
    {432,  0, 32, 96, "top1", "side"},
  },
  parallax  = {
    {0,   0,  .50,  0, true, false, "back1"},
    {0,  15,  .75, .50, true, false, "back2"},
    {0,  64,  .90, .90, true, false, "back3"},
  }
}
