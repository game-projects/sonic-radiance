return {
  name = "Battle Highway",
  blocks    = {
    {  0,  64, 32, 32, "top1", "side1a"},
    {160,   0, 32, 32, "top1", "side1b"},
    {224, 160, 32, 32, "top1", "side1a"},
    { 64, 224, 32, 32, "top1", "side1a"},
    {160,  32, 08, 64, "top2", "side2"},
    { 88, 160, 08, 64, "top2", "side2"},
    { 32,  88, 64, 08, "top3", "side3"},
    {160, 160, 64, 08, "top3", "side3"},
  },
  parallax  = {
    {0,    0,  .5,   0, true, false, "back1"},
    {0,  -28,   1,   1, true, false, "back2"},
    {0,  86,   1,   1, true, false, "back3"},
  }
}
