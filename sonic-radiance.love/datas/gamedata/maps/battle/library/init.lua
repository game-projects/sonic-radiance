return {
  name = "Library",
  blocks    = {
    { 16,  32, 32, 32, "top", "side1"},
    { 16, 128, 32, 32, "top", "side2"},
    {176,  32, 32, 32, "top", "side2"},
    {176, 128, 32, 32, "top", "side1"},
  },
  parallax  = {
    {0, -48, 1, 1, true, true, "back1"}
  }
}
