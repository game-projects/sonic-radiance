return {
  name = "Green Hill Zone",
  blocks    = {
    {  0,   0, 64, 32, "top1", "side2"},
    {448,  96, 64, 32, "top1", "side2"},
    { 96,  64, 32, 64, "top2", "side1"},
    {384,   0, 32, 64, "top2", "side1"},
    {384,   0, 32, 64, "top2", "side1"},
    {208,  32, 96, 64, "top3", "side3"},
  },
  parallax  = {
    {0,  -48,  .5,  0, true, false, "back1"},
    {0,   80,   1,  1, true, false, "back2"},
  }
}
