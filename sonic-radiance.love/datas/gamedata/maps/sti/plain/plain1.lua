return {
  version = "1.4",
  luaversion = "5.1",
  tiledversion = "1.4.3",
  name = "plain1",
  tilewidth = 16,
  tileheight = 16,
  spacing = 0,
  margin = 0,
  columns = 8,
  image = "plain1.png",
  imagewidth = 128,
  imageheight = 480,
  objectalignment = "unspecified",
  tileoffset = {
    x = 0,
    y = 0
  },
  grid = {
    orientation = "orthogonal",
    width = 16,
    height = 16
  },
  properties = {},
  terrains = {},
  tilecount = 240,
  tiles = {
    {
      id = 8,
      type = "solid"
    },
    {
      id = 9,
      type = "solid"
    },
    {
      id = 10,
      type = "solid"
    },
    {
      id = 11,
      type = "solid"
    },
    {
      id = 16,
      type = "solid"
    },
    {
      id = 17,
      type = "solid"
    },
    {
      id = 18,
      type = "solid"
    },
    {
      id = 19,
      type = "solid"
    },
    {
      id = 20,
      type = "solid"
    },
    {
      id = 21,
      type = "solid"
    },
    {
      id = 22,
      type = "solid"
    },
    {
      id = 23,
      type = "solid"
    },
    {
      id = 24,
      type = "solid"
    },
    {
      id = 26,
      type = "solid"
    },
    {
      id = 30,
      type = "solid"
    },
    {
      id = 31,
      type = "solid"
    },
    {
      id = 32,
      type = "solid"
    },
    {
      id = 33,
      type = "solid"
    },
    {
      id = 34,
      type = "solid",
      objectGroup = {
        type = "objectgroup",
        draworder = "index",
        id = 2,
        name = "",
        visible = true,
        opacity = 1,
        offsetx = 0,
        offsety = 0,
        properties = {},
        objects = {
          {
            id = 1,
            name = "",
            type = "",
            shape = "rectangle",
            x = 0,
            y = 0,
            width = 16,
            height = 16,
            rotation = 0,
            visible = true,
            properties = {}
          }
        }
      }
    },
    {
      id = 38,
      type = "solid"
    },
    {
      id = 40,
      type = "solid"
    },
    {
      id = 41,
      type = "solid"
    },
    {
      id = 42,
      type = "solid"
    },
    {
      id = 43,
      type = "solid"
    },
    {
      id = 44,
      type = "solid"
    },
    {
      id = 48,
      type = "solid"
    },
    {
      id = 50,
      type = "solid"
    },
    {
      id = 51,
      type = "solid"
    },
    {
      id = 52,
      type = "solid"
    },
    {
      id = 53,
      type = "solid"
    },
    {
      id = 54,
      type = "solid"
    },
    {
      id = 56,
      type = "solid"
    },
    {
      id = 57,
      type = "solid"
    },
    {
      id = 58,
      type = "solid"
    },
    {
      id = 61,
      type = "solid"
    },
    {
      id = 62,
      type = "solid"
    },
    {
      id = 64,
      type = "solid"
    },
    {
      id = 65,
      type = "solid"
    },
    {
      id = 66,
      type = "solid"
    },
    {
      id = 69,
      type = "solid"
    },
    {
      id = 70,
      type = "solid"
    },
    {
      id = 72,
      type = "solid"
    },
    {
      id = 73,
      type = "solid"
    },
    {
      id = 74,
      type = "solid"
    },
    {
      id = 75,
      type = "solid"
    },
    {
      id = 76,
      type = "solid"
    },
    {
      id = 77,
      type = "solid"
    },
    {
      id = 80,
      type = "solid"
    },
    {
      id = 82,
      type = "solid"
    },
    {
      id = 83,
      type = "solid"
    },
    {
      id = 84,
      type = "solid"
    },
    {
      id = 85,
      type = "solid"
    },
    {
      id = 86,
      type = "solid"
    },
    {
      id = 88,
      type = "solid"
    },
    {
      id = 89,
      type = "solid"
    },
    {
      id = 90,
      type = "solid"
    },
    {
      id = 93,
      type = "solid"
    },
    {
      id = 94,
      type = "solid"
    },
    {
      id = 96,
      type = "solid"
    },
    {
      id = 97,
      type = "solid"
    },
    {
      id = 98,
      type = "solid"
    },
    {
      id = 101,
      type = "solid"
    },
    {
      id = 102,
      type = "solid"
    },
    {
      id = 108,
      type = "solid"
    },
    {
      id = 109,
      type = "solid"
    },
    {
      id = 110,
      type = "solid"
    },
    {
      id = 116,
      type = "solid"
    },
    {
      id = 117,
      type = "solid"
    },
    {
      id = 118,
      type = "solid"
    },
    {
      id = 124,
      type = "solid"
    },
    {
      id = 125,
      type = "solid"
    },
    {
      id = 126,
      type = "solid"
    },
    {
      id = 129,
      type = "solid"
    },
    {
      id = 130,
      type = "solid"
    },
    {
      id = 132,
      type = "solid"
    },
    {
      id = 133,
      type = "solid"
    },
    {
      id = 134,
      type = "solid"
    },
    {
      id = 140,
      type = "solid"
    },
    {
      id = 141,
      type = "solid"
    },
    {
      id = 142,
      type = "solid"
    },
    {
      id = 148,
      type = "solid"
    },
    {
      id = 149,
      type = "solid"
    },
    {
      id = 150,
      type = "solid"
    },
    {
      id = 156,
      type = "solid"
    },
    {
      id = 157,
      type = "solid"
    },
    {
      id = 158,
      type = "solid"
    }
  }
}
