return {
  metadata = {
    width       = 64,
    height      = 54,
    defaultAnim = "default",
    ox          = 32,
    oy          = 48,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 15,
      loop        = 1,
      speed       = 30,
      pauseAtEnd  = false,
    },
  }
}
