return {
  name        = "Amy",
  fullname    = "Amy Rose",
  class       = "power",
  speed       = 3,
  jump        = 3,
  turns       = 2,
  move        = 4,

  startlevel  = 1,

  isUnlockedAtStart = true,
  canGoSuper        = true,
  canBreakCraft     = false,

  weakTo = {},
  resists = {},

  icon = 3,
  charset = "perso",
  charId = 3,
}
