local CONST = require "datas.consts.stats"

return {
  hpmax   = CONST.RANK_A, --
  ppmax   = CONST.RANK_C, --

  attack  = CONST.RANK_A, --
  defense = CONST.RANK_B, --
  technic = CONST.RANK_C, -- How much items & wisps will be powerfull for this character.
  power   = CONST.RANK_B, --
  mind    = CONST.RANK_C, -- Magic defense.
  speed   = CONST.RANK_B, -- Où le personnage se trouve dans le tour.
}
