return {
  name        = "Sonic",
  fullname    = "Sonic the Hedgehog",
  class       = "speedster",
  speed       = 5,
  jump        = 3,
  turns       = 3,
  move        = 4,

  startlevel  = 1,

  isUnlockedAtStart = true,
  canGoSuper        = true,
  canBreakCraft     = false,

  weakTo = {"water"},
  resists = {"wind"},

  icon = 1,
  charset = "perso",
  charId = 1,
}
