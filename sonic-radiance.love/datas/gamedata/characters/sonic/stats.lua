local CONST = require "datas.consts.stats"

return {
  hpmax   = CONST.RANK_B, --
  ppmax   = CONST.RANK_C, --

  attack  = CONST.RANK_B, --
  defense = CONST.RANK_B, --
  technic = CONST.RANK_C, -- How much items & wisps will be powerfull for this character.
  power   = CONST.RANK_C, --
  mind    = CONST.RANK_D, -- Magic defense.
  speed   = CONST.RANK_S, -- Où le personnage se trouve dans le tour.
}
