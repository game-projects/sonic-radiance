return {
  name        = "Tails",
  fullname    = "Miles \"Tails\" Prower",
  class       = "technic",
  speed       = 2,
  jump        = 5,
  turns       = 2,
  move        = 3,

  startlevel  = 1,

  isUnlockedAtStart = true,
  canGoSuper        = true,
  canBreakCraft     = false,

  weakTo = {"light"},
  resists = {"earth"},

  icon = 2,
  charset = "perso",
  charId = 2,
}
