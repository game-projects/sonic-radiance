local CONST = require "datas.consts.stats"

return {
  hpmax   = CONST.RANK_C, --
  ppmax   = CONST.RANK_A, --

  attack  = CONST.RANK_D, --
  defense = CONST.RANK_C, --
  technic = CONST.RANK_S, -- How much items & wisps will be powerfull for this character.
  power   = CONST.RANK_C, --
  mind    = CONST.RANK_A, -- Magic defense.
  speed   = CONST.RANK_B, -- Où le personnage se trouve dans le tour.
}
