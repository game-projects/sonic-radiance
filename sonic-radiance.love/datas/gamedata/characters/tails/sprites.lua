return {
  metadata = {
    height      = 64,
    width       = 64,
    ox          = 32,
    oy          = 32,
    defaultAnim = "idle"
  },
  animations = {
    ["idle"] = {
      startAt     = 1,
      endAt       = 8,
      loop        = 1,
      speed       = 10,
      pauseAtEnd  = false,
    },
    ["walk"] = {
      startAt     = 9,
      endAt       = 17,
      loop        = 10,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["jump"] = {
      startAt     = 18,
      endAt       = 21,
      loop        = 21,
      speed       = 10,
      pauseAtEnd  = true,
    },
    ["fall"] = {
      startAt     = 22,
      endAt       = 26,
      loop        = 26,
      speed       = 10,
      pauseAtEnd  = true,
    },
    ["hit1start"] = {
      startAt     = 27,
      endAt       = 30,
      loop        = 30,
      speed       = 25,
      pauseAtEnd  = true,
    },
    ["hit1end"] = {
      startAt     = 31,
      endAt       = 32,
      loop        = 32,
      speed       = 25,
      pauseAtEnd  = true,
    },
    ["hit2start"] = {
      startAt     = 33,
      endAt       = 37,
      loop        = 37,
      speed       = 25,
      pauseAtEnd  = true,
    },
    ["hit2end"] = {
      startAt     = 38,
      endAt       = 38,
      loop        = 38,
      speed       = 25,
      pauseAtEnd  = true,
    },
    ["hit3start"] = {
      startAt     = 42,
      endAt       = 46,
      loop        = 46,
      speed       = 25,
      pauseAtEnd  = true,
    },
    ["hit3end"] = {
      startAt     = 46,
      endAt       = 48,
      loop        = 48,
      speed       = 25,
      pauseAtEnd  = true,
    },
    ["spindash"] = {
      startAt     = 66,
      endAt       = 73,
      loop        = 66,
      speed       = 25,
      pauseAtEnd  = false,
    },
    ["spindash_full"] = {
      startAt     = 66,
      endAt       = 73,
      loop        = 66,
      speed       = 25,
      pauseAtEnd  = false,
    },
    ["spin"] = {
      startAt     = 62,
      endAt       = 65,
      loop        = 65,
      speed       = 25,
      pauseAtEnd  = false,
    },
    ["spinjump"] = {
      startAt     = 49,
      endAt       = 54,
      loop        = 51,
      speed       = 25,
      pauseAtEnd  = false,
    },
    ["spinjump_nucurl"] = {
      startAt     = 51,
      endAt       = 54,
      loop        = 51,
      speed       = 25,
      pauseAtEnd  = false,
    },
    ["hurt"] = {
      startAt     = 49,
      endAt       = 51,
      loop        = 51,
      speed       = 15,
      pauseAtEnd  = true,
    },
    ["getKo"] = {
      startAt     = 53,
      endAt       = 59,
      loop        = 59,
      speed       = 15,
      pauseAtEnd  = true,
    },
    ["ko"] = {
      startAt     = 59,
      endAt       = 59,
      loop        = 59,
      speed       = 15,
      pauseAtEnd  = true,
    },
    ["defend"] = {
      startAt     = 60,
      endAt       = 61,
      loop        = 61,
      speed       = 15,
      pauseAtEnd  = true,
    }
  }
}
