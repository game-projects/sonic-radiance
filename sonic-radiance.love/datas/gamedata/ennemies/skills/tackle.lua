-- A basic file describing the basic attack, in order to make it customizable one
-- day ?

-- Also serve as a tutoriel for how to create a file attack and choregraphy

return {
  name = "tackle",
  cost = 0,

  targetNumber = 1, -- 0 for targeting all ennemies
  targetEnnemies = true,

  isSpecial = false,

  choregraphy = { -- the main attack choregraphy
    {"goTo3D", "none", "actor", "target", -0.4, 0, 0, 0.5, true},
    {"sendDamage", "none", 120, "basic", "none", false},
    {'addGFX', "sentDamage", 'hit1', "target", -0.4, 0, 0, true, false},
    {'playSFX', "sentDamage", 'hitconnect'},
    {'jumpBack', "none", "actor", 4, 8, true},
    {"wait", "none", 0.1},
    {'goTo3D', "none", "actor", "start", 0, 0, 0, 0.3, true},
  },

  onContact = { -- if the attack move and touch multiple ennemies, you can add
    -- specific effect when you touch the ennemy.
  },
}
