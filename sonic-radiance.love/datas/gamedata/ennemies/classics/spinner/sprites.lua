return {
  metadata = {
    width       = 64,
    height      = 64,
    ox          = 32,
    oy          = 32,
    defaultAnim = "idle"
  },
  animations = {
    ["idle"] = {
      startAt     = 1,
      endAt       = 6,
      loop        = 1,
      speed       = 12,
      pauseAtEnd  = false,
    },
    ["walk"] = {
      startAt     = 1,
      endAt       = 6,
      loop        = 1,
      speed       = 12,
      pauseAtEnd  = false,
    }
  }
}
