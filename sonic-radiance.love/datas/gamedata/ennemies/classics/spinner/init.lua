return {
  name        = "Spinner",
  fullname    = "E-06 Spinner",
  type        = "badnics",
  element     = "none",
  protectypes = {"aerial"},
  rarity      = 0,
  isAerial    = true,
  distAttack  = false,
  turns       = 2,

  hudHeight   = 24,
  behaviour   = "random",
  behaviourAlt = "random",

  giveExp     = 20,
  giveRings   = 30,
}
