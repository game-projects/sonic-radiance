return {
  metadata = {
    height      = 64,
    width       = 64,
    ox          = 32,
    oy          = 32,
    defaultAnim = "idle"
  },
  animations = {
    ["idle"] = {
      startAt     = 1,
      endAt       = 4,
      loop        = 1,
      speed       = 12,
      pauseAtEnd  = false,
    },
    ["walk"] = {
      startAt     = 5,
      endAt       = 8,
      loop        = 5,
      speed       = 12,
      pauseAtEnd  = false,
    }
  }
}
