return {
  name        = "Motobug",
  fullname    = "E-03 Motobug",
  type        = "badnics",
  element     = "none",
  protectypes = {},
  rarity      = 0,
  isAerial    = false,
  distAttack  = false,
  turns       = 2,

  hudHeight   = 18,
  behaviour   = "random",
  behaviourAlt = "random",

  giveExp     = 20,
  giveRings   = 30,
}
