return {
  music = "battle2",
  ennemies = {
    {"normal", "classics", "motobug", 1},
    {"boss", "classics", "motobug", 10, 1.8, true},
    {"normal", "classics", "motobug", 1}}
}

-- There are three possible type of ennemies

-- NORMAL   {"normal", category, name, number}
-- BOSS :   {"boss",  category, pvbonus, statbonus}
-- RIVALS : {"rival", charname, level, pvbonus}
