return {
  music = "battle1",
  ennemies = {
    {"normal", "classics", "motobug", 1},
    {"normal", "classics", "spinner", 1},
    {"normal", "classics", "motobug", 1},
  }
}

-- There are three possible type of ennemies

-- NORMAL {"normal", category, name, number}
-- BOSS : {"boss", category, pvbonus, statbonus}
-- RIVALS : {"rival", charname, level, pvbonus}
