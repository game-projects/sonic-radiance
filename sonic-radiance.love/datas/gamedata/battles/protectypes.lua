return {
    ["basic"] = {
        resistTo = {},
        backDamage = {}
    },
    ["aerial"] = {
        resistTo = {"basic"},
        backDamage = {}
    },
    ["shielded"] = {
        resistTo = {"basic", "projectile"},
        backDamage = {}
    },
    ["spiked"] = {
        resistTo = {"basic", "aerial"},
        backDamage = {"basic", "aerial"}
    },
    ["backspiked"] = {
        resistTo = {"aerial"},
        backDamage = {"aerial"}
    },
}