return {
    ["none"] = {
        fullname = "None",
        weakTo = {},
        resists = {}
    },
    ["water"] = {
        fullname = "Water",
        weakTo = {"light"},
        resists = {"fire", "water"}
    },
    ["fire"] = {
        fullname = "Fire",
        weakTo = {"water"},
        resists = {"ice", "fire"}
    },
    ["ice"] = {
        fullname = "Fire",
        weakTo = {"fire"},
        resists = {"wind", "ice"}
    },
    ["wind"] = {
        fullname = "Fire",
        weakTo = {"ice"},
        resists = {"earth", "wind"}
    },
    ["earth"] = {
        fullname = "Fire",
        weakTo = {"wind"},
        resists = {"light", "earth"}
    },
    ["light"] = {
        fullname = "Fire",
        weakTo = {"earth"},
        resists = {"water", "light"}
    },
    ["chaos"] = {
        fullname = "Chaos",
        weakTo = {"chaos"},
        resists = {}
    }
}