return {
    name = "lightsneakers",
    fullname = "Light Sneakers",
    description = "Simple sneakers that protect a little",
    conditions = {},
    effects= {},
    statsBoost = {
        speed = 2,
        defense = 1
    },
    isEquipement = true,
    usableInBattle=false,
    usableOnMap=false,
    affectEverybody=false,
    duration=0,
  }
  