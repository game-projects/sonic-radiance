return {
    name = "slowmoshoes",
    fullname = "SlowMo Shoes",
    description = "Shoes that make you slower",
    conditions = {},
    effects = {},
    statsBoost = {
        speed = -5,
        mind = 6
    },
    isEquipement = true,
    usableInBattle = false,
    usableOnMap = false,
    affectEverybody = false,
    duration = 0
}
