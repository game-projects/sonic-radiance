return {
    name = "simplehammer",
    fullname = "Simple Hammer",
    description = "A basic hammer",
    conditions = {},
    effects = {},
    statsBoost = {
        attack = 3
    },
    isEquipement = true,
    usableInBattle = false,
    usableOnMap = false,
    affectEverybody = false,
    duration = 0
}
