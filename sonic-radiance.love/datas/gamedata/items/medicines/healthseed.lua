return {
  name = "healthseed",
  fullname = "Health Seed",
  description = "The seed of a plant known for its healing effects. Heal a bit",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "hp", "fixed", 50}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
