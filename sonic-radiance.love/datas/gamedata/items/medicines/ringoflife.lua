return {
  name = "ringoflife",
  fullname = "Ring of Life",
  description = "A ring with the power to put the fallen back into fighting shape. Revives entirely.",
  conditions = {
    {"status", "ko", true}
  },
  effects= {
    {"setStatus", "ko", false},
    {"heal", "hp", "percent", 100}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
