return {
  name = "refresherwave",
  fullname = "Refresher Wave",
  description = "A device that produces an energy pulse that invigorates all party members",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "mp", "fixed", 20}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=true,
}
