return {
  name = "eggdog",
  fullname = "Egg Dog",
  description = "Eggman's best hot dog, complete with motor oil. Doesn't really heal.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "hp", "fixed", -1}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
