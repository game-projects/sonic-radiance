return {
  name = "tonicinfusion",
  fullname = "Tonic Infusion",
  description = "A refreshing formula that invigorates mind and body.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "mp", "fixed", 25}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
