return {
  name = "healthfruit",
  fullname = "Health Fruit",
  description = "The fruit of a plant known for its healing effects. Heal completely.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "hp", "percent", 100}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
