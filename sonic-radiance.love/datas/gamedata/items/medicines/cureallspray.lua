return {
  name = "cureallspray",
  fullname = "Cure-All Spray",
  description = "A medicine that cure every illness",
  conditions = {
    {"status", "haveNegative", true},
    {"status", "ko", false},
  },
  effects= {
    {"setStatus", "allNegative", false}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
