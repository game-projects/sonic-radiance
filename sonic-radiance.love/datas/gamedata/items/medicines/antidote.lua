return {
  name = "antidote",
  fullname = "Antidote",
  description = "A medicine that heal a character from poison",
  conditions = {
    {"status", "poison", true},
  },
  effects= {
    {"setStatus", "poison", false},
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
