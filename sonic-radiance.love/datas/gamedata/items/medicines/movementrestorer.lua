return {
  name = "movementrestorer",
  fullname = "Movement Restorer",
  description = "A medicine that restore a character's movement capacity.",
  conditions = {
    {"status", "paralysis", true}
  },
  effects= {
    {"setStatus", "paralysis", false}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
