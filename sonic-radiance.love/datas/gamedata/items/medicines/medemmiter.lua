return {
  name = "medemmiter",
  fullname = "Med Emmiter",
  description = "A device that produces an energy pulse that heals all party members",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "hp", "fixed", 100}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=true,
}
