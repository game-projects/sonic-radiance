return {
  name = "tonicdrink",
  fullname = "Tonic Drink",
  description = "A refreshing formula that invigorates mind and body.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "mp", "fixed", 10}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
