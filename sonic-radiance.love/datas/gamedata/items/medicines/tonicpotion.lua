return {
  name = "tonicpotion",
  fullname = "Tonic Potion",
  description = "A refreshing formula that invigorates mind and body.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "mp", "fixed", 50}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
