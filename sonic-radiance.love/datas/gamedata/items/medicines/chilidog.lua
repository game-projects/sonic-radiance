return {
  name = "chilidog",
  fullname = "Chili Dog",
  description = "Sonic's favorite meal, complete with jalapeños. Heal a bit",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "hp", "fixed", 10}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
