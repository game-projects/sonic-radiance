return {
  name = "healthroot",
  fullname = "Health Root",
  description = "The root of a plant known for its healing effects. Heal grealty.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "hp", "fixed", 250}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
