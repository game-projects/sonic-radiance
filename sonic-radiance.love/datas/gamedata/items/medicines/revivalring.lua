return {
  name = "revivalring",
  fullname = "Revival Ring",
  description = "A ring with the power to put the fallen back on their feet. Revives partially. ",
  conditions = {
    {"status", "ko", true}
  },
  effects= {
    {"setStatus", "ko", false},
    {"heal", "hp", "percent", 25}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
