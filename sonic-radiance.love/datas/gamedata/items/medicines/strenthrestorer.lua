return {
  name = "strenthrestorer",
  fullname = "Strenth Restorer",
  description = "A medicine that restore a character's strenth",
  conditions = {
    {"status", "weakened", true}
  },
  effects= {
    {"setStatus", "weakened", false}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
