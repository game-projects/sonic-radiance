return {
  name = "healunit",
  fullname = "Heal Unit",
  description = "A special medicine that restore entirely the body.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "allNegative", false},
    {"heal", "hp", "percent", 100}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
