return {
  name = "healthleaf",
  fullname = "Health Leaf",
  description = "The leaf of a plant known for its healing effects. Heal a good amount.",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"heal", "hp", "fixed", 100}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
