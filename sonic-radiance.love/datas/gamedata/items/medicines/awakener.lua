return {
  name = "awakener",
  fullname = "Awakener",
  description = "A medicine that awaken an asleep character.",
  conditions = {
    {"status", "sleep", true}
  },
  effects= {
    {"setStatus", "sleep", false}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
