return {
  name = "defenserestorer",
  fullname = "Defense Restorer",
  description = "A medicine that restore a character's defense",
  conditions = {
    {"status", "vulnerable", true}
  },
  effects= {
    {"setStatus", "vulnerable", false}
  },
  usableInBattle=true,
  usableOnMap=true,
  affectEverybody=false,
}
