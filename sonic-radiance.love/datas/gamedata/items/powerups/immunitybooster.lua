return {
  name = "immunitybooster",
  fullname = "Immunity Booster",
  description = "A rather immediate yet short-term vaccine",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"blockStatus", "negative"}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=5,
}
