return {
  name = "focusrock",
  fullname = "Focus Rock",
  description = "A curious rock that makes one more focused",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "focus", true}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=5,
}
