return {
  name = "psychicwater",
  fullname = "Psychic Water",
  description = "A strange infusion that enhance stealth",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "hidden", true}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=5,
}
