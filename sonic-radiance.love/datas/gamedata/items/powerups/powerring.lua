return {
  name = "powerring",
  fullname = "Power Ring",
  description = "A fantastic ring infused with Chaos Energy",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "hyper", true}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=5,
}
