return {
  name = "shield",
  fullname = "Shield",
  description = "A basic yet effective shield to protect a hero",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "fortified", true}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=5,
}
