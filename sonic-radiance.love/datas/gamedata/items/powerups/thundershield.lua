return {
  name = "thundershield",
  fullname = "Thunder Shield",
  description = "A special shield to protect a hero from lightning damage",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "fortified", true},
    {"protectElement", "light"}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=3,
}
