return {
  name = "watershield",
  fullname = "Water Shield",
  description = "A special shield to protect a hero from water damage",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "fortified", true},
    {"protectElement", "water"}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=3,
}
