return {
  name = "speedup",
  fullname = "Speed Up",
  description = "Improve a character ability to attack several time",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "speedup", true}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=1,
}
