return {
  name = "fireshield",
  fullname = "Fire Shield",
  description = "A special shield to protect a hero from fire damage",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "fortified", true},
    {"protectElement", "fire"}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=3,
}
