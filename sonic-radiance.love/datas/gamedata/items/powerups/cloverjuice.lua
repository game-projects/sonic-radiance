return {
  name = "cloverjuice",
  fullname = "Clover Juice",
  description = "The potion to take before going to Casinopolis",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "lucky", true}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=5,
}
