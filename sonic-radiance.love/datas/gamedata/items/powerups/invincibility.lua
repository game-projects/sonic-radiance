return {
  name = "invincibility",
  fullname = "Invincibility",
  description = "Make a character",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "invincibility", true}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=1,
}
