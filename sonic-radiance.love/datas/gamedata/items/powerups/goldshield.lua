return {
  name = "fireshield",
  fullname = "Fire Shield",
  description = "A special shield to protect a hero and give him back some pp",
  conditions = {
    {"status", "ko", false}
  },
  effects= {
    {"setStatus", "fortified", true},
    {"regen", "pp", 6}
  },
  usableInBattle=true,
  usableOnMap=false,
  affectEverybody=false,
  duration=3,
}
