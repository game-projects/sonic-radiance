return {
    name = "basicgloves",
    fullname = "Basic Gloves",
    description = "Basic gloves that help fighting",
    conditions = {},
    effects = {},
    statsBoost = {
        attack = 2,
        defense = 1
    },
    isEquipement = true,
    usableInBattle = false,
    usableOnMap = false,
    affectEverybody = false,
    duration = 0
}
