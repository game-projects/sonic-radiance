return {
    name = "darkgloves",
    fullname = "Dark Gloves",
    description = "Gloves engluffed with darkness",
    conditions = {},
    effects = {},
    statsBoost = {
        attack = 5,
        hpmax = -10
    },
    isEquipement = true,
    usableInBattle = false,
    usableOnMap = false,
    affectEverybody = false,
    duration = 0
}
