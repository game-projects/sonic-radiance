return {
  [1] = {
    name = "medicines",
    fullname = "Medicines",
    description = "Healing items",
    inBattle = true,
    isEquipement = false,
  },
  [2] = {
    name = "powerups",
    fullname = "Powerups",
    description = "Helping items usable in battle",
    inBattle = true,
    isEquipement = false,
  },
  [3] = {
    name = "wisps",
    fullname = "Wisps",
    description = "Little alien creatures that can attack ennemies",
    inBattle = true,
    isEquipement = false,
  },
  [4] = {
    name = "gloves",
    fullname = "Gloves",
    description = "All equipables gloves",
    inBattle = false,
    isEquipement = true,
  },
  [5] = {
    name = "shoes",
    fullname = "Shoes",
    description = "All equipables shoes",
    inBattle = false,
    isEquipement = true,
  },
  [6] = {
    name = "accessories",
    fullname = "Accessories",
    description = "All equipables accessories",
    inBattle = false,
    isEquipement = true,
  },
  [7] = {
    name = "chao",
    fullname = "Chao's Items",
    description = "All chao-related items",
    inBattle = false,
    isEquipement = false,
  },
  [8] = {
    name = "quest",
    fullname = "Quest Items",
    description = "All quest items",
    inBattle = false,
    isEquipement = false,
  },

}
