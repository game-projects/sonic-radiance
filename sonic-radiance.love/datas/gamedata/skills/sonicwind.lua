-- A basic file describing the basic attack, in order to make it customizable one
-- day ?

-- Also serve as a tutoriel for how to create a file attack and choregraphy

return {
  name = "sonicwind", -- unused for this attack, but still usefull sometimes
  fullname = "Sonic Wind",
  cost = 08,  -- the pp cost of the attack. Will be ignored if it's set
              -- as character default attack
  description = "Spin to attack. Roll at the right time for more damages",

  targetNumber = 0, -- 0 for targeting all ennemies
  targetEnnemies = true,

  choregraphy = { -- the main attack choregraphy
    {"setAnimation", "none", "actor", "walk", false},
    {"taggedAction", "playerMove", {"goTo", "none", "actor", "center", 0, 0, 0.5, true}},
    {'setAnimation', "none", "actor", 'heal', false},
    {"wait", "none", 0.2},

    {"startSubChoregraphies", "none", 0.3, true},
    {'setAnimation', "none", "actor", 'idle', false},
    {"waitFor", "none", "hasSubChoregraphiesActive:not"},

    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 0, 0, 0.5, true},
    {'setAnimation', "none", "actor", 'idle', false},
  },

  subChoregraphy = {
    {'addGFX', "none", 'tornado', "target", 0, 0, 0, true, false},
    {"sendDamage", "none", 30, "basic", "none", false},
    {"taggedAction", "ennemyJump", {'jump', "none", "target", 3, true, false}},
    {"waitFor", "none", "actionFinished:ennemyJump"},
  },

  onContact = { -- if the attack move and touch multiple ennemies, you can add
    -- specific effect when you touch the ennemy.
  },
}
