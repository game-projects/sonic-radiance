-- A basic file describing the basic attack, in order to make it customizable one
-- day ?

-- Also serve as a tutoriel for how to create a file attack and choregraphy

return {
  name = "hitcombo",
  fullname = "Hit Combo",
  cost = 03,
  description = "Chain punches and strike with a final blow",

  needTarget = true,
  targetNumber = 1, -- 0 for targeting all ennemies
  targetEnnemies = true,

  choregraphy = { -- the main attack choregraphy
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "target", -1, 0, 0.3, false},
    {"wait", "none", 0.15},
    {"taggedAction", "comboQte", {"addQTE", "none", {"simplePrompt", {{"A", 1}, {"A", 1}, {"B", 1}}, 0.5}, "target", false}},
    {"wait", "none", 0.15},

    {'playSFX', "none", 'hit'},
    {'setAnimation', "none", "actor", 'hit1start', true},
    {'sendDamage', "none", 40, "basic", "none", false},
    {'addGFX',"sentDamage", 'hit1', "actor", 0.75, 0, 0, true, false},
    {'playSFX', "sentDamage", 'hitconnect'},
    {'setAnimation', "none", "actor", 'hit1end', true},

    {'playSFX', "none", 'hit'},
    {'setAnimation', "none", "actor", 'hit2start', true},
    {'sendDamage', "none", 40, "basic", "none", false},
    {'addGFX',"sentDamage", 'hit1', "actor", 0.75, 0, 0, true, false},
    {'playSFX', "sentDamage", 'hitconnect'},
    {'setAnimation', "none", "actor", 'hit2end', true},

    {'playSFX', "none", 'hit'},
    {'setAnimation', "none", "actor", 'hit3start', true},
    {'sendDamage', "none", 40, "basic", "none", false},
    {'addGFX',"sentDamage", 'hit1', "actor", 0.75, 0, 0, true, false},
    {'playSFX', "sentDamage", 'hitconnect'},
    {'setAnimation', "none", "actor", 'hit3end', true},
    {'setAnimation', "none", "actor", 'idle', false},
    {'waitFor', "none", "actionFinished:comboQte"},

    {"setAnimation", "qteSuccess:1", "actor", "upper", false},
    {"waitFor", "qteSuccess:1", "haveFrameSignal:hitconnect"},
    {'sendDamage', "qteSuccess:1", 75, "basic", "none", false},
    {'addGFX', {"qteSuccess:1", "sentDamage"}, 'hit1', "actor", 0.75, 0, 0, true, false},
    {'playSFX', {"qteSuccess:1", "sentDamage"}, 'hitconnect'},

    {'wait', "none", 0.2},
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 0, 0, 0.3, true},
    {'setAnimation', "none", "actor", 'idle', false},
  },

  onContact = { -- if the attack move and touch multiple ennemies, you can add
    -- specific effect when you touch the ennemy.
  },
}

--[[
                        CHOREGRAPHY POSSIBLE EFFECTS
-- "wait" :: Simply wait before doing the next command
--        :: {"wait", condition, duration}

-- "setAnimation" :: Change the animation of the battler
--                   {"setAnimation", condition, animation, blockProcess}

-- "addGFX" :: Show a GFX relatively to the target position
--          :: (player if target is nil)
--          :: {"addGFX", condition, "gfxname", x, y, affectedByDirection, blockProcess}

-- "sendDamage" :: Send Damage on the whole target Area
--              ::
--              :: {"sendDamage", condition, damageEffect, accuracy, isSpecial, isAerial}

-- "sendDamageFromPos" :: Send Damage on a point from Actor position
--                     ::
--                     :: {"sendDamageFromPos", condition, dx, dy, damageEffect, accuracy, isSpecial, isAerial, affectedByDirection}

-- "sendDamageFromCursor" :: Send Damage on a point from Cursor final position
--                        ::
--                        :: {"sendDamageFromCursor", condition, dx, dy, damageEffect, accuracy, isSpecial, isAerial, affectedByDirection}


-- "dashForward" :: Dash until your are stopped
--               :: {"dashForward", condition, speed, blockProcess}

-- "jumpBack" :: jump to initial position
--               :: {"jumpBack", condition, blockProcess}

                        CONDITION TYPES
--
-- "sentDamage" :: an ennemy have received damage
--
--

]]
