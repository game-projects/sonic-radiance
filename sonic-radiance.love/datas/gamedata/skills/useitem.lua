-- A basic file describing the basic attack, in order to make it customizable one
-- day ?

-- Also serve as a tutoriel for how to create a file attack and choregraphy

return {
  name = "item",  -- unused for this attack, but still usefull sometimes
  cost = 00,        -- the pp cost of the attack. Will be ignored if it's set
                    -- as character default attack

  needTarget = true,
  targetNumber = 1, -- 0 for targeting all ennemies

  choregraphy = { -- the main attack choregraphy
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 2, 0, 0.5, true},

    {'setAnimation', "none", "actor", 'idle', false},
    {'wait', "none", 0.4},
    {'useItemEffect', "none"},

    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 0, 0, 0.5, true},
    {'setAnimation', "none", "actor", 'idle', false},
  },

  onContact = { -- if the attack move and touch multiple ennemies, you can add
    -- specific effect when you touch the ennemy.
  },
}
