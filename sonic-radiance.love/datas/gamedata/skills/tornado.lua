-- A basic file describing the basic attack, in order to make it customizable one
-- day ?

-- Also serve as a tutoriel for how to create a file attack and choregraphy

return {
  name = "tornado", -- unused for this attack, but still usefull sometimes
  fullname = "Blue Tornado",
  altName = {
    amy = "Piko Tornado"
  },
  cost = 08,  -- the pp cost of the attack. Will be ignored if it's set
              -- as character default attack
  description = "Spin to attack. Roll at the right time for more damages",

  targetNumber = 1, -- 0 for targeting all ennemies
  targetEnnemies = true,

  choregraphy = { -- the main attack choregraphy
    {"setAnimation", "none", "actor", "walk", false},
    {"taggedAction", "playerMove", {"goTo", "none", "actor", "target", -2, 0, 1, false}},
    {"wait", "none", 0.3},

    {"taggedAction", "tornadoQte", {"addQTE", "none", {"simplePrompt", {{"right", 1}, {"left", 1}}, 0.9}, "target", false}},
    {"waitFor", "none", {"or", "qteSuccess", "actionFinished:playerMove"}},
    {'setAnimation', "actionFinished:playerMove", "actor", 'idle', true},
    {"stopMov", "qteSuccess", "actor"},
    {"waitFor", "none", "actionFinished:tornadoQte"},
    {"setAnimSpeed", "qteFailure", "actor", 0.66},
    {"taggedAction", "upperAnimation", {'setAnimation', "none", "actor", 'upper', false}},
    {"waitFor", "none", "haveFrameSignal:tornado"},

    {"sendDamage", "none", 120, "basic", "none", false},
    {"taggedAction", "tornadoGFX", {'addGFX', "sentDamage", 'tornado', "actor", 0, 0, 0, true, false}},
    {"goTo", "none", "tornadoGFX", "target", 0, 0, 0.3, true},
    {"taggedAction", "ennemyJump", {'jump', "none", "target", 5.5, true, false}},
    {'playSFX', "sentDamage", 'hitconnect'},

    {"waitFor", "none", "actionFinished:upperAnimation"},
    {"setAnimSpeed", "none", "actor", 1},

    {'setAnimation', "none", "actor", 'idle', false},
    {"wait", "none", 0.3},

    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 0, 0, 0.3, true},
    {'setAnimation', "none", "actor", 'idle', false},
    {"waitFor", "none", "actionFinished:ennemyJump"}
  },

  onContact = { -- if the attack move and touch multiple ennemies, you can add
    -- specific effect when you touch the ennemy.
  },
}
