return {
  name = "spindash",
  fullname = "Spin Dash",
  description = "Sonic's signature attack ! Charge for more damage",
  cost = 05,
  targetNumber = 1, -- 0 for targeting all ennemies
  targetEnnemies = true,
  choregraphy = {
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 2, 0, 0.5, true},
    {"setAnimation", "none", "actor", "spindash", false},
    {'playSFX', "none", 'spincharge'},
    {"wait", "none", 1},
    {"setAnimation", "none", "actor", "spin", false},
    {'playSFX', "none", 'spinrelease'},
    {"goTo", "none", "actor", "target", 0, 0, 0.3, true},
    {"sendDamage", "none", 120, "basic", "none", false},
    {'addGFX', "sentDamage", 'hit1', "target", 0, 0, 0, true, false},
    {'playSFX', "sentDamage", 'hitconnect'},
    {'jumpBack', "none", "actor", 4, 8, true},
    {"wait", "none", 0.1},
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 0, 0, 0.3, true},
    {'setAnimation', "none", "actor", 'idle', false},
  }
}
