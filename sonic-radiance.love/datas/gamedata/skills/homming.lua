-- A basic file describing the basic attack, in order to make it customizable one
-- day ?

-- Also serve as a tutoriel for how to create a file attack and choregraphy

return {
  name = "homming", -- unused for this attack, but still usefull sometimes
  fullname = "Homming Attack",
  cost = 00,  -- the pp cost of the attack. Will be ignored if it's set
              -- as character default attack
  description = "Make consecutive attacks with A",

  targetNumber = 1, -- 0 for targeting all ennemies
  targetEnnemies = true,

  choregraphy = { -- the main attack choregraphy
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 5, 0, 0.5, true},
    {"taggedAction", "jumpingHero", {'jump', "none", "actor", 5.5, true, false}},

    {"taggedAction", "hommingQte", {"addQTE", "none", {"simplePrompt", {{"A", 1}}, 0.4}, "target", false}},
    {"waitFor", "none", "actionFinished:hommingQte"},
    {"taggedAction", "dashToEnnemy", {"goTo3D", "none", "actor", "target", 0, 0, 8, 0.2, false}},
    {'addGFX', "none", 'homming', "actor", 0, 0, -4, true, false},
    {"setAnimation", "none", "actor", "spin", false},
    {'playSFX', "none", 'spinrelease'},
    {"waitFor", "none", "actionFinished:dashToEnnemy"},
    {"sendDamage", "none", 30, "aerial", "none", false},
    {'addGFX', "sentDamage", 'hit1', "target", 0, 0, 4, true, false},
    {'playSFX', "sentDamage", 'hitconnect'},
    {"taggedAction", "jumpingHero", {'jumpBack', "none", "actor", 5.5, 5.5, false}},
    {"wait", "none", 0.1},
    {"setCounter", "none", "spinSpam", 1, true},

    {"taggedAction", "hommingQte", {"addQTE", {"counter:spinSpam:lt:5", "qteSuccess"}, {"simplePrompt", {{"A", 1}}, 0.3}, "target", false}},
    {"waitFor", "none", {"or", "actionFinished:hommingQte", "actionFinished:jumpingHero"}},
    {"skipTo", {"counter:spinSpam:lt:5", "qteSuccess"}, "dashToEnnemy"},

    {"waitFor", "none", "actionFinished:jumpingHero"},
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 0, 0, 0.3, true},
    {'setAnimation', "none", "actor", 'idle', false},
  },

  onContact = { -- if the attack move and touch multiple ennemies, you can add
    -- specific effect when you touch the ennemy.
  },
}
