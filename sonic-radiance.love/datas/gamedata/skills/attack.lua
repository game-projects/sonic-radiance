-- A basic file describing the basic attack, in order to make it customizable one
-- day ?

-- Also serve as a tutoriel for how to create a file attack and choregraphy

return {
  name = "attack", -- unused for this attack, but still usefull sometimes
  fullname = "Spin Attack",
  cost = 00,  -- the pp cost of the attack. Will be ignored if it's set
              -- as character default attack
  description = "Spin to attack. Roll at the right time for more damages",

  targetNumber = 1, -- 0 for targeting all ennemies
  targetEnnemies = true,

  choregraphy = { -- the main attack choregraphy
    {"setAnimation", "none", "actor", "walk", false},
    {"taggedAction", "playerMove", {"goTo", "none", "actor", "target", 0, 0, 0.6, false}},
    {"wait", "none", 0.3},
    {"setAnimation", "none", "actor", "spin", false},
    {'playSFX', "none", 'spinrelease'},
    {"waitFor", "none", "actionFinished:playerMove"},
    {"sendDamage", "none", 100, "basic", "none", false},
    {"sendStatus", "none", "vulnerable", 5, true},
    {'addGFX', "sentDamage", 'hit1', "target", 0, 0, 0, true, false},
    {'playSFX', "sentDamage", 'hitconnect'},
    {'jumpBack', "none", "actor", 4, 8, true},
    {"wait", "none", 0.1},
    {"setAnimation", "none", "actor", "walk", false},
    {'goTo', "none", "actor", "start", 0, 0, 0.3, true},
    {'setAnimation', "none", "actor", 'idle', false},
  },

  onContact = { -- if the attack move and touch multiple ennemies, you can add
    -- specific effect when you touch the ennemy.
  },
}
