return {
    position = {
        x = 10,
        y = 10,
        area = "test.plain"
    },
    baseteam = {"sonic", "tails", "amy"},
    actions = {"run", "punch", "fly"}
}