return {
  headings = {"type"},
  argumentLists = {
    ["heal"]  = {"healType", "computationMode", "value"},
    ["setStatus"] = {"status", "set"},
    ["protectElement"] = {"element"},
    ["blockStatus"] = {"blockWhat"},
  },
  argumentWrapper = "",
}
