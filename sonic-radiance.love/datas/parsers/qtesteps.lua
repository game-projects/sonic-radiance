return {
  headings = {"name"},
  argumentLists = {
    ["simplePrompt"] = {"key", "duration"}
  },
  argumentWrapper = "arguments",
}
