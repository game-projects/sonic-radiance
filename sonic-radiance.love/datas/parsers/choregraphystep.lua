return {
  headings = {"name", "condition"},
  argumentLists = {
    ["wait"] = {"duration"},
    ["addGFX"] = {'sprite', "origin", "x", "y", "z", "affectedByDirection", 'blockProcess'},
    ["playSFX"] = {"sfx"},
    ["sendDamage"] = {"power", "type", "element", "isSpecial"},
    ["sendStatus"] = {"status", "duration", "force"},
    ["goTo"] = {"who", "origin", "x", "y", "duration", "blockProcess"},
    ["goTo3D"] = {"who", "origin", "x", "y", "z", "duration", "blockProcess"},
    ["setAnimation"] = {"who", "animation", "blockProcess"},
    ["jump"] = {"who", "power", "useDefaultAnimation", "blockProcess"},
    ["jumpTo"] = {"who", "origin", "x", "y", "duration", "blockProcess"},
    ["jumpBack"] = {"who", "height", "speed", "blockProcess"},
    ["waitActorFinished"] = {"waitFor"},
    ["setAnimSpeed"] = {"who", "speed"},
    ["waitFor"] = {"waitFor"},
    ["skipTo"] = {"skipTo"},
    ["addQTE"] = {"qteData", "origin", "blockProcess"},
    ["setCounter"] = {"counterName", "number", "relative"},
    ["useItemEffect"] = {},
    ["stopMov"] = {"who"},
    ["startSubChoregraphies"] = {"waitTime", "blockProcess"},
    --[name] = {args},
  },
  argumentWrapper = "arguments",
}

-- The "origin" argument can have the different values
-- - "target" : the action target. if there is multiple target, it'll
-- be equal to the middle of the ennemy line
--
-- - "self" : the fighter's actor position
--
-- - "start" : the fighter's starting position
--
-- - "absolute" : the 0,0 coordinates
