return { 
    headings = {"name", "condition"},
        argumentLists = {
        ["wait"] = {"duration"},
        ["simpleMessage"] = {"message"},
        ["dialogBox"] = {"message", "title", "avatar"},
        ["optionBox"] = {"message", "title", "avatar", "option1", "option2", "option3", "flag"},
        ["playSFX"] = {"sfx"},
        ["getRings"] = {"number"},
        ["getItems"] = {"type", "item", "number"},
        ["teleport"] = {"area", "x", "y", "charDir"},
        ["showGFX"] = {"x", "y", "spritename"}
        --[name] = {args...},
    },
    argumentWrapper = "arguments",
}

-- The "origin" argument can have the different values
-- - "target" : the action target. if there is multiple target, it'll
-- be equal to the middle of the ennemy line
--
-- - "self" : the fighter's actor position
--
-- - "start" : the fighter's starting position
--
-- - "absolute" : the 0,0 coordinates
