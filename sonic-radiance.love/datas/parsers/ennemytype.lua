return {
  headings = {"type"},
  argumentLists = {
    ["normal"]  = {"category", "name", "number"},
    ["boss"]    = {"category", "name", "pvFactor", "statFactor", "cheapEffect"},
    ["rival"]   = {"character", "level", "pvFactor"}
  },
  argumentWrapper = "",
}
