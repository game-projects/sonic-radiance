return {
  ["tilesets"] = {
    {"charicons",     "assets/sprites/characters/charicons"},
    {"normaltiles",   "assets/backgrounds/normaltile"},
    {"sptiles",       "assets/backgrounds/specialtile"},
    {"borders",       "assets/backgrounds/borders"},
  },
  ["sprites"]  = {
    {"cursorground",  "assets/gui/cursor/ground"},
    {"hitGFX",        "assets/sprites/gfx/hit1"},
    {"encounter",        "assets/sprites/encounter"},
    {"punch",        "assets/sprites/gfx/punch"},
    {"dash",        "assets/sprites/gfx/dash"},
    {"smallsmoke",        "assets/sprites/gfx/smallsmoke"},
    {"sparkles",        "assets/sprites/gfx/sparkles"},
  },
  ["textures"] = {
    {"statusbar",     "assets/gui/status_bar.png"},
    {"actorsShadow",  "assets/sprites/shadow.png"},

    {"guiRing",     "assets/gui/ring.png"},
    {"lvl",     "assets/gui/strings/lvl.png"},
    {"exp",     "assets/gui/strings/exp.png"},

    {"itembox", "assets/gui/itembox.png"},
    {"shadow", "assets/sprites/owshadow.png"},
  },
  ["sfx"] = {
    {"hit", "assets/sfx/sommersault.wav"},
    {"jump", "assets/sfx/woosh.wav"},
    {"fly", "assets/sfx/flight.wav"},
    {"dash", "assets/sfx/dash.wav"},
    {"woosh", "assets/sfx/woosh.wav"},
    {"spincharge", "assets/sfx/spincharge.wav"},
    {"spinrelease", "assets/sfx/spinrelease.wav"},
    {"ring", "assets/sfx/ring.wav"},
    {"pop", "assets/sfx/pop.wav"},
    {"fall", "assets/sfx/fall.wav"},
    {"splash", "assets/sfx/splash.wav"}
  }
}
