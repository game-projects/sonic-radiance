return {
  ["textures"] = {
    {"shadow", "assets/sprites/shadow.png"}
  },
  ["sprites"] = {
    {"ring",      "assets/sprites/items/ring"}
  },
  ["imagefonts"] = {
    {"menu",    "assets/gui/fonts/SA2font"},
    {"numbers",    "assets/gui/fonts/hudnumbers"},
    {"smallnumbers",    "assets/gui/fonts/hudsmallnumbers"},
  },
  ["sfx"] = {
    {"navigate",  "assets/sfx/menus/beep.wav"},
    {"confirm",   "assets/sfx/menus/select.wav"},
    {"cancel",    "assets/sfx/menus/back.wav"},
  },
  ["tilesets"] = {
    {"sptiles", "assets/backgrounds/specialtile"}
  }
}
