return {
  ["sprites"]  = {
    {"cursorground",  "assets/gui/cursor/ground"}
  },
  ["tilesets"] = {
    {"itembox", "assets/gui/itembox"}
  },
  ["textures"] = {
    {"menucursor",    "assets/gui/cursor-menulist.png"},
    {"statusbar",     "assets/gui/status_bar.png"},
    {"cursorpeak",    "assets/gui/cursor/peak.png"},

    {"e_speedster", "assets/gui/emblem_speedster.png"},
    {"e_technic",   "assets/gui/emblem_technic.png"},
    {"e_power",     "assets/gui/emblem_power.png"},

    {"m_speedster", "assets/gui/emblem_speedster_mask.png"},
    {"m_technic",   "assets/gui/emblem_technic_mask.png"},
    {"m_power",     "assets/gui/emblem_power_mask.png"},

    {"background",    "assets/artworks/back.png"}
  },
  ["fonts"]    = {
    {"small",         "assets/gui/fonts/PixelOperator.ttf", 16}
  },
  ["imagefonts"]    = {
    {"hudnbrs",       "assets/gui/fonts/hudnumbers"},
    {"hudnbrs_small", "assets/gui/fonts/hudsmallnumbers"},
    {"SA2font", "assets/gui/fonts/SA2font"},
  },
  ["sfx"] = {
    {"mBack", "assets/sfx/menus/back.wav"},
    {"mBeep", "assets/sfx/menus/beep.wav"},
    {"mSelect", "assets/sfx/menus/select.wav"},
    {"mError", "assets/sfx/menus/error.wav"},
  }
}
