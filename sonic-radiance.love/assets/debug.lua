return {
  ["tilesets"] = {
    {"charicons",     "assets/sprites/characters/charicons"},
    {"normaltiles",   "assets/backgrounds/normaltile"},
    {"sptiles",       "assets/backgrounds/specialtile"},
    {"borders",       "assets/backgrounds/borders"},
  },
  ["sprites"]  = {
    {"cursorground",  "assets/gui/cursor/ground"},
    {"hitGFX",        "assets/sprites/gfx/hit1"},
  },
  ["textures"] = {
    {"statusbar",     "assets/gui/status_bar.png"},
    {"actorsShadow",  "assets/sprites/shadow.png"},
    {"emptytile",     "assets/backgrounds/tilemask.png"},

    {"hudturn",     "assets/gui/strings/hudturn.png"},
    {"battlecompleted", "assets/gui/strings/battle_completed.png" }
  },
  ["sfx"] = {
    {"hit", "assets/sfx/hit.wav"},
    {"hitconnect", "assets/sfx/hitconnect.wav"},
    {"jump", "assets/sfx/jump.wav"},
    {"woosh", "assets/sfx/woosh.wav"},
    {"spincharge", "assets/sfx/spincharge.wav"},
    {"spinrelease", "assets/sfx/spinrelease.wav"}
  }
}
