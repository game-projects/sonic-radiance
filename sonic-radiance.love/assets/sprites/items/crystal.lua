return {
  metadata = {
    height      = 16,
    width       = 16,
    defaultAnim = "default"
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 1,
      loop        = 1,
      speed       = 0,
      pauseAtEnd  = true,
    },
  }
}
