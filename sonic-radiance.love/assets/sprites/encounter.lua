return {
  metadata = {
    height      = 25,
    width       = 32,
    defaultAnim = "default",
    ox = 8,
    oy = 25
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 2,
      loop        = 1,
      speed       = 4,
      pauseAtEnd  = false,
    },
  }
}
