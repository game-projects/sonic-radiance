return {
  metadata = {
    width       = 33,
    height      = 44,
    defaultAnim = "default",
    ox          = 16,
    oy          = 22,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 7,
      loop        = 1,
      speed       = 15,
      pauseAtEnd  = false,
    },
  }
}
