return {
  metadata = {
    width       = 32,
    height      = 28,
    defaultAnim = "default",
    ox          = 0,
    oy          = 14,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 4,
      loop        = 1,
      speed       = 15,
      pauseAtEnd  = false,
    },
  }
}
