return {
  metadata = {
    width       = 26,
    height      = 44,
    defaultAnim = "default",
    ox          = 0,
    oy          = 22,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 4,
      loop        = 1,
      speed       = 15,
      pauseAtEnd  = false,
    },
  }
}
