return {
  metadata = {
    width       = 18,
    height      = 22,
    defaultAnim = "default",
    ox          = 9,
    oy          = 11,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 5,
      loop        = 1,
      speed       = 15,
      pauseAtEnd  = true,
    },
  }
}
