return {
  metadata = {
    height      = 32,
    width       = 32,
    defaultAnim = "default",
    ox          = 16,
    oy          = 32,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 5,
      loop        = 1,
      speed       = 15,
      pauseAtEnd  = false,
    },
  }
}
