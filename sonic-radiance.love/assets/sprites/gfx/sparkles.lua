return {
  metadata = {
    width       = 16,
    height      = 16,
    defaultAnim = "default",
    ox          = 8,
    oy          = 8,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 4,
      loop        = 1,
      speed       = 20,
      pauseAtEnd  = true,
    },
  }
}
