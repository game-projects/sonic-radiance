return {
  metadata = {
    width       = 28,
    height      = 11,
    defaultAnim = "default"
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 3,
      loop        = 1,
      speed       = 3,
      pauseAtEnd  = false,
    },
  }
}
