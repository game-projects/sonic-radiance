return {
  ["tilesets"] = {
    {"charicons",     "assets/sprites/characters/charicons"},
    {"normaltiles",   "assets/backgrounds/normaltile"},
    {"sptiles",       "assets/backgrounds/specialtile"},
    {"borders",       "assets/backgrounds/borders"},
    {"ranks",         "assets/gui/ranks"},
    {"qtebtn",        "assets/gui/qtebtn"}
  },
  ["textures"] = {
    {"expbar",     "assets/gui/expbar.png"},
    {"actorsShadow",  "assets/sprites/shadow.png"},
    {"emptytile",     "assets/backgrounds/tilemask.png"},

    {"badnicsIcon",     "assets/sprites/characters/badnics.png"},

    {"hudturn",     "assets/gui/strings/hudturn.png"},
    {"battlecompleted", "assets/gui/strings/battle_completed.png" },
    {"egghead", "assets/gui/egghead.png"},

    {"crown", "assets/gui/crown.png"}
  },
  ["sfx"] = {
    {"hit", "assets/sfx/hit.wav"},
    {"hitconnect", "assets/sfx/hitconnect.wav"},
    {"jump", "assets/sfx/jump.wav"},
    {"woosh", "assets/sfx/woosh.wav"},
    {"spincharge", "assets/sfx/spincharge.wav"},
    {"spinrelease", "assets/sfx/spinrelease.wav"},
    {"badnicsBoom", "assets/sfx/badnicsDestroyed.wav"}
  }
}
