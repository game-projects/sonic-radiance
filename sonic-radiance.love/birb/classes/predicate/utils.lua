-- predicate/utils :: Simple utilities for predicates

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local ConditionsUtils = {}

function ConditionsUtils.testVariables(var1, testType, var2)
    local var2 = tonumber(var2)
    if (testType == "eq" and var1 == var2) then
        return true
    end
    if (testType == "ne" and var1 ~= var2) then
        return true
    end
    if (testType == "gt" and var1 > var2) then
        return true
    end
    if (testType == "ge" and var1 >= var2) then
        return true
    end
    if (testType == "lt" and var1 < var2) then
        return true
    end
    if (testType == "le" and var1 <= var2) then
        return true
    end

    return false
end

function ConditionsUtils.testBool(bool, boolType)
    return (bool == (boolType == "V"))
end

function ConditionsUtils.merge(cond)
    local returnString = ""
    for i, condElement in ipairs(cond) do
        returnString = returnString .. condElement
        if (i < #cond) then
            returnString = returnString .. ":"
        end
    end
    return returnString
end

return ConditionsUtils;