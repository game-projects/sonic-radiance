-- classes/predicates :: a predicate system, to be an intermediary between condition
-- solvers and complex conditions. You simply create a predicate with your data and the
-- solver to create a predicate, then use the Predicate:solve() API to get your response

-- To create a Predicate, prefer the Predicate.createPredicate instead of Predicate:new()
-- As it'll allow you to dynamically create simple or complex predicate on the fly
-- while Predicate:new() will try to create a complex predicate

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Predicate = Object:extend()
local SimplePredicate = require "birb.classes.predicate.simple"

function Predicate.createPredicate(data, solver, asker)
    local predicate = nil
    if (type(data) == "table") then
        predicate = Predicate(data, solver, asker)
    end
    if (type(data) == "string") then
        predicate = SimplePredicate(data, solver, asker)
    end

    if (predicate ~= nil) then
        return predicate
    else
        error("Predicate data aren't a table or a string : " .. tostring(data))
    end
end

function Predicate:new(data ,solver, asker)
    self.solver = solver
    self.asker = asker
    self.beginAt = 1
    self:setType(data)
    self.notTag = false
    self:setList(data)
end

function Predicate:solve()
    local predicateSolved
    if (self.type == "or") then
        predicateSolved = self:solveOr()
    else
        predicateSolved = self:solveAnd()
    end

    if (self.notTag) then
        predicateSolved = (predicateSolved == false)
    end

    return predicateSolved
end

-- INTERNAL FUNCTIONS
-- Handle the complex predicate

function Predicate:setType(data)
    self.type = "and"
    if (type(data[1]) == "string") then
        if (data[1] == "or") or (data[1] == "and") then
            self.beginAt = 2
            self.type = data[1]
        end
    end
end

function Predicate:setList(data)
    self.list = {}
    for i = self.beginAt, #data, 1 do
        if (i == #data) and (data[i] == "not") then
            self.notTag = true
        else
            table.insert(self.list, Predicate.createPredicate(data[i], self.solver, self.asker))
        end
    end
end

function Predicate:solveOr()
    for i, predicate in ipairs(self.list) do
        if (predicate:solve()) then
            return true
        end
    end
    return false
end

function Predicate:solveAnd()
    for i, predicate in ipairs(self.list) do
        if (not predicate:solve()) then
            return false
        end
    end
    return true
end

return Predicate