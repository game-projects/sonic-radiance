-- indexedbox.lua :: An indexed box is a box indexed to a point,
-- Basically a box that have its coordinate recalculated from an existing
-- point

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Rect = require "birb.classes.2D.rect"
local IndexedRect = Rect:extend()

function IndexedRect:new(origin, ox, oy, oz, w, h, d)
    self.setOrigin(origin)
    self.ox = ox
    self.oy = oy
    self.oz = oz
    local x, y, z = self:getPosition()
    IndexedRect.super.new(self, x, y, w, h)
end

function IndexedRect:setRelativePosition(ox, oy, oz)
    self.ox = ox or self.ox
    self.oy = oy or self.oy
    self.oz = oz or self.oz
end

function IndexedRect:setOrigin(origin)
    self.origin = origin
    -- We should check if the origin is really a point
end

function IndexedRect:getOrigin(origin)
    return self.origin
end

function IndexedRect:getPosition()
    return self.origin.x + self.ox, self.origin.y + self.oy, self.origin.z + self.oz
end

function IndexedRect:updateBox()
    local x, y, z = self:getPosition()
    self:setPosition(x, y, z)
    return x, y, self.w, self.h, self.d
end

function IndexedRect:modify(ox, oy, oz, w, h, d)
    self.ox = ox
    self.oy = oy
    self.oz = oz
    self:setSize(w, h, d)
    self:updateBox()
end

return IndexedRect