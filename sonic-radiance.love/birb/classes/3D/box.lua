-- box.lua :: a 3D box.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Point = require "birb.classes.3D.point3D"
local Box = Point:extend()

function Box:new(x, y, z, w, h, d)
    Box.super.new(self, x, y, z)
    self:setSize(w, h, d)
end

function Box:setSize(w, h, d)
    self.w = w or self.w
    self.h = h or self.h
    self.d = d or self.d
end

function Box:getArea()
    local x, y, z = self:getPosition()
    return x, y, z, self.w, self.h, self.d
end

function Box:getShape()
    local x, y, z, w, h, d = self:getArea()
    return x, y-z-d, w, h+d
end

function Box:getCorners()
    local x, y, z, w, h, d = self:getArea()
    return x, y, z, x + w, y + h, z + d
end

function Box:getCenter()
    local x, y, z, w, h, d = self:getArea()
    return math.floor(x + (w/2)), math.floor(y + (h/2)), math.floor(z + (d/2))
end

function Box:areCoordInside(dx, dy, dz)
    local x, y, z, w, h, d = self:getArea()
    return ((dx > x) and (dx < x + w) and (dy > y) and (dy < y + h)) and (dz > z) and (dz < z + d)
end

function Box:getRelativeCoordinate(dx, dy, dz)
    return dx - self.x, dy - self.y, dz - self.z
end

function Box:drawBox()
    utils.graphics.box(self.x, self.y, self.w, self.h)
end

return Box