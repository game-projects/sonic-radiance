-- point.lua :: a 2D point.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Point = Object:extend()

function Point:new(x, y)
    self.x, self.y = x, y
end

function Point:setPosition(x, y)
    self.x, self.y = x, y
end

function Point:goToward(x, y)
    local x = x or 0
    local y = y or 0
    self:setPosition(self.x + x, self.y + y)
end

function Point:getPosition()
    return self.x, self.y
end

function Point:getDistance(x, y)
    return utils.math.pointDistance(self.x, self.y, x, y)
end

function Point:getDistanceFromPoint(other)
    local x, y = other:getPosition()
    self:getDistance(x, y)
end

function Point:getDirection(x, y)
    return utils.math.pointDirection(self.x, self.y, x, y)
end

function Point:getDirectionFromPoint(other)
    local x, y = other:getPosition()
    self:getDirection(x, y)
end

function Point:getMiddlePoint(x, y)
    return utils.math.getMiddlePoint(self.x, self.y, x, y)
end

function Point:getMiddlePointFromPoint(other)
    local x, y = other:getPosition()
    self:getMiddlePoint(x, y)
end

return Point