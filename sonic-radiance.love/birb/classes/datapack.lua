-- datapack.lua :: a categorized and indexed pack of data. Aim to make easy to get
-- the category from an element, without having

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local DataPack = Object:extend()

local UNCATEGORIZED = "uncategorized"

function DataPack:new(foldername, categories, areFiles, subfiles)
    self.foldername = foldername
    self.areFiles = (areFiles ~= false)
    self.subfiles = subfiles or {}
    self.categories = categories or {}
    --print("loading " .. foldername)
    self:loadAllDatas()
end

function DataPack:getBaseDirectory(lua)
    local foldername = self.foldername
    if (not lua) then
        foldername = string.gsub(foldername, "%.", "/")
    end
    return utils.datas.concatDataFolder(foldername, lua)
end

function DataPack:getCategoryDirectory(category, lua)
    if (category == UNCATEGORIZED) then
        return self:getBaseDirectory(lua)
    else
        return utils.datas.concatFolder(self:getBaseDirectory(lua), category, lua)
    end
end

function DataPack:haveCategories()
    return (#self.categories > 0)
end

function DataPack:loadAllDatas()
    self.index = {}
    self.reverseIndex = {}

    if (self:haveCategories()) then
        for i, category in ipairs(self.categories) do
            self:loadDatas(category)
        end
    else
        self:loadDatas(UNCATEGORIZED)
    end
end

function DataPack:loadDatas(category)
    local directory = self:getCategoryDirectory(category)
    local items = love.filesystem.getDirectoryItems(directory)
    self.reverseIndex[category] = {}
    for i, filename in ipairs(items) do
        if (utils.datas.isLuaFile(filename) == self.areFiles) then
            if (self.areFiles) then
                filename = utils.datas.luaFileToModule(filename)
            end

            if (self.index[filename] ~= nil) then
                error("Data " .. filename .. " already exists for " .. self.foldername)
            end
            --print("loading " .. filename .. " from category " .. category .. " for " .. self.foldername)
            self.index[filename] = category
            table.insert(self.reverseIndex[category], filename)
        end
    end
end

function DataPack:dataExists(name)
    return (self.index[name] ~= nil)
end

function DataPack:getPath(name, lua)
    if (not self:dataExists(name)) then
        error("Data " .. name .. " do not exist for " .. self.foldername)
    end
    local category = self.index[name]

    local categoryDirectory = self:getCategoryDirectory(category, true)

    return utils.datas.concatFolder(categoryDirectory, name, lua)
end

function DataPack:get(name)
    local path = self:getPath(name, true)
    local data = utils.datas.copy(path)
    if (not self.areFiles) then
        for i, subfile in ipairs(self.subfiles) do
            data[subfile] = utils.datas.copyDataset(path, subfile)
        end
    end
    return data
end

function DataPack:getFromCategory(category)
    return self.reverseIndex[category] or {}
end

return DataPack
