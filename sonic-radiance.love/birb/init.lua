-- birb :: The main birb script, loading the core and main utilities

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

birb = {}

-- GLOBAL UTILS/FUNCTION LOADING
-- Load in the global namespace utilities that'll need to be reusable everywhere
-- in the game

Object  = require("birb.libs.classic")
utils   = require("birb.utils")
enum    = require("birb.utils.enum")

birb.Core = require("birb.core")

function birb.start(gamemodule, args)
  birb.startCore(args)
  if (gamemodule ~= nil) then
    birb.startGame(gamemodule)
  end
end

function birb.startCore(args)
  core = birb.Core(args)
end

function birb.startGame(gamemodule)
  local GameObject = require(gamemodule)
  game = GameObject()
end

require("birb.callbacks")