local Bools = {}

function Bools.either(bool, val1, val2)
    if (bool) then
        return val1
    else
        return val2
    end
end

function Bools.toString(bool)
    return Bools.either(bool, "true", "false")
end

return Bools
