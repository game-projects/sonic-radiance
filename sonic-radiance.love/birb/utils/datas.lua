--- loveutils.datas : simple functions for data manipulation.

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local DataUtils = {}

local DATADIR = "datas"
local GAMEDATADIR = "gamedata"

--- Verify if a name correspond to a lua file
---@param name string the filename you want to test
---@return boolean isLua if the filename finish by .lua
function DataUtils.isLuaFile(name)
  local extension = name:sub(#name - 3, #name)
  return (extension == ".lua")
end

--- Transform a lua filename to a module name
---@param luaFile string the filename you want to transform
---@return boolean moduleName the module name
function DataUtils.luaFileToModule(luaFile)
  if (DataUtils.isLuaFile(luaFile)) then
    return luaFile:sub(1, #luaFile - 4)
  else
    return luaFile
  end
end

--- Transform a list of lua filename to a list of module name
---@param luaFileList table the filename list you want to transform
---@return table moduleList the module name list
function DataUtils.luaFileListToModuleList(luaFileList)
  local moduleList = {}
  for i,luaFile in ipairs(luaFileList) do
    table.insert(moduleList, DataUtils.luaFileToModule(luaFile))
  end
  return moduleList
end

--- Use "require" on a a composited filename
---@param parent string the parent filename
---@param filename string the filename of the file
---@return any data the content of the lua file
function DataUtils.require(parent, filename)
  return require(DataUtils.concatFolder(parent, filename, true))
end

--- Require and copy the content of a lua file 
--- It's mostly used for lua file containing data, for not having to copy their data
---@param filepath string the filepath of the file
---@return table data the copied content of the lua file
function DataUtils.copy(filepath)
  local orig = require(filepath)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in pairs(orig) do
          copy[orig_key] = orig_value
      end
  else -- number, string, boolean, etc
      copy = orig
  end
  return copy
end


--- Require and copy the content of a lua file from it's parent folder
--- It's mostly used for lua file containing data, for not having to copy their data
---@param parent string the path of the parent
---@param filename string the filename of the file
---@return table data the copied content of the lua file
function DataUtils.copyDataset(parent, filename)
  local orig = DataUtils.require(parent, filename)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in pairs(orig) do
          copy[orig_key] = orig_value
      end
  else -- number, string, boolean, etc
      copy = orig
  end
  return copy
end

--- Get a data directory path from the gamedata dir
---@param folder string the folder name
---@param lua boolean if we are dealing with lua modules
---@return string path the data directory path
function DataUtils.concatDataFolder(folder, lua)
  return DataUtils.concatFolder(DataUtils.getGameDataDir(lua), folder, lua)
end

--- Get the game data directory
---@param lua boolean if we are dealing with lua modules
---@return string path the gamedirectory path
function DataUtils.getGameDataDir(lua)
  return DataUtils.concatFolder(DATADIR, GAMEDATADIR, lua)
end

--- Concat a folder path
---@param parent string the path of the parent folder
---@param filename string the name of the element you want to concat to the path
---@param lua boolean if we are dealing with lua modules
---@return string path the path
function DataUtils.concatFolder(parent, filename, lua)
  return parent .. DataUtils.sep(lua) .. filename
end

--- Return the separater character for lua modules or filesystem pathname
---@param lua boolean if we are dealing with lua modules
---@return string sep the separator
function DataUtils.sep(lua)
  if (lua) then
    return "."
  else
    return "/"
  end
end

return DataUtils
