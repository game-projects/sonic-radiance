local TransitionParent = require "birb.modules.transitions.parent"
local CanvasTransition = TransitionParent:extend()

function CanvasTransition:new(func, ox, oy, fadeOut, easeIn, easeOut, duration, wait)
    CanvasTransition.super.new(self, func, ox, oy, fadeOut, easeIn, easeOut, duration, wait)
    self:generateCanvas(0)
end

function CanvasTransition:update(dt)
    CanvasTransition.super.update(self, dt)
    self:generateCanvas(dt)
end

function CanvasTransition:generateCanvas(dt)
    self.canvas = love.graphics.newCanvas(424, 240)
    love.graphics.setCanvas(self.canvas)
    self:drawCanvas(dt)
    love.graphics.setCanvas()
end

function CanvasTransition:drawCanvas(dt)

end

function CanvasTransition:draw()
    if (self.canvas ~= nil) then
        love.graphics.setBlendMode("multiply", "premultiplied")
        love.graphics.draw(self.canvas, 0, 0)
        love.graphics.setBlendMode("alpha")
    end
end

return CanvasTransition