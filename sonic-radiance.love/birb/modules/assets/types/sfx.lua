-- assets/sfx :: the sfx object

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]
local SFX = Object:extend()

-- INIT FUNCTIONS
-- Initilizing and configuring option

function SFX:new(filepath)
    self.source = love.audio.newSource(filepath, "static")
end

-- INFO FUNCTIONS
-- get information with these functions

-- None for the moment

-- DRAW FUNCTIONS
-- Draw texture using these functions

function SFX:play()
    self.source:stop()
    self.source:setVolume(core.options.data.audio.sfx / 100)
    love.audio.play(self.source)
end

function SFX:stop()
    self.source:stop()
end

return SFX
