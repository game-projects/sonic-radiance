-- actor3D.lua :: the implementation of a 2D actor. It contain every element
-- needed to create your own 2D actors.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Hitbox  = require("birb.modules.world.actors.utils.hitbox3D")
local Boxes   = require("birb.modules.world.actors.utils.boxes")
local BasicBox = require "birb.classes.3D.box"

local BaseActor = require("birb.modules.world.actors.mixins.base")
local SpritedActor = require("birb.modules.world.actors.mixins.sprites")
local TimedActor = require("birb.modules.world.actors.mixins.timers")
local InputActor = require("birb.modules.world.actors.mixins.inputs")
local PhysicalActor = require("birb.modules.world.actors.mixins.physics")
local Shape3DActor = require("birb.modules.world.actors.mixins.shapes")

local Actor3D = BasicBox:extend()
Actor3D:implement(BaseActor)
Actor3D:implement(SpritedActor)
Actor3D:implement(TimedActor)
Actor3D:implement(InputActor)
Actor3D:implement(PhysicalActor)
Actor3D:implement(Shape3DActor)

-- INIT FUNCTIONS
-- Initialise the actor and its base functions

function Actor3D:new(world, type, x, y, z, w, h, d, isSolid)
  Actor3D.super.new(self, x, y, z, w, h, d)
  self:init(world, type)
  self:initPhysics(Hitbox, isSolid)
  self:initTimers()
  self:initSprite()
  self:initKeys()
  self:initShape(Boxes, true)
end

function Actor3D:destroy()
  self:destroyShape()
  self.mainHitbox:destroy()
  self.world:removeActor(self)
  self.isDestroyed = true
end

-- PHYSICS FUNCTIONS
-- Handle movement and collisions.

function Actor3D:moveToFuturePosition(dt)
  local dx, dy, dz = self:getFuturePosition(dt)
  local _, _, _, cols, colNumber = self:move(dx, dy, dz)
  return cols, colNumber
end

function Actor3D:changeSpeedToCollisionNormal(normal)
  local xsp, ysp, zsp = self.xsp, self.ysp, self.zsp
  local nx, ny, nz = normal.x, normal.y, normal.z

  if (nx < 0 and xsp > 0) or (nx > 0 and xsp < 0) then
    xsp = -xsp * self.bounceFactor
  end

  if (ny < 0 and ysp > 0) or (ny > 0 and ysp < 0) then
    ysp = -ysp * self.bounceFactor
  end

  if (nz < 0 and zsp > 0) or (nz > 0 and zsp < 0) then
    zsp = -zsp * self.bounceFactor
  end

  self.xsp, self.ysp, self.zsp = xsp, ysp, zsp
end

function Actor3D:move(dx, dy, dz)
  local cols, colNumber = {}, 0
  local oldx, oldy, oldz = self.x, self.y, self.z
  if (self.isDestroyed == false) then
    self.x, self.y, self.z, cols, colNumber = self.mainHitbox:checkCollisionAtPoint(dx, dy, dz, self.filter)
    self.mainHitbox:updatePosition()
    self.world:updateShape(self)
  end

  if (oldx ~= self.x) or (oldy ~= self.y) or (oldz ~= self.z) or (self.shadowTargetsPrevious == nil) then
    if (self.doCastShadows) then
      self:castShadow()
    end
  end

  return self.x, self.y, self.z, cols, colNumber
end

function Actor3D:checkCollisionAtPoint(dx, dy, dz)
  local x, y, z, cols, colNumber = dx, dy, dz, {}, 0
  if (self.isDestroyed == false) then
    x, y, z, cols, colNumber = self.mainHitbox:checkCollisionAtPoint(dx, dy, dz, self.filter)
  end
  return self.x, self.y, self.z, cols, colNumber
end

-- GRAVITY SYSTEM FUNCTIONS
-- All functions related to gravity

function Actor3D:applyGravity(dt)
  local grav = self.grav * -1
  self.zsp = self.zsp + (grav * dt)

  if utils.math.sign(self.zsp) == utils.math.sign(grav) then
    self:checkGround( )
  end
end

function Actor3D:checkGround()
  local dx, dy, dz = self.x, self.y, self.z - utils.math.sign(self.grav)
  local newx, newy, newz, cols, colNumber = self:checkCollisionAtPoint(dx, dy, dz)
  for i, col in ipairs(cols) do
    if (col.type == "touch") or (col.type == "bounce") or (col.type == "slide") then
      if not (self.grav == 0) then
        if col.normal.z == utils.math.sign(self.grav) then self.onGround = true end
      end
    end
  end
end

-- COORDINATE/MOVEMENT FUNCTIONS
-- Handle coordinate

function Actor3D:getViewCenter()
  local x, y, z = self:getCenter()
  return x, y - (self.d/2)
end

-- HITBOXES FUNCTIONS
-- Functions related to actor hitboxes

function Actor3D:packForHitbox()
  return {0, 0, 0, self.w, self.h, self.d}
end

-- DRAW FUNCTIONS
-- Draw the actors.

function Actor3D:drawShadow(x, y)
  love.graphics.setColor(0, 0, 0, 1)
  love.graphics.rectangle("fill", x, y, self.w, self.h)
  utils.graphics.resetColor()
end

function Actor3D:draw()
  self:drawStart()
  if (self.box ~= nil) then
    self.box:draw(self.x, self.y, self.z)
  else
    local x, y = math.floor(self.x), math.floor(self.y - self.z - self.d + (self.h/2))
    self:drawSprite(x, y)
  end
  self:drawEnd()
end

return Actor3D
