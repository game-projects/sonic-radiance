-- hitbox2D.lua :: a basic 2D hitbox object. It's used by the actors to check
-- collisions and to handle different type of responses.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Hitbox2D = Object:extend()
local rectStructure = require "birb.structures.rect"

-- INIT FUNCTIONS
-- Initialise the actor and its base functions

function Hitbox2D:new(owner, type, data, sx, sy, isSolid)
  self.owner    = owner
  self.world    = owner.world

  self.type     = type
  self:setFromData(data, sx, sy)
  self.x, self.y = self:getPosition()
  self.isSolid  = isSolid

  self.isMainHitBox = false

  self:setDebugColor(0,0,0)
  self:register()
end

function Hitbox2D:advertiseAsMainHitbox()
  self.isMainHitBox = true
end

function Hitbox2D:modify(ox, oy, w, h)
  self.ox       = ox
  self.oy       = oy
  self.x, self.y = self:getPosition()
  self.w        = w
  self.h        = h
end

function Hitbox2D:setFromData(data, sx, sy)
  local rect = utils.table.parse(data, rectStructure)

  self.ox = rect.x
  self.oy = rect.y
  self.w = rect.w
  self.h = rect.h

  self:applyScale(sx, sy)
end

function Hitbox2D:applyScale(sx, sy)
  local sx = sx or 1
  local sy = sy or 1

  if (sx < 0) then
    self.ox = self.owner.w - self.ox - self.w
  end

  if (sy < 0) then
    self.oy = self.owner.h - self.oy - self.h
  end
end

function Hitbox2D:setDebugColor(r,g,b)
  self.debug = {}
  self.debug.r = r
  self.debug.g = g
  self.debug.b = b
end

function Hitbox2D:register()
  self.world:registerBody(self)
end

function Hitbox2D:destroy()
  self.world:removeBody(self)
end

-- COORDINATE FUNCTIONS
-- Handle Hitbox position

function Hitbox2D:updatePosition()
  self.x, self.y = self:getPosition()
  self.world:updateBody(self)
  return self.x, self.y
end

function Hitbox2D:getPosition()
  return self.ox + self.owner.x, self.oy + self.owner.y
end

function Hitbox2D:getOwnerPosition()
  return self.x - self.ox, self.y - self.oy
end

function Hitbox2D:getNewOwnerPosition(x, y)
  return x - self.ox, y - self.oy
end

function Hitbox2D:getCenter()
  return self.x + (self.w/2), self.y + (self.h/2)
end

-- COLLISION FUNCTIONS
-- Handle Hitbox position

function Hitbox2D:checkCollision(filter)
  local _, _, cols, colNumber = self:checkCollisionAtPoint(self.owner.x, self.owner.y, filter)
  return cols, colNumber
end

function Hitbox2D:checkCollisionAtPoint(dx, dy, filter)
  self:updatePosition()

  local nx, ny = self.ox + dx, self.oy + dy
  if (self.owner.useTileCollision and self.world.map.supportTileCollision) then
    if (self:checkTileCollision(nx, self.y)) then
      nx = self.x
      local relx = dx - self.x
      for i = 1, math.ceil(math.abs(relx)), 1 do
        local nnx = self.x + i * utils.math.sign(relx)
        if (not self:checkTileCollision(nnx, self.y)) then
          nx = nnx
        end
      end
      self.owner.xsp = 0
    end
    if (self:checkTileCollision(self.x, ny)) then
      ny = self.y
      local rely = dy - self.y
      for i = 1, math.ceil(math.abs(rely)), 1 do
        local nny = self.y + i * utils.math.sign(rely)
        if (not self:checkTileCollision(self.x, nny)) then
          ny = nny
        end
      end
      self.owner.ysp = 0
    end
  end
  local x, y, cols, colNumber = self.world:checkCollisionAtPoint(self, nx, ny, filter)
  local newx, newy = self:getNewOwnerPosition(x, y)

  return newx, newy, cols, colNumber
end

function Hitbox2D:checkTileCollision(dx, dy)
  return self.world:haveTileTypeInRect(dx + 1, dy + 1, self.w - 2, self.h - 2, "solid")
end

-- DRAW FUNCTIONS
-- Just some debug function to draw hitbox

function Hitbox2D:draw()
  local x, y = self:getPosition()
  love.graphics.setColor(self.debug.r, self.debug.g, self.debug.b, 1)
  utils.graphics.box(x, y, self.w, self.h)
  utils.graphics.resetColor()
end

return Hitbox2D
