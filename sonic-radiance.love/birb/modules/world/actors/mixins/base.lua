-- BaseActor.lua :: the global implementation of an actor. Basically, it abstract
-- everything that isn't only 2D or 3D related to the actor system.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local BaseActor = Object:extend()

-- INIT FUNCTIONS
-- Initialise the actor and its base functions

function BaseActor:init(world, type)
  self.type     = type or ""
  self.depth    = 0

  self.updateFunctions = {}

  self:setManagers(world)

  self:setDebugColor(1, 1, 1)
  self:register()
  self.useTileCollision = true
end

function BaseActor:setProperties(properties)
  -- Do something here
end

function BaseActor:addUpdateFunction(func)
  table.insert(self.updateFunctions, func)
end

function BaseActor:applyUpdateFunctions(dt)
  for key, func in ipairs(self.updateFunctions) do
    func(self, dt)
  end
end

function BaseActor:setManagers(world)
  self.world  = world
  self.scene  = world.scene
  self.obj    = world.obj
  self.assets = self.scene.assets
end

function BaseActor:setDebugColor(r,g,b)
  self.debug = {}
  self.debug.r = r
  self.debug.g = g
  self.debug.b = b
end

function BaseActor:register()
  self.world:registerActor(self)
  self.isDestroyed = false
end

function BaseActor:destroy()
  self.world:removeActor(self)
  self.isDestroyed = true
end

-- UPDATE FUNCTIONS
-- Theses functions are activated every steps

function BaseActor:updateStart(dt)

end

function BaseActor:update(dt)
  self:updateStart(dt)
  self:applyUpdateFunctions(dt)
  self:updateEnd(dt)
end

function BaseActor:updateEnd(dt)

end

-- DRAW FUNCTIONS
-- Draw the actors.

function BaseActor:drawStart()

end

function BaseActor:draw()
  self:drawStart()
  self:drawEnd()
end

function BaseActor:drawEnd()

end

return BaseActor
