-- SpritedActor.lua :: Handle the sprite of the actor.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local SpritedActor = Object:extend()
local Sprite  = require("birb.modules.world.actors.utils.sprites")

function SpritedActor:initSprite()
    self:addUpdateFunction(self.updateSprite)
end

function SpritedActor:setSprite(spritename, isClone, ox, oy)
    self.sprite = Sprite(self, spritename, ox, oy)
    if (isClone) then
        self.sprite:clone()
    end
end

function SpritedActor:changeAnimation(animation, restart)
    if (self.sprite ~= nil) then
        core.debug:warning("actor", "the function SpritedActor:changeAnimation is deprecated, prefer SpritedActor.sprite:changeAnimation()")
        self.sprite:changeAnimation(animation, restart)
    end
end

function SpritedActor:animationEnded(animation)
    -- Empty placeholder function
end

function SpritedActor:setCustomSpeed(customSpeed)
    if (self.sprite ~= nil) then
        core.debug:warning("actor", "the function SpritedActor:setCustomSpeed is deprecated, prefer SpritedActor.sprite:setCustomSpeed()")
        self.sprite:setCustomSpeed(customSpeed)
    end
end

function SpritedActor:updateSprite(dt)
    if (self.sprite ~= nil) then
        self.sprite:update(dt)
    end
end

function SpritedActor:getCurrentAnimation()
    if (self.sprite ~= nil) then
        core.debug:warning("actor", "the function SpritedActor:getCurrentAnimation is deprecated, prefer SpritedActor.sprite:getCurrentAnimation()")
        return self.sprite:getCurrentAnimation()
    end
end

function SpritedActor:getSpriteScalling()
    core.debug:warning("actor", "the function SpritedActor:getSpriteScalling is deprecated, prefer SpritedActor.sprite:getScalling()")
    return self.sprite:getScalling()
end

function SpritedActor:getFrame()
    if (self.sprite ~= nil) then
        core.debug:warning("actor", "the function SpritedActor:getFrame is deprecated, prefer SpritedActor.sprite:getFrame()")
        return self.sprite:getFrame()
    end
end

function SpritedActor:getRelativeFrame()
    if (self.sprite ~= nil) then
        core.debug:warning("actor", "the function SpritedActor:getRelativeFrame is deprecated, prefer SpritedActor.sprite:getRelativeFrame()")
        return self.sprite:getRelativeFrame()
    end
end

function SpritedActor:getAnimationDuration()
    if (self.sprite ~= nil) then
        core.debug:warning("actor", "the function SpritedActor:getAnimationDuration is deprecated, prefer SpritedActor.sprite:getAnimationDuration()")
        return self.sprite:getAnimationDuration()
    end
end

function SpritedActor:drawSprite(x, y, r, sx, sy, ox, oy, kx, ky)
    if (self.sprite ~= nil) then
        self.sprite:draw(x, y, r, sx, sy, ox, oy, kx, ky)
    end
end

return SpritedActor