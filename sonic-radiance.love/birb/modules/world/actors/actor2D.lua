-- actor2D.lua :: the implementation of a 2D actor. It contain every element
-- needed to create your own 2D actors.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]
local Rect = require "birb.classes.2D.rect"

local BaseActor = require "birb.modules.world.actors.mixins.base"
local SpritedActor = require("birb.modules.world.actors.mixins.sprites")
local TimedActor = require("birb.modules.world.actors.mixins.timers")
local InputActor = require("birb.modules.world.actors.mixins.inputs")
local PhysicalActor = require("birb.modules.world.actors.mixins.physics")

local Actor2D = Rect:extend()
Actor2D:implement(BaseActor)
Actor2D:implement(SpritedActor)
Actor2D:implement(TimedActor)
Actor2D:implement(InputActor)
Actor2D:implement(PhysicalActor)

local Hitbox  = require "birb.modules.world.actors.utils.hitbox2D"

-- INIT FUNCTIONS
-- Initialise the actor and its base functions

function Actor2D:new(world, type, x, y, w, h, isSolid)
  Actor2D.super.new(self, x, y, w, h)

  self:init(world, type)
  self:initPhysics(Hitbox, isSolid)
  self:initTimers()
  self:initSprite()
  self:initKeys()
end

function Actor2D:destroy()
  self.world:removeActor(self)
  self.mainHitbox:destroy()
  self.isDestroyed = true
end

-- PHYSICS FUNCTIONS
-- Handle movement and collisions.

function Actor2D:moveToFuturePosition(dt)
  local dx, dy = self:getFuturePosition(dt)
  local _, _, cols, colNumber = self:move(dx, dy)
  return cols, colNumber
end

function Actor2D:changeSpeedToCollisionNormal(normal)
  local xsp, ysp = self.xsp, self.ysp
  local nx, ny = normal.x, normal.y

  if (nx < 0 and xsp > 0) or (nx > 0 and xsp < 0) then
    xsp = -xsp * self.bounceFactor
  end

  if (ny < 0 and ysp > 0) or (ny > 0 and ysp < 0) then
    ysp = -ysp * self.bounceFactor
  end

  self.xsp, self.ysp = xsp, ysp
end

function Actor2D:move(dx, dy)
  local cols, colNumber = {}, 0
  if (self.isDestroyed == false) then
    self.x, self.y, cols, colNumber = self.mainHitbox:checkCollisionAtPoint(dx, dy, self.filter)
    self.mainHitbox:updatePosition()
  end
  return self.x, self.y, cols, colNumber
end

function Actor2D:checkCollisionAtPoint(dx, dy)
  local x, y, cols, colNumber = dx, dy, {}, 0
  if (self.isDestroyed == false) then
    x, y, cols, colNumber = self.mainHitbox:checkCollisionAtPoint(dx, dy, self.filter)
  end
  return self.x, self.y, cols, colNumber
end

-- GRAVITY SYSTEM FUNCTIONS
-- All functions related to gravity

function Actor2D:applyGravity(dt)
  self.ysp = self.ysp + self.grav * dt

  if utils.math.sign(self.ysp) == utils.math.sign(self.grav) then
    self:checkGround( )
  end
end

function Actor2D:checkGround()
  local dx, dy = self.x, self.y + utils.math.sign(self.grav)
  local newx, newy, cols, colNumber = self:checkCollisionAtPoint(dx, dy)

  for i, col in ipairs(cols) do
    if (col.type == "touch") or (col.type == "bounce") or (col.type == "slide") then
      if not (self.grav == 0) then
        if col.normal.y ~= utils.math.sign(self.grav) then self.onGround = true end
      end
    end
  end
end

-- COORDINATE/MOVEMENT FUNCTIONS
-- Handle coordinate

function Actor2D:getViewCenter()
  local x, y = self:getCenter()
  return x, y
end

-- HITBOXES FUNCTIONS
-- Functions related to actor hitboxes

function Actor2D:packForHitbox()
  return {0, 0, self.w, self.h}
end

-- DRAW FUNCTIONS
-- Draw the actors.

function Actor2D:draw()
  self:drawStart()
  local x, y = math.floor(self.x), math.floor(self.y)
  self:drawSprite(x, y)
  self:drawEnd()
end

return Actor2D
