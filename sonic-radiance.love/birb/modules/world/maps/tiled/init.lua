local Parent = require "birb.modules.world.maps.parent"
local TiledMap = Parent:extend()
local StiWrapper = require "birb.modules.world.maps.tiled.stiwrapper"

local TiledMixins = require "birb.modules.world.maps.tiled.mixins"

TiledMap:implement(TiledMixins)

function TiledMap:new(world, mapfile)
    self.wrapper = StiWrapper(self, mapfile, 0, 0)
    TiledMap.super.new(self, world)
    self.supportTileCollision = true

    self:setBackgroundColorFromTable(self.wrapper.sti.backgroundcolor)
    self.mapname = self:getMapName(mapfile)
end

function TiledMap:getMapName(mapfile)
    local filepath = utils.string.split(mapfile, "/")
    local filenameMap = utils.string.split(filepath[#filepath], ".")
    return filepath[#filepath - 1] .. "." .. filenameMap[1]
end

function TiledMap:getDimensions()
    return self.wrapper:getDimensions()
end

function TiledMap:loadObjects()
   --self.wrapper:loadObjects()
end

-- TILE FUNCTIONS
-- Get tiles

function TiledMap:haveTileTypeInRect(x, y, w, h, type)
    return self.wrapper:haveTileTypeInRect(x, y, w, h, type)
end

-- UPDATE FUNCTION
-- Update or modify the map

function TiledMap:resize(w, h)
    self.wrapper:resize(w, h)
end

function TiledMap:update(dt)
    self.wrapper:update(dt)
end

-- DRAW FUNCTIONS
-- Handle drawing the wrapper


function TiledMap:drawUpperLayers()
    self.wrapper:drawUpperLayers()
end

function TiledMap:drawLowerLayers()
    self.wrapper:drawLowerLayers()
end

return TiledMap
