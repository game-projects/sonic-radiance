local Rect = require "birb.classes.2D.rect"
local GuiElement = Rect:extend()

local TweenManager = require "birb.classes.time"

function GuiElement:new(name, x, y, w, h)
    GuiElement.super.new(self, x, y, w, h)
    self.name = name

    self.isVisible = true
    self.screen = nil

    self.depth = 10

    self.tweens = TweenManager(self)
    self:initWrapper()

    self:register()
end

function GuiElement:initWrapper()
    self.scene = core.scenemanager.nextScene or core.scenemanager.currentScene
    self.gui = self.scene.gui
    self.assets = self.scene.assets
end

function GuiElement:setKeyPressAction(func)
    self.func = func
end

function GuiElement:register()
    self.creationId = self.gui:addElement(self.name, self)
end

function GuiElement:destroy()
    self.gui:deleteElement(self.name)
    if (self.screen ~= nil) then
        self.screen:deleteElement(self.name)
    end
end

-- VISIBILITY/ACTIVITY
-- Handle drawing and how we interact with

function GuiElement:setDepth(depth)
    self.depth = depth or 0
end

function GuiElement:getVisibility()
    if (self.screen ~= nil) then
        return (self.isVisible and self.screen.isVisible)
    else
        return self.isVisible
    end
end

function GuiElement:setVisibility(visibility)
    self.isVisible = visibility
end

function GuiElement:getFocus(widgetId, page)
    self.gui:setFocus(self.name, widgetId, page)
end

function GuiElement:haveFocus()
    return (self.gui.focusedElement == self.name)
end

function GuiElement:looseFocus()
    if (self:haveFocus()) then
        self.gui:removeFocus()
    end
end

function GuiElement:setSubFocus()
    -- Useless for basic element
end

function GuiElement:isTransforming()
    return self.tweens:haveTween()
end

-- UPDATE FUNCTIONS
-- Update the menu every game update

-- External update function
function GuiElement:updateElement(dt)
    self:update(dt)
    self.tweens:update(dt)
end

-- Internal update function
function GuiElement:update(dt)
    -- Cette fonction ne contient rien par défaut
end

-- TWEEN FUNCTIONS
-- Handle tweening

function GuiElement:newTween(start, duration, target, easing)
    self.tweens:newTween(start, duration, target, easing)
end

function GuiElement:newMovement(start, duration, x, y, easing)
    self:newTween(start, duration, {x = x, y = y}, easing)
end

function GuiElement:newSwitch(start, bools)
    self.tweens:newSwitch(start, bools)
end

function GuiElement:newFunc(start, name, func)
    self.tweens:newFunc(start, name, func)
end

function GuiElement:delayFocus(start)
    self.tweens:newFunc(start, "focus", function ()
        self:getFocus()
    end)
end

-- DRAW FUNCTIONS
-- Draw the menu and its content

function GuiElement:redraw()

end

function GuiElement:drawElement()
    self:draw()
end

function GuiElement:draw()
    -- nothing here
end

-- KEYBOARD FUNCTIONS
-- Handle key press

function GuiElement:keypressed(key)
    if (self.func ~= nil) then
        self.func(key)
    end
end

-- MOUSE FUNCTIONS
-- Handle pointers (clic/touch)

function GuiElement:mousemoved(x, y)
    -- Cette fonction ne contient rien par défaut
end

function GuiElement:mousepressed(x, y, button, istouch)
    -- Cette fonction ne contient rien par défaut
end

return GuiElement