local Parent = require "birb.modules.gui.elements.parent"
local DrawableElement = Parent:extend()

function DrawableElement:new(name, drawable, x, y,r,sx,sy,ox,oy, opacity)
    self.drawable = drawable

    local w, h = self.drawable:getDimensions()
    DrawableElement.super.new(self, name, x, y, w, h)
    self.r = r or 0
    self.sx, self.sy = sx or 1, sy or 1
    self.ox, self.oy = self:parseOrigin(ox, w), self:parseOrigin(oy, h)
    self.opacity = opacity or 1
end

function DrawableElement:parseOrigin(origin, size)
    if (origin == "center") then
        return size/2
    elseif (origin == "end") then
        return size
    else
        return origin or 0
    end
end

function DrawableElement:draw()
    love.graphics.setColor(1, 1, 1, self.opacity)
    love.graphics.draw(self.drawable, self.x,self.y,self.r,self.sx,self.sy,self.ox,self.oy)
    love.graphics.setColor(1, 1, 1, 1)
end

return DrawableElement