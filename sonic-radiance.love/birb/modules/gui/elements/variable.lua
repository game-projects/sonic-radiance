local Parent = require "birb.modules.gui.elements.text"
local VariableElement = Parent:extend()

function VariableElement:new(name, fontName, object, varName, x, y, align)
    VariableElement.super.new(self, name, fontName, "", x, y, align)
    self.object = object
    self.variable = varName
end

function VariableElement:getText()
    return self.object[self.variable]
end

return VariableElement