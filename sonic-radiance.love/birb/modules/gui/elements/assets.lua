local Parent = require "birb.modules.gui.elements.drawable"
local AssetElement = Parent:extend()

function AssetElement:new(name, assetType, assetName, x, y,r,sx,sy,ox,oy, opacity)
    self:initWrapper()
    local asset = self.assets[assetType][assetName]
    assert(asset ~= nil, assetName .. ' (' .. assetType .. ") doesn't exist")

    AssetElement.super.new(self, name, asset, x, y,r,sx,sy,ox,oy, opacity)
end

function AssetElement:draw()
    self.drawable:draw(self.x,self.y,self.r,self.sx,self.sy,self.ox,self.oy)
end

return AssetElement