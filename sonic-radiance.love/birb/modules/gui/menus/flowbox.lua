-- flowbox :: flexible box menu, that handle in grid the widgets

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.flowbox$', '') .. "."

local Menu = require(cwd .. "parent")
local FlowBox = Menu:extend()

local View2D = require(cwd .. "views.view2D")

-- INIT FUNCTIONS
-- Initialize and configure the flowbox

function FlowBox:new(name, x, y, w, h, slots_hor, slots_vert)
  self.view = View2D(slots_hor, slots_vert)
  FlowBox.super.new(self, name, x, y, w, h)
  self:setRealSize()
end

function FlowBox:setRealSize()
  -- On fait en sorte que la hauteur et la largeur
  -- soit un multiple du nombre de slot et de leur dimensions
  self:updateWidgetSize()
  self.w = self.view.colNumber * self.widgetSize.w
  self.h = self.view.lineNumber * self.widgetSize.h
end

function FlowBox:resetView()
  self.view:reset()
end

-- UPDATE FUNCTIONS
-- Update the menu and its view

function FlowBox:updateWidgetSize()
  self.widgetSize.h = math.floor( self.h / self.view.lineNumber )
  self.widgetSize.w = math.floor( self.w / self.view.colNumber )
end

function FlowBox:update(dt)
  self.view:updateFirstSlot(self.widget:getSelected())
end

-- CURSOR FUNCTIONS
-- Handle the cursor in a 2D menu

function FlowBox:moveCursor2D(new_col, new_line)
  local lastcol, lastline = self.view:getCoord(self.widget:lenght())

  new_line = utils.math.wrapAndLimit(new_line, 0, lastline)

  if (new_line == lastline) then
    new_col = utils.math.wrapAndLimit(new_col, 0, lastcol)
  else
    new_col = utils.math.wrapAndLimit(new_col, 0, (self.view.colNumber - 1))
  end

  self.widget:moveCursor((new_line * self.view.colNumber) + new_col + 1)
end

-- KEYS FUNCTIONS
-- Handle the keyboard/controller inputs

function FlowBox:moveByKeys(key)
  local col, line = self.view:getCoord(self.widget:getSelected())
  if key == 'left' then
    self:moveCursor2D(col - 1, line)
    self:playNavigationSound()
  end

  if key == 'right' then
    self:moveCursor2D(col + 1, line)
    self:playNavigationSound()
  end

  if key == 'up' then
    self:moveCursor2D(col, line - 1)
    self:playNavigationSound()
  end

  if key == 'down' then
    self:moveCursor2D(col, line + 1)
    self:playNavigationSound()
  end
end

-- POSITION FUNCTIONS
-- Get a widget by a position.

function FlowBox:getWidgetAtPoint(x, y)
  local col = math.floor(x / self.widgetSize.w)
  local line = math.floor(y / self.widgetSize.h)
  return self.view:getFromCoord(col, line)
end

-- DRAW FUNCTIONS
-- Draw the menu and its content

function FlowBox:drawTexture()
  self.view:updateFirstSlot(self.widget:getSelected())
  local widgety = self.canvas.padding
  local widgetx = self.canvas.padding

  local listWidget = self.widget:getList(self.view.firstSlot, self.view.slotNumber)

  for _, widget in ipairs(listWidget) do
    widget:drawWidget(widgetx, widgety, self.w, self.widgetSize.h)

    -- On calcule la position du prochain widget
    widgetx = widgetx + self.widgetSize.w
      if widgetx >= (self.x + self.w) then
        widgetx = self.x
        widgety = widgety + self.widgetSize.h
      end
  end
end

function FlowBox:getGraphicalCursorPosition()
  self.view:updateFirstSlot(self.widget:getSelected())
  local _, beginline = self.view:getCoord(self.view.firstSlot)
  local w, h  = self:getWidgetSize()
  local col, line = self.view:getCoord(self.widget:getSelected())
  local x     = (col) * h
  local y     = (line - beginline) * h
  return x, y, w, h
end

return FlowBox
