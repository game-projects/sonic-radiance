-- hlistbox : add an horizontal list of widgets.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.hlistbox$', '') .. "."

local Menu = require(cwd .. "parent")
local HListBox = Menu:extend()

local View1D = require(cwd .. "views.view1D")

-- INIT FUNCTIONS
-- Initialize and configure functions.

function HListBox:new(name, x, y, w, h, slotNumber)
  self.view = View1D(slotNumber)
  HListBox.super.new(self, name, x, y, w, h)
  self.w = slotNumber * self.widgetSize.w -- On fait en sorte que la hauteur
  -- soit un multiple du nombre de slot et de leur hauteur
end

-- UPDATE FUNCTIONS
-- Update the menu every step.

function HListBox:updateWidgetSize()
  self.widgetSize.h = self.h
  self.widgetSize.w = math.floor( self.w / self.view.slotNumber )
end

function HListBox:update(dt)
  self.view:updateFirstSlot(self.widget:getSelected())
end

function HListBox:resetView()
  self.view:reset()
end

-- KEYBOARD FUNCTIONS
-- Handle key check.

function HListBox:moveByKeys(key, code)
  if key == 'left' then
    self.widget:moveCursor(-1)
    self.canvas.needRedraw = true
    self:playNavigationSound()
  end

  if key == 'right' then
    self.widget:moveCursor(1)
    self.canvas.needRedraw = true
    self:playNavigationSound()
  end
end

-- POSITION FUNCTIONS
-- Get a widget by a position.

function HListBox:getWidgetAtPoint(x, y)
  return (self.view.firstSlot + math.floor(x / self.widgetSize.w))
end

-- DRAW FUNCTIONS
-- Draw the menu and its content

function HListBox:drawTexture()
  self.view:updateFirstSlot(self.widget:getSelected())

  local widgetx = self.canvas.padding

  local listWidget = self.widget:getList(self.view.firstSlot, self.view.slotNumber)

  for _, widget in ipairs(listWidget) do
    widget:drawWidget(widgetx, self.canvas.padding, self.widgetSize.w, self.h)
    widgetx = widgetx + self.widgetSize.w
  end
end

function HListBox:getGraphicalCursorPosition()
  self.view:updateFirstSlot(self.widget:getSelected())
  local w, h  = self:getWidgetSize()
  local x     = (self.widget:getSelected() - self.view.firstSlot) * w

  return x, 0, w, h
end

return HListBox
