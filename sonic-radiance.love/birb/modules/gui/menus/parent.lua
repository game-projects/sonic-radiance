-- parent.lua : The parent of the functions.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local GuiElement = require "birb.modules.gui.elements.canvas"

local Menu = GuiElement:extend()
local MenuModel = require "birb.modules.gui.menus.model"

local menuUtils = require "birb.modules.gui.utils"

-- INIT FUNCTIONS
-- Initialize and configure functions.

function Menu:new(name, x, y, w, h)
  Menu.super.new(self, name, x, y, w, h)

  --TODO: remove this
  self.menusystem = self.gui 

  self.widget = MenuModel()
  self.widgetSize = {}
  self:updateWidgetSize()

  self:initCanvas()
  self.cancelFunc = nil
  self.canvas.dual = true
end

-- FUNCTIONS FUNCTIONS
-- Add functions to the menu system

function Menu:addCancelAction(func)
  self.cancelFunc = func
end

function Menu:addHoverAction(func)
  self.widget:addHoverAction(func)
end

-- INTERACTION FUNCTIONS
-- Keyboard and mouse

function Menu:keypressed(key)
  self:moveByKeys(key)
  self:actionAndCancel(key)
end

function Menu:mousemoved(x, y)
  local widgetID = self:getWidgetAtPoint(x, y)

  if (widgetID ~= nil) then
    if self.widget:trySelectWidget(widgetID) then
      self:getFocus()
    end
  end
end

function Menu:mousepressed(x, y, button, istouch)
  local widgetID = self:getWidgetAtPoint(x, y)

  if (widgetID ~= nil) then
    if self.widget:trySelectWidget(widgetID) then
      self:getFocus()
      self.widget:action(widgetID, "pointer")
    end
  end
end

function Menu:moveByKeys(key)
  -- Cette fonction ne contient rien par défaut
end

function Menu:actionAndCancel(key)
  if key == "A" then
    self.widget:selectedAction()
    self.canvas.needRedraw = true
  end

  if key == "B" then
    self:cancelAction()
    self.canvas.needRedraw = true
  end
end

-- PAGE FUNCTIONS
-- Wrapper around pages functions from the model

function Menu:addPage(pageName)
  self.widget:addPage(pageName)
end

function Menu:addSubmenu(pageName, parent)
  self.widget:addSubmenu(pageName, parent)
end

function Menu:removePage(pageName)
  self.widget:removePage(pageName)
end

function Menu:pageExists(pageName)
  return self.widget:pageExists(pageName)
end

function Menu:getPage(pageName)
  self.widget:getPage(pageName)
end

function Menu:getCurrentPageName()
  return self.widget:getCurrentPageName()
end

function Menu:switch(pageName)
  self.widget:switch(pageName)
  self:resetView()
  self.canvas.needRedraw = true
end

function Menu:back()
  self.widget:back()
  self:resetView()
  self.canvas.needRedraw = true
end

function GuiElement:setSubFocus(widgetId, pageName)
  if (pageName ~= nil) then
    self.widget:switch(pageName)
  end
  self.widget:trySelectWidget(widgetId)
end

-- ACTION FUNCTIONS
-- Send actions to the widgets

function Menu:cancelAction()
  if (self.cancelFunc ~= nil) then
    self.cancelFunc(self)
  else
    self.widget:cancelAction()
  end
end

function Menu:clear()
  self.widget:clear()
end

function Menu:resize(x,y,w,h)
  self:set(x,y,w,h)

  self:updateWidgetSize()
end

-- UPDATE FUNCTIONS

function Menu:updateElement(dt)
  self.widget:update(dt)
  Menu.super.updateElement(self, dt)
end

-- DRAW FUNCTIONS
-- Draw the menu and its content

function Menu:drawElement()
  self:draw()
end

function Menu:drawFinalTexture()
  if (self:haveFocus()) then
    local x, y, w, h = self:getGraphicalCursorPosition()
    self:drawGraphicalCursor(self.canvas.padding + x, self.canvas.padding + y, w, h)
  end
end

function Menu:drawGraphicalCursor(x, y, w, h)
  menuUtils.drawCursor(x, y, w, h)
end

function Menu:drawCanvas()

end

function Menu:drawWidgetBackground(x, y, w, h)

end

function Menu:redraw()
  self.widget:redraw()
  Menu.super.redraw(self)
end

-- WIDGET FUNCTIONS
-- Handle widgets of the functions

function Menu:addWidget(newwidget)
  self.widget:addWidget(newwidget)
  self.canvas.needRedraw = true
end

function Menu:setCancelWidget(id)
  self.widget:setCancelWidget(id)
end

function Menu:updateWidgetSize()
  self.widgetSize.h = 0
  self.widgetSize.w = 0
end

function Menu:getWidgetSize(id)
  return self.widgetSize.w, self.widgetSize.h
end

function Menu:getWidgetNumber()
  return self.widget:lenght()
end

function Menu:getWidgetAtPoint(x, y)

end

-- CURSOR FUNCTIONS
-- Set or move the cursor of the menu

function Menu:setCursor(cursorid)
  self.widget:setCursor(cursorid)
  self.canvas.needRedraw = true
end

function Menu:moveCursor(new_selected)
  self.widget:moveCursorAbsolute(new_selected)
  self.canvas.needRedraw = true
end

-- SOUND FUNCTION
-- Handle SFX

function Menu:playNavigationSound()
  self:playSFX("navigate")
end

function Menu:playSFX(name)
  self.gui:playSFX(name)
end

-- VIEW FUNCTIONS
-- Handle the view of the menu

function Menu:resetView()
  -- ne sert à rien ici, c'est juste pour éviter des crash
end

function Menu:updateView()
  -- ne sert à rien ici, c'est juste pour éviter des crash
end

function Menu:moveView()
  -- ne sert à rien ici, c'est juste pour éviter des crash
end

return Menu
