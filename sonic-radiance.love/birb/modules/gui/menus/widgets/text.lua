-- widgets/text.lua :: basic text widget object

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]
local BaseWidget = require "birb.modules.gui.menus.widgets.base"
local TextWidget = BaseWidget:extend()

-- TEXT WIDGET
-- Simple text widget

function TextWidget:new(menuName, font, label, position, padding)
    TextWidget.super.new(self, menuName)
    self.font = font
    self.labels = {}
    self.padding = padding or 0

    -- We add the first label
    local position = position or "center"
    self:addLabel(label, position)
    self:setColor(1, 1, 1)
end

function TextWidget:addLabel(label, position)
    local complexLabel = {}
    assert(label ~= nil, "Label can't be nil")
    complexLabel.label = label
    complexLabel.position = position or "left"
    table.insert(self.labels, complexLabel)
end

function TextWidget:replaceLabel(id, newLabel)
    self.labels[id].label = newLabel
end

function TextWidget:setColor(r, g, b)
    self.color = {}
    self.color.r = r
    self.color.g = g
    self.color.b = b
end

function TextWidget:setSelectedColor(r, g, b)
    self.selectedColor = {}
    self.selectedColor.r = r
    self.selectedColor.g = g
    self.selectedColor.b = b
end

function TextWidget:getFont()
    return self.assets:getFont(self.font)
end

function TextWidget:getSelectedColor()
    if (self.selectedColor ~= nil) then
        return self.selectedColor.r, self.selectedColor.g, self.selectedColor.b
    else
        return self:getColor()
    end
end

function TextWidget:getColor()
    return self.color.r, self.color.g, self.color.b
end

function TextWidget:getPadding()
    return self.padding
end

function TextWidget:getPaddingLeft()
    return self.paddingLeft or self:getPadding()
end

function TextWidget:getPaddingRight()
    return self.paddingRight or self:getPadding()
end

function TextWidget:drawCanvas()
    local w, h
    local font = self:getFont()
    h = math.floor(self.height / 2) - (font:getHeight() / 2)

    for _, complexLabel in pairs(self.labels) do
        if (complexLabel.position == "center") then
            w = math.floor(self.width / 2)
        elseif (complexLabel.position == "left") then
            w = self:getPaddingLeft()
        elseif (complexLabel.position == "right") then
            w = math.floor(self.width - self:getPaddingRight())
        else
            error("Position " .. complexLabel.position .. " is unknown for label " .. complexLabel.label)
        end
        font:draw(complexLabel.label, w, h, -1, complexLabel.position)
    end
end

function TextWidget:draw(x, y, w, h)
    local r, g, b = self:getColor()
    love.graphics.setColor(r, g, b, 1)
    if self.canvas.texture ~= nil then
        love.graphics.draw(self.canvas.texture, x, y)
    end
    utils.graphics.resetColor()
end

function TextWidget:drawSelected(x, y, w, h)
    local r, g, b = self:getSelectedColor()
    love.graphics.setColor(r, g, b, 1)
    if self.canvas.texture ~= nil then
        love.graphics.draw(self.canvas.texture, x, y)
    end
    utils.graphics.resetColor()
end

return TextWidget
