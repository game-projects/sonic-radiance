-- widgets/base.lua :: basic widget object

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]
local BaseWidget = Object:extend()

-- INIT FUNCTIONS
-- Initialize and configure the widget

function BaseWidget:new(menuName)
    self:initWrapper()
    self.menu = self:getMenuByName(menuName)

    self.destroyed = false
    self.selectable = false
    self.selection_margin = 0
    self.margin = 2

    self.canvas = {}
    self.canvas.texture = nil
    self.canvas.needRedraw = true
    self.canvas.isAnimated = false

    self.order = 0
    self:register()
    self.func = nil
    self.type = "select"
end

function BaseWidget:initWrapper()
    self.scene = core.scenemanager.nextScene or core.scenemanager.currentScene
    self.gui = self.scene.gui
    self.assets = self.scene.assets
end

function BaseWidget:setFunc(func)
    self.func = func
end

function BaseWidget:getMenuByName(name)
    assert(name ~= nil, "Name cant be nil")
    return self.gui.elements[name]
end

function BaseWidget:getScene()
    return core.scenemanager.nextScene or core.scenemanager.currentScene
end

function BaseWidget:getAssets()
    local scene = core.scenemanager.nextScene or core.scenemanager.currentScene
    return scene.assets
end

function BaseWidget:register()
    self.creationID = self.menu:getWidgetNumber()
    self.menu:addWidget(self)
end

function BaseWidget:redraw()
    if (self.canvas.needRedraw or self.canvas.isAnimated) then
        self:generateTexture()
    end
end

function BaseWidget:generateTexture()
    self.width, self.height = self.menu:getWidgetSize(self.id)

    local canvas = love.graphics.newCanvas(self.width, self.height)
    love.graphics.setCanvas(canvas)

    self:drawCanvas()
    self.canvas.needRedraw = false

    love.graphics.setCanvas()
    local imageData = canvas:newImageData()
    self.canvas.texture = love.graphics.newImage(imageData)
    canvas:release()
    imageData:release()
end

function BaseWidget:invalidateCanvas()
    self.canvas.needRedraw = true
end

function BaseWidget:drawCanvas()
    self.r = love.math.random(128) / 256
    self.g = love.math.random(128) / 256
    self.b = love.math.random(128) / 256

    love.graphics.setColor(self.r, self.g, self.b, 70)
    love.graphics.rectangle("fill", 0, 0, self.width, self.height)
    love.graphics.setColor(self.r, self.g, self.b)
    love.graphics.rectangle("line", 0, 0, self.width, self.height)
    utils.graphics.resetColor()
end

function BaseWidget:selectAction()
    -- Do nothing
end

-- DRAW WIDGETS
-- Draw the widget

function BaseWidget:drawWidget(x, y, w, h)
    if (self:haveFocus()) then
        self:drawSelected(x, y, w, h)
    else
        self:draw(x, y, w, h)
    end
end

function BaseWidget:draw(x, y, w, h)
    if self.canvas.texture ~= nil then
        love.graphics.draw(self.canvas.texture, x, y)
    end
end

function BaseWidget:drawSelected(x, y, w, h)
    self:draw(x, y, w, h)
end

-- UPDATE FUNCTIONS
-- Update the widget

function BaseWidget:update(dt)
    -- N/A
end

-- FOCUS FUNCTIONS
-- Detect if the widget have focus

function BaseWidget:haveFocus()
    return (self.menu:haveFocus() and self.menu.widget:getSelected() == self.id)
end

-- ACTION FUNCTION
-- Functions to handle actions and selection.

function BaseWidget:playSFX()
    self.menu:playSFX(self.type)
end

function BaseWidget:action(source)
    if (self.func ~= nil) then
        self.func(self)
    end
end

function BaseWidget:destroy()
    self.destroyed = true
end

return BaseWidget
