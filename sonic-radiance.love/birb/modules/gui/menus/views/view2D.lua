-- view2D.lua : A basic 2D view for menus.
-- Must be used as a subobject of a Menu

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]
local View1D = require "birb.modules.gui.menus.views.view1D"
local View2D = View1D:extend()

function View2D:new(colNumber, lineNumber)
  self.colNumber = colNumber
  self.lineNumber = lineNumber
  View2D.super.new(self, (colNumber * lineNumber))
end

function View2D:updateFirstSlot(widgetID)
  local _, line = self:getCoord(widgetID)
  local _, beginline = self:getCoord(self.firstSlot)

  beginline = math.min(line, math.max(0, beginline, line - (self.lineNumber - 1)))
  self.view.firstSlot = (beginline * self.view.colNumber) + 1
end

-- INFO FUNCTIONS
-- Get informations

function View2D:getCoord(x)
  -- On simplifie les calcul en prenant 0 comme départ
  local x = x - 1

  local line, col
  line = math.floor(x / self.view.colNumber)
  col = x - (line * self.view.colNumber)

  return col, line
end

function View2D:getFromCoord(col, line)
  local _, beginline = self.view:getCoord(self.view.firstSlot)
  local line = beginline + line
  return (line * self.view.colNumber) + col + 1
end

return View2D
