-- listbox : a text-based menu.
-- It allow to easily handle a listbox with text-widget and automatically add a widget
-- for submenu.
-- It also make the menu manage the font and the selection color, for a simpler handling

--[[
  Copyright © 2020 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local ListBox = require "birb.modules.gui.menus.listbox"
local TextMenu = ListBox:extend()

TextMenu.baseWidgets = require "birb.modules.gui.textmenu.widgets"

local BASE_PADDING = 8

function TextMenu:new(name, font, x, y, w, slotNumber, padding, lineSize)
    lineSize = lineSize or 1
    self:setFont(font)
    self.lineHeight = self.font:getHeight() * lineSize
    self.name = name

    self.padding = padding or BASE_PADDING
    TextMenu.super.new(self, name, x, y, w, (self.lineHeight * slotNumber), slotNumber)
end

function TextMenu:addItem(text, position, func, type, additionnalItems, color, additionnalDatas)
  local widget = TextMenu.baseWidgets.Base(self.name, text, position)
  widget:setFunc(func)
  widget.type = type or "select"
  if (additionnalItems ~= nil) then
    for _, item in ipairs(additionnalItems) do
      widget:addLabel(item[1], item[2])
    end
  end
  if (color ~= nil) then
    widget:setColor(color[1], color[2], color[3])
  end
  if (additionnalDatas ~= nil) then
    widget.datas = additionnalDatas
  end
end

function TextMenu:generateSubmenu(pageName, label, parent, list, func, backWidget)
  self:addSubmenu(pageName, label, parent, backWidget)
  for _, data in ipairs(list) do
    self:addItem(func(data))
  end
end

function TextMenu:setFont(fontName)
  local scene = core.scenemanager.currentScene
  self.font = scene.assets:getFont(fontName)
end

function TextMenu:getFont()
  return self.font
end

function TextMenu:setSelectedColor(r, g, b)
  self.selectedColor = {}
  self.selectedColor.r = r
  self.selectedColor.g = g
  self.selectedColor.b = b
end

function TextMenu:getSelectedColor()
  return self.selectedColor
end

function TextMenu:getPadding()
  return self.padding
end

function TextMenu:getPaddingLeft()
  return self.paddingLeft or self:getPadding()
end

function TextMenu:getPaddingRight()
  return self.paddingRight or self:getPadding()
end


function TextMenu:addSubmenu(pageName, label, parent, backWidget)
  local label = label or pageName
  local parent = parent or "main"
  self:switch(parent)
  TextMenu.baseWidgets.SubMenu(self.name, pageName, label)
  TextMenu.super.addSubmenu(self, pageName, parent)
  if (backWidget ~= false) then
    TextMenu.baseWidgets.Back(self.name)
    self:setCancelWidget("last")
  end
end

return TextMenu