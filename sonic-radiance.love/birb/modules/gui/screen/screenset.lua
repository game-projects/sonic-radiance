-- screens/screenset :: a set of exclusive screens
-- Useful to handle a complexe menu with several screens

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local ScreenSet = Object:extend()

function ScreenSet:new(owner)
    self.set = {}
    self.defaultScreen = ""
    self.currentScreen = ""
    self.owner = owner
    self.delay = 0
end

function ScreenSet:show(screenName)
    local screenName = screenName
    if (screenName == nil) then
      if self.currentScreen == "" then
        screenName = self.defaultScreen
      else
        return
      end
    end
    if screenName ~= "" then
      self.set[screenName]:showSimple()
    end
end

function ScreenSet:setCurrentScreen(screenName)
  screenName = screenName or self.defaultScreen
  local time = self:hideCurrent() + self.delay
  self.currentScreen = screenName
  return time
end

function ScreenSet:hideCurrent()
  local time = 0
  if (self.currentScreen ~= "") then
    time = self.set[self.currentScreen]:hide()
    self.currentScreen = ""
  end
  return time
end

function ScreenSet:add(screen, setAsDefault)
  self.set[screen.name] = screen
  if (setAsDefault == true or self.defaultScreen == "") then
    self.defaultScreen = screen.name
  end
  screen:setParentSet(self)
end

return ScreenSet