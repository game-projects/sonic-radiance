-- core/debug.lua :: The basic internal debug framework of the birb engine.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local DebugSystem = Object:extend()

local lovebird

local Levels = enum {
  "ERROR",
  "WARNING",
  "INFO",
  "DEBUG"
}

function DebugSystem:new(controller, debugLevel)
  self.controller = controller

  self.debugLevel = debugLevel or Levels.WARNING
  self.active = (self.debugLevel == Levels.DEBUG)
  if (self.active) then
    lovebird = require "birb.libs.lovebird"
    lovebird.update()
  end
end

function DebugSystem:update(dt)
  if (self.active and lovebird ~= nil) then
    lovebird.update(dt)
  end
end

-- PRINT FUNCTIONS
-- Print and log debug string

function DebugSystem:debug(context, string)
  self:log(Levels.DEBUG, context, string)
end

function DebugSystem:print(context, string)
  self:log(Levels.INFO, context, string)
end

function DebugSystem:warning(context, string)
  self:log(Levels.WARNING, context, string)
end

function DebugSystem:error(context, string)
  self:log(Levels.ERROR, context, string)
  error(self:getMessage(": ", context, string))
end

function DebugSystem:log(level, context, string)
  if (self.debugLevel >= level) then
    print(self:getLogLine(Levels[level], context, string))
  end
end

function DebugSystem:getLogLine(type, context, string)
  local head = self.controller:getIdentity(false) .. "|" .. os.date("%x-%X") .. "|" .. type .. "|"
  return head .. self:getMessage("|", context, string)
end

function DebugSystem:getMessage(separator, string1, string2)
  if (string2 == nil) then
    return string1
  else
    return string1 .. separator .. string2
  end
end


return DebugSystem
