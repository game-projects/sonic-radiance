-- scene.lua :: a basic scene management system, that work by sending the different
-- core functions to the scene, normally without the scene itself having to manage
-- them.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local SceneManager  = Object:extend()

-- INIT FUNCTIONS
-- Initialize and configure the scene manager

function SceneManager:new(controller)
  self.controller   = controller
  self.currentScene = nil
  self.nextScene = nil

  self.storage      = {}
end

function SceneManager:setScene(scene)
  --self.currentScene = nil
  self.nextScene = scene
end

function SceneManager:haveStoredScene(name)
  return (self.storage[name] ~= nil)
end

function SceneManager:storeCurrentScene(name)
  self.storage[name] = self.currentScene
end

function SceneManager:setStoredScene(name)
  local storedScene = self.storage[name]
  if storedScene ~= nil then
    self.currentScene   = storedScene
    self.storage[name]  = nil
    collectgarbage()
    self.currentScene:restored()
  end
end

function SceneManager:clearStorage()
  self.storage = {}
end

function SceneManager:clearScene()
  self.currentScene = nil
end

-- UPDATE FUNCTIONS
-- Update the current scene and its subobjects

function SceneManager:update(dt)
  if (self.nextScene ~= nil) then
    self.currentScene = self.nextScene
    self.nextScene = nil
    collectgarbage()
  end

  if (self.currentScene ~= nil) then
    self.currentScene:updateScene(dt)
  end
end

-- MOUSE FUNCTIONS
-- Send pointer data to the scene

function SceneManager:mousemoved(x, y, dx, dy)
  if (self.currentScene ~= nil) then
    self.currentScene.mouse.x,
    self.currentScene.mouse.y = x, y
    self.currentScene:mousemoved(x, y, dx, dy)
  end
end

function SceneManager:mousepressed( x, y, button, istouch )
  if (self.currentScene ~= nil) then
    self.currentScene:mousepressed( x, y, button, istouch )
  end
end

-- KEYBOARD FUNCTIONS
-- Add send keys functions to the scene

function SceneManager:keypressed( key, scancode, isrepeat )
  if (self.currentScene ~= nil) then
    self.currentScene:keypressed( key, scancode, isrepeat )
  end
end

function SceneManager:keyreleased( key )
  if (self.currentScene ~= nil) then
    self.currentScene:keyreleased( key )
  end
end

-- DRAW FUNCTIONS
-- Draw the current scene

function SceneManager:draw()
  self.controller.screen:apply()
  if (self.currentScene ~= nil) then
    self.currentScene:drawScene()
  end
  self.controller.screen:cease()
end

function SceneManager:redraw()
  self.currentScene:redraw()
end

return SceneManager
