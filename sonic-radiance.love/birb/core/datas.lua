-- datas.lua :: The main file of the core system, an object full of subsystem
-- loaded by the game to handle the main functions (like screen, translation,
-- inputs…)

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local DataManager = Object:extend()
local DataPack = require "birb.classes.datapack"
local Parser = require "birb.classes.parser"
local index = require "datas.gamedata.index"

function DataManager:new(core)
    self.core = core
    self:loadDatas()
    self:loadParsers()
end

function DataManager:loadDatas()
    self.datapacks = {}
    for key, datas in pairs(index.datapacks) do
        self.core.debug:debug("datamanager", "loading data for " .. key)
        self.datapacks[key] = DataPack(datas[1], datas[2], datas[3], datas[4], datas[5])
    end
end

function DataManager:get(datapack, name)
    return self.datapacks[datapack]:get(name)
end

function DataManager:exists(datapack, name)
    return self.datapacks[datapack]:dataExists(name)
end

function DataManager:getFromCategory(datapack, category)
    return self.datapacks[datapack]:getFromCategory(category)
end

function DataManager:getCategories(datapack)
    return self.datapacks[datapack].categories
end

-- Load parsers

function DataManager:loadParsers()
    self.parsers = {}
    local items = love.filesystem.getDirectoryItems("datas/parsers")
    for i, item in ipairs(items) do
        local filename = utils.datas.luaFileToModule(item)
        local data = require("datas.parsers." .. filename)

        self.core.debug:debug("datamanager", "creating parser for " .. filename)
        self.parsers[filename] = Parser(data.headings, data.argumentLists, data.argumentWrapper, 0)
    end
end

function DataManager:parse(parser, data)
    return self.parsers[parser]:parse(data)
end


return DataManager