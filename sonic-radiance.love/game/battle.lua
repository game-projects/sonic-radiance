local CoreCBS = Object:extend()

local defTransitions = require "birb.modules.transitions"
local radTransitions = require "game.modules.transitions"

local DEFAULTX = 424/2
local DEFAULTY = 240/2

function CoreCBS:new()
    self.lastx, self.lasty = DEFAULTX, DEFAULTY
end

function CoreCBS:startBattle(category, name, x, y)
    local data = core.datas:get("battles", name)
    self.lastx, self.lasty = x or DEFAULTX, y or DEFAULTY
    core.screen:startTransition(radTransitions.eggman, radTransitions.borders, function() scenes.cbs(data) end, x, y)
end

function CoreCBS:endBattle(isFleeing)
    local transitionEnd = radTransitions.sonic
    if (isFleeing) then
        transitionEnd = defTransitions.circle;
    end
    core.screen:startTransition(radTransitions.borders, transitionEnd,
        function()
            if (core.scenemanager:haveStoredScene("afterBattle")) then
                core.scenemanager:setStoredScene("afterBattle")
            else
                scenes.menus.main()
            end
        end, self.lastx, self.lasty)
    self.lastx, self.lasty = DEFAULTX, DEFAULTY
end



return CoreCBS