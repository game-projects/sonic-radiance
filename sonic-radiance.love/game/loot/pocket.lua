local Serializable = require "birb.classes.serializable"
local Pocket = Serializable:extend()

function Pocket:new(pocketdata)
  self.name = pocketdata.name
  self.fullname = pocketdata.fullname
  self.inBattle = pocketdata.inBattle
  self.description = pocketdata.description
  self.isEquipement = pocketdata.isEquipement
  self.list = {}
  Pocket.super.new(self, {"list"})
end

function Pocket:addItem(item, number)
  local success = false

  for i,itemData in ipairs(self.list) do
    if (itemData.name == item) then
      itemData.number = itemData.number + number
      success = true
    end
  end

  if (not success) then
    local itemData = {}
    itemData.name = item
    itemData.number = number
    table.insert(self.list, itemData)
  end
end

function Pocket:removeItem(item, number)
  for i,itemData in ipairs(self.list) do
    if (itemData.name == item) then
      if (itemData.number > number) then
        itemData.number = itemData.number - number
      else
        table.remove(self.list, i)
      end
    end
  end
end

function Pocket:getItem(name)
  for i, itemData in ipairs(self.list) do
    if (itemData.name == name) then
      return itemData
    end
  end
  return nil
end

function Pocket:getItemNumber(name)
  local item = self:getItem(name)
  if (item == nil) then
    return 0
  else
    return item.number
  end
end

return Pocket