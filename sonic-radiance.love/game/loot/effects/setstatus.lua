local ParentEffect = require "game.loot.effects.parent"
local StatusEffect = ParentEffect:extend()

function StatusEffect:new(effect, character, duration)
    self.effect = effect
    self.character = character
    self.duration = duration or -1
end

function StatusEffect:applyEffect()
    if (self.effect.set) then
        self:addStatut()
    else
        self:removeStatut()
    end
end

function StatusEffect:addStatut()
    self.character:addStatut(self.effect.status, self.duration)
end

function StatusEffect:removeStatut()
    self.character:removeStatut(self.effect.status)
end

function StatusEffect:getText()
    local returnString = ""
    if (self.effect.set) then
        returnString = returnString .. "Give "
    else
        returnString = returnString .. "Remove "
    end

    if (self.effect.status == "allNegative") then
        returnString = returnString .. "all negative effects"
    else
        returnString = returnString .. self.effect.status .. " " .. "effect"
    end

    return returnString
end

return StatusEffect