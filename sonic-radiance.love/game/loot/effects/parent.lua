local EffectParent = Object:extend()

function EffectParent:new(effect, character, duration)
  self.effect = effect
  self.character = character
  self.duration = duration
end

function EffectParent:applyEffect()

end

function EffectParent:getText()
  return "No effect"
end

function EffectParent:battleCallback(fighter)

end

return EffectParent
