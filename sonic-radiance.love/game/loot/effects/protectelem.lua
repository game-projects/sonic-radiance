local ParentEffect = require "game.loot.effects.parent"
local ProtectElemEffect = ParentEffect:extend()

function ProtectElemEffect:new(effect, character)
    self.effect = effect
    self.character = character
end

function ProtectElemEffect:applyEffect()

end

function ProtectElemEffect:getText()
    local returnString = "Protect from " .. self.effect.element

    return returnString
end

return ProtectElemEffect