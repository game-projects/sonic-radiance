local ParentEffect = require "game.loot.effects.parent"
local BlockStatusEffect = ParentEffect:extend()

function BlockStatusEffect:new(effect, character)
    self.effect = effect
    self.character = character
end

function BlockStatusEffect:applyEffect()

end

function BlockStatusEffect:getText()
    return "Block " .. self.effect.blockWhat .. " " .. "effects"
end

return BlockStatusEffect