local ParentEffect = require "game.loot.effects.parent"
local HealEffect = ParentEffect:extend()

function HealEffect:new(effect, character)
    self.effect = effect
    self.character = character
    self.recovered = 0
end

function HealEffect:applyEffect()
    self:autosetHealFactor()
    self:heal(self.recovered)
end

function HealEffect:autosetHealFactor()
    local recovered = 0
    local max = self:getMaxValue()
    if (self.character:isAlive()) then
        if (self.effect.computationMode == "percent") then
            recovered = max * (self.effect.value/100)
        else
            recovered = self.effect.value
        end
        recovered = math.min(recovered, max - self:getCurrentValue())
    end
    self.recovered = recovered
end

function HealEffect:getCurrentValue()
    if (self.effect.healType == "hp") then
        return self.character.hp
    elseif (self.effect.healType == "mp") then
        return self.character.pp
    end
end

function HealEffect:getMaxValue()
    local stats = self.character.stats
    if (self.effect.healType == "hp") then
        return stats:get(stats.HPMAX)
    elseif (self.effect.healType == "mp") then
        return stats:get(stats.PPMAX)
    end
end

function HealEffect:heal(value)
    if (self.effect.healType == "hp") then
        self.character:setHP(value, true)
    elseif (self.effect.healType == "mp") then
        self.character:setPP(value, true)
    end
end

function HealEffect:battleCallback(fighter)
    if (self.effect.healType == "hp") then
        fighter.actor:setDamageNumber(self.recovered)
        fighter:updateHP()
    elseif (self.effect.healType == "mp") then
        fighter.actor:setDamageNumber(self.recovered, true)
        fighter:updatePP()
    end
end

function HealEffect:getText()
    local num = self.effect.value
    if (self.effect.computationMode == "percent") then 
        num = num .. "%"
    end
    local type = ""
    if (self.effect.healType == "hp") then
        type = "HP"
    end
    if (self.effect.healType == "mp") then
        type = "MP"
    end
    return "Heal " .. num .. " " .. type;
end

return HealEffect