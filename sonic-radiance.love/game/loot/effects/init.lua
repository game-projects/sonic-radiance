return {
    heal = require "game.loot.effects.heal",
    setStatus = require "game.loot.effects.setstatus",
    protectElement = require "game.loot.effects.protectelem",
    blockStatus = require "game.loot.effects.blockstatus"
}