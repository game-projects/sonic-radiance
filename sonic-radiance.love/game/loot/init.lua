local Serializable = require "birb.classes.serializable"
local LootManager = Serializable:extend()
local Pocket = require "game.loot.pocket"
local EffectManager = require "game.loot.effectManager"

function LootManager:new(controller)
  self.controller = controller
  self.rings = 0
  self.inventory = {}
  self.pocketIndex = {}
  self.effects = EffectManager()

  self:generatePockets()
  LootManager.super.new(self, {}, {"inventory"})
end

function LootManager:generatePockets()
  local structure = require "datas.gamedata.items"
  for i,pocketdata in ipairs(structure) do
    local pocket = Pocket(pocketdata)
    self.pocketIndex[pocketdata.name] = i
    table.insert(self.inventory, pocket)
  end
end

function LootManager:getPocketIdByName(name)
  return self.pocketIndex[name]
end

function LootManager:getPocketById(id)
  return self.inventory[id]
end

function LootManager:getPocketByName(name)
  return self.inventory[self.pocketIndex[name]]
end

function LootManager:addItem(type, item, number)
  local pocket = self:getPocketByName(type)
  if (core.datas:exists("items", item)) then
    pocket:addItem(item, number)
  end
end

function LootManager:removeItem(type, item, number)
  local pocket = self:getPocketByName(type)
  if (core.datas:exists("items", item)) then
    pocket:removeItem(item, number)
  end
end

function LootManager:getItemNumber(type, item)
  local pocket = self:getPocketByName(type)
  return pocket:getItemNumber(item)
end

function LootManager:applyItemEffect(category, item, target)
  self.effects:getItemData(category, item)
  self.effects:applyItemEffect(target)
end

function LootManager:getEffectStrings(category, item)
  self.effects:getItemData(category, item)
  return self.effects:getEffectStrings(nil)
end

return LootManager
