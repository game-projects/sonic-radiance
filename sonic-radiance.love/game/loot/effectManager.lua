local EffectManager = Object:extend()

local effectList = require "game.loot.effects"

function EffectManager:new()
    self.itemdata = nil;
end

function EffectManager:getItemData(category, item)
    self.itemdata = core.datas:get("items", item)
end

function EffectManager:applyEffects(character)
    for _, effect in ipairs(self.itemdata.effects) do
        local effectInstance = self:getEffectObject(effect, character, self.itemdata.duration)
        effectInstance:applyEffect()
    end
end

function EffectManager:applyEffectsBattle(battleTarget)
    local character = battleTarget.abstract
    for _, effect in ipairs(self.itemdata.effects) do
        local effectInstance = self:getEffectObject(effect, character, self.itemdata.duration)
        effectInstance:applyEffect()
        effectInstance:battleCallback(battleTarget)
    end
end

function EffectManager:getEffectStrings(character)
    local returnString = "No Effect";
    if (#self.itemdata.effects >= 1) then
        local effectInstance = self:getEffectObject(self.itemdata.effects[1], character, self.itemdata.duration)
        if (effectInstance ~= nil) then
            returnString = effectInstance:getText() .. "\n"
        end
    end
    if (#self.itemdata.effects >= 2) then
        local effectInstance = self:getEffectObject(self.itemdata.effects[2], character, self.itemdata.duration)
        if (effectInstance ~= nil) then
            returnString = returnString .. effectInstance:getText()
        end
    end
    return returnString
end

function EffectManager:getEffectObject(rawEffectData, character, duration)
    local effect = core.datas:parse("itemeffects", rawEffectData)
    if (effectList[effect.type] ~= nil) then
        return effectList[effect.type](effect, character, duration)
    else
        return nil
    end
end

return EffectManager
