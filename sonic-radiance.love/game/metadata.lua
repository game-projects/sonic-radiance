-- game.metadata :: Basic metadata subsystem

--[[
  Copyright © 2021 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Serializer = require "birb.classes.serializable.serializer"
local Metadata = Serializer:extend()

local META_FILE = "metadata.save"

function Metadata:new(game)
    self.game = game
    self:reset()
    Metadata.super.new(self, {"data"})
end

function Metadata:reset()
    self.data = {}
    for i = 1, self.game.slotNumber, 1 do
        local newMetadata = {}
        newMetadata.exist = false
        newMetadata.completion = 0
        newMetadata.gametime = 0
        newMetadata.team = {}
        newMetadata.rings = 0
        newMetadata.emeralds = 0
        newMetadata.location = ""
        table.insert(self.data, newMetadata)
    end
end

function Metadata:update()
    self:read()
    if (self.data[self.game.slot] == nil) then
        self.data[self.game.slot] = {}
    end
    self.data[self.game.slot].exist = true
    self.data[self.game.slot].completion = self.game.completion
    self.data[self.game.slot].gametime = self.game.gametime
    self.data[self.game.slot].team = self.game.characters.team
    self.data[self.game.slot].rings = self.game.loot.rings
    self.data[self.game.slot].emeralds = 0
    self.data[self.game.slot].location = self.game.mapName
    self:write()
end

function Metadata:get()
    self:read()
    return self.data
end

function Metadata:write()
    self:serialize(META_FILE)
end

function Metadata:read()
    self:deserialize(META_FILE)
end

function Metadata:remove(slot)
    self.data[slot].exist = false
    self:write()
  end

return Metadata
