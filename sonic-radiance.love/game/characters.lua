-- game/characters :: The character handler. This object handle all the character
-- and is able to get and set datas about them.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Serializable = require "birb.classes.serializable"
local CharacterManager = Serializable:extend()

local AbstractCharacter = require "game.abstractmobs.character"
local startdata   = require "datas.gamedata.startdata"

function CharacterManager:new(controller)
  self.controller = controller
  self.namelist = require "datas.gamedata.characters"
  self.list     = {}
  self.team     = startdata.baseteam
  self.active   = 1
  self:init()
  CharacterManager.super.new(self, {"team"}, {"list"}, {list = AbstractCharacter})
end

function CharacterManager:init()
  for k, name in pairs(self.namelist) do
    if (core.datas:exists("characters", name)) then
      self.list[name] = AbstractCharacter(name)
    end
  end
end

-- WRAPPER FUNCTIONS
-- Simple wrappers around characters functions

function CharacterManager:setLevel(name, newlevel)
  self.list[name]:setLevel(newlevel)
end

function CharacterManager:levelUp(name)
  self.list[name]:levelUp()
end

function CharacterManager:heal(name)
  self.list[name]:heal()
end

-- DATA FUNCTIONS
-- function to handle saving

function CharacterManager:getData()
  local data = {}
  data.team = self.team
  data.list = {}
  for name, character in pairs(self.list) do
    data.list[name] = character:getData()
  end
  return data
end

function CharacterManager:setData(data)
  local data = data
  self.team = data.team
  for name, charData in pairs(data.list) do
    self.list[name]:setData(charData)
  end
end

-- TEAM FUNCTIONS
-- Team handling and management

function CharacterManager:addToTeam(name)
 self:heal(name)
 if (#self.team < 4) then
   table.insert(self.team, name)
 end
end

function CharacterManager:removeToTeam(teamid)
  if (#self.team > 1) then
    table.remove(self.team, teamid)
  end
end

function CharacterManager:getPositionInTeam(charName)
  local charId = -1
  for i,name in ipairs(self.team) do
    if (name == charName) then
      charId = i
    end
  end
  return charId
end

function CharacterManager:addOrRemoveToTeam(charName)
  local charId = self:getPositionInTeam(charName)
  if (charId == -1) then
    self:addToTeam(charName)
  else
    self:removeToTeam(charId)
    if (charId < self.active) then
      self.active = self.active - 1
    end
  end
  self:fixActiveCharacter()
end

function CharacterManager:setActiveCharacter(direction)
  local direction = direction or 1
  local count = direction
  self.active = self.active + utils.math.sign(direction)
  if (self.active > #self.team) then
    self.active = 1
  end
  if (self.active < 1) then
    self.active = #self.team
  end
  if (self.list[self.team[self.active]].hp <= 0) and not game.difficulty:get("playerKoChar") then
    count = count + self:setActiveCharacter(direction)
  end
  return count
end

function CharacterManager:fixActiveCharacter()
  if (self.active < 1) then
    self.active = 1
  elseif (self.active > #self.team) then
    self.active = #self.team
  end
end

function CharacterManager:getActiveCharacter()
  return self.team[self.active]
end

function CharacterManager:getActiveCharacterData()
  return self.list[self.team[self.active]]
end

-- SCENES FUNCTIONS
-- Handle damages from the scenes

function CharacterManager:sendDamageFromMap(name, damageRatio)
  local character = self.list[name]
  if (character.hp > 0) then
    local newHP = math.floor(character.hp - (character.stats:get(character.stats.HPMAX) * damageRatio))
    if (not game.difficulty:get("hazardMakesKo")) then
      newHP = math.max(1, newHP)
    end
    self.list[name]:setHP(newHP, false)
  end
end

function CharacterManager:loadSprite(assets, name)
  assets:addSprite(name, "datas/gamedata/characters/" .. name .. "/sprites")
end

-- DEBUG FUNCTIONS

function CharacterManager:printCharacter(id)
  local character = self.list[id]
  core.debug:print(id .. ". " .. character.fullname)
  core.debug:print("Lvl " .. character.level .. " (" .. character.exp .. "/" .. character.exp_next .. " exp)")
end

function CharacterManager:printTeam()
  for i,v in ipairs(self.team) do
    self:printCharacter(v)
    core.debug:print("-----")
  end
end

return CharacterManager
