local CharUtils = {}

local CONST = require "datas.consts.stats"

function CharUtils.getExpValue(level)
  return math.floor( ( CONST.EXP_MULTIPLICATOR * ( level ^ 3 ) ) / CONST.EXP_RATIO )
end

function CharUtils.getRelativeExpValue(exp, level)
  return exp - CharUtils.getExpValue(level)
end

function CharUtils.getLevelExpRange(level)
  return CharUtils.getExpValue(level + 1) - CharUtils.getExpValue(level)
end

function CharUtils.getRemainingExp(exp, level)
  return CharUtils.getExpValue(level + 1) - exp
end

function CharUtils.getStatValue(level, base)
  return math.floor( (((base * CONST.MULT_STAT) * level)/100) ) + CONST.BASE_STAT
end

function CharUtils.getHPValue(level, base)
  return math.floor( (((CONST.SALT_HP + base * CONST.MULT_HP) * level)/100) ) + CONST.BASE_HP + level
end

function CharUtils.getPPValue(level, base)
  return math.floor( (((base * CONST.MULT_MP) * level)/100) ) + CONST.BASE_MP
end

return CharUtils
