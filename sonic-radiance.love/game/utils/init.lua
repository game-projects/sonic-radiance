local gameutils = {}

function gameutils.getMapPath(maptype, mapname)
  local dir = gameutils.getMapDirectory(maptype, mapname)
  local path = ""
  if maptype == "sti" then
    path = dir .. "map.lua"
  else
    path = "datas.gamedata.maps." .. maptype .. "." .. mapname
  end
  core.debug:print("game/utils", "path is " .. path)
  return path
end

function gameutils.getMapDirectory(maptype, mapname)
  if not gameutils.validateMapType(maptype) then
    error("Map type " .. maptype .. " doesn't exist.")
  end

  return "datas/gamedata/maps/" .. maptype .. "/" .. mapname .. "/"
end

function gameutils.validateMapType(maptype)
  local types     = {"battle", "sti", "test", "shoot"}
  local validated = false

  for i, type in ipairs(types) do
    if (type == maptype) then
      validated = true
    end
  end

  return validated
end

return gameutils
