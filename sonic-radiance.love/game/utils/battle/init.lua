local BattleUtils = {}
local CONSTS = require "datas.consts.battle"
local protectypes = require "datas.gamedata.battles.protectypes"

local STATS = require "datas.consts.stats"

function BattleUtils.computeLaunchingDamages(base, fighter, isSpecial)
    local damages = base / CONSTS.DAMAGE.DIVISOR
    if (isSpecial) then
        damages = damages * fighter:getStat(STATS.POWER)
    else
        damages = damages * fighter:getStat(STATS.ATTACK)
    end

    damages = damages * BattleUtils.computerArmorAndDamage(fighter:getStat(STATS.DAMAGE))

    if (BattleUtils.rollDice(fighter:getStat(STATS.CRITICAL))) then
        damages = damages * 2
    end

    return damages
end

function BattleUtils.computeReceivingDamages(base, fighter, isSpecial, isDefending)
    local damages = base
    if (isSpecial) then
        damages = damages / fighter:getStat(STATS.MIND)
    else
        damages = damages / fighter:getStat(STATS.DEFENSE)
    end

    damages = damages * BattleUtils.computerArmorAndDamage(fighter:getStat(STATS.ARMOR), true)

    if (isDefending) then
        damages = damages * CONSTS.DAMAGE.DEFFACTOR
    end

    return damages
end

function BattleUtils.applyProtectTypes(value, type, protectypeList)
    for _, protectype in ipairs(protectypeList) do
        local protecttypeData = protectypes[protectype]
        if protecttypeData ~= nil then
            if (utils.table.contain(protecttypeData.resistTo, type)) then
                value = value * CONSTS.ATKWRONGTYPE
            end
        else
            core.debug:warning("battleutils", "protectype " .. protectype .. " doesn't exists ")
        end
    end
    return value
end

function BattleUtils.applyWeaknesses(damages, element, weaknessList)
    if (utils.table.contain(weaknessList, element)) then
        damages = damages * 1.2
    end
    return damages
end

function BattleUtils.applyResistences(damages, element, resistsList)
    if (utils.table.contain(resistsList, element)) then
        damages = damages * 0.8
    end
    return damages
end

function BattleUtils.rollDice(stat)
    local stat = stat or 100
    return (math.random(100) <= stat)
end

function BattleUtils.computerArmorAndDamage(stat, isNegative)
    if (isNegative) then
        stat = stat * -1
    end
    if (stat < 0) then
        stat = stat * STATS.ARMOR_AND_DAMAGE_RATIO
    end
    return (100 + (stat))/100
end

return BattleUtils
