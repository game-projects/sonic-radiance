local BirbScene = require "birb.modules.scenes"
local RadianceScene = BirbScene:extend()

local Overlay = require "game.modules.gui.overlay"
local MessageQueue = require "game.modules.messagequeue"
local ActionPrompt = require "game.modules.gui.actionPrompt"

function RadianceScene:new(haveBorder, showVersion)
    RadianceScene.super.new(self)

    -- Importation Global des assets
    self.assets:batchImport("assets.commons")
    self.assets.fonts["small"]:setLineHeight(16/18)
    self.assets.fonts["small"]:setFilter("shadow")
    self.gui:addSFX("select", "mSelect")
    self.gui:addSFX("navigate", "mBeep")
    self.gui:addSFX("back", "mBack")
    self.gui:addSFX("error", "mError")

    Overlay(haveBorder, showVersion)
    MessageQueue(self)
    ActionPrompt()
end

function RadianceScene:hideOverlay()
    self.gui:hideScreen("overlay")
end

function RadianceScene:showMessage(message)
    self.gui.elements["messageQueue"]:addMessage(message)
end

function RadianceScene:setPrompt(message)
    self.gui.elements["actionPrompt"]:setText(message)
end

function RadianceScene:showOverlay(darken)
    self.gui:showScreen("overlay")
    if (darken) then
        self.gui:playScreenTransform("overlay", "showBackground")
    end
end


return RadianceScene