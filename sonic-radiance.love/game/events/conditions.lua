local Conditions = {}

function Conditions.haveMoney(cond, predicate, asker)
    return predicate.utils.testVariables(game.loot.rings , cond[2], cond[3])
end

function Conditions.default(cond, predicate, asker)
    local flag = predicate.utils.merge(cond)
    return asker:haveFlag(flag)
end

return Conditions