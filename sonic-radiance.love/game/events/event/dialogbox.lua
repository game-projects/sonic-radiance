local StepParent = require "game.events.event.parent"
local DialogBox = StepParent:extend()

local Talkies = require('birb.libs.talkies')

function DialogBox:new(controller, args)
    DialogBox.super.new(self, controller, args)
    Talkies.font = love.graphics.newFont("assets/gui/fonts/PixelOperator.ttf", 16)
    Talkies.talkSound = love.audio.newSource("assets/sfx/talk.wav", "static")
    Talkies.optionOnSelectSound = love.audio.newSource("assets/sfx/menus/select.wav", "static")
    Talkies.optionSwitchSound = love.audio.newSource("assets/sfx/menus/beep.wav", "static")
end

function DialogBox:start()
    local args = self:addOptions()
    
    if (args == nil) then
        Talkies.say(self.arguments.title, self.arguments.message)
    else
        Talkies.say(self.arguments.title, self.arguments.message, args)
    end
end

function DialogBox:addOptions()
    local args = {}
    args.options = {}
    local haveAddedSomething = false
    if (self.arguments.option1 ~= nil) then
        table.insert(args.options, {self.arguments.option1, function() self:setOption(1) end})
        haveAddedSomething = true
    end
    if (self.arguments.option2 ~= nil) then
        table.insert(args.options, {self.arguments.option2, function() self:setOption(2) end})
        haveAddedSomething = true
    end
    if (self.arguments.option3 ~= nil) then
        table.insert(args.options, {self.arguments.option3, function() self:setOption(3) end})
        haveAddedSomething = true
    end
    if (haveAddedSomething) then
        return args
    end
end

function DialogBox:setOption(num)
    self.events:addFlag(self.arguments.flag, num)
    self:finish()
end

function DialogBox:update(dt)
    Talkies.update(dt)

    if (not Talkies.isOpen()) then
        self:finish()
    end
    local keys = self.events.scene.sources[1].keys
    if (keys["up"].isPressed) then Talkies.prevOption();
    elseif (keys["down"].isPressed) then Talkies.nextOption();
    elseif (keys["A"].isPressed) then Talkies.onAction();
    end
end

function DialogBox:draw()
    Talkies.draw()
end

return DialogBox;
