local StepParent = require "game.events.event.parent"
local SimpleMessageStep = StepParent:extend()

function SimpleMessageStep:new(controller, args)
    SimpleMessageStep.super.new(self, controller, args)
end

function SimpleMessageStep:start()
    self.events.scene.world.obj.GFX(self.events.scene.world, self.arguments.x, self.arguments.y, self.arguments.spritename)
end

function SimpleMessageStep:update(dt)
    self:finish()
end

return SimpleMessageStep;
