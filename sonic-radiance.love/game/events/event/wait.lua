local StepParent = require "game.events.event.parent"
local WaitStep = StepParent:extend()

function WaitStep:new(controller, args)
  WaitStep.super.new(self, controller, args)
end

function WaitStep:start()
  self.timer = 0
end

function WaitStep:update(dt)
  self.timer = self.timer + dt
  if (self.timer > self.arguments.duration) then
    self:finish()
  end
end

return WaitStep;
