local StepParent = require "game.events.event.parent"
local SimpleMessageStep = StepParent:extend()
local defTransitions = require "birb.modules.transitions"

function SimpleMessageStep:new(controller, args)
    SimpleMessageStep.super.new(self, controller, args)
end

function SimpleMessageStep:start()
    local charDir = self.arguments.charDir
    if (charDir == "default") then
        charDir = self.events.scene.world.players[1].actor.charDir or "down"
    end
    core.screen:startTransition(defTransitions.default, defTransitions.default,
    function() self.events.scene.world:teleport(self.arguments.area, self.arguments.x, self.arguments.y, charDir) end,
    0, 0)
end

function SimpleMessageStep:update(dt)
    self:finish()
end

return SimpleMessageStep;
