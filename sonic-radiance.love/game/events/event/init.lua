return {
    ["wait"] = require("game.events.event.wait"),
    ["simpleMessage"] = require("game.events.event.simpleMessage"),
    ["dialogBox"] = require("game.events.event.dialogbox"),
    ["optionBox"] = require("game.events.event.dialogbox"),
    ["playSFX"] = require("game.events.event.playSFX"),
    ["getRings"] = require("game.events.event.getRings"),
    ["getItems"] = require("game.events.event.getItems"),
    ["teleport"] = require("game.events.event.teleport"),
    ["showGFX"] = require("game.events.event.showGFX")
}