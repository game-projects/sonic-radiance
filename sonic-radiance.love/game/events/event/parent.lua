local StepParent = Object:extend()

function StepParent:new(eventSystem, arguments)
  self.events = eventSystem
  self.arguments = arguments
  self.isStarted = false
end

function StepParent:updateStep(dt)
  if (not self.isStarted) then
    self:start()
    self.isStarted = true
  else
    self:update(dt)
  end
end

function StepParent:finish()
  self.events:endStep()
end

function StepParent:draw()

end

return StepParent