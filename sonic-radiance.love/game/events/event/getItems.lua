local StepParent = require "game.events.event.parent"
local SimpleMessageStep = StepParent:extend()

function SimpleMessageStep:new(controller, args)
    SimpleMessageStep.super.new(self, controller, args)
end

function SimpleMessageStep:start()
    game.loot:addItem(self.arguments.type, self.arguments.item, self.arguments.number)
end

function SimpleMessageStep:update(dt)
    self:finish()
end

return SimpleMessageStep;
