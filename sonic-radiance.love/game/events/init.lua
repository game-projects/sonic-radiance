local EventManager = Object:extend()

local stepObjectList = require "game.events.event"
local Conditions = require "game.events.conditions"

local Predicates = require "birb.classes.predicate"

function EventManager:new(scene)
    self.scene = scene
    self.world = scene.world

    self.isStarted = false
    self.currentStepId = 0
    self.currentStep = nil
    self.stepList = {}
    self.flags = {}
end

function EventManager:startEvent(owner, stepList)
  self.stepList = stepList
  self.flags = {}
  self.owner = owner
  self.isStarted = true
  self.currentStepId = 0
  self.currentStep = nil
  self.scene:startEvent()
end

function EventManager:addFlag(flagname, flagcontent)
  if (flagcontent == true) then
    flagcontent = "V"
  end
  if (flagcontent == false) then
    flagcontent = "F"
  end
  local flag = flagname .. ":" .. flagcontent
  table.insert(self.flags, flag)
end

function EventManager:testCondition(condition)
  local predicate = Predicates.createPredicate(condition, Conditions, self)
  return predicate:solve()
end

function EventManager:haveFlag(flagToTest)
  if (flagToTest == nil or flagToTest == "") then
    return true
  end
  return utils.table.contain(self.flags, flagToTest)
end

function EventManager:update(dt)
    if (self.isStarted) then
        if (self.currentStep ~= nil) then
            self.currentStep:updateStep(dt)
        else
            self:switchStep()
        end
    end
end

function EventManager:switchStep()
  if self:haveNextStep() then
    self.currentStepId = self.currentStepId + 1
    local stepData = core.datas:parse("eventstep", self.stepList[self.currentStepId])
    core.debug:print("event", "Starting step " .. stepData.name)
    if (not self:testCondition(stepData.condition)) then
      self:switchStep()
    else
      if (stepObjectList[stepData.name] ~= nil) then
        self.currentStep = stepObjectList[stepData.name](self, stepData.arguments)
      end
    end
  else
    self:endEvent()
  end
end

function EventManager:endStep()
  self.currentStep = nil
end

function EventManager:endEvent()
  core.debug:print("event", "Ending event, giving back controle")
  self.scene:endEvent()
  self.isStarted = false
end

function EventManager:haveNextStep()
  return ((self.currentStepId + 1) <= #self.stepList)
end

function EventManager:draw()
  if (self.isStarted) then
    if (self.currentStep ~= nil) then
      self.currentStep:draw()
    end
  end
end

return EventManager
