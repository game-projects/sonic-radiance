local PauseScreen = require("birb.modules.gui.screen"):extend()

local BoxedMenu = require("game.modules.gui.boxedmenu")
local TextElement = require("birb.modules.gui.elements.text")

local WIDTH = 80
local X, Y, TXT_Y = 424/2, 240/2, 240/6
local OX, OY = WIDTH/2, (3*17)/2

local show = {
    {"pauseMenu", "tween", 0.1, 0.3, {opacity = 1, sx = 1, sy = 1}, "inOutQuart"},
    {"pauseText", "tween", 0.1, 0.3, {opacity = 1, y = TXT_Y}, "inOutQuart"},
}

local hide = {
    {"pauseMenu", "tween", 0, 0.3, {opacity = 0, sx = 0.8, sy = 0.8}, "inOutQuart"},
    {"pauseText", "tween", 0, 0.3, {opacity = 0, y = TXT_Y - 16}, "inOutQuart"},
}

function PauseScreen:new()
    PauseScreen.super.new(self, "pauseScreen")
    self:addTransform("show", show)
    self:addTransform("hide", hide)
    self.defaultFocus = "pauseMenu"
end

function PauseScreen:createElements()
    local pauseMenu = BoxedMenu("pauseMenu", X, Y, WIDTH, 3, true, false)
    pauseMenu:addItem("Resume", "left", function() self.scene:unpause() end, "back")
    pauseMenu:setCancelWidget()
    pauseMenu:addItem("Restart", "left", function() self.scene:restartLevel() end, "back")
    pauseMenu:addItem("Exit", "left", function() self.scene:exitLevel() end, "back")
    pauseMenu.opacity = 0
    pauseMenu.sx, pauseMenu.sy = 0.8, 0.8
    pauseMenu.ox, pauseMenu.oy = OX, OY

    local text = TextElement("pauseText", "SA2font", "Pause", X, TXT_Y - 16, "center")
    text.opacity = 0
    return {
        {pauseMenu, 0, 1},
        {text, 0, 1}
    }
end

return PauseScreen