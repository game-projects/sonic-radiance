local Scene = require("game.scenes")
local PlayStyle = Scene:extend()

local TweenManager = require "birb.classes.time"
local PauseScreen = require("game.modules.subgames.pause")
local TestWorld = require("game.modules.subgames.world.parent")

function PlayStyle:new(supportedLevels, missionfile)
  PlayStyle.super.new(self, false, false)
  self.timer = 0
  self.assets:batchImport("assets.subgames")
  --self:loadMissionFile(supportedLevels, missionfile)

  self:initWorld()
  self:initMission()
  self:initCharacters()

  self.tweens = TweenManager(self)
  PauseScreen()

  self.haveStarted = false
  self.canPause = true
end

function PlayStyle:loadMissionFile(supportedLevels, missionfile)
  self.mission = require("datas.gamedata.missions." .. missionfile)
  self.assets:setMusic("assets/music/" .. self.mission.music)
end

function PlayStyle:initWorld()
  TestWorld(self)
end

function PlayStyle:initMission()
  -- NOTHING
end

function PlayStyle:initCharacters()
  self.world:setPlayerNumber(1)
  self.world.cameras:setMode("split")
end

function PlayStyle:getCharacterName(charID)
  return self.characterList[charID]
end

function PlayStyle:update(dt)
  if (self.haveStarted) then
    PlayStyle.super.update(self, dt)
    self:updatePauseMenus(dt)
  else
    self:startLevel()
  end
end

function PlayStyle:updatePauseMenus(dt)
    self.tweens:update(dt)
    if self.gui.screens["pauseScreen"].isVisible == false then
      self.timer = self.timer + dt
    end

    local keys = self:getKeys(1);


    if (keys["start"].isPressed and self.canPause) then
      if (not self.isPaused) then
        if (not self.gui.screens["overlay"].isVisible) then
          self.assets.sfx["mSelect"]:play()
          self:pause()
        end
      else
        self.assets.sfx["mBack"]:play()
        self:unpause()
      end
    end
end


function PlayStyle:timerResponse(timer)
  if (timer == "unPause") then
    self.isPaused = false
    self.world.isActive = true
  end
end

function PlayStyle:pause()
  self.gui:showScreen("overlay")
  self.gui:showScreen("pauseScreen")
  self.gui:playScreenTransform("overlay", "showBackgroundPause")
  self.isPaused = true
  self.world.isActive = false
end

function PlayStyle:unpause()
  self.tweens:newTimer(0.2, "unPause")
  self.gui:playScreenTransform("overlay", "hideBackgroundPause")
  self.gui:hideScreen("overlay")
  self.gui:hideScreen("pauseScreen")
end

function PlayStyle:startLevel()
  self.haveStarted = true
  self.world:loadMap()
  --self.assets:playMusic()
end

function PlayStyle:restartLevel()
  self:unpause()
  self.world:reset()
end

function PlayStyle:exitLevel()
  scenes.menus.title()
end

return PlayStyle
