local ParentWorld = require "game.modules.subgames.world.parent"
local BattleWorld = ParentWorld:extend()

local customMap = require "game.modules.subgames.world.maps"

function BattleWorld:new(scene, mapname)
  local mappath = game.utils.getMapPath("battle", mapname)
  BattleWorld.super.new(self, scene, "battle", mapname)

  self.mapname = mapname
end

function BattleWorld:createMapController()
  customMap.Battle(self, self.maptype, self.mapname)
end

return BattleWorld
