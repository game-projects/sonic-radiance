local BaseMap   = require "birb.modules.world.maps.parent"
local ShootMap  = BaseMap:extend()

local TILESIZE = 31
local TESTZONE = "forest"

local zoneDatas   = require "datas.gamedata.maps.shoot.zones"
local Background  = require "game.modules.drawing.parallaxBackground"

local Chunk = require "game.modules.subgames.world.maps.tools.chunk"

function ShootMap:new(world, maptype, mapname)
  ShootMap.super.new(self, world)

  self:setPadding(0, 0, 0, 0)
  self:getLevelData(mapname)
  self.parallaxBackground = Background(world.scene, 5, 1, self.datas.background)
  self.layout = {}
  self.chunklist = {}

  self.width = 0
end

function ShootMap:updateWidth()
  self.size   = #self.layout * 8
  self.width  = self.size * 31
end

function ShootMap:getLevelData(mapname)
  local file  = mapname or "testlevel.test1"
  local level = require("datas.gamedata.maps.shoot." .. file)

  self.zone = level.datas.zone

  local currentZone_datas   = zoneDatas[self.zone]
  local bypass_data = level.datas.bypass_data

  self.datas = {}

  if (level.datas.bypass_zone) then
    self.datas.tiles      = bypass_data.tiles       or currentZone_datas.tiles
    self.datas.background = bypass_data.background  or currentZone_datas.background
  else
    self.datas = currentZone_datas
  end

  self.datas.layout = level.layout
  self.datas.parts  = level.parts
end

function ShootMap:loadCollisions()
  self:loadLayout();
  local w, h = self:getDimensions()
  --self.world:newCollision("floor", 0, 0, -16, w, h, 16)
  for i, chunkname in ipairs(self.layout) do

    self.chunklist[chunkname]:spawnGrounds((i-1)*31*8)
    self.chunklist[chunkname]:spawnObjects((i-1)*31*8)
  end

  local CHUNKSIZE = TILESIZE*8
  self.world:newCollision("fakefloor", -CHUNKSIZE, 0, -48, self.width+CHUNKSIZE*2, 200, 48)
end

function ShootMap:loadLayout()
  self.layout = {}
  -- [ShootMap].datas.layout est un tableau composée d'une série de tableau,
  -- chacuns en deux partie : le premier (part[1]) contient le nom du fichier
  -- où se trouve des partie de niveau, et la seconde partie (part[2]) une liste
  -- d'identifiant de partie de niveau à rajouter.

  -- Ce format permet de simplifier l'écriture de layout, si on a besoin de
  -- plusieur partie de niveau se trouvant dans le même fichier.
  for i, partContainer in ipairs(self.datas.layout) do
    for j, part in ipairs(partContainer[2]) do
      self:addPart(partContainer[1], part)
    end
  end
end

function ShootMap:addPart(source, partId)
  local source = source or "self"
  local partdata = {}

  -- On recupère les données de chunks

  -- Si le nom est "self", cela veut dire que fichier est le même que celui ou
  -- se trouve le layout, qui est forcément le fichier du niveau. Cela veut dire
  -- qu'il faut utiliser les parties de niveau du niveau actuel, localisée dans
  -- self.controller.parts
  if (source == "self") or (source == "") then
    partdata = self.datas.parts
  else
    -- Si c'est un autre nom, on charge dans "partdata" la liste des partie de niveau
    -- du fichier qu'on réfère.
    local chunkfile = require("datas.gamedata.maps.shoot.chunks." .. source)
    partdata = chunkfile.parts
  end

  local chunklist = partdata[partId]

  -- chunklist fonctionne de la même manière que partlist.
  -- chaque entrée dans la liste (chunkContainer) contient deux partie :
  -- chunkContainer[1]: l'identifiant du fichier où se trouve le chunk voulu
  -- chunkContainer[2]: la liste de chunks voulu. chaque entrée dedans est
  -- un nombre correspondant à un chunk
  for i, chunkContainer in ipairs(chunklist) do
    for j, chunk in ipairs(chunkContainer[2]) do
      self:addChunk(source, chunkContainer[1], chunk)
    end
  end

end

function ShootMap:addChunk(source, filename, chunkid)
  local filename = filename or "self"
  if (filename == "self") or (filename == "") then
    filename = source
  end

  local chunkfile = require("datas.gamedata.maps.shoot.chunks." .. filename)
  local chunkdata = chunkfile.chunks
  local chunkpos = #self.layout
  local chunkname = filename .. chunkid
  if self.chunklist[chunkname] == nil then
    self.chunklist[chunkname] = Chunk(self, chunkdata[chunkid])
  end
  table.insert(self.layout, chunkname)
  self:updateWidth()
end

function ShootMap:getChunk(id)
  return self.chunklist[self.layout[id]]
end

function ShootMap:addBlock(x, y, w, h, top, bottom)
  -- Empty Placeholder function
end

function ShootMap:getDimensions()
  return 3000, 100
end

function ShootMap:loadPlayers()
  self.world:addPlayer(16, 50, 0, 1)
end

function ShootMap:loadActors()
  -- Empty Placeholder function
end

function ShootMap:draw(x, y, w, h)
  self:drawChunks(x)
end

function ShootMap:drawParallax(x, y, w, h)
  self.parallaxBackground:drawParallax(x, y, w, h)
end

function ShootMap:drawChunks(x)
  local x = x or 0
  local firstChunk = math.floor(x / (8*31)) - 2

  for i=1, 6 do
    local chunkID = firstChunk + i
    local chunk = self:getChunk(chunkID)
    if (chunk ~= nil) then
      chunk:draw((chunkID-1) * (8*31))
    end
  end
end

return ShootMap
