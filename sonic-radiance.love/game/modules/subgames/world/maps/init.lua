local customMap = {}

customMap.Test    = require "game.modules.subgames.world.maps.test"
customMap.Battle  = require "game.modules.subgames.world.maps.battle"
customMap.Shoot   = require "game.modules.subgames.world.maps.shoot"

return customMap
