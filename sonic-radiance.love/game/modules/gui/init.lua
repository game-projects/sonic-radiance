local gui = {}
local TileSet = require "birb.modules.assets.types.tileset"

local barborder = love.graphics.newImage("assets/gui/barborder.png")
local barSmall = TileSet("assets/gui/bar_small")

local mask_shader = love.graphics.newShader[[
   vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
      if (Texel(texture, texture_coords).rgb == vec3(0.0)) {
         // a discarded pixel wont be applied as the stencil.
         discard;
      }
      return vec4(1.0);
   }
]]

local function myStencilFunction(mask)
  love.graphics.setShader(mask_shader)
  love.graphics.draw(mask, 0, 0)
  love.graphics.setShader()
end

function gui.newBorder(width, height, middlePosition)
  local tileset   = love.graphics.newImage("assets/gui/borders.png")
  local tilequad  = {}
  local w, h = tileset:getDimensions()
  tilequad[1]     = love.graphics.newQuad(0, 0,  20, 20, w, h)
  tilequad[2]     = love.graphics.newQuad(20, 0, 20, 20, w, h)
  tilequad[3]     = love.graphics.newQuad(40, 0, 20, 20, w, h)
  tilequad[4]     = love.graphics.newQuad(60, 0, 20, 20, w, h)

  local canvas = love.graphics.newCanvas(width, height)

  love.graphics.setCanvas(canvas)
  utils.graphics.resetColor()

  local height  = math.ceil(height / 20)
  local width   = math.ceil(width  / 20)

  for i=1, width do
    if i < middlePosition then
      love.graphics.draw(tileset, tilequad[1], (i-1) * 20, 0)
    elseif (i == middlePosition) then
      love.graphics.draw(tileset, tilequad[2], (i-1) * 20, 0)
    else
      love.graphics.draw(tileset, tilequad[3], (i-1) * 20, 0)
    end

    if height > 1 then
      for j = 2, height do
        love.graphics.draw(tileset, tilequad[4], (i-1) * 20, (j-1) * 20)
      end
    end
  end

  love.graphics.setCanvas( )

  local imagedata = canvas:newImageData()
  local texture   = love.graphics.newImage( imagedata )
  imagedata:release()
  canvas:release()

  return texture
end

function gui.newChoiceBack(approximateWidth)
  local asset   = love.graphics.newImage("assets/gui/attacklist.png")
  local sw, sh  = asset:getDimensions()
  local startAsset  = love.graphics.newQuad(0, 0, 21, sh, sw, sh)
  local midAsset    = love.graphics.newQuad(21, 0, 12, sh, sw, sh)
  local endAsset    = love.graphics.newQuad(sw-25, 0, 25, sh, sw, sh)

  local iterations = math.floor((approximateWidth / 12) - 4)
  local width = (iterations * 12)
  local height = 16
  local canvasWidth = width + 21 + 25

  local canvas = love.graphics.newCanvas(canvasWidth, height)

  love.graphics.setCanvas(canvas)

  love.graphics.draw(asset, startAsset, 0, 0)
  love.graphics.draw(asset, endAsset, 21 + width, 0)

  for i=1, (iterations) do
    love.graphics.draw(asset, midAsset, 21 + ((i-1)*12), 0)
  end

  love.graphics.setCanvas( )

  local imagedata = canvas:newImageData()
  local texture   = love.graphics.newImage( imagedata )
  imagedata:release()
  canvas:release()

  return texture
end

function gui.drawBar(x, y, width, height)
  width = math.floor(width)

  if (width > 0) then
    height = 7
    if (width <= 8) then
      barSmall:drawTile(9 - width, 0, 0)
    else
      love.graphics.draw(barborder, x, y)
      local barwidth = math.max(width-14, 0)
      love.graphics.rectangle("fill", x+7, y, barwidth, height)
      love.graphics.draw(barborder, x+barwidth+7, y, 0, -1, -1, 7, 7)
    end
  end
end

function gui.newTextBox(filename, width, height)
  local baseimage = love.graphics.newImage(filename)
  width, height = math.floor(width), math.floor(height)
  local quad, quad_mask = {}, {}
  quad[1] = love.graphics.newQuad(00, 00, 8, 8, 48, 32)
  quad[2] = love.graphics.newQuad(00, 08, 8, 8, 48, 32)
  quad[3] = love.graphics.newQuad(00, 16, 8, 8, 48, 32)
  quad[4] = love.graphics.newQuad(08, 00, 8, 8, 48, 32)
  quad[5] = love.graphics.newQuad(08, 08, 8, 8, 48, 32)
  quad[6] = love.graphics.newQuad(08, 16, 8, 8, 48, 32)
  quad[7] = love.graphics.newQuad(16, 00, 8, 8, 48, 32)
  quad[8] = love.graphics.newQuad(16, 08, 8, 8, 48, 32)
  quad[9] = love.graphics.newQuad(16, 16, 8, 8, 48, 32)

  quad_mask[1] = love.graphics.newQuad(24+00, 00, 8, 8, 48, 32)
  quad_mask[2] = love.graphics.newQuad(24+00, 08, 8, 8, 48, 32)
  quad_mask[3] = love.graphics.newQuad(24+00, 16, 8, 8, 48, 32)
  quad_mask[4] = love.graphics.newQuad(24+08, 00, 8, 8, 48, 32)
  quad_mask[5] = love.graphics.newQuad(24+08, 08, 8, 8, 48, 32)
  quad_mask[6] = love.graphics.newQuad(24+08, 16, 8, 8, 48, 32)
  quad_mask[7] = love.graphics.newQuad(24+16, 00, 8, 8, 48, 32)
  quad_mask[8] = love.graphics.newQuad(24+16, 08, 8, 8, 48, 32)
  quad_mask[9] = love.graphics.newQuad(24+16, 16, 8, 8, 48, 32)

  local back = love.graphics.newQuad(00, 24, 8, 8, 48, 32)
  local canvas      = love.graphics.newCanvas(width, height)
  local backCanvas  = love.graphics.newCanvas(width, height)
  local maskCanvas  = love.graphics.newCanvas(width, height)


  love.graphics.setCanvas( backCanvas )

  for i=1, math.ceil(width/8) do
    for j=1, math.ceil(height/8), 1 do
      love.graphics.draw(baseimage, back, (i-1)*8, (j-1)*8)
    end
  end

  --love.graphics.setScissor(0, 0, math.floor(width/8)))
  love.graphics.setCanvas( maskCanvas )
  love.graphics.rectangle("fill", 0, 0, width, height)

  love.graphics.draw(baseimage, quad_mask[1], 0, 0)
  love.graphics.draw(baseimage, quad_mask[7], width- 8, 0)
  love.graphics.draw(baseimage, quad_mask[3], 0, height- 8)
  love.graphics.draw(baseimage, quad_mask[9], width - 8, height - 8)

  love.graphics.setCanvas( {canvas, stencil = true} )

  love.graphics.stencil(function () myStencilFunction(maskCanvas) end, "replace", 1)
  love.graphics.setStencilTest("greater", 0)
  love.graphics.draw(backCanvas)
  love.graphics.setStencilTest()

  love.graphics.setScissor(8, 0, width-15, height)
  for i = 0, math.floor(width/8), 1 do
    love.graphics.draw(baseimage, quad[4], i*8, 0)
    love.graphics.draw(baseimage, quad[6], i*8, height - 8)
  end

  love.graphics.setScissor(0, 8, width, height-15)
  for i = 0, math.floor(height/8), 1 do
    love.graphics.draw(baseimage, quad[2], 0, i*8)
    love.graphics.draw(baseimage, quad[8], width - 8, i * 8)
  end

  love.graphics.setScissor( )

  love.graphics.draw(baseimage, quad[1], 0, 0)
  love.graphics.draw(baseimage, quad[7], width- 8, 0)
  love.graphics.draw(baseimage, quad[3], 0, height- 8)
  love.graphics.draw(baseimage, quad[9], width - 8, height - 8)


  love.graphics.setCanvas( )

  local imagedata = canvas:newImageData()
  local texture   = love.graphics.newImage( imagedata )
  imagedata:release()
  canvas:release()
  return texture
end

function gui.getEmeraldsTexture(number)
  local canvas  = love.graphics.newCanvas(95, 31)
  local emeralds = love.graphics.newImage("assets/gui/emeralds.png")
  love.graphics.setCanvas( canvas )
  for i = 1, 7, 1 do
    local emerald
    local x = (i-1)*12
    if (i > number) then
      emerald = love.graphics.newQuad(0,0,23,17,23*8, 17)
    else
      emerald = love.graphics.newQuad((i*23),0,23,17,23*8, 17)
      --emerald = love.graphics.newQuad((i*23),0,23,17,23*7, 17)
    end

    local isPair = (i%2 == 0)
    local y = 0
    if (isPair == true) then
      y = 14
    end
    love.graphics.draw(emeralds, emerald,x,y)
    emerald:release()
  end

  love.graphics.setCanvas( )
  local imagedata = canvas:newImageData()
  local texture   = love.graphics.newImage( imagedata )
  imagedata:release()
  canvas:release()
  return texture
end

function gui.drawEmptyIcon(x, y)
  local outlineLight = 0.15
  love.graphics.circle("fill", x + 8, y + 8, 2, 8)
  love.graphics.setColor(outlineLight, outlineLight, outlineLight, 1)
  love.graphics.circle("line", x + 8, y + 8, 2, 8)
  utils.graphics.resetColor()
end


return gui
