local CanvasElement = require "birb.modules.gui.elements.canvas"
local ChoiceElement = CanvasElement:extend()

local gui = require "game.modules.gui"

function ChoiceElement:new(name, label1, label2, label3, x, y, approximateWidth)
    self.background = gui.newChoiceBack(approximateWidth)
    local w, h = self.background:getDimensions()
    ChoiceElement.super.new(self, name, x, y, w, h)
    self.label1 = label1
    self.label2 = label2
    self.label3 = label3
end

function ChoiceElement:drawTexture()
    love.graphics.draw(self.background, 0, 0)
    self.assets.fonts["small"]:draw(self.label1, 16, -2, -1, "left")
    self.assets.fonts["small"]:draw(self.label2, self.w/2, -2, -1, "center")
    self.assets.fonts["small"]:draw(self.label3, self.w - 20, -2, -1, "right")
end

return ChoiceElement