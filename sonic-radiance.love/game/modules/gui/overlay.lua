local GuiScreen = require "birb.modules.gui.screen"
local TextureElement = require "birb.modules.gui.elements.drawable"
local ColorElement = require "birb.modules.gui.elements.color"
local TextElement = require "birb.modules.gui.elements.text"

local OverlayScreen = GuiScreen:extend()

local gui         = require "game.modules.gui"
local either      = utils.math.either

local OVERLAY_OPACITY = 0.5
local wasActive = false
local hadVersion = false

local animateAppear = {
    {"upBorder", "movement", 0.0, 0.5, 0, 30, "inOutQuart"},
    {"downBorder", "movement", 0.0, 0.5, 424, 210, "inOutQuart"}
}

local animateDisappear = {
    {"upBorder", "movement", 0.0, 0.5, 0, 0, "inOutQuart"},
    {"downBorder", "movement", 0.0, 0.5, 424, 240, "inOutQuart"},
    {"version", "movement", 0.0, 0.5, 380, 250, "inOutQuart"},
    {"overlayDarken", "tween", 0.0, 0.6, {opacity = 0}, "outExpo"}
}

local showBackground = {
    {"overlayDarken", "tween", 0.0, 0.6, {opacity = OVERLAY_OPACITY}, "inExpo"}
}

local hideBackground = {
    {"overlayDarken", "tween", 0.0, 0.6, {opacity = 0}, "outExpo"}
}


local showBackgroundPause = {
    {"overlayDarken", "tween", 0.0, 0.2, {opacity = OVERLAY_OPACITY}, "inOutQuart"}
}

local hideBackgroundPause = {
    {"overlayDarken", "tween", 0.0, 0.2, {opacity = 0}, "inOutQuart"}
}

local showVersion = {
    {"version", "movement", 0.0, 0.5, 380, 220, "inOutQuart"},
}

local hideVersion = {
    {"version", "movement", 0.0, 0.5, 380, 250, "inOutQuart"},
}

function OverlayScreen:new(active, doShowVersion)
    self.borders = gui.newBorder(424, 30, 8)
    self.doShowVersion = doShowVersion

    if (active == false) then
        wasActive = active
        hadVersion = self.doShowVersion
    end

    OverlayScreen.super.new(self, "overlay")

    local transformStuff = false
    if (active == wasActive) then
        self.isVisible = active
    else
        transformStuff = true
    end

    self:addTransform("show", animateAppear)
    self:addTransform("hide", animateDisappear)
    self:addTransform("showBackground", showBackground)
    self:addTransform("hideBackground", hideBackground)
    self:addTransform("showBackgroundPause", showBackgroundPause)
    self:addTransform("hideBackgroundPause", hideBackgroundPause)
    self:addTransform("showVersion", showVersion)
    self:addTransform("hideVersion", hideVersion)

    if (transformStuff) then
        if (active) then
            self:show()
        else
            self:hide()
        end
    end

    if (active) then
        if (self.doShowVersion) then
            self:playTransform("showVersion")
        else
            self:playTransform("hideVersion")
        end
    end
end

function OverlayScreen:update(dt)
    wasActive = self.isVisible
    hadVersion = self.doShowVersion
    OverlayScreen.super.update(self, dt)
end

function OverlayScreen:createElements()
    local d = either(wasActive, 30, 0)
    local v = either(hadVersion, 30, 0)
    return {
        {TextureElement("upBorder", self.borders, 0, d, 0, 1, -1, 0, 0, 1), 0, -1},
        {TextureElement("downBorder", self.borders, 424, 240 - d, 0, -1, 1, 0, 0, 1), 0, -1},
        {ColorElement("overlayDarken", 0, 0, 0, 0), 0, 5},
        {TextElement("version", "small", "v" .. game.version, 380, 250 - v, "left"), 0, -1}
    }
end

return OverlayScreen