local ParentMenu = require "birb.modules.gui.textmenu"
local FancyMenu  = ParentMenu:extend()

local gui = require "game.modules.gui"

function FancyMenu:new(name, x, y, w, itemNumber, haveBiseau)
  FancyMenu.super.new(self, name, "small", x, y, w, itemNumber)
  self.biseau = utils.math.either(haveBiseau, 8, 0)
  self.itemNumber = itemNumber
  self.box = gui.newChoiceBack(self.w + 24)
  self.cursorTexture = love.graphics.newImage("assets/gui/cursor-menulist.png")
  self.canvas.padding = 24
end

function FancyMenu:getDimensions()
    local w, h = FancyMenu.super.getDimensions(self)
    return w + (self.biseau*self.itemNumber), h
end

function FancyMenu:getListPart(i)
    local x, y, w, h = FancyMenu.super.getListPart(self, i)
    return x + (self.biseau*i), y, w, h
end

function FancyMenu:drawWidgetBackground(x, y, w, h)
    love.graphics.draw(self.box, x - 8, y + 2)
end

function FancyMenu:drawGraphicalCursor(x, y, w, h)
    love.graphics.draw(self.cursorTexture, x - 3, y + 1)
end

return FancyMenu
