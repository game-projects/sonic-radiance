local CanvasElement = require "birb.modules.gui.elements.parent"
local ActionPrompt = CanvasElement:extend()

function ActionPrompt:new()
    self.text = ""
    ActionPrompt.super.new(self, "actionPrompt", 0, 0, 424, 240)
    self.depth = -2
    self.opacity = 0
    self.isShown = false
end

function ActionPrompt:setText(text)
    if (utils.string.isEmpty(text)) then
        self:hide()
    else
        self.text = text
        self:show()
    end
end

function ActionPrompt:hide()
    if (self.isShown) then
        self.tweens.tweens = {}
        self:newTween(0.0, 0.6, {opacity = 0}, "outExpo")
    end
    self.isShown = false
end

function ActionPrompt:show()
    if (not self.isShown) then
        self.tweens.tweens = {}
        self:newTween(0.0, 0.6, {opacity = 1}, "outExpo")
    end
    self.isShown = true
end

function ActionPrompt:draw()
    if (not utils.string.isEmpty(self.text)) then
      local w = self.assets.fonts["small"]:getWidth(self.text) + 16
      love.graphics.setColor(0,0,0,0.5 * self.opacity)
      local x, y = 424 - w/2 - 4, 240 - 20
      love.graphics.rectangle("fill", x - w/2, y + 1, w, 15, 8, 8)
      love.graphics.setColor(1, 1, 1, self.opacity)
      self.assets.fonts["small"]:setColor(1, 1, 1, self.opacity)
      self.assets.fonts["small"]:draw(self.text, x, y, -1, "center")
      self.assets.fonts["small"]:setColor(1, 1, 1, 1)
      utils.graphics.resetColor()
    end
end


return ActionPrompt