local TextureElement = require "birb.modules.gui.elements.parent"
local MenuBack = TextureElement:extend()

local backx = 0
local bordery = 0
local turn = 0

local fancyBackShader = love.graphics.newShader[[
    uniform number screenWidth;
    uniform number screenHeight;
    vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
      vec4 pixel = Texel(texture, texture_coords); //This is the current pixel color
      number value = (screen_coords.x / screenWidth) * (screen_coords.y / screenHeight) * 0.5;
      number lighten = 0.25;
      return vec4(0.5 + value + lighten,0.0 + lighten,0.5 - value + lighten, 1 - pixel.r);
      //return vec4(1,1,1, pixel.r);
    }
  ]]

function MenuBack:new()
    self.back = love.graphics.newImage("assets/gui/back/background.png")
    self.border = love.graphics.newImage("assets/gui/back/border.png")
    self.emblem = love.graphics.newImage("assets/gui/back/emblem.png")
    self.star = love.graphics.newImage("assets/gui/back/star.png")
    self.backImage = love.graphics.newImage("assets/artworks/back.png")

    self.canvas = nil
    local w, h = love.graphics.getDimensions()
    fancyBackShader:send("screenWidth",w)
    fancyBackShader:send("screenHeight",h)
    MenuBack.super.new(self, "menuBack", 0, 0, w, h)
    self.depth = 200
end


function MenuBack:update(dt)
    backx = (backx + dt * 20) % 96
    bordery = (bordery + dt * 35) % 160
    turn = turn + (dt/1.5) % 1

    self.canvas = love.graphics.newCanvas( 424, 240 )
    love.graphics.setCanvas(self.canvas)
    for i = 0, (math.ceil(424/96)), 1 do
        for j = 0, (math.ceil(240/96)), 1 do
            love.graphics.draw(self.back, backx + ((i - 1 ) * 96), backx + ((j - 1 ) * 96))
        end
    end

    for j = 0, (math.ceil(240/160)), 1 do
        love.graphics.draw(self.border, 0, bordery + ((j - 1) * 160))
    end
    love.graphics.draw(self.emblem, 424, 240 - 32, 0, 0.8, 0.8, 200, 200)
    love.graphics.draw(self.star, 424, 240 - 32, turn, 0.8, 0.8, 200, 200)
    love.graphics.setColor(1, 1, 1, 1)

    love.graphics.setCanvas()
end


function MenuBack:draw()
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle("fill", 0, 0, 424, 240)
    love.graphics.setShader(fancyBackShader)
    if (self.canvas ~= nil) then
        love.graphics.draw(self.canvas, 0, 0)
    end
    love.graphics.setShader()
    utils.graphics.resetColor()
    love.graphics.draw(self.backImage, 0, 0)
end

return MenuBack