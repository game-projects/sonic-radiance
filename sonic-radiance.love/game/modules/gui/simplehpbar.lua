local SimpleHPBar = Object:extend()

local TweenManager = require "birb.classes.time"
local gui         = require "game.modules.gui"

function SimpleHPBar:new(hp)
  self.tweens = TweenManager(self)
  self.hp = hp
  self.baseHP = hp
end

function SimpleHPBar:setHP(newHP)
  self.tweens:newTween(0, 0.1, {hp = newHP}, 'inCubic')
end

function SimpleHPBar:update(dt)
  self.tweens:update(dt)
end

function SimpleHPBar:draw(x, y)
  love.graphics.setColor(0, 0, 0, 1)
  gui.drawBar(x, y, 26, 4)
  love.graphics.rectangle("fill", x, y, 24, 4)
  love.graphics.setColor(248/255, 160/255, 0, 1)
  local bar = math.max(0, math.floor(22 * (self.hp / self.baseHP)))
  love.graphics.rectangle("fill", x + 1, y + 1, math.floor(bar), 2)
  love.graphics.setColor(1, 1, 1, 1)
end

return SimpleHPBar
