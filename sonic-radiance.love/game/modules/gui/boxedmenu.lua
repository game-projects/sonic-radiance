local ParentMenu = require "birb.modules.gui.textmenu"
local BoxedList = ParentMenu:extend()

local gui = require "game.modules.gui"

function BoxedList:new(name, x, y, w, slotNumber, isBoxed, smallborder)
    BoxedList.super.new(self, name, "small", x, y, w, slotNumber, 0)
    self.paddingLeft = 12
    self.paddingRight = 6
    self.canvas.padding = 24

    self.cursorTexture = love.graphics.newImage("assets/gui/cursor-menulist.png")
    self.cursorTransition = 0

    self.isBoxed = isBoxed
    self.border = utils.math.either(smallborder, 8, 16)
    self.box = gui.newTextBox("assets/gui/dialogbox.png", self.w, self.h + self.border)
end

function BoxedList:finalize()
    self:setCancelWidget()
end

function BoxedList:update(dt)
    BoxedList.super.update(self, dt)
end

function BoxedList:drawTexture()
    if (self.isBoxed) then
        local w, h = self.box:getDimensions()
        love.graphics.draw(self.box, self.canvas.padding, self.canvas.padding - self.border/2)
    end
    BoxedList.super.drawTexture(self)
end

function BoxedList:drawGraphicalCursor(x, y, w, h)
    love.graphics.draw(self.cursorTexture, x, y + 1)
end

return BoxedList
