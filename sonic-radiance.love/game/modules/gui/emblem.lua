local Emblem = Object:extend()
local greyscale = require "game.modules.drawing.greyscale"

function Emblem:new(abstract, scene)
  self.assets = scene.assets
  self.abstract = abstract

  self.charid = self.abstract.simplename
  self.stats = self.abstract:getStats()
end

function Emblem:draw(x, y)
  self:drawBackground(x, y)
  self:drawForeground(x, y)
end

function Emblem:drawForeground(x, y)
  local emblem2 = "m_" .. self.abstract.data.class
  core.screen:setScissor(x, y-16, 32, 40)
  if (self.abstract.hp > 0) then
    self.assets.sprites[self.charid]:draw(x+14, y+14)
  else
    greyscale.startShader()
    self.assets.sprites[self.charid]:drawFrame(1, x+14, y+14)
  end
  core.screen:resetScissor( )
  self.assets.images[emblem2]:draw(x, y)
  greyscale.endShader()
end

function Emblem:drawBackground(x, y)
  local emblem1 = "e_" .. self.abstract.data.class

  if (self.abstract.hp > 0) then
    self.assets.images[emblem1]:draw(x, y)
  else
    greyscale.startShader()
    self.assets.images[emblem1]:draw(x, y)
    greyscale.endShader()
  end

end


return Emblem
