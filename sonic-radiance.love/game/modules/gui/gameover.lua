local Screen = require "birb.modules.gui.screen"
local GameOverScreen = Screen:extend()

local TextElement = require "birb.modules.gui.elements.text"
local ConfirmDialog = require "game.modules.confirmdialog"

local defTransitions = require "birb.modules.transitions"

local HEIGHT = 40

local show = {
    {"gameText", "movement", 0.9, 0.4, 424/2 - 4, HEIGHT, "inExpo"},
    {"overText", "movement", 0.9, 0.4, 424/2 + 4, HEIGHT, "inExpo"}
  }

function GameOverScreen:new()
    GameOverScreen.super.new(self, "titleScreen")
    self:addTransform("show", show)
    self.scene:showOverlay(true)
    self:show()
end

function GameOverScreen:createElements()
    return {
        {TextElement("gameText", "SA2font", "GAME", 0, HEIGHT, "right"), 0, 1},
        {TextElement("overText", "SA2font", "OVER", 424, HEIGHT, "left"), 0, 1},
        {ConfirmDialog(self.scene, "Do you want to return to title ? \nYou can also reload your latest save.",
            function() self:returnToTitle() end, "Return to title",
            function() self:loadLastSave() end, "Reload last save"),
        1.8, 0}
    }
end

function GameOverScreen:returnToTitle()
  core.screen:startTransition(defTransitions.default, defTransitions.circle, function() scenes.menus.title(true) end, 424/2, 240/2)
end

function GameOverScreen:loadLastSave()
  self.scene.tweens:newTween(0, 0.3, {borderPosition=0}, "inOutQuad")
  core.screen:startTransition(defTransitions.default, defTransitions.default, function() game:reload() scenes.overworld() end, 424/2, 240/2)
end


return GameOverScreen