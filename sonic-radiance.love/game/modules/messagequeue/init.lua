local GuiElement = require "birb.modules.gui.elements.parent"
local MessageQueue = GuiElement:extend()
local Message = require "game.modules.messagequeue.message"
local MAX = 3

function MessageQueue:new(scene)
    self.messages = {}
    self.permaMessage = nil
    self.scene = scene
    MessageQueue.super.new(self, "messageQueue", 0, 0, 424, 240)
    self.depth = 0
end

function MessageQueue:addMessage(newMessage)
    for _, message in ipairs(self.messages) do
        message:move()
    end
    table.insert(self.messages, 1, Message(self.scene, newMessage, false))
    self.messages[MAX + 1] = nil
end

function MessageQueue:pinMessage(message)
    if (self.permaMessage ~= nil and self.permaMessage.text ~= message) then
        self.permaMessage = Message(self.scene, message, true)
    end
end

function MessageQueue:update(dt)
    for _, message in ipairs(self.messages) do
        message:update(dt)
    end
end

function MessageQueue:draw()
    for _, message in ipairs(self.messages) do
        message:draw()
    end
end

return MessageQueue