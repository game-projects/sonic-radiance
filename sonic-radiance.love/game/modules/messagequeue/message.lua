local Message = Object:extend()
local TweenManager = require "birb.classes.time"

local SPACING = 20
local PLAYER_MESSAGE = 240 - 24

function Message:new(scene, message, isPinned)
    self.message = message
    self.scene = scene
    assert(message ~= nil, "the message must be set")
    self.isPinned = isPinned or false
    self.opacity = 0
    self.y = 0

    self.tweens = TweenManager(self)
    self.tweens:newTween(0, 0.2, {opacity = 1}, "inOutCubic")
    self.tweens:newTween(1, 0.2, {opacity = 0}, "inOutCubic")
end

function Message:move()
    self.tweens:newTween(0, 0.2, {y = self.y - SPACING}, "inOutCubic")
end

function Message:update(dt)
    self.tweens:update(dt)
end

function Message:draw()
    local yTransparency = math.max(0, math.min(1, (math.abs(self.y) - 32) / SPACING))
    local opacity = math.max(0, math.min(1, self.opacity - yTransparency))

    love.graphics.setColor(0, 0, 0, 0.5 * opacity)
    if (opacity > 0) then
        love.graphics.rectangle("fill", 0, PLAYER_MESSAGE + self.y, 424, 16)
        self.scene.assets.fonts["small"]:setColor(1, 1, 1, opacity)
        self.scene.assets.fonts["small"]:draw(self.message, 424 / 2, PLAYER_MESSAGE - 1 + self.y, -1, "center")
        self.scene.assets.fonts["small"]:setColor(1, 1, 1, 1)
    end
    utils.graphics.resetColor()
end

return Message
