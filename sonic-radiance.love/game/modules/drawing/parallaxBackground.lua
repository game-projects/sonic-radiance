local ParallaxBackground = Object:extend()

local maputils = require "scenes.battlesystem.utils"

function ParallaxBackground:new(scene, height, bottomBorder, type)
  self.scene  = scene
  self.assets = self.scene.assets

  self.datas = {}
  self.datas.type       = type or "forest"

  self.height = height
  self.bottomBorder = bottomBorder

  local zones = require "datas.gamedata.maps.shoot.zones"
  local datas = zones[self.datas.type]
  self.datas.background = datas.background
  self.datas.tiles      = datas.tiles
  self.datas.borders    = datas.borders

  self.assets:addTileset("normaltiles", "assets/backgrounds/normaltile")
  self.assets:addTileset("borders", "assets/backgrounds/borders")

  local backpath = "assets/backgrounds/parallax/" .. self.datas.background
  self.assets:addImage("back1", backpath .. "-back.png")
  self.assets:addImage("back2", backpath .. "-fore.png")
  self.assets:addImage("cliff", backpath .. "-cliff.png")

  self.texture = {}
  self.texture.floor  = self:generateFloor(datas.tiles)
end

-- GET FUNCTIONS
-- Get information from the map

function ParallaxBackground:getTotalHeight()
  return self.height + self.bottomBorder;
end

function ParallaxBackground:getTerrain(x, y)
  if (y <= self.height) then
    return 0
  else
    return nil
  end
end

function ParallaxBackground:isInGrid(x, y)
  return ( self:getTerrain(x, y) ~= nil )
end

function ParallaxBackground:getStartY()
  return 240 - (self:getTotalHeight() * 20) - 10
end

function ParallaxBackground:gridToPixel(x, y, center)
  local pixelx, pixely
  local center = center or false
  local x, y = x, y

  if (center) then
    x = x + .5
    y = y + .5
  end

  pixelx = maputils.CONST.STARTX + ((x-1) * 31) + ((y-1) * 10)
  pixely = self:getStartY() + ((y-1) * 20)

  return math.floor(pixelx), math.floor(pixely)
end

-- DRAW FUNCTIONS
-- Draw the battle map

function ParallaxBackground:generateFloor(tile)
  local canvas = love.graphics.newCanvas(31*16, self:getTotalHeight() * 20)
  local tile    = tile or 1

  love.graphics.setCanvas( canvas )

  for i=1, self:getTotalHeight() do
    for j=0, 18 do
      local tiley = (i-1)*20
      local tilex = (j-2)*31 + (i-1)*10
      local variant = 1 + ((i + j) % 2)
      local tiles = self.datas.tiles*2 + variant
      if (not self:isInGrid(j, i)) then
        love.graphics.setColor(.66, .66, .66, 1)
      end
      self.assets.tileset["normaltiles"]:drawTile(tiles, tilex, tiley)
      utils.graphics.resetColor()
    end
  end

  love.graphics.setCanvas( )

  local imagedata = canvas:newImageData()
  local texture   = love.graphics.newImage( imagedata )
  imagedata:release()
  canvas:release()
  return texture
end

function ParallaxBackground:draw()
  self:drawParallax(-maputils.CONST.STARTX, -self:getStartY(), 424, 240)
end

function ParallaxBackground:drawParallax(x, y, w, h)
  self:drawBackground(x, y, w, h)
  self:drawForeground(x, y, w, h)
  --love.graphics.draw(self.texture.floor, maputils.CONST.STARTX, self:getStartY())
  local w2, _ =  self.texture.floor:getDimensions()
  for i=1, 2 do
    local x2 = x % w2
    love.graphics.draw(self.texture.floor, ((i-1)*31*16)-x2, -y)
  end
  self:drawBorders(x, y, w, h)
end


function ParallaxBackground:drawBackgrounds()
  local w, _    = core.screen:getDimensions()

  local w2, h2 = self.assets.images["back1"]:getDimensions()
  local imax = math.ceil(w / w2) + 1
  for i=1, imax do
    self.assets.images["back1"]:draw((i-1)*w2, 0, 0, 1, 1)
  end

  local w2, h2  = self.assets.images["back2"]:getDimensions()
  local imax = math.ceil(w / w2) + 1
  for i=1, imax do
    self.assets.images["back2"]:draw((i-1)*w2, self:getStartY()-h2, 0, 1, 1)
  end

end

function ParallaxBackground:drawBackground(x, y, w, h)
  local w2, h2 = self.assets.images["back1"]:getDimensions()
  local imax = math.ceil(w / w2) + 1
  for i=1, imax do
    local x1 = (x / 5) % w2
    self.assets.images["back1"]:draw((i-1)*w2 - x1, 0, 0, 1, 1, 0, 0)
  end
end

function ParallaxBackground:drawForeground(x, y, w, h)
  local w2, h2 = self.assets.images["back2"]:getDimensions()
  local imax = math.ceil(w / w2) + 1
  for i=1, imax do
    local x1 = (x / 2) % w2
    self.assets.images["back2"]:draw((i-1)*w2 - x1, -y-10, 0, 1, 1, 0, h2)
  end
end

function ParallaxBackground:drawBorders(x, y)
  local border = self.datas.borders + 1
  for i=1, 7 do
    local x2 = x % 80
    self.assets.tileset["borders"]:drawTile(border, (i-1)*80 - x2, -y, 0, 1, 1, 0, 10)
    self.assets.tileset["borders"]:drawTile(border, (i-1)*80 - x2, -(y-self:getTotalHeight()*20), 0, 1, 1)
  end
end

return ParallaxBackground
