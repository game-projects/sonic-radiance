local GuiElement = require "birb.modules.gui.elements.canvas"
local ConfirmDialog = GuiElement:extend()

local gui = require "game.modules.gui"

local WIDTH = 256
local PADWIDTH = 16
local PADHEIGHT = 16
local DEFAULT_LINES = 2

function ConfirmDialog:new(scene, message, choice1func, choice1, choice2func, choice2)
    self.scene = scene
    self.lines = DEFAULT_LINES
    self.message = message

    self.choiceLabel = {}
    self.choiceFunc = {}
    self.choiceSound = {}
    self.choiceLabel[1] = choice1 or "Yes"
    self.choiceLabel[2] = choice2 or "No"
    self.choiceFunc[1] = choice1func
    self.choiceFunc[2] = choice2func or function() self:dismiss() end
    self.choiceSound[1] = "mSelect"
    self.choiceSound[2] = "mBack"
    self.autoDismiss = false

    self.darken = true

    self.currentChoice = 0
    self.cancelChoice = -1

    ConfirmDialog.super.new(self, "dialog", 424/2, 240/2, WIDTH + PADWIDTH, self:computeHeight())
    self.back = gui.newTextBox("assets/gui/dialogbox.png", self.w, self.h)
    self.ox = self.w/2
    self.oy = self.h/2
    self.canvas.padding = 0
    self.depth = 0
    self.isVisible = 1
    self:getFocus()
end

function ConfirmDialog:setLines(lines)
    self.lines = lines
    self.h = self:computeHeight()
    self.oy = self.h/2
    self.canvas.needRedraw = true
    self.back = gui.newTextBox("assets/gui/dialogbox.png", self.w, self.h)
end

function ConfirmDialog:computeHeight()
    return 32 + (self.lines * 16) + PADHEIGHT
end

function ConfirmDialog:setCancelChoice(choice)
    self.cancelChoice = choice - 1
end

function ConfirmDialog:keypressed(key)
    if (key == "up" or key == "down") then
        self.currentChoice = (self.currentChoice + 1) % 2
        self.scene.assets.sfx["mBeep"]:play()
        self.canvas.needRedraw = true
    elseif (key == "A") then
        self:doAction(self.currentChoice)
    elseif (key == "B") then
        self:doAction(self.cancelChoice)
    end
end

function ConfirmDialog:doAction(choice)
    if (self.choiceFunc[choice + 1] ~= nil) then
        self.scene.assets.sfx[self.choiceSound[choice + 1]]:play()
        self.choiceFunc[choice + 1]()
        if (self.autoDismiss) then
            self:dismiss()
        end
    end
end

function ConfirmDialog:dismiss()
    self.gui:setLastFocus()
    self:destroy()
end

function ConfirmDialog:drawTexture()
    love.graphics.draw(self.back, 0)
    local padx, pady = 8, 6
    self.scene.assets.fonts["small"]:draw(self.message ,padx, pady, WIDTH,"left")
    for i = 1, 2, 1 do
        self.scene.assets.fonts["small"]:draw(self.choiceLabel[i], padx + 8, pady + (self.lines + i - 1)*16)
        if ((self.currentChoice + 1) == i) then
            self.scene.assets.fonts["small"]:draw(">", padx, pady + (self.lines + i - 1)*16)
        end
    end
end

return ConfirmDialog