local DecalTransition = require "birb.modules.transitions.decal"
local SonicTransition = DecalTransition:extend()

function SonicTransition:new(func, ox, oy, fadeOut)
    SonicTransition.super.new(self, func, ox, oy, fadeOut, "sonicdecal")
end

return SonicTransition