local CanvasTransition = require "birb.modules.transitions.canvas"
local ZigZagTransition = CanvasTransition:extend()

function ZigZagTransition:new(func, ox, oy, fadeOut)
    self.offset = 0
    ZigZagTransition.super.new(self, func, ox, oy, fadeOut, "inQuad", "outQuad", 1, 0.1)

end

function ZigZagTransition:loadResources()
    self.border = love.graphics.newImage("assets/transitions/border.png")
end

function ZigZagTransition:drawCanvas(dt)
    local w, _ = self.border:getDimensions()
    self.offset = (self.offset + (dt * 256)) % w
    utils.graphics.resetColor()
    love.graphics.rectangle("fill", 0, 0, 424, 240)
    love.graphics.setColor(0,0,0,1)
    local max = 120 + 16
    self:drawBorder(0 + max * (self.value), false)
    utils.graphics.resetColor()
    self:drawBorder(240 - max * (self.value), true)
end

function ZigZagTransition:drawBorder(y, reversed)
    local offset = self.offset
    local sy = 1
    if (reversed) then
        offset = self.offset * -1
        sy = -1
    end
    local w, h = self.border:getDimensions()
    local ox, oy = 0, h
    for i = -1, math.ceil(424 / w), 1 do
        love.graphics.draw(self.border,(i * w) + offset,y,0,1,sy,ox,oy)
    end

end

return ZigZagTransition