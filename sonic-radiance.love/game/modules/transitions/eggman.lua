local DecalTransition = require "birb.modules.transitions.decal"
local EggmanTransition = DecalTransition:extend()

function EggmanTransition:new(func, ox, oy, fadeOut)
    EggmanTransition.super.new(self, func, ox, oy, fadeOut, "eggdecal")
end

return EggmanTransition