local EnnemyManager = Object:extend()

local AbstractEnnemy = require "game.abstractmobs.ennemy"

function EnnemyManager:new(controller)
  self.controller = controller
end

function EnnemyManager:getEnnemyData(category, ennemy)
  return AbstractEnnemy(category, ennemy)
end

return EnnemyManager
