local Serializable = require "birb.classes.serializable"
local Difficulty = Serializable:extend()

function Difficulty:new()
    self.toggles = {}
    self.toggles.hazardMakesKo = false
    self.toggles.playerKoChar = true
    self.toggles.easierBattles = false
    self.toggles.checkPointRegen = false
    self.toggles.levelUpHeal = false
    self.toggles.allDamage = true
    Difficulty.super.new(self, {"toggles"})
end

function Difficulty:toggle(toggleName)
    self.toggles[toggleName] = (self.toggles[toggleName] == false)
end

function Difficulty:get(toggleName)
    return self.toggles[toggleName]
end

return Difficulty