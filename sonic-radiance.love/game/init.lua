-- game :: The main game subsystem. Basically a big object that handle all the
-- game-related data like characters, monsters, etc. While the core aim to be
-- reusable at will, the game is specifically made for the current game.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Serializer  = require "birb.classes.serializable.serializer"

local Game        = Serializer:extend()
local Characters  = require "game.characters"
local Ennemies    = require "game.ennemies"
local Loot        = require "game.loot"
local CBSCore     = require "game.battle"
local Difficulty  = require "game.difficulty"
local Metadata    = require "game.metadata"

local startdata   = require "datas.gamedata.startdata"

Game.utils  = require "game.utils"
Game.gui    = require "game.modules.gui"

local VAR_TO_SERIALIZE = {
  "gametime",
  "destroyedGizmo",
  "variables",
  "flags",
  "position",
  "actions",
  "characters",
  "loot",
  "difficulty"
}

function Game:new()
  self.slot = -1
  self.slotNumber = 3
  self.version = core.conf.gameversion or "N/A"
  self:reset()
  self.metadata = Metadata(self)
  Game.super.new(self, VAR_TO_SERIALIZE)
end

function Game:initPosition()
  self.position = {}
  self.position.x = startdata.position.x
  self.position.y = startdata.position.y
  self.position.area = startdata.position.area
end

function Game:getMetadataFile(absolute)
  local dir = ""
  if absolute then
    dir = love.filesystem.getSaveDirectory() .. "/"
    if not utils.filesystem.exists(dir) then
      love.filesystem.createDirectory( "" )
    end
  end

  local filepath = dir .. "metadata.save"

  return filepath
end

function Game:getMetadata()
  return self.metadata:get()
end

function Game:reset()
  self.gametime = 0

  self.characters = Characters(self)
  self.ennemies   = Ennemies(self)
  self.loot       = Loot(self)
  self.cbs        = CBSCore(self)
  self.difficulty = Difficulty(self)

  self:initPosition()

  self.flags = {}
  self.destroyedGizmo = {}
  self.variables = {}
  self.completion = 0
  self.mapName = ""
  self.actions = startdata.actions
end

function Game:reload()
  self:read(self.slot)
end

function Game:read(save_id)
  self.slot = save_id
  if (self.slot > 0) then
    self:deserialize(self:getSaveName())
  end
end

function Game:deleteCurrentSave()
  if (self.slot > 0) then
    self:delete(self:getSaveName())
    self.metadata:remove(self.slot)
  end
end

function Game:write()
  if (self.slot > 0) then
    self:serialize(self:getSaveName())
    self.metadata:update()
  end
end

function Game:getSaveName(saveslot)
  local saveslot = saveslot or self.slot
  return "save" .. saveslot .. ".save"
end

function Game:resetSaves()
  for i=1, self.slotNumber do
    self:delete(self:getSaveName(i))
  end
end

function Game:update(dt)
  self.gametime = self.gametime + dt
end

function Game:getTime()
  return utils.time.getFields(self.gametime)
end

function Game:getTimeString() 
  return utils.time.toString(self.gametime)
end

function Game:printTime()
  core.debug:print(self:getTimeString())
end

return Game
