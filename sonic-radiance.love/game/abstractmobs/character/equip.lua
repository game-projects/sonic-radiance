local CharacterEquip = Object:extend()

local categories = {"gloves", "shoes", "accessories"}

function CharacterEquip:initEquip()
    self.equip = {}
    if self.data.inventory == nil then
        core.debug:warning("character/equip", "Initial equip not set for " .. self.simplename)
        for _, category in ipairs(categories) do
            self.equip[category] = ""
        end
    else
        for _, category in ipairs(categories) do
            self.equip[category] = self.data.inventory[category] or ""
        end
    end 
end

function CharacterEquip:setEquip(category, name)
    if (not utils.string.isEmpty(self.equip[category])) then
        game.loot:addItem(category, self.equip[category], 1)
    end
    self.equip[category] = name
    game.loot:removeItem(category, name, 1)
    self:updateHPPP()
end

function CharacterEquip:removeEquip(category)
    if (not utils.string.isEmpty(self.equip[category])) then
        game.loot:addItem(category, self.equip[category], 1)
        self.equip[category] = ""
        self:updateHPPP()
    end
end

function CharacterEquip:predictStat(statName, category, name)
    return self.stats:predictStat(statName, category, name)
end

function CharacterEquip:getEquipStats(stat, ignore)
    local boost = 0
    local ignore = ignore or ""
    for _, category in ipairs(categories) do
        if (category ~= ignore) then
            boost = boost + self:getEquipStatByType(stat, category)
        end
    end
    return boost
end

function CharacterEquip:getEquipStatByType(stat, category)
    if (not utils.string.isEmpty(self.equip[category])) then
        local data = core.datas:get("items", self.equip[category])
        local boost = data.statsBoost[stat] or 0
        return boost
    end
    return 0
end

return CharacterEquip