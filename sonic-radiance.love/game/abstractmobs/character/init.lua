local AbstractMobParent = require "game.abstractmobs.parent"
local AbstractCharacter = AbstractMobParent:extend()

local CharacterHealth = require "game.abstractmobs.character.health"
local CharacterLevels = require "game.abstractmobs.character.levels"
local CharacterDatas = require "game.abstractmobs.character.datas"
local CharacterEquip = require "game.abstractmobs.character.equip"
AbstractCharacter:implement(CharacterHealth)
AbstractCharacter:implement(CharacterLevels)
AbstractCharacter:implement(CharacterDatas)
AbstractCharacter:implement(CharacterEquip)

local CharacterStatManager = require "game.abstractmobs.character.statmanager"

function AbstractCharacter:new(name)
    self.simplename = name
    AbstractCharacter.super.new(self, {"simplename", "level", "exp", "exp_next", "hp", "pp", "statuts", "equip"}, nil, CharacterStatManager)
    self:updateHPPP()
end

function AbstractCharacter:initBasicElements()
    self:getCommonData()
    self:initEquip()
    self:initLevel()
end

function AbstractCharacter:updateHPPP()
    if (self.hp ~= nil) then
        self.hp = math.min(self.hp, self.stats:get(self.stats.HPMAX))
    end
    if (self.pp ~= nil) then
        self.pp = math.min(self.pp, self.stats:get(self.stats.PPMAX))
    end
end

function AbstractCharacter:getStat(statName, ignoreEquip)
    return self.stats:get(statName, ignoreEquip)
end

function AbstractCharacter:createSkills()
    local learnedlist = {}

    for i, v in ipairs(self.data.skills) do
        local tech_name, tech_level, isLearned = v[1], v[2], false
        if tech_level <= self.level then
            local canLearn = true
            for i, learnedSkill in ipairs(learnedlist) do
                -- We check if the skill have already been learned, to level-up it
                if learnedSkill.name == tech_name then
                    canLearn = false
                    learnedSkill.level = learnedSkill.level + 1
                end
            end

            if (canLearn) then
                local skilldata = {}
                skilldata.name = tech_name
                skilldata.level = 1
                skilldata.learnedAt = tech_level

                table.insert(learnedlist, skilldata)
            end
        end
        -- On continue ensuite d'itérer dans la liste
    end

    return learnedlist
end

function AbstractCharacter:finishDeserialization()
    self.skills = self:createSkills()
end

return AbstractCharacter
