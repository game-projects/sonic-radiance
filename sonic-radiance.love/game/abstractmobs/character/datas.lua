local CharacterData = Object:extend()

function CharacterData:getCommonData()
    self.data = core.datas:get("characters", self.simplename)
    self.name = self.data.name
    self.fullname = self.data.fullname
    self.turns = self.data.turns
end

function CharacterData:getWeaknesses()
    return self.data.weakTo
end

function CharacterData:getResistences()
    return self.data.resists
end

function CharacterData:getSkillName(skill)
    local skilldata = core.datas:get("skills", skill)
    local name = skilldata.fullname


    if (skilldata.altName ~= nil) then
        if (skilldata.altName[self.simplename] ~= nil) then
            name = skilldata.altName[self.simplename]
        end
    end
    print(name)
    return name
end

return CharacterData
