local CharacterLevel = Object:extend()

local charutils = require "game.utils.characters"
local STATS = require "datas.consts.stats"

function CharacterLevel:initLevel()
    self.level     = self.data.startlevel
    self.exp       = charutils.getExpValue(self.level)
    self.exp_next  = charutils.getExpValue(self.level + 1)
end

function CharacterLevel:setLevel(newlevel)
    self.level = newlevel
    local exp
    local exp_min = charutils.getExpValue(self.level)
    local exp_max = charutils.getExpValue(self.level + 1)
    exp = self.exp

    self.exp = math.max(math.min(exp, exp_max - 1), exp_min)
    self.exp_next = exp_max

    self:updateHPPP()
    self.skills = self:createSkills()
end

function CharacterLevel:levelUp()
    if (game.difficulty:get("levelUpHeal")) then
        self:heal()
    end
    self:setLevel(self.level + 1)
end

function CharacterLevel:getLevelStat(statname)
    local baseStat = self.data.stats[statname]
    if (baseStat == nil) then
        error("Stat " .. statname .. " doesn't exist")
    else
        local stat = 0

        if (statname == "hpmax") then
            stat = charutils.getHPValue(self.level, baseStat)
        elseif (statname == "ppmax") then
            stat = charutils.getPPValue(self.level, baseStat)
        else
            stat = charutils.getStatValue(self.level,  baseStat)
        end

        return stat
    end
end

function CharacterLevel:getComputedStat(statname)
    core.debug:warning("CharacterLevel", "Function getComputedStat is deprecated")
    return self:getLevelStat(statname)
end

function CharacterLevel:getStatList()
    return STATS.LIST
end

return CharacterLevel
