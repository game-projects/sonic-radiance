local StatManager = require "game.abstractmobs.statmanager"
local CharacterStatManager = StatManager:extend()

function CharacterStatManager:new(owner)
    CharacterStatManager.super.new(self, owner)
end

function CharacterStatManager:computeStat(statname, ignoreEquip)
    local stat = self:getBaseStat(statname) + self.owner:getEquipStats(statname, ignoreEquip)
    return stat
end

function CharacterStatManager:predictStat(statname, category, name)
    local data = core.datas:get("items", name)
    local boost = data.statsBoost[statname] or 0
    return self:computeStat(statname, category) + boost
end

function CharacterStatManager:getBaseStat(statname)
    if (self:isBattleStat(statname)) then
        return self:getBattleStat(statname)
    else
        return self.owner:getLevelStat(statname)
    end
end

return CharacterStatManager