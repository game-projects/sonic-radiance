local StatManager = Object:extend()
local CONST = require "datas.consts.stats"
StatManager.CONST = CONST

StatManager.HPMAX = CONST.HPMAX
StatManager.PPMAX = CONST.PPMAX
StatManager.ATTACK = CONST.ATTACK
StatManager.POWER = CONST.POWER
StatManager.DEFENSE = CONST.DEFENSE
StatManager.MIND = CONST.MIND
StatManager.TECHNIC = CONST.TECHNIC
StatManager.SPEED = CONST.SPEED

function StatManager:new(owner, battleStatType)
    local battleStatType = battleStatType or "HERO"
    self.owner = owner
    self.battleStats = CONST.BATTLESTAT[battleStatType]
end

function StatManager:get(statname)
    return self:computeStat(statname)
end

function StatManager:computeStat(statname)
    return self.list[statname]
end

function StatManager:isBattleStat(statname)
    return utils.table.contain(CONST.BATTLELIST, statname)
end

function StatManager:getBattleStat(statname)
    return self.battleStats[statname]
end

return StatManager