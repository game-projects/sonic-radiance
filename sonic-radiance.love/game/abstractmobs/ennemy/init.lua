local AbstractMobParent = require "game.abstractmobs.parent"

local AbstractEnnemy = AbstractMobParent:extend()
local elements = require "datas.gamedata.battles.elements"
local EnnemyStatManager = require "game.abstractmobs.ennemy.statmanager"

function AbstractEnnemy:new(directory, name)
  self.simplename = name
  self.directory = directory
  AbstractEnnemy.super.new(self, nil, nil, EnnemyStatManager)
end

function AbstractEnnemy:getWeaknesses()
  local elementData = elements[self.data.element] or elements["none"]
  return elementData.weakTo
end

function AbstractEnnemy:getResistences()
  local elementData = elements[self.data.element] or elements["none"]
  return elementData.resists
end

function AbstractEnnemy:haveProtecType(protectype)
  return utils.table.contain(self.data.protectypes, protectype)
end

function AbstractEnnemy:getProtecTypes()
  return self.data.protectypes
end

function AbstractEnnemy:initBasicElements()
  self.data = core.datas:get("ennemies", self.simplename)
  self.name = self.data.name
  self.fullname = self.data.fullname
  self.turns = self.data.turns
end

function AbstractEnnemy:createSkills()
  return self.data.skills
end

function AbstractEnnemy:setBonus(pvFactor, statFactor)
  self.stats:setBonus(pvFactor, statFactor)
  self.hp = self.stats:get(self.stats.HPMAX)
end

return AbstractEnnemy
