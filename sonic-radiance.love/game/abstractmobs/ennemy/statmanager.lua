local StatManager = require "game.abstractmobs.statmanager"
local EnnemyStatManager = StatManager:extend()

function EnnemyStatManager:new(owner)
    EnnemyStatManager.super.new(self, owner, "ENNEMI")
    self.pvFactor = 1
    self.statFactor = 1
end

function EnnemyStatManager:setBonus(pvFactor, statFactor)
    self.pvFactor = pvFactor or 1
    self.statFactor = statFactor or 1
end

function EnnemyStatManager:computeStat(statname)
    local stat = self.owner.data.stats[statname]

    if (self:isBattleStat(statname)) then
        return self:getBattleStat(statname)
    end

    if statname == EnnemyStatManager.HPMAX then
        return stat * self.pvFactor
    elseif (statname ~= EnnemyStatManager.PPMAX) then
        return stat * self.statFactor
    else
        return stat
    end
end

return EnnemyStatManager