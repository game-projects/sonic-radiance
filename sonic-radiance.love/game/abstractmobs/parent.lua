local Serializable = require "birb.classes.serializable"
local AbstractMobParent = Serializable:extend()
local StatManager = require "game.abstractmobs.statmanager"

local statutStatList = require "datas.gamedata.statuses"

function AbstractMobParent:new(serializeFields, listSerializable, statManager)
  local statManager = statManager or StatManager
  self:initBasicElements()

  self.stats = statManager(self)
  self.skills = self:createSkills()
  self.statuts = {}
  self:initLife()
  AbstractMobParent.super.new(self, serializeFields, listSerializable)
end

function AbstractMobParent:initBasicElements()
  self.name = "PlaceHolder"
  self.fullname = "PlaceHolder"
  self.turns = 2
end

function AbstractMobParent:haveProtecType()
  return false
end

function AbstractMobParent:getProtecTypes()
  return {}
end

function AbstractMobParent:createSkills()
  return {}
end

-- LIFE FUNCTIONS
-- Handle HP and stuff like that

function AbstractMobParent:initLife()
  self.hp = self.stats:get(self.stats.HPMAX)
  self.pp = self.stats:get(self.stats.PPMAX)
  self.status = 0
end

function AbstractMobParent:setHP(newHP, relative)
  if (relative) then
    self.hp = self.hp + newHP
  else
    self.hp = newHP
  end
  self.hp = math.floor(math.max(0, self.hp))
  if (self.hp == 0) then
    self:die()
  end
  self.hp = math.min(self.hp, self.stats:get(self.stats.HPMAX))
end

function AbstractMobParent:setPP(newPP, relative)
  if (relative) then
    self.pp = self.pp + newPP
  else
    self.pp = newPP
  end
  self.pp = math.floor(math.max(0, self.pp))
  self.pp = math.min(self.pp, self.stats:get(self.stats.PPMAX))
end

function AbstractMobParent:getStats()
  return self.stats
end

-- STATUT HANDLING

function AbstractMobParent:addStatut(name, duration, source)
  local statut = self:getStatutData(name)
  local duration = duration or 1

  if (statut.remove ~= nil) then
    duration = - self:removeStatutTurns(statut.remove, duration)
  end

  if (duration > 0) then
    if (statut.replaceAll == true) then
      self:resetStatut()
    else
      self:removeStatut(name)
    end
    self.statuts[name] = {}
    self.statuts[name].duration = duration
  end
end

function AbstractMobParent:getStatutKey(statutName)
  local statut = self:getStatutData(statutName)
  local key = ""
  if (self.statuts[statutName] ~= nil) then
    key = statutName
  end
  if (statut.alternative ~= nil) then
    for _, alternative in ipairs(statut.alternative) do
      if (self.statuts[alternative] ~= nil) then
        key = alternative
      end
    end
  end
  return key
end

function AbstractMobParent:haveStatuts(statutName)
  return (self.statuts[self:getStatutKey(statutName)] ~= nil)
end

function AbstractMobParent:removeStatut(statutName)
  local key = self:getStatutKey(statutName)
  if (key ~= nil) then
    self.statuts[key] = nil
    return true
  end
  return false
end

function AbstractMobParent:removeStatutTurns(statutName, turns)
  local statut = self:getStatutData(statutName)
  if (statut.temporary == false) then
    return 0
  end
  if (not self:haveStatuts(statutName)) then
    return turns
  end
  local key = self:getStatutKey(statutName)
  local relativeTurns = self.statuts[key].duration - turns

  if (relativeTurns <= 0) then
    self:removeStatut(statutName)
  else
    self.statuts[key].duration = relativeTurns
  end

  return relativeTurns
end

function AbstractMobParent:resetStatut()
  self.statuts = {}
end

function AbstractMobParent:die()
  self:addStatut("ko")
end

function AbstractMobParent:isAlive()
  return (not self:haveStatuts("ko"))
end

function AbstractMobParent:removeOneTurnToStatut()
  for name, _ in pairs(self.statuts) do
    self:removeStatutTurns(name, 1)
  end
end

function AbstractMobParent:getStatutsStat(statName)
  local stat = 0
  for statutName, _ in pairs(self.statuts) do
    local statut = self:getStatutData(statutName)
    if (statut.stats ~= nil) then
      for _, statutStat in ipairs(statut.stats) do
        if (statutStat[1] == statName) then
          stat = stat + statutStat[2]
        end
      end
    end
  end
  return stat
end

function AbstractMobParent:getStatutData(statutName)
  return statutStatList[statutName]
end

return AbstractMobParent
