local World = require "birb.modules.world.world2D"
local RPGWorld = World:extend()
local objFile = "scenes.overworld.actors"
local RPGMap = require "scenes.overworld.map"

function RPGWorld:new(scene, area, playerx, playery)
  self.area = area or game.position.area
  self.playerx = playerx or game.position.x
  self.playery = playery or game.position.y
  RPGWorld.super.new(self, scene, objFile, nil, nil)
end

function RPGWorld:createMapController()
  RPGMap(self, self.area, self.playerx, self.playery)
end

function RPGWorld:savePosition()
  local x, y = self.players[1].actor.x, self.players[1].actor.y
  game.position.x = math.floor(x / 16)
  game.position.y = math.floor(y / 16)
  game.position.area = self.area
end

function RPGWorld:teleport(area, x, y, charDir)
  if (area == self.area) then
    self.players[1].actor.x = x * 16
    self.players[1].actor.y = y * 16
  else
    self.area = area
    self.playerx = x
    self.playery = y
    self:reset()
    collectgarbage()
    self.players[1].actor.charDir = charDir
  end
end

function RPGWorld:restoreActions()
  self.encounter:destroy()
  local currentMap = self.map:getMapAtPoint(self.players[1].actor.x, self.players[1].actor.y)
  self.players[1].actor.xsp = 0
  self.players[1].actor.ysp = 0
  self.scene:playMapMusic(currentMap)
end

return RPGWorld
