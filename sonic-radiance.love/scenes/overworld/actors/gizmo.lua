local Parent = require "scenes.overworld.actors.parent"
local Gizmo = Parent:extend()

local defaultEvent = {
    {"simpleMessage", "", "ERROR 000 NO EVENT"},
}

function Gizmo:new(world, x, y, w, h, overrides)
    local w = w or 16;
    local h = h or 16;
    Gizmo.super.new(self, world, "gizmo", x, y, w, h, false)
    self.overrides = overrides
    self.drawDebugBox = false
    self.event = defaultEvent
    self.interactionName = "Talk"
end

function Gizmo:getUniqueId()
    return "gizmo" .. ":" .. self.mapname .. ":" .. self.x .. ":" .. self.y
end

function Gizmo:setProperties(properties)
    self:setDefaultProperties()
    self:replaceProperties(properties)
    if (self.overrides ~= nil) then
        self:replaceProperties(self.overrides)
    end
    self:applyProperties()
end

function Gizmo:applyProperties()
    -- Apply properties to the internal item system
    self.isSolid = self.properties.isSolid
    self.mainHitbox.isSolid = self.properties.isSolid
    if (self.isSolid) then
        self:addHitbox("btnInput", "btnInput", {-1, -1, self.w + 2, self.h + 2}, false)
    end
    self.needButton = self.properties.needButton
    self.charId = self.properties.charId or 1
    self.charset = self.properties.charset or nil
    self.charDir = self.properties.charDir or "down"
    self.cantTurn = self.properties.cantTurn or self.cantTurn
    self.isTurning = (self.properties.isTurning == true)
    if (self.properties.event ~= nil) then
        local data = require("datas.gamedata.events." .. self.properties.event)
        self.event = data.actions
    end
    self.uniqueId = self:getUniqueId()
    if (game.destroyedGizmo[self.uniqueId] == true) then
        self:destroy()
    end
    self.interactionName = self.properties.interactionName or self.interactionName
end

function Gizmo:setDefaultProperties()
    local default = require "game.utils.gizmo.properties"
    self.properties = {}
    for key, value in pairs(default) do
        self.properties[key] = value
    end
end

function Gizmo:replaceProperties(properties)
    for key, _ in pairs(properties) do
        if (properties[key] ~= nil) then
            self.properties[key] = properties[key]
        end
    end
end

function Gizmo:doAction()
    self:action()
    if (self.properties.destroy == "forever") then
        game.destroyedGizmo[self.uniqueId] = true
        self:destroy()
    end
    if (self.properties.destroy == "temp") then
        self:destroy()
    end
end

function Gizmo:action()
    self.scene.events:startEvent(self, self.event)
end

return Gizmo;