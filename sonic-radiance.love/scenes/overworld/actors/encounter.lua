local Gizmo = require "scenes.overworld.actors.gizmo"
local Encounter = Gizmo:extend()

local overrides = {
    ["isSolid"] = false,
    ["charset"] = "item1",
    ["charId"] = 2,
    ["destroy"] = "none",
    ["needButton"] = false,
    ["isTurning"] = true,
}

function Encounter:new(world, x, y)
    Encounter.super.new(self, world, x, y, 16, 16, overrides)
end

function Encounter:action()
    core.scenemanager:storeCurrentScene("afterBattle")
    local ox, oy = self.world.cameras:getViewCoordinate(1)
    game.cbs:startBattle("test", "testBattle", self.x - ox, self.y - oy)
    self.world.encounter = self
    self.scene.isPlaying = ""
end

function Encounter:drawCharset(charset, charId)
    local x, y = utils.math.floorCoord(self.x, self.y)
    y = y - 2

    love.graphics.setColor(1,1,1,0.5)
    self.assets.images["shadow"]:draw(x, y + 11)
    utils.graphics.resetColor()

    self.assets.sprites["encounter"]:draw(x - 1, y + 10)
  end

return Encounter