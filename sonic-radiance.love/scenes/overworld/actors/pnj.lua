local Gizmo = require "scenes.overworld.actors.gizmo"
local PNJ = Gizmo:extend()

local overrides = {
    ["isSolid"] = true,
    ["destroy"] = "nope",
    ["needButton"] = true,
    ["interactionName"] = "Talk",
}

function PNJ:new(world, x, y)
    PNJ.super.new(self, world, x, y, 16, 16, overrides)
end

function PNJ:applyProperties()
    PNJ.super.applyProperties(self)
    self.messages = {}
    for i = 1, 9, 1 do
        local message = self.properties["message" .. i]
        if (message ~= nil) then
            table.insert(self.messages, message)
        end
    end
    self.messageType = self.properties.messageType or "random"
    self.currentMessage = 1
end

function PNJ:setMessage()
    local message = self.messages[1]
    if (self.messageType == "random") then
        message = self.messages[math.random(#self.messages)]
    elseif (self.messageType == "list") then
        message = self.messages[self.currentMessage]
        self.currentMessage = self.currentMessage + 1
        if (self.currentMessage > #self.messages) then
            self.currentMessage = 1
        end
    end
    self.event = {
        {"dialogBox", "", message, self.properties.title, ""},
    }
end

function PNJ:action()
    self:setMessage()
    PNJ.super.action(self)
end


return PNJ