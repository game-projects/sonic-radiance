local Obj = {}

-- On charge toutes les différentes types d'acteurs
local cwd  = (...):gsub('%.init$', '') .. "."
Obj.Player = require(cwd .. "player")
Obj.Gizmo = require(cwd .. "gizmo")
Obj.Teleporter = require(cwd .. "teleport")
Obj.PNJ = require(cwd .. "pnj")
Obj.Encounter = require(cwd .. "encounter")
Obj.GFX = require(cwd .. "gfx")

Obj.loot = require(cwd .. "loot")

Obj.index     = {}
Obj.index["player"]   = Obj.Player
Obj.index["gizmo"]   = Obj.Gizmo
Obj.index["teleporter"]   = Obj.Teleporter
Obj.index["pnj"]   = Obj.PNJ
Obj.index["encounter"]   = Obj.Encounter

Obj.index["ring"]   = Obj.loot.Ring
Obj.index["itembox"]   = Obj.loot.ItemBox
Obj.index["ringbox"]   = Obj.loot.RingBox

Obj.collisions = {}
Obj.collisions["wall"] = require(cwd .. "wall")
Obj.collisions["gizmo-collision"]   = Obj.Gizmo

return Obj
