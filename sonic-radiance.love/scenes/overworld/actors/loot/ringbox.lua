local Gizmo = require "scenes.overworld.actors.gizmo"
local RingBox = Gizmo:extend()

local overrides = {
    ["isSolid"] = true,
    ["charset"] = "item1",
    ["charId"] = 1,
    ["charDir"] = "down",
    ["destroy"] = "forever",
    ["needButton"] = true,
    ["interactionName"] = "Take",
}

function RingBox:new(world, x, y)
    RingBox.super.new(self, world, x, y, 16, 16, overrides)
end

function RingBox:applyProperties()
    RingBox.super.applyProperties(self)
    self.event = {
        {"getRings", "", self.properties.number},
        {"playSFX", "", "pop"},
        {"simpleMessage", "", "You got " .. self.properties.number .. " rings"},
        {"showGFX", "", self.x + 16, self.y + 14, "smallsmoke"}
    }
end

return RingBox