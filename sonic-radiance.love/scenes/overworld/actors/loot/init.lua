local Loot = {}

Loot.Ring = require "scenes.overworld.actors.loot.ring"
Loot.RingBox = require "scenes.overworld.actors.loot.ringbox"
Loot.ItemBox = require "scenes.overworld.actors.loot.itembox"

return Loot