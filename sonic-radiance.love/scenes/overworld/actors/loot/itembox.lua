local Gizmo = require "scenes.overworld.actors.gizmo"
local ItemBox = Gizmo:extend()

local overrides = {
    ["isSolid"] = true,
    ["charset"] = "item1",
    ["charId"] = 1,
    ["charDir"] = "right",
    ["destroy"] = "forever",
    ["needButton"] = true,
    ["interactionName"] = "Take",
}

function ItemBox:new(world, x, y)
    ItemBox.super.new(self, world, x, y, 16, 16, overrides)
end

function ItemBox:applyProperties()
    ItemBox.super.applyProperties(self)
    self.event = {
        {"getItems", "", self.properties.category, self.properties.item, self.properties.number},
        {"playSFX", "", "pop"},
        {"simpleMessage", "", "You got " .. self.properties.number .. " " .. self.properties.item},
        {"showGFX", "", self.x + 16, self.y + 14, "smallsmoke"}
    }
end

return ItemBox