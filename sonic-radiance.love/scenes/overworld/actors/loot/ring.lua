local Gizmo = require "scenes.overworld.actors.gizmo"
local Ring = Gizmo:extend()

local overrides = {
    ["isSolid"] = false,
    ["charset"] = "item1",
    ["charId"] = 2,
    ["destroy"] = "temp",
    ["needButton"] = false,
    ["isTurning"] = true,
}

function Ring:new(world, x, y)
    Ring.super.new(self, world, x, y, 16, 16, overrides)
end

function Ring:action()
    self.assets.sfx["ring"]:play()
    game.loot.rings = game.loot.rings + 1
    self.world.obj.GFX(self.world, self.x + 16, self.y + 8, "sparkles")
end

return Ring