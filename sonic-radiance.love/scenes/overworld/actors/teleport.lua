local Gizmo = require "scenes.overworld.actors.gizmo"
local Teleporter = Gizmo:extend()

function Teleporter:new(world, x, y, w, h)
    Teleporter.super.new(self, world, x, y, w, h, nil)
    self.interactionName = "Enter"
end

function Teleporter:applyProperties()
    Teleporter.super.applyProperties(self)
    local charDir = self.properties.charDir or "default"
    self.event = {
        {"teleport", "", self.properties.area, self.properties.x, self.properties.y, charDir},
    }
end

return Teleporter