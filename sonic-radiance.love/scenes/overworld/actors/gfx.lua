local cwd   = (...):gsub('%.gfx$', '') .. "."
local Parent = require(cwd .. "parent")
local GFX     = Parent:extend()

function GFX:new(world, x, y, spritename)
  local width, height = world.scene.assets.sprites[spritename]:getDimensions()

  GFX.super.new(self, world, "gfx", x - (width/2), y - (height/2), width, height)
  self:setSprite(spritename, true)
end

function GFX:animationEnded(animation)
  core.debug:print("gfx2D", 'Current animation "' .. animation .. '" have ended, destroying gfx')
  self:destroy()
end

function GFX:draw()
    local x, y = math.floor(self.x), math.floor(self.y)
    self:drawSprite(x, y)
end

return GFX