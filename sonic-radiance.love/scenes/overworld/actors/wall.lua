local Base = require "birb.modules.world.actors.actor2D"
local Wall = Base:extend()

function Wall:new(world, x, y, w, h)
  Wall.super.new(self, world, "wall", x, y, w, h, true)
  self:setDebugColor(0,0,0)
end

function Wall:draw()
  --self:drawHitbox()
  --utils.graphics.resetColor( )
end

return Wall
