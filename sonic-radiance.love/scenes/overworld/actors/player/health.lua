local PlayerHealth = Object:extend()

function PlayerHealth:initHealth()
    self.fallDamage = 0
    self.fallSound = ""
end

function PlayerHealth:takeDamage(damage)
    local damage = damage or 10
    damage = damage / 100
    if (game.difficulty:get("allDamage")) then
        for _, name in ipairs(game.characters.team) do
            game.characters:sendDamageFromMap(name, damage)
        end
    else
        game.characters:sendDamageFromMap(game.characters.team[game.characters.active], damage)
    end
end

return PlayerHealth