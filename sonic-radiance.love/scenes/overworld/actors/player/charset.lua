local PlayerCharset = Object:extend()

local ACTIONS_LARGEANIM = {"jump", "jumpdash"}
local ACTIONS_ISFAST = {"jump", "fly", "run", "jumpdash"}
local ACTIONS_ALWAYSWALK = {"fly"}
local ACTIONS_DONTWALK = {"slide"}

function PlayerCharset:initPlayerCharset()
    self.scissorSprite = true
    self:updateCurrentCharset()
end

function PlayerCharset:updateCurrentCharset()
    self:setCharset(self:getCharset(), self.active.data.charId)
    self.isFast = self:getIsFast()
    self.largeAnim = self:getLargeAnim()
    self.alwaysWalk = self:getAlwaysWalk()
    self.cantWalk = self:getDontWalk()
end

function PlayerCharset:getCharset()
    local charset = self.active.data.charset
    if (self.currentAction == "jump" or self.currentAction == "jumpdash") then
        charset = charset .. "-jump"
    elseif (self.currentAction == "fly") then
        charset = charset .. "-flight"
    elseif (self.currentAction == "punch") then
        charset = charset .. "-jump"
    end
    return charset
end

function PlayerCharset:getLargeAnim()
    return utils.table.contain(ACTIONS_LARGEANIM, self.currentAction)
end

function PlayerCharset:getIsFast()
    return utils.table.contain(ACTIONS_ISFAST, self.currentAction)
end

function PlayerCharset:getAlwaysWalk()
    return utils.table.contain(ACTIONS_ALWAYSWALK, self.currentAction)
end

function PlayerCharset:getDontWalk()
    if (self.forceAction ~= nil) then
        return utils.table.contain(ACTIONS_DONTWALK, self.forceAction)
    else
        return utils.table.contain(ACTIONS_DONTWALK, self.currentAction)
    end
end

return PlayerCharset
