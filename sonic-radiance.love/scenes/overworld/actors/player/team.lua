local Team = Object:extend()

function Team:initTeam()
    self.active = game.characters:getActiveCharacterData()
    self.canChangeActive = true
end

function Team:updateActiveCharacter()
  local everybodyIsKo = true
  for id, name in ipairs(game.characters.team) do
    if (game.characters.list[name].hp > 0) then
      everybodyIsKo = false
      break;
    end
  end
  if (everybodyIsKo) then
    self.scene:gameover()
  else
    if ((self.active.hp == 0) and not game.difficulty:get("playerKoChar")) then
      self:switchActiveCharacter()
    end
  end
end

function Team:getCurrentCharType()
    return self.active.data.class
end

function Team:switchActiveCharacter()
    if (self.canChangeActive) then
        local count = game.characters:setActiveCharacter()
        self.active = game.characters:getActiveCharacterData()
        self.canChangeActive = false
        self.tweens:newTimer(0.3, "changeCharacter")
        self.scene.gui:getElement("teamEmblems"):changeActivePlayer(count)
    end
end

function Team:endCharacterSwitchAnimation()
    self.canChangeActive = true
    self:updateCurrentCharset()
end

return Team