local PlayerMap = Object:extend()
local defTransitions = require "birb.modules.transitions"

function PlayerMap:initMap()
    self.previousMap = 0
end

function PlayerMap:updateCurrentMap()
    local currentMap = self.world.map:getMapAtPoint(self.x, self.y)
    if (currentMap ~= nil) then
      if (self.previousMap ~= currentMap.id) then
        self.previousMap = currentMap.id
        self.scene:updateCurrentMap(currentMap)
      end
    end
end

function PlayerMap:updateOutsideMap()
  if ((self.x > self.world.map.w + 8) or (self.x < -8) or (self.y > self.world.map.h + 16) or (self.y < -8)) 
      and (self.world.map.data.exitTo ~= nil) then
    local arguments = self.world.map.data.exitTo
    core.screen:startTransition(defTransitions.default, defTransitions.default,
    function() self.world:teleport(arguments.area, arguments.x, arguments.y, arguments.charDir) end,
    0, 0)
  end
end

return PlayerMap