local PlayerActions = Object:extend()

local BASE_SPEED = 120
local RUN_FACTOR = 2.5
local JMP_STRENGHT = 2.5

local ACTIONS = {}
ACTIONS["speedster"] = {"run"}
ACTIONS["technic"] = {"fly"}
ACTIONS["power"] = {"punch"}

function PlayerActions:initActions()
    self.currentAction = "idle"
    self.canJump = true
    self.canAct = true
    self.speedFactor = 1
    self.forceAction = nil
    self.dashJustStarted = false
end

function PlayerActions:canDoAction(action)
    local classCanDoAction = utils.table.contain(ACTIONS[self:getCurrentCharType()], action)
    local playerCanDoAction = utils.table.contain(game.actions, action)
    return (classCanDoAction and playerCanDoAction)
end

function PlayerActions:act()
    if (self.forceAction ~= nil) then
        self:forcedAction()
    else
        self:actionMove()
        self:actionJump()
        self:actionSwitch()
        self:actionRun()
        self:actionPunch()
    end
end

function PlayerActions:forcedAction()
    local charSpeed = BASE_SPEED * self.speedFactor

    if (self.forceAction == "slide") then
        if (self.xsp ~= 0 or self.ysp ~= 0) then
            if (self.currentAction == "run") then
                charSpeed = charSpeed * RUN_FACTOR
            end
            self.xsp, self.ysp = utils.math.lengthdir(charSpeed, math.rad(self.charsetManager.angle[self.charDir]))
            self.xsp = -self.xsp
        else
            self:actionMove()
        end
    end
end

function PlayerActions:actionMove()
    local charSpeed = BASE_SPEED * self.speedFactor

    if (self.currentAction == "punch") then
        return
    end

    if (self.currentAction == "jumpdash" or self.currentAction == "run") then
        local xsp, ysp = self:getDash()
        if (utils.table.contain({"up", "down"}, self.charDir)) then
            if self.keys["left"].isDown then
                xsp = -charSpeed
            end
            if self.keys["right"].isDown then
                xsp = charSpeed
            end
            if (self.ysp == 0) then
                if (self.currentAction == "jumpdash") then
                    self.currentAction = "jump"
                else
                    self.currentAction = "idle"
                end
            elseif (self.dashJustStarted) then
                self.dashJustStarted = false
                self.assets.sfx["dash"]:play()
            end
        else
            if self.keys["up"].isDown then
                ysp = -charSpeed
            end
            if self.keys["down"].isDown then
                ysp = charSpeed
            end
            if (self.xsp == 0) then
                if (self.currentAction == "jumpdash") then
                    self.currentAction = "jump"
                else
                    self.currentAction = "idle"
                end
            elseif (self.dashJustStarted) then
                self.dashJustStarted = false
                self.assets.sfx["dash"]:play()
            end
        end
        self.xsp, self.ysp = xsp, ysp
    else
        if self.keys["up"].isDown then
            self.ysp = -charSpeed
            self.charDir = "up"
        end
        if self.keys["down"].isDown then
            self.ysp = charSpeed
            self.charDir = "down"
        end
        if self.keys["left"].isDown then
            self.xsp = -charSpeed
            self.charDir = "left"
        end
        if self.keys["right"].isDown then
            self.xsp = charSpeed
            self.charDir = "right"
        end
    end
end

function PlayerActions:getCharspeed()
    return BASE_SPEED * self.speedFactor
end

function PlayerActions:getDash()
    local xsp, ysp = utils.math.lengthdir(self:getCharspeed() * RUN_FACTOR, math.rad(self.charsetManager.angle[self.charDir]))
    return -xsp, ysp
end

function PlayerActions:actionJump()
    if self.keys["B"].isPressed then
        if (self.onGround and self.canJump) then
            self:goUpward(JMP_STRENGHT)
            self.assets.sfx["jump"]:play()
            if (self.currentAction == "run") then
                self.currentAction = "jumpdash"
            else
                self.currentAction = "jump"
            end
        elseif (self.currentAction == "jump" and self:canDoAction("fly")) then
            self.currentAction = "fly"
            self.grav = 0
            self.zsp = 0
            self.tweens:newTimer(0.75, "endFly")
            self.assets.sfx["fly"]:play()
        end
    end

    if self.keys["B"].isReleased then
        self:endFly()
    end
end

function PlayerActions:endFly()
    if (self.currentAction == "fly") then
        self:goUpward(0)
        self.currentAction = "idle"
    end
end

function PlayerActions:actionSwitch()
    if self.keys["select"].isPressed and (self.currentAction == "idle") then
        self:switchActiveCharacter()
    end
end

function PlayerActions:actionRun()
    if self.keys["C"].isPressed then
        if (self:canDoAction("run") and self.speedFactor > 0) then
            self.dashJustStarted = true
            if (utils.table.contain({"run", "idle"}, self.currentAction)) then
                self.currentAction = "run"
            elseif (utils.table.contain({"jumpdash", "jump"}, self.currentAction)) then
                self.currentAction = "jumpdash"
            end
            self.xsp, self.ysp = self:getDash()
        end
    elseif (not self.keys["C"].isDown) then
        if (self.currentAction == "run") then
            self.currentAction = "idle"
        elseif (self.currentAction == "jumpdash") then
            self.currentAction = "jump"
        end
    end
end

function PlayerActions:actionPunch()
    if self.keys["C"].isPressed then
        if (self:canDoAction("punch")) then
            self.assets.sfx["hit"]:play()
            self.xsp, self.ysp = utils.math.lengthdir(BASE_SPEED * RUN_FACTOR, math.rad(self.charsetManager.angle[self.charDir]))
            self.xsp = -self.xsp
            self.currentAction = "punch"
            self.tweens:newTimer(0.15, "endPunch")
            self.assets.sprites["punch"]:changeAnimation("default", true)
        end
    end
end

function PlayerActions:endPunch()
    if (self.currentAction == "punch") then
        self.currentAction = "idle"
    end
end

function PlayerActions:endJump()
    if (self.currentAction == "jump") then
        self.currentAction = "idle"
    elseif (self.currentAction == "jumpdash") then
        self.currentAction = "run"
    end
end

function PlayerActions:drawActionEffect()
    if (((self.currentAction == "run") or  (self.currentAction == "jumpdash")) and (self.xsp ~= 0 or self.ysp ~= 0)) then
        local dx, dy = utils.math.lengthdir(20, math.rad(self.charsetManager.angle[self.charDir]))
        if (self.charDir == "down") then
            dy = 8
        elseif (self.charDir == "left" or self.charDir == "right") then
            dy = -8
        end
        local x, y = self.x + 8 - dx, self.y + 8 - self.z + dy
        self.assets.sprites["dash"]:draw(x, y, math.rad(self.charsetManager.angle[self.charDir]))
    end
    if (self.currentAction == "punch") then
        local dx, dy = utils.math.lengthdir(20, math.rad(self.charsetManager.angle[self.charDir]))
        if (self.charDir == "down") then
            dy = 8
        end
        local x, y = self.x + 8 - dx, self.y + 8 - self.z + dy
        self.assets.sprites["punch"]:draw(x, y, math.rad(self.charsetManager.angle[self.charDir]))
    end
end

return PlayerActions
