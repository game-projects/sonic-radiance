local Parent = require "scenes.overworld.actors.parent"
local Player = Parent:extend()

local TweenManager = require "birb.classes.time"
local Team = require "scenes.overworld.actors.player.team"
local Interactions = require "scenes.overworld.actors.player.interactions"
local Actions = require "scenes.overworld.actors.player.actions"
local Charset = require "scenes.overworld.actors.player.charset"
local Map = require "scenes.overworld.actors.player.map"
local Health = require "scenes.overworld.actors.player.health"

Player:implement(Team)
Player:implement(Interactions)
Player:implement(Actions)
Player:implement(Charset)
Player:implement(Map)
Player:implement(Health)

local FRICTION = 480 * 3
local GRAV = 10
local DEFAULT_GROUND_LEVEL = 0
local DEFAULT_GROUND_HEIGHT = 0
local RESPAWN_LIMIT = -32

function Player:new(world, x, y, id)
  Player.super.new(self, world, "player", x, y, 16, 16, true)
  self.groundLevel = DEFAULT_GROUND_LEVEL
  self.groundHeight = DEFAULT_GROUND_HEIGHT
  self.z = self.groundLevel
  self.grav = GRAV
  self.tweens = TweenManager(self)

  self.onGround = true
  self.xfrc, self.yfrc = FRICTION, FRICTION

  self:initTeam()
  self:initInteractions()
  self:initActions()
  self:initPlayerCharset()
  self:initMap()
  self:initHealth()
end

function Player:updateStart(dt)
  self.scene.gui:getElement("interactions"):setInteractionName("")
  self.tweens:update(dt)
  self:updateTerrain()
  self:updateActiveCharacter()

  self:act()

  self.world:getTileTypeAtPoint(self.x, self.y)

  self:updateInteraction()
  self:updateCurrentCharset()
  self:updateCurrentMap()
  self:updateOutsideMap()
end

-- PHYSICS FUNCTIONS
-- Some functions to hook up the physic system
function Player:goUpward(zsp)
    self.zsp = zsp
    self.grav = GRAV
    self.onGround = false
end

function Player:applyGravity(dt)
  local grav = self.grav * -1
  self.zsp = self.zsp + (grav * dt)

  if utils.math.sign(self.zsp) == utils.math.sign(grav) then
    self:checkGround()
  end
end

function Player:checkGround()
  if (self.z + self.zsp <= self.groundLevel) then
    self.onGround = true
    self.z = self.groundLevel
    self.zsp = 0
    self:endJump()
    if (self.z <= RESPAWN_LIMIT) then
      self.x = self.lastPos.x
      self.y = self.lastPos.y
      self:takeDamage(self.fallDamage)
      if (not utils.string.isEmpty(self.fallSound)) then
        self.assets:playSFX(self.fallSound)
      end
    end
  end
end

function Player:autoMove(dt)
  Player.super.autoMove(self, dt)
  self.z = self.z + self.zsp
end

-- RESPONSES
-- Reponse to timer and collisions

function Player:collisionResponse(col)
  local hitbox = col.other
  local other = col.other.owner
  if (not other.isDestroyed) then
    if (hitbox.type == "gizmo") then
      self:collideWithGizmo(other)
    elseif (hitbox.type == "btnInput" and other.needButton) then
      self:talkToGizmo(other)
    end
  end
end

function Player:timerResponse(response)
  if (response == "changeCharacter") then
    self:endCharacterSwitchAnimation()
  elseif (response == "endFly") then
    self:endFly()
  elseif (response == "endPunch") then
    self:endPunch()
  end
end

function Player:draw()
  Player.super.draw(self)
  self:drawActionEffect()
end

return Player
