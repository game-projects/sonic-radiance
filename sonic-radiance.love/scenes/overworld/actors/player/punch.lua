local Base = require "birb.modules.world.actors.actor2D"
local Punch = Base:extend()

function Punch:new(world, x, y, dir)
    Punch.super.new(self, world, "wall", x, y, 8, 8, false)
end

function Punch:update(dt)
    self.depth = -self.y
    Punch.super.update(self, dt)
end

function Punch:draw()

end

return Punch