local Base = require "birb.modules.world.actors.actor2D"
local Parent = Base:extend()

function Parent:new(world, type, x, y, w, h, isSolid)
  self.scene = world.scene
  self.z = 0
  Parent.super.new(self, world, type, x, y, w, h, isSolid)
  self:initCharset()
  self.drawDebugBox = false
end

function Parent:update(dt)
  self.depth = -self.y
  Parent.super.update(self, dt)
  self.depth = -self.y
end

function Parent:isMoving()
  return ((math.abs(self.ysp) > 0.01) or (math.abs(self.xsp) > 0.01))
end

-- Charset / Draw functions
-- Handle the charset of an object

function Parent:initCharset()
  self.charsetManager = self.world.scene.charsetManager
  self.charset = nil
  self.charDir = "down"
  self.cantWalk = false
  self.cantTurn = false
  self.isTurning = false
  self.isFast = false
  self.largeAnim = false
  self.alwaysWalk = false
  self.scissorSprite = false
  self.groundHeight = 0
end

function Parent:setCharset(charset, charId, cantWalk)
  self.charset = charset
  self.charId = charId
  self.cantWalk = (cantWalk == true)
end

function Parent:drawCharset(charset, charId)
  local x, y = utils.math.floorCoord(self.x, self.y)
  y = y - 2
  local z = math.floor(self.z)

  love.graphics.setColor(1,1,1,0.5)
  if (self.groundHeight == 0) then
    self.assets.images["shadow"]:draw(x + 1, y + 11)
  end
  utils.graphics.resetColor()

  if (self.scissorSprite) then
    local _, camy = self.world.cameras:getViewCoordinate(1)
    local viewy = math.floor((self.y + self.h - self.groundHeight) - camy)
    love.graphics.setScissor(0, 0, 424, viewy)
  end

  if (self.largeAnim) then
    self.charsetManager:drawLargeAnim(self.charset, self.charId, self.charDir, x, y - z, self.isFast)
  else
    if (not self.isTurning) then
      if ((self:isMoving() and (not self.cantWalk)) or self.alwaysWalk) then
        self.charsetManager:drawMoving(self.charset, self.charId, self.charDir, x, y - z, self.isFast)
      else
        self.charsetManager:drawStanding(self.charset, self.charId, self.charDir, x, y - z)
      end
    else
      self.charsetManager:drawTurning(self.charset, self.charId, x, y - z, self.isFast)
    end
  end

  if (self.scissorSprite) then
    love.graphics.setScissor()
  end
end

function Parent:draw()
  if (self.drawDebugBox) then
    love.graphics.rectangle("line", math.floor(self.x), math.floor(self.y), self.w, self.h)
  end
  if (self.charset ~= nil) then
    self:drawCharset()
  end
end

return Parent
