local Parent = require "birb.modules.world.maps.parent"
local TiledMultiMap = Parent:extend()
local StiWrapper = require "birb.modules.world.maps.tiled.stiwrapper"

local TiledMixins = require "birb.modules.world.maps.tiled.mixins"
TiledMultiMap:implement(TiledMixins)

local mapFolder = "datas/gamedata/maps/sti/"
local areaFolder = "datas.gamedata.maps.area."

function TiledMultiMap:new(world, area, playerx, playery)
    self.wrappers = {}
    self.data = require(areaFolder .. area)
    self.w, self.h = 0, 0
    self.playerx, self.playery = playerx * 16, playery * 16
    self:loadMaps()
    TiledMultiMap.super.new(self, world)
    self.supportTileCollision = true

    self:setBackgroundColorFromTable(self.data.color)
    self.mapname = self.data.areaName
end

function TiledMultiMap:getMapName()
    return self.data.areaName
end

function TiledMultiMap:getDimensions()
    return self.w, self.h
end

function TiledMultiMap:loadObjects()
    for i, wrapper in ipairs(self.wrappers) do
        wrapper:loadObjects()
    end
    self:loadWalls()
    self:loadPlayer()
end

function TiledMultiMap:loadWalls()
    if (self.data.exitTo == nil) then
        self.world.obj.collisions["wall"](self.world, -16, -16, self.w, 16)
        self.world.obj.collisions["wall"](self.world, -16, -16, 16, self.h)
        self.world.obj.collisions["wall"](self.world, self.w, -16, 16, self.h)
        self.world.obj.collisions["wall"](self.world, -16, self.h, self.w, 16)
    end

end

function TiledMultiMap:loadPlayer()
    self.world:addPlayer(self.playerx, self.playery, 0, 1)
end

-- MAP HANDLING FUNCTIONS
-- Handle maps

function TiledMultiMap:loadMaps()
    for i, mapData in ipairs(self.data.maps) do
        self:addMap(i, mapData)
    end
end

function TiledMultiMap:addMap(id, mapData)
    core.debug:print("Loading map " .. mapData.name)
    local mapFile = mapFolder .. mapData.folder .. "/" .. mapData.map .. ".lua"
    local wrapper = StiWrapper(self, mapFile, mapData.x, mapData.y, false)
    wrapper.id = id
    wrapper.music = mapData.music
    wrapper.name = mapData.name
    table.insert(self.wrappers, wrapper)
    self:pushDimensions(wrapper)
end

function TiledMultiMap:pushDimensions(wrapper)
    local x, y, w, h = wrapper:getRect()
    self.w = math.max(self.w, x + w)
    self.h = math.max(self.h, y + h)
end

function TiledMultiMap:getMapAtPoint(x, y)
    for i, wrapper in ipairs(self.wrappers) do
        local recx, recy, recw, rech = wrapper:getRect()
        local relx, rely = x - recx, y - recy
        if (relx <= recw and relx >= 0) then
            if (rely <= rech and rely >= 0) then
                return wrapper
            end
        end
    end
end

function TiledMultiMap:getMapsInRect(x, y, w, h)
    local listWrapper = {}
    for i, wrapper in ipairs(self.wrappers) do
        local recx, recy, recw, rech = wrapper:getRect()
        local relx, rely = x - recx, y - recy
        if (relx <= recw and relx + w >= 0) then
            if (rely <= rech and rely + h >= 0) then
                table.insert(listWrapper, wrapper)
            end
        end
    end
    return listWrapper
end

-- TILE FUNCTIONS
-- Get tiles

function TiledMultiMap:getTileTypeAtPoint(x, y)
    local wrapper = self:getMapAtPoint(x, y)
    if (wrapper ~= nil) then
        return wrapper:getTileTypeAtPoint(x, y)
    end
end

function TiledMultiMap:haveTileTypeInRect(x, y, w, h, type)
    local listWrapper = self:getMapsInRect(x, y, w, h)
    for i, wrapper in ipairs(listWrapper) do
        local haveTile = wrapper:haveTileTypeInRect(x, y, w, h, type)
        if (haveTile) then
            return true
        end
        return false
    end
end

-- UPDATE FUNCTION
-- Update or modify the map

function TiledMultiMap:resize(w, h)
    for i, wrapper in ipairs(self.wrappers) do
        wrapper:resize(w, h)
    end
end

function TiledMultiMap:update(dt)
    local x, y, w, h = self.world.cameras:getViewCoordinate(1)
    local listWrapper = self:getMapsInRect(x, y, w, h)
    for i, wrapper in ipairs(listWrapper) do
        wrapper:update(dt)
    end
end

-- DRAW FUNCTIONS
-- Handle drawing the wrapper


function TiledMultiMap:drawUpperLayers()
    local player = self.world.players[1].actor
    local pos1AUpperLayer = self:haveUpperLayerAtCoord(player.x, player.y + 8)
    local pos2AUpperLayer = self:haveUpperLayerAtCoord(player.x + 16, player.y + 8)

    local y = math.floor((player.y + 8)/16)*16
    if (pos1AUpperLayer or pos2AUpperLayer) then
        y = math.floor((player.y - player.z)/16)*16
    end
    local camx, camy = self.world.cameras:getViewCoordinate(1)
    local viewy = math.floor(y - camy - 16)
    love.graphics.setScissor(0, viewy, 424, 240)
    self:drawUpperWrappers()
    love.graphics.setScissor()
end

function TiledMultiMap:haveUpperLayerAtCoord(x, y)
    local wrapper = self:getMapAtPoint(x, y)
    if (wrapper == nil) then
        return false
    end
    return wrapper:haveUpperLayerAtCoord(x, y)
end

function TiledMultiMap:drawUpperWrappers()
    local x, y, w, h = self.world.cameras:getViewCoordinate(1)
    local listWrapper = self:getMapsInRect(x, y, w, h)
    for i, wrapper in ipairs(listWrapper) do
        wrapper:drawUpperLayers()
    end
end

function TiledMultiMap:drawLowerLayers()
    local x, y, w, h = self.world.cameras:getViewCoordinate(1)
    local listWrapper = self:getMapsInRect(x, y, w, h)
    for i, wrapper in ipairs(listWrapper) do
        wrapper:drawLowerLayers()
    end
    self:drawUpperWrappers()
end

return TiledMultiMap
