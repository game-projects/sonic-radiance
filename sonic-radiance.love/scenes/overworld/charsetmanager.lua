local Charset = Object:extend()

local folder = "assets/sprites/charset/"
local animation = {1, 2, 3, 2}
local directionList = {"down", "right", "up", "left"}

local angle = {
  down = 270,
  right = 180,
  up = 90,
  left = 0
}

local CHARWIDTH = 38
local CHARHEIGHT = 48
local FAST_BOOST = 2.5

local FRAME_STANDING = 2

function Charset:new(scene)
  self.char = {}
  self.list = {}

  for i=1, 2 do
    for j=1, 4 do
      local id = ((i-1)*4) + j
      self.char[id] = self:addChar(i, j)
    end
  end
  self.currentFrame = 0
  self.fastFrame = 0
  self.angle = angle
end

function Charset:update(dt)
  if (core.screen:isActive()) then
    self.currentFrame = ((self.currentFrame + (dt*5)) % 4)
    self.fastFrame = ((self.fastFrame + (dt*5*FAST_BOOST)) % 4)
  end
end

function Charset:addChar(ii, jj)
  local charx, chary = (jj-1)*(CHARWIDTH*3), (ii-1)*(CHARHEIGHT*4)
  local char = {}
  for i=1, 4 do
    local animatedDirection = {}
    local running = {}
    for j=1, 3 do
      local x, y = charx + ((j-1)*CHARWIDTH), (chary + (i-1)*CHARHEIGHT)
      --print(x, y)
      running[j] = love.graphics.newQuad(x, y, CHARWIDTH, CHARHEIGHT, CHARWIDTH*12, CHARHEIGHT*8)
    end
    local direction = directionList[i]
    char[direction] = running
  end
  return char
end

function Charset:getTexture(charsetName)
  if (self.list[charsetName] == nil) then
    self:addTexture(charsetName)
  end
  return self.list[charsetName]
end

function Charset:addTexture(charsetName)
  self.list[charsetName] = love.graphics.newImage(folder .. charsetName .. ".png")
end

-- FRAME FUNCTIONS
-- Easily get frame

function Charset:getCurrentFrame(isFast)
  local frame = self.currentFrame
  if (isFast == true) then
    frame = self.fastFrame
  end
  return math.min(math.floor(frame) + 1, 4)
end

function Charset:getQuad(frame, charID, direction)
  local char = self.char[charID]
  local animatedDirection = char[direction]
  local trueFrame = animation[frame]
  return animatedDirection[trueFrame]
end

-- DRAW FUNCTIONS
-- Draw the charset in various situations
function Charset:draw(charsetName, charID, direction, frame, x, y, isMirrored)
  local drawable = self:getTexture(charsetName)
  local quad = self:getQuad(frame, charID, direction)
  local sx = 1
  if (isMirrored == true) then
    sx = -1
  end
  love.graphics.draw(drawable, quad, math.floor(x) + 8, math.floor(y), 0, sx, 1, 19, 32)
end

function Charset:drawMoving(charsetName, charID, direction, x, y, isFast, isMirrored)
  local frame = self:getCurrentFrame(isFast)
  self:draw(charsetName, charID, direction, frame, x, y, isMirrored)
end

function Charset:drawStanding(charsetName, charID, direction, x, y, isMirrored)
  self:draw(charsetName, charID, direction, FRAME_STANDING, x, y, isMirrored)
end

function Charset:drawLargeAnim(charsetName, charID, direction, x, y, isFast)
  local frame = FRAME_STANDING
  local isMirrored = false
  for i, dir in ipairs(directionList) do
    if (dir == direction) then
      frame = animation[i]
    end
  end
  if (direction == "left") then
    isMirrored = true
  end
  self:drawTurning(charsetName, charID, x, y, isFast, frame, isMirrored)
end

function Charset:drawTurning(charsetName, charID, x, y, isFast, frame, isMirrored)
  local frame = frame or FRAME_STANDING
  local dir = self:getCurrentFrame(isFast)
  return self:draw(charsetName, charID, directionList[dir], frame, x, y, isMirrored)
end

return Charset
