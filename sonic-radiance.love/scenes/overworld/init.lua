-- scenes/moveplayer :: a basic player movement example

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Scene = require "game.scenes"
local OverWorld = Scene:extend()

local World = require "scenes.overworld.world"
local CharsetManager = require "scenes.overworld.charsetmanager"

local TweenManager = require "birb.classes.time"
local EventManager = require "game.events"

local OWScreen = require "scenes.overworld.gui.init"
local MenusScreen = require "scenes.overworld.gui.menus"

function OverWorld:new(area, playerx, playery)
  OverWorld.super.new(self, false, false)
  self.charsetManager = CharsetManager(self)
  self.assets:batchImport("assets.overworld")

  self.tweens = TweenManager(self)

  World(self, area, playerx, playery)
  self.world:setPlayerNumber(1)
  self.world:loadMap()

  self.isPaused = false
  self.canPause = false
  self.gui:hideScreen("overlay")
  self.tweens:newSwitch(0.6, {"canPause"})

  self.isPlaying = ""

  self.events = EventManager(self)

  OWScreen()
  MenusScreen()
end

function OverWorld:updateCurrentMap(map)
  self:playMapMusic(map)

  self:showMessage(map.name)
  game.mapName = map.name
end

function OverWorld:playMapMusic(map)
  local newMusic = map.music
  if (newMusic ~= self.isPlaying) then
    self.assets:setMusic("assets/music/" .. newMusic .. ".mp3")
    self.assets:playMusic()
    self.isPlaying = newMusic
  end
end

function OverWorld:startEvent()
  self.world.isActive = false
  self.canPause = false
end

function OverWorld:endEvent()
  self.world.isActive = true
  self.canPause = true
end

function OverWorld:update(dt)
  local keys = self:getKeys(1)
  self.tweens:update(dt)
  self.events:update(dt)

  if (self.world.isActive) then
    self.charsetManager:update(dt)
  end

  if (keys["start"].isPressed and self.canPause) then
    if (not self.isPaused) then
      if (not self.gui.screens["overlay"].isVisible) then
        self.assets.sfx["mSelect"]:play()
        self:pause()
      end
    else
      self.assets.sfx["mBack"]:play()
      self:unpause()
    end
  end
end

function OverWorld:pause()
  self.gui:showScreen("overlay")
  self.gui:playScreenTransform("hud", "pause")
  self.gui:playScreenTransform("overlay", "showBackgroundPause")
  self.isPaused = true
  self.world.isActive = false
  self.gui:showScreen("startmenu")
end

function OverWorld:gameover()
  self.gui:showScreen("overlay")
  self.gui:hideScreen("hud")
  self.gui:playScreenTransform("overlay", "showBackgroundPause")
  self.world.isActive = false
end

function OverWorld:restored()
  self.world:restoreActions()
end

function OverWorld:timerResponse(timer)
  if (timer == "unPause") then
    self.isPaused = false
    self.world.isActive = true
  end
end

function OverWorld:unpause()
  self.tweens:newTimer(0.2, "unPause")
  self.gui:playScreenTransform("overlay", "hideBackgroundPause")
  self.gui:hideScreen("overlay")
  self.gui:playScreenTransform("hud", "unpause")
  self.gui:hideScreen("startmenu")
end

function OverWorld:getEmblemsPosition()
  return self.emblemPosition
end

function OverWorld:draw()
  self.events:draw()
end

return OverWorld
