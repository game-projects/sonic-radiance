local GuiElement = require "birb.modules.gui.elements.parent"
local Interactions = GuiElement:extend()

function Interactions:new()
    Interactions.super.new(self, "interactions", 428, 240 - 24, 50, 50)
    self.interactionName = ""
end

function Interactions:setInteractionName(interactionName)
    self.interactionName = interactionName
end

function Interactions:draw()
    if (not utils.string.isEmpty(self.interactionName)) then
        local w = self.assets.fonts["small"]:getWidth(self.interactionName) + 16
        love.graphics.setColor(0, 0, 0, 0.5)
        local x = self.x - w
        love.graphics.rectangle("fill", x - w / 2, self.y + 1, w, 15, 8, 8)
        love.graphics.setColor(1, 1, 1, 1)
        self.assets.fonts["small"]:draw(self.interactionName, x, self.y, -1, "center")
    end
    utils.graphics.resetColor()
end

return Interactions
