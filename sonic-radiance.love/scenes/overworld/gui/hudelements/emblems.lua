local GuiElement = require "birb.modules.gui.elements.parent"
local TeamEmblems = GuiElement:extend()

local Emblem = require "game.modules.gui.emblem"

function TeamEmblems:new(x, y)
    TeamEmblems.super.new(self, "teamEmblems", x, y, 50, 50)
    self.activeVisible = game.characters.active

    self.emblems = {}
    for i, name in ipairs(game.characters.team) do
      game.characters:loadSprite(self.assets, name)
      self.emblems[i] = Emblem(game.characters.list[name], self.scene)
    end
end

function TeamEmblems:changeActivePlayer(count)
  self.tweens:newTween(0, 0.3, {activeVisible = self.activeVisible + count}, "inQuad")
end

function TeamEmblems:draw()
  for i,emblem in ipairs(self.emblems) do
    local angle = ((i-self.activeVisible) * (360/#self.emblems)) - 90
    local rad = math.rad(angle)
    local emblemX, emblemY = utils.math.lengthdir(18, rad)
    emblem:draw(self.x + emblemX, self.y + emblemY)
  end
end

return TeamEmblems
