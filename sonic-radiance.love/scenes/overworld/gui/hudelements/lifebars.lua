local GuiElement = require "birb.modules.gui.elements.parent"
local LifeBars = GuiElement:extend()

local ComplexHPBar = require "game.modules.gui.complexhpbar"

local HPBAR_SIZE = 80

function LifeBars:new(x, y)
    LifeBars.super.new(self, "lifebars", x, y, 50, 50)
    self.hpbar = ComplexHPBar(HPBAR_SIZE)
    self.hpbar:setColorForeground(248/255, 160/255, 0, 1)
    self.hpbar:setColorBackground(112/255, 0, 0)
end

function LifeBars:draw()
    for i, name in ipairs(game.characters.team) do
        local yy = self.y + (i * 17)
      local character = game.characters.list[name]
      self.scene.assets.fonts["hudnbrs_small"]:set()
      self.hpbar:drawWithLabels(self.x + 18, yy, character.hp, character.stats:get(character.stats.HPMAX))
      self.assets.tileset["charicons"]:drawTile(character.data.icon, self.x, yy - 3)
    end
end

return LifeBars
