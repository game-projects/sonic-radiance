local TextElement = require "birb.modules.gui.elements.text"
local TimeElement = TextElement:extend()

function TimeElement:new(fontName, x, y, align)
    TimeElement.super.new(self, "time", fontName, "", x, y, align)
end

function TimeElement:getText()
    return game:getTimeString()
end

return TimeElement