local GuiScreen = require "birb.modules.gui.screen"
local OWScreen = GuiScreen:extend()

local Composite = require "birb.modules.gui.elements.composite"
local Counter = require "birb.modules.gui.elements.counter"
local Asset = require "birb.modules.gui.elements.assets"

local TimeElement = require "scenes.overworld.gui.hudelements.time"
local Emblems = require "scenes.overworld.gui.hudelements.emblems"
local Lifebars = require "scenes.overworld.gui.hudelements.lifebars"
local Interactions = require "scenes.overworld.gui.hudelements.interactions"

local show = {
    {"rings", "movement", 0, 0.3, 16, 16, "inOutQuart"},
    {"time", "movement", 0, 0.3, 408, 250, "inOutQuart"},
    {"teamEmblems", "movement", 0, 0.3, 368, 24, "inOutQuart"},
    {"lifebars", "movement", 0, 0.3, 8, 168, "inOutQuart"},
}

local hide = {
    {"rings", "movement", 0, 0.3, -16, -16, "inOutQuart"},
    {"time", "movement", 0, 0.3, 408, 250, "inOutQuart"},
    {"teamEmblems", "movement", 0, 0.3, 500, 24, "inOutQuart"},
    {"lifebars", "movement", 0, 0.3, -124, 168, "inOutQuart"},
}

local showMenu = {
    {"rings", "movement", 0, 0.5, 8, 8, "inOutQuart"},
    {"time", "movement", 0, 0.5, 408, 221, "inOutQuart"},
    {"teamEmblems", "movement", 0, 0.3, 500, 24, "inOutQuart"},
    {"lifebars", "movement", 0, 0.3, -124, 168, "inOutQuart"},
}

local hideMenu = {
    {"rings", "movement", 0, 0.5, 16, 16, "inOutQuart"},
    {"time", "movement", 0, 0.5, 408, 250, "inOutQuart"},
    {"teamEmblems", "movement", 0, 0.3, 368, 24, "inOutQuart"},
    {"lifebars", "movement", 0, 0.3, 8, 168, "inOutQuart"}
}

function OWScreen:new()
    OWScreen.super.new(self, "hud")
    self:addTransform("show", show)
    self:addTransform("hide", hide)
    self:addTransform("pause", showMenu)
    self:addTransform("unpause", hideMenu)
    self:show()
end

function OWScreen:createElements()
    local list = {
        {Composite("rings", -16, -16, {
            {Asset("guiRing", "images", "guiRing", -1, -1), 0, 0},
            {Counter("turnCnt", "hudnbrs", game.loot, "rings", 3, -1, -1), 14, 1}
        }), 0, -100},
        {TimeElement("hudnbrs", 408, 250, "right"), 0, -100},
        Emblems(500, 24),
        Lifebars(-124, 168),
        Interactions()
    }

    return list
end

return OWScreen
