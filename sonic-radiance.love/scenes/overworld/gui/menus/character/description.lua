local Description = require("birb.modules.gui.elements.canvas"):extend()
local PLAYER_MESSAGE = 240 - 24

function Description:new(owner, name)
    self.owner = owner
    Description.super.new(self, name, 0, PLAYER_MESSAGE, 424, 16)
    self.canvas.isAnimated = true
end

function Description:drawTexture()
    if (not utils.string.isEmpty(self.owner:getDescription())) then
      love.graphics.setColor(0,0,0, 0.66)
      love.graphics.rectangle("fill", self.canvas.padding, self.canvas.padding, self.w, self.h)
      self.gui.scene.assets.fonts["small"]:setColor(1,1,1, 1)
      self.gui.scene.assets.fonts["small"]:draw(self.owner:getDescription(), self.canvas.padding, self.canvas.padding - 1, self.w, "center")
      self.gui.scene.assets.fonts["small"]:setColor(1,1,1, 1)
      utils.graphics.resetColor()
    end
end

return Description