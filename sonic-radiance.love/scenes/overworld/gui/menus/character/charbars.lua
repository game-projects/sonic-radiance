local CharBars = require("birb.modules.gui.elements.canvas"):extend()

local HPBAR_SIZE = 80
local W, H = HPBAR_SIZE*2, 32

local const = require "scenes.overworld.gui.menus.commons.const"

local ComplexHPBar = require "game.modules.gui.complexhpbar"

function CharBars:new(name, x, y, character)
    CharBars.super.new(self, name, x, y, W, H)
    self.canvas.padding = 24
    self.character = character

    self.hpbar = ComplexHPBar(HPBAR_SIZE)
    self.ppbar = ComplexHPBar(HPBAR_SIZE)
    self.hpbar:setColorForeground(248/255, 160/255, 0, 1)
    self.hpbar:setColorBackground(112/255, 0, 0)
    self.ppbar:setColorForeground(0, 248/255, 248/255, 1)
    self.ppbar:setColorBackground(0, 54/255, 229/255)

    self.opacity = 0
end

function CharBars:drawTexture()
    local stats = self.character.stats
    local x, y = self.canvas.padding, self.canvas.padding
    self.scene.assets.fonts["hudnbrs_small"]:set()
    self.hpbar:drawWithLabels(x, y - 4, self.character.hp, stats:get(stats.HPMAX))
    local xx = x + const.CHARPAGESIZE - HPBAR_SIZE - 7
    self.ppbar:drawWithLabels(xx, y - 4, self.character.pp, stats:get(stats.PPMAX))
end

return CharBars