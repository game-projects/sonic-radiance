local TextsFuncs = {}

local STATS = require "datas.consts.stats"

function TextsFuncs.getIdentityBox(character)
    local identityString = character.fullname .. "\n"
    identityString = identityString .. "Class: " .. character.data.class

    return {{1, identityString}}
end

function TextsFuncs.getLevelBox(character)
    local levelString = "Level: " .. character.level .. "\n"
    levelString = levelString .. "Current exp: " .. character.exp .. "\n"
    levelString = levelString .. "Next level: " .. character.exp_next

    return {{1, levelString}}
end

function TextsFuncs.getWeakStrongBox(character)
    local weakStrongString = "Weak to: Nothing" .. "\n"--"Earth, Lightning"
    weakStrongString = weakStrongString .. "Resist To: Nothing"

    return {{1, weakStrongString}}
end

function TextsFuncs.getStatsBox(character, pageSize)
    local char = character
    local statStringList = {}
    for i, statName in ipairs(STATS.LIST) do
        local xStat = (((i - 1) % 2) * (pageSize/2)) + 6
        local line = math.floor((i + 1)/2)
        local middle = xStat + 10 + pageSize/4
        local stat = char.stats:get(statName)
        table.insert(statStringList, {line, STATS.SIMPLENAME[statName], "left", xStat, 0, {1,1,1,1}, pageSize})
        table.insert(statStringList, {line, stat, "center", middle, 0, {1,1,1,0.9}, -1})
    end
    return statStringList
end

return TextsFuncs