local PageIndicator = require("birb.modules.gui.elements.canvas"):extend()

local DIST = 16
local RAD_SMALL, RAD_LARGE = 2, 3
local OUTLINE_LIGHT = 0.15

function PageIndicator:new(owner, name, x, y, pageList)
    self.pageList = pageList
    self.owner = owner
    local w, h = (DIST*(#pageList + 1)), 16
    PageIndicator.super.new(self, name, x, y, w, h)
    self.canvas.isAnimated = true
    self.opacity = 0
end

function PageIndicator:drawTexture()
    for i, pageName in ipairs(self.pageList) do
        local radius = utils.math.either (pageName == self.owner:getCurrentPageName(), RAD_LARGE, RAD_SMALL)
        local x, y = (i+1) * DIST, math.floor(self.h / 2)
        love.graphics.circle("fill", x, y, radius, 8)
        love.graphics.setColor(OUTLINE_LIGHT, OUTLINE_LIGHT, OUTLINE_LIGHT, 1)
        love.graphics.circle("line", x, y, radius, 8)
        utils.graphics.resetColor()
    end
end

return PageIndicator