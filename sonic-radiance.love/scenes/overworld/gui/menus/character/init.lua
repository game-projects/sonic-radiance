local CharacterScreen = require("birb.modules.gui.screen"):extend()
local DrawableElement = require("birb.modules.gui.elements.drawable")

local BoxedText = require "scenes.overworld.gui.menus.commons.boxedtext"
local PageIndicator = require "scenes.overworld.gui.menus.character.pageindicator"
local CharBars = require "scenes.overworld.gui.menus.character.charbars"
local Description = require "scenes.overworld.gui.menus.character.description"

local FancyMenu = require "game.modules.gui.fancymenu"
local BoxedMenu = require "game.modules.gui.boxedmenu"

local const     = require "scenes.overworld.gui.menus.commons.const"
local subpages  = require "scenes.overworld.gui.menus.character.subpages"
local textFunc  = require "scenes.overworld.gui.menus.character.texts"

local MENU_W = 128
local MENU_X, MENU_Y = const.X2 - MENU_W - 14, const.Y2 - 48 + 2
local PAGEINDIC_X, PAGEINDIC_Y = const.X2 - 16*5, const.Y - 14

function CharacterScreen:new(characterName)
    self.characterName = characterName
    self.artworkPosition = require("datas.gamedata.characters." .. characterName .. ".artwork")
    self.character = game.characters.list[characterName]
    CharacterScreen.super.new(self, characterName)
    self:setCustomTransforms()

    self.defaultFocus = self.characterName .. "ActMenu"
    self.nbr = 1
end

function CharacterScreen:setCustomTransforms()
    local show = {}
    local hide = {}
    self:addCustomTween(show, "Artwork", false, 1, nil, self.artworkPosition.x)
    self:addCustomTween(hide, "Artwork", true, -1, nil, self.artworkPosition.x)
    self:addCustomTween(show, "ActMenu", false, 1, nil, MENU_X)
    self:addCustomTween(hide, "ActMenu", true, -1, nil, MENU_X)
    self:addCustomTween(show, "Indicator", false, 1, nil, PAGEINDIC_X)
    self:addCustomTween(hide, "Indicator", true, -1, nil, PAGEINDIC_X)
    self:addTweens(show, "basic", false, -1)
    self:addTweens(hide, "", true, 1)

    self:addTransform("show", show)
    self:addTransform("hide", hide)
    self.desc = ""
end

function CharacterScreen:createElements()
    local elementDatas = subpages.getElements()
    local list = {}

    local drawable = love.graphics.newImage("datas/gamedata/characters/" .. self.characterName .. "/artwork.png")
    table.insert(list, {DrawableElement(self.characterName .. "Artwork", drawable, self.artworkPosition.x, self.artworkPosition.y,0,1,1,0,0, 0), 0, 1})

    self.dataMenus = {}
    for i, element in ipairs(elementDatas) do
        local name = self.characterName .. element.name
        local x, y, w, h = const.X - 16, const.Y + element.data.y, const.CHARPAGESIZE, element.data.h
        if (element.data.isTextBox) then
            local boxedText = BoxedText(name, x, y, w, h, function() return textFunc["get" .. element.name](self.character, const.CHARPAGESIZE) end)
            table.insert(list, {boxedText, 0, 1})
        elseif (element.data.isMenu) then
            self.dataMenus[element.name] = BoxedMenu(name, x, y + 4, w, h, true, true)
            self.dataMenus[element.name].opacity = 0
            self.dataMenus[element.name]:addCancelAction(function ()
                self.gui.scene.assets.sfx["mBack"]:play()
                self.gui:setFocus(self.characterName .. "ActMenu")
                self.desc = ""
            end)
            table.insert(list, {self.dataMenus[element.name], 0, 1})
        else
            table.insert(list, {CharBars(name, x, y, self.character), 0, 1})
        end
    end

    self:rebuildMenus()
    table.insert(list, {self:createGlobalMenu(), 0, 1})
    table.insert(list, {PageIndicator(self, self.characterName .. "Indicator", PAGEINDIC_X, PAGEINDIC_Y, subpages.list), 0, 1})
    table.insert(list, {Description(self, self.characterName .. "Description"), 0, -100})

    return list
end

function CharacterScreen:rebuildMenus()
    for i, equip in ipairs(subpages.equip) do
        local fullname = "No " .. equip
        local obj = self.character.equip[equip]
        if (not utils.string.isEmpty(obj)) then
            fullname =  core.datas:get("items", obj).fullname
        end
        self.dataMenus["EquipMenu"]:addItem(fullname, "left", function () end, "select")
    end

    for _, skill in ipairs(self.character.skills) do
        local skillData = core.datas:get("skills", skill.name)
        local cost = utils.math.numberToString(skillData.cost, 2)
        self.dataMenus["SkillsMenu"]:addItem(skillData.fullname, "left", function () end, "select", {{cost, "right"}}, nil, {skillData.description})
    end

    self.dataMenus["SkillsMenu"]:addHoverAction(function (widget) 
        self.desc = widget.datas[1]
    end)
end

function CharacterScreen:getDescription()
    return self.desc or ""
end

function CharacterScreen:createGlobalMenu()
    self.menu = FancyMenu(self.characterName .. "ActMenu", MENU_X, MENU_Y, MENU_W, 3, false)
    self.menu.opacity = 0

    for i, name in ipairs(subpages.list) do
        self.menu:addPage(name)
    end

    self.menu:switch("skills")
    self.menu:addItem("See skill", "left", function()
            self.gui:setFocus(self.characterName .. "SkillsMenu")
            self.desc = core.datas:get("skills", self.character.skills[1].name).description
        end, "navigate")
    self.menu:switch("stats")
    self.menu:addItem("Remove item", "left", function() self.gui:setFocus(self.characterName .. "EquipMenu") end, "navigate")

    for i, name in ipairs(subpages.list) do
        self.menu:switch(name)
        self.menu:addItem("Back", "left", function() self.gui:showScreen("mainmenuScreen", "navCharMenu", self.nbr, "main") end, "back")
        self.menu:setCancelWidget()
    end
    self.menu:addLateralAction(function (key) if (key == "left") then self:changePage(-1) else self:changePage(1) end end)
    self.menu.packAtEnd = 1

    self.menu:switch(subpages.list[1])
    return self.menu
end

function CharacterScreen:changePage(direction)
    local currentPage = self.menu:getCurrentPageName()
    self.menu:switch(subpages.relativePage(currentPage, direction))
    self:hidePage(currentPage, utils.math.sign(direction))
    self:showPage(self.menu:getCurrentPageName(), utils.math.sign(direction))
    self.gui.scene.assets.sfx["mBeep"]:play()
end

function CharacterScreen:hidePage(name, direction)
    local transform = {}
    self:addTweens(transform, name, true, direction)
    self.gui:transform(transform, 0)
end

function CharacterScreen:showPage(name, direction)
    local transform = {}
    self:addTweens(transform, name, false, direction)
    self.gui:transform(transform, 0)
end

function CharacterScreen:addTweens(list, name, isHiding, direction)
    for _, elem in ipairs(subpages.getElements(name)) do
        self:addCustomTween(list, elem.name, isHiding, direction)
    end
end

function CharacterScreen:addCustomTween(list, name, isHiding, direction, speed, x)
    local begin, duration = utils.math.either(isHiding, 0, 0.1), speed or 0.3
    local trueDirection, opacity = utils.math.either(isHiding, direction, 0), utils.math.either(isHiding, 0, 1)
    x = x or const.X
    if (not isHiding) then self:addCustomTween(list, name, true, direction * -1, 0.1, x) end
    table.insert(list, {self.characterName .. name, "tween", begin, duration, {x = x - 16*trueDirection, opacity = opacity}, "inOutQuad"})
end

function CharacterScreen:getCurrentPageName()
    return self.menu:getCurrentPageName()
end

return CharacterScreen