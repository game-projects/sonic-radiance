local const = require "scenes.overworld.gui.menus.commons.const"
local subpages = {}

subpages.list = {"basic", "stats", "skills"}

subpages.elements = {
    ["basic"] = {"IdentityBox", "Bars", "LevelBox", "WeakStrongBox"},
    ["stats"] = {"EquipMenu", "StatsBox"},
    ["skills"] = {"SkillsMenu"}
}

subpages.equip = {"gloves", "shoes", "accessories"}

subpages.elementsData = {
    IdentityBox     = {y = 0,   h = 40, isTextBox = true },
    Bars            = {y = 48,  h = 0,  isTextBox = false, isMenu = false},
    LevelBox        = {y = 72-13,  h = 56, isTextBox = true},
    WeakStrongBox   = {y = 132-13, h = 40, isTextBox = true },
    EquipMenu       = {y = 0,   h = 3,  isTextBox = false, isMenu = true},
    StatsBox        = {y = 72,  h = 72, isTextBox = true },
    SkillsMenu      = {y = 0,   h = 9,  isTextBox = false, isMenu = true},
}

function subpages.fromId(id)
    return subpages.list[id]
end

function subpages.fromName(name)
    for i, subPageName in ipairs(subpages.list) do
        if subPageName == name then
            return i
        end
    end
    return -1
end

function subpages.relativePage(pageName, direction)
    local id = utils.math.wrap(subpages.fromName(pageName) + direction, 1, #subpages.list)
    return subpages.fromId(id)
end

function subpages.getElements(filter)
    local list = {}
    for i, subpageName in ipairs(subpages.list) do
        if (filter == nil or filter == "" or filter == subpageName) then
            for j, elementName in ipairs(subpages.elements[subpageName]) do
                local data = subpages.elementsData[elementName]
                table.insert(list, {name = elementName, page = subpageName, data = data})
            end
        end
    end

    return list
end

return subpages