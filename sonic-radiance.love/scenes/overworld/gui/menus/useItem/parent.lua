local UseScreenParent = require("birb.modules.gui.screen"):extend()
local CharacterMenu = require("scenes.overworld.gui.menus.commons.charmenu")
local BoxedText = require "scenes.overworld.gui.menus.commons.boxedtext"
local ItemNumber = require "scenes.overworld.gui.menus.useItem.number"
local const = require "scenes.overworld.gui.menus.commons.const"

local DESC_H = 40 + 48
local NBR_Y = 24

function UseScreenParent:new(prefix, descW, showEquip)
    self.prefix = prefix

    self.descW = descW
    self.showEquip = showEquip
    self.nbrX = const.X + self.descW - 48
    self.charMenuX = const.X + self.descW + 8

    UseScreenParent.super.new(self, prefix .. "Screen")
    self:addTransformsWithPrefix()
    self.defaultFocus = prefix .. "Menu"
end

function UseScreenParent:addTransformsWithPrefix()
    local show = {
        {self.prefix .. "Desc", "movement", 0.1, 0.3, const.X, const.Y, "inOutQuart"},
        {self.prefix .. "Desc", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
        {self.prefix .. "Nbr", "movement", 0, 0.3, self.nbrX, const.Y2 - NBR_Y, "inOutQuart"},
        {self.prefix .. "Nbr", "tween", 0, 0.3, {opacity = 1}, "inOutQuart"},
        {self.prefix .. "Menu", "movement", 0.1, 0.3, self.charMenuX, const.Y, "inOutQuart"},
        {self.prefix .. "Menu", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
    }

    local hide = {
        {self.prefix .. "Desc", "movement", 0, 0.3, const.X - 16, const.Y, "inOutQuart"},
        {self.prefix .. "Desc", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
        {self.prefix .. "Nbr", "movement", 0, 0.3, self.nbrX - 16, const.Y2 - NBR_Y, "inOutQuart"},
        {self.prefix .. "Nbr", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
        {self.prefix .. "Menu", "movement", 0, 0.3, self.charMenuX + 16, const.Y, "inOutQuart"},
        {self.prefix .. "Menu", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
    }

    self:addTransform("show", show)
    self:addTransform("hide", hide)
end

function UseScreenParent:createElements()
    self.charMenu = CharacterMenu(self.prefix .. "Menu", self.charMenuX + 16, function(name) self:useItem(name) end, self.showEquip)
    self.charMenu:addCancelAction(function() self:exitScreen() end)
    self.charMenu.opacity = 0
    return {
        {self.charMenu, 0, 1},
        {BoxedText(self.prefix .. "Desc", const.X - 16, const.Y, self.descW, DESC_H,
            function() if (self.itemData ~= nil) then return self:getDescription() else return {{1, ""}} end end), 0, 1},
        {ItemNumber(self, self.prefix .. "Nbr", self.nbrX - 16, const.Y2 - NBR_Y), 0, 1}
    }
end

function UseScreenParent:useItem(charName)
    local character = game.characters.list[charName]
    self:applyEffect(character)
    self.gui:getScreen("itemScreen"):rebuildMenu()
    if (self.item.number <= 1) then
        self:exitScreen()
    end
end

function UseScreenParent:exitScreen()
    self.gui:showScreen("itemScreen", nil, self.nbr, self.category)
    self.gui.scene.assets:playSFX("mBack")
end

function UseScreenParent:setDatas(datas)
    --category, item, widgetId)
    local category = datas[1]
    self.item = datas[2]
    self.nbr = datas[3] or 1
    self.itemData = core.datas:get("items", self.item.name)
    self.category = category

    self.effects = game.loot:getEffectStrings(self.category, self.item.name)
end

function UseScreenParent:getItemNumber()
    if (self.item ~= nil) then
        return self.item.number or 0
    end
    return 0
end


return UseScreenParent