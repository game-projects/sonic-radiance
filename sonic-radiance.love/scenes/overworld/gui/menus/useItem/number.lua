local ItemNumberElement = require("birb.modules.gui.elements.canvas"):extend()

local W = 48 + 16
local gui = require "game.modules.gui"

function ItemNumberElement:new(owner, name, x, y)
    ItemNumberElement.super.new(self, name, x, y, W, 18)

    self.choiceBack = gui.newChoiceBack(W)
    self.canvas.isAnimated, self.canvas.padding = true, 0
    self.owner = owner
    self.opacity = 0
end

function ItemNumberElement:drawTexture()
    love.graphics.draw(self.choiceBack, 0, 0)

    self.scene.assets.fonts["small"]:draw("x" .. self.owner:getItemNumber(), 16, -2, -1, "left")
end

return ItemNumberElement