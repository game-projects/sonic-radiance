local SetEquipScreen = require("scenes.overworld.gui.menus.useItem.parent"):extend()
local STATS = require "datas.consts.stats"

function SetEquipScreen:new()
    SetEquipScreen.super.new(self, "setEquip", 32 * 6, true)
end

function SetEquipScreen:applyEffect(character)
    character:setEquip(self.category, self.item.name)
end

function SetEquipScreen:getDescription()
    local returnData = {}
    table.insert(returnData, {1, self.itemData.fullname})
    self.charName = self.gui:getElement("setEquipMenu").charName
    if (self.charName ~= "") then
        local char = game.characters.list[self.charName]

        for i, statName in ipairs(STATS.LIST) do
            local lineNbr = math.floor((i-1)/2)
            local isLeft = ((i%2) == 1)

            local stat = char.stats:get(statName)
            local newStat = char:predictStat(statName, self.category, self.item.name)

            local sep = ":  "
            if i > 2 then
                sep = ": "
            end

            local line = STATS.SIMPLENAME[statName] .. sep .. utils.math.numberToString(stat, 3) .. " > " .. utils.math.numberToString(newStat, 3)
            local color = {1, 1, 1, 1}

            if (newStat > stat) then
                color = {0.3, 1, 0.3, 0.9}
            elseif (newStat < stat) then
                color = {1, 0.3, 0.3, 0.9}
            end

            table.insert(returnData, {lineNbr + 2, line, utils.math.either(isLeft, "left", "right"), 0, 0, color})
        end

    end
    return returnData
end

function SetEquipScreen:setDatas(datas)
    SetEquipScreen.super.setDatas(self, datas)
    self.charMenu.showEquip = self.category
end

return SetEquipScreen