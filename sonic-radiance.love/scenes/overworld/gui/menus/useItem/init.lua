local UseItemScreen = require("scenes.overworld.gui.menus.useItem.parent"):extend()
local EffectManager = require "game.loot.effectManager"

function UseItemScreen:new()
    self.effectManager = EffectManager()
    UseItemScreen.super.new(self, "useItem", 128, false)
end

function UseItemScreen:applyEffect(character)
    self.effectManager:applyEffects(character)
    game.loot:removeItem(self.category, self.item.name, 1)
end

function UseItemScreen:setDatas(datas)
    UseItemScreen.super.setDatas(self, datas)
    self.effectManager:getItemData(self.category, self.item.name)
end

function UseItemScreen:getDescription()
    return {{1, self.itemData.fullname .. ":\n" .. self.effects}}
end

return UseItemScreen