local ConstMenu = {}

ConstMenu.X = 28
ConstMenu.Y = 40
ConstMenu.WIDTH = 424 - 56
ConstMenu.HEIGHT = 240 - 80
ConstMenu.X2 = 424 - 28
ConstMenu.Y2 = 240 - 40
ConstMenu.CHARPAGESIZE = 176

return ConstMenu
