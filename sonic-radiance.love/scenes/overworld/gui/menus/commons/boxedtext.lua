local BoxedTextElement = require("birb.modules.gui.elements.canvas"):extend()
local gui = require "game.modules.gui"

local PADDING = 6
local PADDING_H = 4
local LINEHEIGHT = 16

function BoxedTextElement:new(name, x, y, w, h, linesFunc)
    BoxedTextElement.super.new(self, name, x, y, w, h)
    self.textwidth = w - (PADDING * 2)
    self.opacity = 0
    self.canvas.isAnimated = true
    self.linesFunc = linesFunc

    self.box = gui.newTextBox("assets/gui/dialogbox.png", w, h)
    self.canvas.padding = 0
end

function BoxedTextElement:getLines()
    -- return {line, text, align, x, y, color}
    return self.linesFunc()
end

function BoxedTextElement:drawTexture()
    love.graphics.draw(self.box, 0, 0)
    local lines = self:getLines()
    assert(lines ~= nil, "No lines for " .. self.name)
    for i, line in ipairs(lines) do
        local lineNbr, text, align, x, y = line[1], line[2] or "", line[3] or "left", line[4] or 0, line[5] or 0
        local color, width = line[6] or {1, 1, 1, 1}, line[7] or self.textwidth
        self.assets.fonts["small"]:setColor(color[1], color[2], color[3], color[4])
        self.assets.fonts["small"]:draw(text, PADDING + x, PADDING_H + LINEHEIGHT*(lineNbr-1) + y, width, align or "left")
        self.assets.fonts["small"]:setColor(1, 1, 1, 1)
        utils.graphics.resetColor()
    end
end

return BoxedTextElement