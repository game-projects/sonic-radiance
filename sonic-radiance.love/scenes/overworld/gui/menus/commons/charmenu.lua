local CharacterMenu = require("birb.modules.gui.menus.listbox"):extend()
local CharacterWidget = require("birb.modules.gui.menus.widgets.base"):extend()
local const = require "scenes.overworld.gui.menus.commons.const"

local ComplexHPBar = require "game.modules.gui.complexhpbar"
local Emblem = require "game.modules.gui.emblem"

function CharacterMenu:new(name, x, func, showEquip)
    local w = const.WIDTH - x + 28
    CharacterMenu.super.new(self, name, x, const.Y, w, const.HEIGHT, 4)
    self.func = func
    self.showEquip = (showEquip == true)
    self.opacity = 1
    self:buildFromTeam()

    self.canvas.isAnimated = true
end

function CharacterMenu:buildFromTeam()
    self.charName = game.characters.team[1]
    for i, name in ipairs(game.characters.team) do
        local widget = CharacterWidget(self.name, name)
        widget:setFunc(function () self.func(name, i) end)
    end
    self:addHoverAction(function(widget) self.charName = widget.charName end)
end

function CharacterWidget:new(menuName, charName)
    CharacterWidget.super.new(self, menuName)

    self.charName = charName
    self.emblem = Emblem(game.characters.list[charName], self.scene)
    self.font = self.scene.assets.fonts["small"]
    self.font2 = self.scene.assets.fonts["hudnbrs_small"]
    self.hpbar = ComplexHPBar(88)
    self.ppbar = ComplexHPBar(88)
    self.hpbar:setColorForeground(248 / 255, 160 / 255, 0, 1)
    self.hpbar:setColorBackground(112 / 255, 0, 0)
    self.ppbar:setColorForeground(0, 248 / 255, 248 / 255, 1)
    self.ppbar:setColorBackground(0, 54 / 255, 229 / 255)

    self.canvas.isAnimated = true

    self.character = game.characters.list[self.charName]
end

function CharacterWidget:drawCanvas()
    local xDebut = 32
    local yDebut = 16

    self.emblem:draw(0, 6)

    self.font:setFilter("shadow")
    self.font:draw(self.character.fullname, xDebut, 0, -1, "left")

    if (self.menu.showEquip ~= nil and self.menu.showEquip ~= false) then
        self:drawEquip(xDebut, yDebut)
    else
        self:drawHPPP(xDebut, yDebut)
        self:drawLvlExp(xDebut, yDebut)
    end
end

function CharacterWidget:drawLvlExp(x, y)
    self.scene.assets.images["lvl"]:draw(x, y)
    self.scene.assets.images["exp"]:draw(x, y + 10)
    self.font2:print(self.character.level, x + 19, y, "left")
    local expString = self.character.exp .. "/" .. self.character.exp_next
    self.font2:print(expString, x + 19, y + 10, "left")
end

function CharacterWidget:drawEquip(x, y)
    local charEquip = self.character.equip[self.menu.showEquip]
    local equipString = "None"
    if (not utils.string.isEmpty(charEquip)) then
        local data = core.datas:get("items", charEquip)
        equipString = data.fullname
    end
    self.font:draw(equipString, x, y, -1, "left")
end

function CharacterWidget:drawHPPP(x, y)
    local xDebut = x + 20
    local yDebut = y - 1
    self.scene.assets.fonts["hudnbrs_small"]:set()
    local stats = self.character.stats
    self.hpbar:drawWithLabels(xDebut + 53, yDebut, self.character.hp, stats:get(stats.HPMAX))
    self.ppbar:drawWithLabels(xDebut + 64, yDebut + 11, self.character.pp, stats:get(stats.PPMAX))
end

return CharacterMenu
