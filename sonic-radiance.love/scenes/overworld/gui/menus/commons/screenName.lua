local TextElement = require "birb.modules.gui.elements.text"
local ScreenNameElement = TextElement:extend()

local nameList = {
    mainmenuScreen = "Menu",
    itemScreen = "Items",
    useItemScreen = "Use item",
    setEquipScreen = "Equip item"
}

function ScreenNameElement:new(owner, fontName, x, y, align)
    self.owner = owner
    ScreenNameElement.super.new(self, "screenName", fontName, "", x, y, align)
    self.opacity = 0
    self.currentScreen = "Menu"
end


function ScreenNameElement:getText()
    if (self.owner.subscreens == nil) then
        return "Menu"
    end
    local newScreen = self.owner.subscreens.currentScreen
    if (not utils.string.isEmpty(newScreen)) then
        if game.characters.list[newScreen] ~= nil then
            local data = core.datas:get("characters", newScreen)
            self.currentScreen = data.name
        else
            self.currentScreen = nameList[newScreen]
        end
    end
    return self.currentScreen or ""
end

return ScreenNameElement