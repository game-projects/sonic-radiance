local GuiScreen = require "birb.modules.gui.screen"
local MenuScreen = GuiScreen:extend()

local ScreenNameElement = require "scenes.overworld.gui.menus.commons.screenName"

local MainMenuScreen = require "scenes.overworld.gui.menus.mainmenu"
local ItemScreen = require "scenes.overworld.gui.menus.items"
local UseItemScreen = require "scenes.overworld.gui.menus.useItem"
local SetEquipScreen = require "scenes.overworld.gui.menus.useItem.equip"
local CharacterScreen = require "scenes.overworld.gui.menus.character"

local show = {
    {"screenName", "movement", 0, 0.5, 160, 12, "inOutQuart"},
    {"screenName", "tween", 0, 0.5, {opacity = 1}, "inOutQuart"},
}

local hide = {
    {"screenName", "movement", 0, 0.5, 160, -18, "inOutQuart"},
    {"screenName", "tween", 0, 0.5, {opacity = 0}, "inOutQuart"},
}

function MenuScreen:new()
    MenuScreen.super.new(self, "startmenu")
    self:addTransform("show", show)
    self:addTransform("hide", hide)
    self:addSubscreen(MainMenuScreen())
    self:addSubscreen(ItemScreen())
    self:addSubscreen(UseItemScreen())
    self:addSubscreen(SetEquipScreen())
    for charName, _ in pairs(game.characters.list) do
        self:addSubscreen(CharacterScreen(charName))
    end
    self.subscreens.delay = -1
end

function MenuScreen:createElements()
    local list = {
        {ScreenNameElement(self, "SA2font", 160, -18, "left"), 0, -100}
    }

    return list
end

return MenuScreen
