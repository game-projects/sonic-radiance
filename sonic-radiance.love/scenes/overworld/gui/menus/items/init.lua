local ItemsScreen = require("birb.modules.gui.screen"):extend()
local ItemMenu = require("game.modules.gui.boxedmenu")
local BoxedText = require("scenes.overworld.gui.menus.commons.boxedtext")
local ItemRoulette = require("scenes.overworld.gui.menus.items.roulette")

local POCKET_LIST = require "datas.gamedata.items"
local const = require "scenes.overworld.gui.menus.commons.const"

local MENU_WIDTH, DESC_SIZE, USE_SIZE = 128+32, 48*4, 96

local EFFECT_H, DESC_H = 40, 56
local HIDE_X, USE_X = 16, 16 - USE_SIZE / 2
local DESC_Y, USE_Y = (EFFECT_H + 8) + DESC_H, 24

local ConfirmDialog = require "game.modules.confirmdialog"

local show = {
    {"itemRoulette", "movement", 0.1, 0.3, const.X, const.Y, "inOutQuart"},
    {"itemRoulette", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
    {"descText", "movement", 0.1, 0.3, const.X, const.Y2 - DESC_Y, "inOutQuart"},
    {"descText", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
    {"effectText", "movement", 0.1, 0.3, const.X, const.Y2 - EFFECT_H, "inOutQuart"},
    {"effectText", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
    {"itemMenu", "movement", 0.1, 0.3, const.X2 - MENU_WIDTH, const.Y + 4, "inOutQuart"},
    {"itemMenu", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
    {"useMenu", "tween", 0.0, 0.3, {opacity = 0, sx = 1, sy = 1}, "inOutQuart"},
}

local hide = {
    {"itemRoulette", "movement", 0, 0.3, const.X - HIDE_X, const.Y, "inOutQuart"},
    {"itemRoulette", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
    {"descText", "movement", 0, 0.3, const.X - HIDE_X, const.Y2 - DESC_Y, "inOutQuart"},
    {"descText", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
    {"effectText", "movement", 0, 0.3, const.X - HIDE_X, const.Y2 - EFFECT_H, "inOutQuart"},
    {"effectText", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
    {"itemMenu", "movement", 0, 0.3, const.X2 - MENU_WIDTH + HIDE_X, const.Y + 4, "inOutQuart"},
    {"itemMenu", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
    {"useMenu", "tween", 0.0, 0.3, {opacity = 0, sx = 0.8, sy = 0.8}, "inOutQuart"},
}

local showUseMenu = {
    {"useMenu", "tween", 0.0, 0.3, {opacity = 1, sx = 1, sy = 1}, "inOutQuart"},
    {"useMenu", "delayFocus", 0}
}

local hideUseMenu = {
    {"useMenu", "tween", 0.0, 0.3, {opacity = 0, sx = 0.8, sy = 0.8}, "inOutQuart"},
    {"itemMenu", "delayFocus", 0}
}

function ItemsScreen:new()
    self.desc, self.effect = "", ""
    ItemsScreen.super.new(self, "itemScreen")
    self:addTransform("show", show)
    self:addTransform("hide", hide)
    self:addTransform("showUseMenu", showUseMenu)
    self:addTransform("hideUseMenu", hideUseMenu)
    self.defaultFocus = "itemMenu"
end

function ItemsScreen:createElements()
    self.menu = ItemMenu("itemMenu", const.X2 - MENU_WIDTH, const.Y + 4, MENU_WIDTH, 9, true, true)
    self.menu.opacity = 0
    self:rebuildMenu()

    self.use = ItemMenu("useMenu", const.X2 + USE_X, const.Y2 - USE_Y, USE_SIZE, 3, true, false)
    self.use.opacity, self.use.ox, self.use.oy = 0, USE_SIZE/2, self.use.h / 2

    return {{self.menu, 0, 1},
        {BoxedText("descText", const.X, const.Y2 - DESC_Y, DESC_SIZE, DESC_H, function () return {{1, self.desc}} end), 0, 1},
        {BoxedText("effectText", const.X, const.Y2 - EFFECT_H, DESC_SIZE, EFFECT_H, function () return {{1, self.effect}} end), 0, 1},
        {ItemRoulette(self, const.X, const.Y), 0, 1},
        {self.use, 0, 1}
    }
end

function ItemsScreen:rebuildMenu()
    self.menu:clear()
    for i, pocket in ipairs(POCKET_LIST) do
        self.menu:addPage(pocket.name)
        self.menu:switch(pocket.name)
        local pocketData = game.loot:getPocketById(i)

        for j,item in ipairs(pocketData.list) do
            local itemData = core.datas:get("items", item.name)
            self.menu:addItem(itemData.fullname, "left", function () self:showUseMenu(item, j) end, "navigate", {{"x" .. utils.math.numberToString(item.number, 2), "right"}}, nil, {item.name})
        end

        self.menu:addItem("Back", "left", function() self.gui:showScreen("mainmenuScreen") end, "back")
        self.menu:addHoverAction(function(widget) self:setDescEffectFromWidget(widget) end)
        self.menu:setCancelWidget()
        self.menu:addLateralAction(function (key) if (key == "left") then self:changePage(-1) else self:changePage(1) end end)
    end
    self.menu:switch("medicines")
end

function ItemsScreen:showUseMenu(item, i)
    self.dropQuantity = 1
    self.use:clear()
    self.use:addItem("Use", "left", function () self:useItem(item.name, i) end, "")
    self.use:addItem("Drop", "left", function () self:dropItem(item.name) end, "select", {{"<" .. utils.math.numberToString(self.dropQuantity, 2) .. ">", "right"}})
    self.use:addItem("Back", "left", function () self:playTransform("hideUseMenu") end, "back")
    self.use:addLateralAction(function(key, funcWidget, id)
        if (key == "left" and id == 2) then
            self:modifyDrop(-1, funcWidget, item.number)
        elseif (key == "right" and id == 2) then
            self:modifyDrop(1, funcWidget, item.number)
        end end)
    self.use:setCancelWidget()
    self:playTransform("showUseMenu")
end

function ItemsScreen:useItem(itemName, i)
    local pocketData = game.loot:getPocketByName(self.menu:getCurrentPageName())
    local itemData = core.datas:get("items", itemName)
    if (pocketData.isEquipement) then
        self.gui:showScreen("setEquipScreen", nil, nil, nil, {self.menu:getCurrentPageName(), game.loot:getPocketByName(self.menu:getCurrentPageName()).list[i], i})
        self.gui.scene.assets:playSFX("mSelect")
    elseif (itemData.usableOnMap) then
        self.gui:showScreen("useItemScreen", nil, nil, nil, {self.menu:getCurrentPageName(), game.loot:getPocketByName(self.menu:getCurrentPageName()).list[i], i})
        self.gui.scene.assets:playSFX("mSelect")
    else
        self.gui.scene.assets:playSFX("mError")
    end
end

function ItemsScreen:dropItem(itemName)
    local confirm = ConfirmDialog(self.scene, "Do you want to drop these items ? \nYou won't be able to recover them.",
    function()
        game.loot:removeItem(self.menu:getCurrentPageName(), itemName, self.dropQuantity)
        self:rebuildMenu()
        self:playTransform("hideUseMenu")
    end)
    confirm:setCancelChoice(2)
    confirm.autoDismiss = true
end

function ItemsScreen:modifyDrop(direction, widget, max)
    self.dropQuantity = utils.math.wrap(self.dropQuantity + direction, 1, max)
    widget:replaceLabel(2, "<" .. utils.math.numberToString(self.dropQuantity, 2) .. ">")
    widget.canvas.needRedraw = true
    self.use.canvas.needRedraw = true
    self.gui.scene.assets.sfx["mBeep"]:play()
end

function ItemsScreen:setDescEffectFromWidget(widget)
    if (widget.datas ~= nil) then
        self:setDescEffect(widget.datas[1])
    else
        self.desc, self.effect = "", ""
    end
end

function ItemsScreen:changePage(direction)
    local id = self:getPocketData()
    local newMenuIndex = utils.math.wrap(id + direction, 1, #POCKET_LIST)
    self.menu:switch(POCKET_LIST[newMenuIndex].name)
    self.gui.scene.assets.sfx["mBeep"]:play()
end

function ItemsScreen:setDescEffect(name)
    local itemData = core.datas:get("items", name)
    self.desc, self.effect = itemData.description, game.loot:getEffectStrings("", name)
end

function ItemsScreen:getPocketData()
    local id = game.loot:getPocketIdByName(self.menu:getCurrentPageName())
    return id, self.menu:getCurrentPageName(), POCKET_LIST[id].fullname
end

return ItemsScreen