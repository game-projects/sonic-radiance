local ItemRoulette = require("birb.modules.gui.elements.canvas"):extend()
local gui = require "game.modules.gui"

local W, H = 48*4, 48 + 20

function ItemRoulette:new(owner, x, y)
    ItemRoulette.super.new(self, "itemRoulette", x, y, W, H)
    self.choiceBack = gui.newChoiceBack(W)
    self.opacity = 0
    self.owner = owner
    self.canvas.padding = 0
    self.canvas.isAnimated = true
end

function ItemRoulette:drawTexture()
    love.graphics.draw(self.choiceBack, 0, 0)

    local id, _, fullName = self.owner:getPocketData()
    local x2, y2, w2 = 16, -2, (W - 24)
    self.scene.assets.fonts["small"]:draw("<", x2, y2, w2, "left")
    self.scene.assets.fonts["small"]:draw(">", x2, y2, w2, "right")
    self.scene.assets.fonts["small"]:draw(fullName, x2, y2, w2, "center")

    for i, pocket in ipairs(game.loot.inventory) do
        local trueIndex = i - id
        if (trueIndex > 4) then
          trueIndex = trueIndex - 8
        end
        if (trueIndex < -4) then
          trueIndex = trueIndex + 8
        end
        if (trueIndex ~= 0) then
          love.graphics.setColor(.3, .3, .3, .6)
        end
        self.scene.assets.tileset["itembox"]:drawTile(i, W/2 + ((trueIndex)*32), 20, 0, 1, 1, 14, 0)
        utils.graphics.resetColor()
      end
end

return ItemRoulette