local GuiScreen = require "birb.modules.gui.screen"
local MainMenuScreen = GuiScreen:extend()
local FancyMenu = require "game.modules.gui.fancymenu"
local const = require "scenes.overworld.gui.menus.commons.const"

local NavigationMenu = FancyMenu:extend()
local CharacterMenu = require "scenes.overworld.gui.menus.commons.charmenu"

local CHARMENU_X = const.X + 136

local defTransitions = require "birb.modules.transitions"
local ConfirmDialog = require "game.modules.confirmdialog"

local show = {
    {"navigationMenu", "movement", 0.1, 0.3, const.X, const.Y, "inOutQuart"},
    {"navigationMenu", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
    {"navCharMenu", "movement", 0.1, 0.3, CHARMENU_X, const.Y, "inOutQuart"},
    {"navCharMenu", "tween", 0.1, 0.3, {opacity = 1}, "inOutQuart"},
}

local hide = {
    {"navigationMenu", "movement", 0, 0.3, const.X - 16, const.Y, "inOutQuart"},
    {"navigationMenu", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
    {"navCharMenu", "movement", 0, 0.3, CHARMENU_X + 16, const.Y, "inOutQuart"},
    {"navCharMenu", "tween", 0, 0.3, {opacity = 0}, "inOutQuart"},
}

function MainMenuScreen:new()
    MainMenuScreen.super.new(self, "mainmenuScreen")
    self:addTransform("show", show)
    self:addTransform("hide", hide)
    self.defaultFocus = "navigationMenu"
end

function MainMenuScreen:createElements()
    local menu = FancyMenu("navigationMenu", const.X - 16, const.Y, 108, 10, false)
    menu:addItem("Team", "left", function() self.gui:setFocus("navCharMenu") end, "navigate", {{">", "right"}})
    menu:addItem("Quest", "left", function() end, "navigate", {{">", "right"}})
    menu:addItem("Items", "left", function() self.gui:showScreen("itemScreen") end, "navigate", {{">", "right"}})
    menu:addItem("Chao", "left", function() end, "navigate", {{">", "right"}})
    menu:addItem("Encylopedia", "left", function() end, "navigate", {{">", "right"}})
    menu:addSubmenu("save", "Save / Exit", "main", true)
    menu:addItem("Save game", "left", function() self:save() self.scene:unpause() end, "select")
    menu:addItem("Save and exit", "left", function() self:save() self:exitToMenu() end, "navigate")
    menu:addItem("Exit game", "left", function() self:exit() end, "select")
    menu:switch("main")
    menu:addItem("Resume", "left", function() self.scene:unpause() end, "back")
    menu:setCancelWidget()
    menu.opacity = 0

    local charMenu = CharacterMenu("navCharMenu", CHARMENU_X + 16,
        function (name, i)
            self.gui:showScreen(name, nil, 1, "basic")
            self.gui:getScreen(name).menu.canvas.needRedraw = true
            self.gui:getScreen(name).desc = ""
            self.gui:getScreen(name).nbr = i
        end, false)

    charMenu:addCancelAction(function ()
            self.gui:setFocus("navigationMenu")
            self.gui.scene.assets:playSFX("mBack")
        end)
    charMenu.opacity = 0
    return {{menu, 0, 1}, {charMenu, 0, 1}}
end

function MainMenuScreen:save()
    self.scene.world:savePosition()
    game:write()
end

function MainMenuScreen:exit()
    local confirm = ConfirmDialog(self.scene, "Do you to exit the game ? \nAll unsaved data will be lost.",
    function() self:exitToMenu() end)
    confirm:setCancelChoice(2)
end

function MainMenuScreen:exitToMenu()
    core.screen:startTransition(defTransitions.default, defTransitions.circle, function() game:reload() scenes.menus.main() end, 424/2, 240/2)
    self.gui:hideScreen("hud")
end

function NavigationMenu:new()
    NavigationMenu.super.new(self)
end

return MainMenuScreen