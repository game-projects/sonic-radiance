-- scenes/test :: a basic test scene

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Scene       = require "game.scenes"
local Title = Scene:extend()
local TitleScreen = require "scenes.menus.titlescreen.screen"

function Title:new(fromMenu)
  Title.super.new(self, true, true)

  self.assets:addImage("logo", "assets/artworks/logo.png")
  self.assets:addTileset("charicons", "assets/sprites/characters/charicons")

  TitleScreen(fromMenu)
end

function Title:startPressed()
  self.assets:playSFX("mSelect")
  self.gui.focusedElement = nil
  self.gui:playScreenTransform("titleScreen", "showMenu")
end

return Title
