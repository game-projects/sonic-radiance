local Screen = require "birb.modules.gui.screen"
local TitleScreen = Screen:extend()

local AssetElement = require "birb.modules.gui.elements.assets"
local TextureElement = require "birb.modules.gui.elements.drawable"
local PressStart = require "scenes.menus.titlescreen.pressStart"
local ColorElement = require "birb.modules.gui.elements.color"

local Menu = require "scenes.menus.titlescreen.menu"

local either      = utils.math.either

local show = {
    {"fade", "tween", 0.3, 0.4, {opacity = 0}, "outExpo"},
    {"logo", "movement", 0.5, 0.6, 424/2, 80, "inOutQuart"},
    {"flash", "tween", 1.1, 0.03, {opacity = 1}, "inQuart"},
    {"flash", "tween", 1.25, 0.2, {opacity = 0}, "outExpo"},
    --{"pressStart", "switch", 1.2, {"isVisible"}},
    --{"pressStart", "delayFocus", 1.2},
  }
local showMenu = {
    {"save", "tween", 0, 0.25, {sx = 1, sy = 1, opacity = 1}, "inExpo"},
    {"save", "delayFocus", 0.25},
}
local hideMenu = {
    {"save", "tween", 0, 0.25, {sx = 0.8, sy = 0.8, opacity = 0}, "outExpo"},
    {"pressStart", "delayFocus", 0.25},
}

function TitleScreen:new(fromMenu)
    self.fromMenu = (fromMenu == true)
    TitleScreen.super.new(self, "titleScreen")
    self:addTransform("show", show)
    self:addTransform("showMenu", showMenu)
    self:addTransform("hideMenu", hideMenu)
    if (not self.fromMenu) then
        self:show()
    else
        self.isVisible = true
    end
end

function TitleScreen:createElements()
    local background = love.graphics.newImage("datas/gamedata/maps/sti/stuff/tilescreen.png")
    local o = either(self.fromMenu, 0, 1)
    local y = either(self.fromMenu, 80, -190)
    local d = either(self.fromMenu, 0, 1.2)
    return {
        {TextureElement("background", background, 0, 0, 0, 1, 1, 0, 0, 1), 0, 200},
        {ColorElement("fade", 0, 0, 0, o), 0, 5},
        {AssetElement("logo", "images", "logo", 424/2, y, 0, 1, 1, "center", "center", 1, 0), 0, 4},
        {ColorElement("flash", 1, 1, 1, 0), 0, 1},
        {PressStart(self.fromMenu), d, 10, true},
        {Menu(self), 0, 4},
    }
end


return TitleScreen