local RadianceMenu = require "birb.modules.gui.menus.listbox"
local RadianceWidget = require "birb.modules.gui.menus.widgets.base"
local SaveMenu = RadianceMenu:extend()
local SaveWidget = RadianceWidget:extend()

local gui = require "game.modules.gui"

local defTransitions = require "birb.modules.transitions"
local radTransitions = require "game.modules.transitions"

local HPADDING = 68
local VPADDING = 30

function SaveMenu:new()
    local w, h = 424 - (HPADDING * 2), 240 - (VPADDING * 2)

    SaveMenu.super.new(self, "save", 424/2, 240/2, w, h, 3)
    self.ox = w/2
    self.oy = h/2
    self.sx = 0.8
    self.sy = 0.8
    self.opacity = 0
    local metadata = game:getMetadata()
    for i, save in ipairs(metadata) do
        SaveWidget(self.scene, i, save)
    end
    self.textBox = gui.newTextBox("assets/gui/dialogbox.png", w - 8, self.widgetSize.h - 4)
    self.isVisible = true
end

function SaveMenu:cancelAction()
    self:playSFX("back")
    self.gui:playScreenTransform("titleScreen", "hideMenu")
    self:looseFocus()
end

function SaveWidget:new(scene, saveid, savedata)
    SaveWidget.super.new(self, "save")
    self.saveid = saveid
    self.savedata = savedata
    self.emeralds = gui.getEmeraldsTexture(self.savedata.emeralds)
end

function SaveWidget:drawCanvas()
    local basex, basey = 4, 2
    love.graphics.draw(self.menu.textBox, basex, basey)
    if (self.savedata.exist) then
        local str = "Save " .. self.saveid
        str = str .. "(" .. utils.math.numberToString(self.savedata.completion,3) .. "%)"
        str = str .. " - " .. utils.time.toString(self.savedata.gametime)
        str = str .. "\n"
        str = str .. self.savedata.location .. "\n"
        str = str .. "Rings: " .. self.savedata.rings
        self.assets.fonts["small"]:draw(str, basex + 8, basey + 4)

        for i, charName in ipairs(self.savedata.team) do
            local data = core.datas:get("characters", charName)
            local x = 18*(#self.savedata.team - i + 1) + 4 + basex
            self.scene.assets.tileset["charicons"]:drawTile(data.icon,self.width - x, basey + 4)
        end

        love.graphics.draw(self.emeralds, basex + 168, basey + 21)
    else
        self.assets.fonts["small"]:draw("New save", basex + 8, basey + 4)
    end
end

function SaveWidget:action()
    game:read(self.saveid)
    core.screen:startTransition(radTransitions.borders, defTransitions.circle, function() scenes.menus.main() end, 424/2, 240/2)
end

return SaveMenu
