local TextElement = require "birb.modules.gui.elements.text"
local PressStart = TextElement:extend()

function PressStart:new(isVisible)
    PressStart.super.new(self, "pressStart", "SA2font", "PRESS START", 424/2, 240/1.33, "center")
    self.isVisible = isVisible or false
    self.showPressStartTimer = 0
end

function PressStart:update(dt)
    self.showPressStartTimer = self.showPressStartTimer - dt
    if self.showPressStartTimer < 0 then
      self.showPressStart = (self.showPressStart == false)
      self.showPressStartTimer = 0.5
    end
end

function PressStart:draw()
    if (self.showPressStart) then
        PressStart.super.draw(self)
    end
end

function PressStart:keypressed(key)
    if (key == "start") then
        self.scene:startPressed()
    end
end

return PressStart