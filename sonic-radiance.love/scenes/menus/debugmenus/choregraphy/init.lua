local Scene = require "scenes.menus.debugmenus.commons.scene"
local ChoregraphyViewer = Scene:extend()

local World = require "scenes.battlesystem.world"
local Fighter = require "scenes.menus.debugmenus.choregraphy.mocks.fighter"

function ChoregraphyViewer:new()
  ChoregraphyViewer.super.new(self)
  self.assets:batchImport("assets.battle")
  self.world = World(self)

  self:buildMocks()
end

-- MENU FUNCTIONS
function ChoregraphyViewer:initMenu()
  for i,category in ipairs(core.datas:getCategories("ennemies")) do
    self:buildEnnemyMenu(category)
  end

  self.menu:addSubmenu("characters", "Rivals", "main", true)
  for k, character in pairs(game.characters.list) do
    self.menu:addSubmenu(k, character.fullname, "characters")
    self:addItem("rivals", k, core.datas:get("skills", "attack"))
    self:buildSkillMenu(k)
  end
end

function ChoregraphyViewer:buildEnnemyMenu(category)
  self.menu:addSubmenu(category, category, "main", true)

  for i,ennemy in ipairs(core.datas:getFromCategory("ennemies", category)) do
    self.menu:addSubmenu(ennemy, ennemy, category, true)
    local data = core.datas:get("ennemies", ennemy)

    for j,skillName in ipairs(data.skills) do
      if (core.datas:exists("badskills", skillName)) then
        self:addItem(category, ennemy, core.datas:get("badskills", skillName))
      end
    end
  end
end

function ChoregraphyViewer:buildSkillMenu(charName)
  local skillList = require("datas.gamedata.characters." .. charName .. ".skills")
  local skillTreated = {}
  for i,skill  in ipairs(skillList) do
    local skillName = skill[1]
    if (skillTreated[skillName] ~= true) then
      skillTreated[skillName] = true
      if (core.datas:exists("skills", skillName)) then
        self:addItem("rivals", charName, core.datas:get("skills", skillName))
      end
    end
  end
end

function ChoregraphyViewer:addItem(category, ennemy, data)
  self.menu:addItem(data.name, "left", function() self:playEnnemyChoregraphy(category, ennemy, data) end, "select")
end

-- MOCKS FUNCTIONS
function ChoregraphyViewer:buildMocks()
  self.fighter = Fighter(self)
end

function ChoregraphyViewer:playHeroChoregraphy(character, data)
  self.fighter:playHeroChoregraphy(character, data)
end

function ChoregraphyViewer:playEnnemyChoregraphy(category, ennemy, data)
  if (category == "rivals") then
    self.fighter:playHeroChoregraphy(ennemy, data)
  else
    self.fighter:playEnnemyChoregraphy(category, ennemy, data)
  end
end

-- OTHER
function ChoregraphyViewer:update(dt)
  self.fighter:update(dt)
  self.world:update(dt)
end

function ChoregraphyViewer:draw()

end

return ChoregraphyViewer
