local TargetMock = Object:extend()

function TargetMock:new(scene)
  self.scene = scene
  self.actor = self.scene.world.obj.Battler(self.scene.world, 2, 3, 0, self)
end

return TargetMock
