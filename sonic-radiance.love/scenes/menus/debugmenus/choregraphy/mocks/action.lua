local ActionParent = require "scenes.battlesystem.fighters.character.actions.parent"
local ActionMock = ActionParent:extend()

function ActionMock:new(fighter, data)
  ActionMock.super.new(self, fighter)
  self.data = data
end

function ActionMock:needTarget()
  return (self.data.targetNumber == 1), self.data.targetEnnemies
end

function ActionMock:startAction()
  core.debug:print("cbs/action", "Starting mock action")
  self:loadChoregraphyFromSkill(self.data)
end

return ActionMock
