local FighterMock = Object:extend()

local ActionMock = require "scenes.menus.debugmenus.choregraphy.mocks.action"
local TargetMock = require "scenes.menus.debugmenus.choregraphy.mocks.target"
local TurnSystemMock = require "scenes.menus.debugmenus.choregraphy.mocks.turnSystem"

function FighterMock:new(scene)
  self.scene = scene
  self.action = nil
  self.actor = nil
  self.turnSystem = TurnSystemMock()
end

function FighterMock:update(dt)
  if (self.action ~= nil) then
    self.action:update(dt)
  end
end

function FighterMock:playHeroChoregraphy(character, data)
  self.name = character
  self.abstract = game.characters.list[character]
  self.actor = self.scene.world.obj.Hero(self.scene.world, 11, 3, self, 1)
  self.action = ActionMock(self, data)
  self.action:setTarget(TargetMock(self.scene))
  self.action:start()
end

function FighterMock:playEnnemyChoregraphy(category, ennemy, data)
  self.name = ennemy
  self.category = category
  self.abstract = game.ennemies:getEnnemyData(category, ennemy)
  self.actor = self.scene.world.obj.Ennemy(self.scene.world, 11, 3, self, 1)
  self.action = ActionMock(self, data)
  self.action:setTarget(TargetMock(self.scene))
  self.action:start()
end

function FighterMock:finishAction()
  self.action.target.actor:destroy()
  self.actor:destroy()
  self.action = nil
end

return FighterMock
