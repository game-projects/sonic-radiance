local DebugScene = require("game.scenes"):extend()
local BoxedMenu = require("game.modules.gui.boxedmenu")

local defTransitions = require "birb.modules.transitions"

local CONST = {
    MENU = {X = 16, Y = 48, W = (424/2)/ 1.5, ITEM_NUMBER = 8}
}

function DebugScene:new()
    DebugScene.super.new(self, false, false)
    self.assets:batchImport("assets.debug")
    self.menu = BoxedMenu("debugMenu", CONST.MENU.X, CONST.MENU.Y, CONST.MENU.W, CONST.MENU.ITEM_NUMBER, true, false)
    self:initMenu()
    self.menu:switch("main")
    self.menu:addItem("Back", "left", function() 
        core.screen:startTransition(defTransitions.circle, defTransitions.default, function() scenes.menus.debug.menu() end, 424/2, 240/2)
    end, "back")
    self.gui:setFocus("debugMenu")
end

function DebugScene:initMenu()

end

function DebugScene:hideMenu()
    self.menu.isVisible = false
end

function DebugScene:update(dt)
    if (love.keyboard.isDown("z") and (not self.menu.isVisible)) then
        self.menu.isVisible = true
    end
end

return DebugScene