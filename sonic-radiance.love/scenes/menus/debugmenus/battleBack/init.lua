local Scene = require "scenes.menus.debugmenus.commons.scene"
local BackgroundViewer = Scene:extend()

local Background = require "game.modules.drawing.parallaxBackground"
local backgroundList = require "datas.gamedata.maps.shoot.zones"

function BackgroundViewer:new()
  BackgroundViewer.super.new(self)
  self:setBackground("city")
end

function BackgroundViewer:initMenu()
  for backId, backgroundData in pairs(backgroundList) do
    self.menu:addItem(backgroundData.name, "left", function() 
      self:setBackground(backId)
      self:hideMenu()
    end, "select")
  end
end

function BackgroundViewer:setBackground(newBackground)
  self.background = Background(self, 5, 1, newBackground)
end

function BackgroundViewer:draw()
  self.background:draw()
end

return BackgroundViewer
