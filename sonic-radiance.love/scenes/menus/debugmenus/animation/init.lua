local Scene = require "scenes.menus.debugmenus.commons.scene"

local CharAnimViewer = Scene:extend()
local Background = require "game.modules.drawing.parallaxBackground"
local Sprite = require "birb.modules.assets.types.sprites"

function CharAnimViewer:new()
  CharAnimViewer.super.new(self)

  self.background = Background(self, 5, 1, "city")
  self.sprite = nil
end

function CharAnimViewer:initMenu()
  for charName, _ in pairs(game.characters.list) do
    self.menu:addSubmenu(charName, charName, "main", true)
    self.menu:switch(charName)
    local sprite = Sprite("datas/gamedata/characters/" .. charName .. "/sprites")
    for animName, _ in pairs(sprite.data.animations) do
        self.menu:addItem(animName, "left", function()
          self:setSpriteAndAnim(charName, animName)
          self:hideMenu()
        end, "select")
    end
  end
end

function CharAnimViewer:setSpriteAndAnim(character, animationName)
    self.sprite = Sprite("datas/gamedata/characters/" .. character .. "/sprites")
    self.sprite:changeAnimation(animationName, true)
end


function CharAnimViewer:update(dt)
  CharAnimViewer.super.update(self,dt)
  if (self.sprite ~= nil) then
    self.sprite:update(dt)
  end
end

function CharAnimViewer:draw()
  self.background:draw()
  if (self.sprite ~= nil) then
    self.sprite:draw(424/2, 240/1.5)
  end
end

return CharAnimViewer
