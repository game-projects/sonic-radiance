local Scene     = require "game.scenes"
local DebugMenu = Scene:extend()

local MenuBack  = require "game.modules.gui.menuback"
local BoxedMenu = require "game.modules.gui.boxedmenu"

local const     = require "scenes.menus.options.const"
local defTransitions = require "birb.modules.transitions"

function DebugMenu:new(page, widgetId)
  DebugMenu.super.new(self, true, true)

  local screenw, _ = core.screen:getDimensions()
  self.menu = BoxedMenu("debugMenu", screenw/2, const.MENU_Y, const.MENU_W, const.MENU_ITEM_NUMBER, true)
  MenuBack()

  self.menu.ox = const.MENU_W/2

  self:rebuild()

  self.menu:switch(page or "main")
  self.menu:setCursor(widgetId or 1)
  self.gui:setFocus("debugMenu")
end

function DebugMenu:rebuild()
    self:buildBattleMenu()
    self:buildSaveMenu()
    self:buildOtherMenu()
    self.menu:switch("main")
    self.menu:addItem("Back", "left", function() self:changeScene(scenes.menus.main, nil, true) end, "back")
    self.menu:setCancelWidget()
end

function DebugMenu:buildBattleMenu()
    self.menu:addSubmenu("combat", "Battle System", "main", true)
    self.menu:addSubmenu("launchBattle", "Launch Battle", "combat", true)

    local listCat = core.datas:getCategories("battles")

    for i,battleCat in ipairs(listCat) do
      local menuName = "b_" .. battleCat

      self.menu:addSubmenu(menuName, battleCat, "launchBattle", true)

      for j, battleName in ipairs(core.datas:getFromCategory("battles", battleCat)) do
        local data = core.datas:get("battles", battleName)
        self.menu:addItem("Launch " .. battleName, "left", function() self:changeScene(scenes.cbs, data, false) end, "select")
      end
    end

    self.menu:switch("combat")

    self.menu:addItem("Background Viewer ", "left", function() self:changeScene(scenes.menus.debug.battleBack, nil, false) end, "select")
    self.menu:addItem("Animation Viewer ", "left", function() self:changeScene(scenes.menus.debug.animation, nil, false) end, "select")
    self.menu:addItem("Ennemies' Action Viewer ", "left", function() self:changeScene(scenes.menus.debug.choregraphy, nil, false) end, "select")
end


function DebugMenu:buildSaveMenu()
  self.menu:addSubmenu("characters", "Characters", "main", true)
  self.menu:addSubmenu("team", "Team Formation", "characters", true)

  for name, data in pairs(game.characters.list) do
    self.menu:switch("characters")
    self:addCharacterMenu(name, data)
    self.menu:switch("team")
    self.menu:addItem(data.fullname, "left", function()
      game.characters:addOrRemoveToTeam(name)
      game:write()
    end, "select")
  end

  self:addInventory()
end

function DebugMenu:addCharacterMenu(name, data)
  self.menu:addSubmenu(name, data.fullname, "characters", true)
  self.menu:addItem("Level Up", "left", function() data:levelUp() game:write()  end, "select")
end

function DebugMenu:addInventory()
  self.menu:addSubmenu("inventory", "Inventory", "main", true)
  for i,pocket in ipairs(game.loot.inventory) do
    self.menu:addSubmenu(pocket.name, pocket.fullname, "inventory", true)
    for j, item in ipairs(core.datas:getFromCategory("items", pocket.name)) do
      local data = core.datas:get("items", item)
      self.menu:addItem(data.fullname, "left", function() game.loot:addItem(pocket.name, item, 1) game:write() end, "select")
    end
  end
end

function DebugMenu:buildOtherMenu()
  self.menu:addSubmenu("other", "Other gameplay", "main", true)
  self.menu:addItem("Shadow Shot Maps", "left", function() self:changeScene(scenes.test2, nil, false) end, "select")
  self.menu:addSubmenu("battleMaps", "Sonic Battle Maps", "other", true)

  local mapList = require "datas.gamedata.maps.battle"
  for i, name in ipairs(mapList) do
    local mapData = require("datas.gamedata.maps.battle." .. name)
    local trueName = mapData.name
    self.menu:addItem(trueName, "left", function() self:changeScene(scenes.test, name, false) end, "select")
  end

end

function DebugMenu:changeScene(scene, arg, useFade)
  local trans2 = utils.math.either(useFade == true, defTransitions.default, defTransitions.circle)
  core.screen:startTransition(defTransitions.default, trans2, function() scene(arg) end, 424/2, 240/2)
end

return DebugMenu
