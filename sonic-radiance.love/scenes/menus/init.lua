return {
  title = require "scenes.menus.titlescreen",
  main = require "scenes.menus.mainmenu",
  debug = {
    battleBack = require "scenes.menus.debugmenus.battleBack",
    choregraphy = require "scenes.menus.debugmenus.choregraphy",
    animation = require "scenes.menus.debugmenus.animation",
    menu = require "scenes.menus.debugmenus"
  },
  options = require "scenes.menus.options"
}
