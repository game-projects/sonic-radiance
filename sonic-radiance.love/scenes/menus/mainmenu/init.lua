local Scene = require "game.scenes"

local DebugMenu = Scene:extend()

local MenuBack    = require "game.modules.gui.menuback"

local MainMenu = require "scenes.menus.mainmenu.menu"

function DebugMenu:new()
  DebugMenu.super.new(self, true, true)
  MainMenu()
  MenuBack()
end

return DebugMenu;
