local Parent = require "game.modules.gui.fancymenu"
local MainMenu = Parent:extend()

local defTransitions = require "birb.modules.transitions"
local radTransitions = require "game.modules.transitions"

local MENU_X, MENU_Y = 24, 48
local MENU_W = 424/3
local MENU_ITEM_NUMBER = 8

function MainMenu:new()
    MainMenu.super.new(self, "mainmenu", MENU_X, MENU_Y, MENU_W, MENU_ITEM_NUMBER, false)
    self:addItem("Launch game", "left", function() self:launchGame() end)
    if (core.debug.active) then
        self:addItem("Debug Menu", "left", function() self:launchDebug() end)
    end
    self:addItem("Options", "left", function() self:launchOptions() end)
    self:addItem("Return to title", "left", function() self:returnToTitle() end, "back")
    self:setCancelWidget()
    self:addItem("Exit game", "left", function() self:exitGame() end)
    self:getFocus()
end

function MainMenu:launchGame()
    core.screen:startTransition(defTransitions.circle, defTransitions.default, function() scenes.overworld() end, 424/2, 240/2)
    core.scenemanager.currentScene:hideOverlay()
end

function MainMenu:launchDebug()
    core.screen:startTransition(defTransitions.default, defTransitions.default, function() scenes.menus.debug.menu() end, 424/2, 240/2)
end

function MainMenu:launchOptions()
    core.screen:startTransition(defTransitions.default, defTransitions.default, function() scenes.menus.options() end, 424/2, 240/2)
end

function MainMenu:returnToTitle()
    core.screen:startTransition(defTransitions.circle, radTransitions.borders, function() scenes.menus.title(true) end, 424/2, 240/2)
end

function MainMenu:exitGame()
    love.event.quit("000")
end

return MainMenu
