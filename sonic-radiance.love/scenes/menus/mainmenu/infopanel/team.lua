local Parent = require "scenes.menus.mainmenu.infopanel.parent"
local TeamPanel = Parent:extend()

function TeamPanel:new()
  TeamPanel.super.new(self)
end

function TeamPanel:drawContent(x, y)
  local debugString = "# DEBUG - Team" .. "\n"
  for i,name in ipairs(game.characters.team) do
    local char = game.characters.list[name]
    debugString = debugString .. "- " .. char.name
    if (game.characters.active == i) then
      debugString = debugString .. " (Active) "
    end
    debugString = debugString .. "\n"
  end

  love.graphics.print(debugString, x, y)
end

return TeamPanel
