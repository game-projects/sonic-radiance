local ParentPanel = Object:extend()
local gui = require "game.modules.gui"

function ParentPanel:new()
  self.panelBackground = gui.newTextBox("assets/gui/dialogbox.png", 128+32, 128)
end

function ParentPanel:draw(x, y, w, h)
  if (core.debug.active) then
    love.graphics.draw(self.panelBackground, x, y)
    self:drawContent(x+8, y+8)
  end
end

function ParentPanel:drawContent(x, y)

end

return ParentPanel
