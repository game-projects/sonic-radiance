local Parent = require "scenes.menus.mainmenu.infopanel.parent"
local GamePanel = Parent:extend()

function GamePanel:new()
  GamePanel.super.new(self)
end

function GamePanel:drawContent(x, y)
  local debugString = "# DEBUG - Save data" .. "\n"
  debugString = debugString .. "Current slot: " .. game.slot .. " / " .. game.slotNumber .. "\n"
  debugString = debugString .. "Rings: " .. game.loot.rings .. "\n" 
  debugString = debugString .. "Gametime: " .. game:getTimeString() .. "\n"

  love.graphics.print(debugString, x, y)
end

return GamePanel
