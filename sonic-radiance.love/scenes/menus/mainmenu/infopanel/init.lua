local folder = "scenes.menus.mainmenu.infopanel."

return {
  Gamedata = require(folder .. "gamedata"),
  Character = require(folder .. "character"),
  Team = require(folder .. "team"),
}
