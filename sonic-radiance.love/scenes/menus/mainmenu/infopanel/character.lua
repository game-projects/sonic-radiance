local Parent = require "scenes.menus.mainmenu.infopanel.parent"
local CharacterPanel = Parent:extend()

function CharacterPanel:new(character)
  CharacterPanel.super.new(self)
  self.character = character
end

function CharacterPanel:drawContent(x, y)
  local stats = self.character.stats

  local debugString = "# DEBUG - " .. self.character.name .. "(" .. "Lvl " .. self.character.level .. ")" .. "\n"
  local debugString = debugString .. "EXP: " .. self.character.exp .. " / " .. self.character.exp_next .. "\n"
  local debugString = debugString .. "HP: " .. self.character.hp .. " / " .. stats:get(stats.HPMAX) .. "\n"
  local debugString = debugString .. "PP: " .. self.character.pp .. " / " .. stats:get(stats.PPMAX) .. "\n"
  local debugString = debugString .. self:addToDebugString(stats, stats.ATTACK)
  local debugString = debugString .. " " .. self:addToDebugString(stats, stats.DEFENSE)
  local debugString = debugString .. " " .. self:addToDebugString(stats, stats.SPEED) .. "\n"
  local debugString = debugString .. self:addToDebugString(stats, stats.POWER)
  local debugString = debugString .. " " .. self:addToDebugString(stats, stats.MIND)
  local debugString = debugString .. " " .. self:addToDebugString(stats, stats.TECHNIC) .. "\n"

  love.graphics.print(debugString, x, y)
end

function CharacterPanel:addToDebugString(stats, statname)
  local stats = stats
  return stats.CONST.SIMPLENAME[statname] .. ": " .. stats:get(statname)
end

return CharacterPanel
