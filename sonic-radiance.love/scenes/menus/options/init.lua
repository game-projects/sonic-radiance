local Scene     = require "game.scenes"
local OptionsMenu  = Scene:extend()

local OptionMenu = require "scenes.menus.options.menu"
local MenuBack    = require "game.modules.gui.menuback"

function OptionsMenu:new()
  OptionsMenu.super.new(self, true, true)

  self.keyDetector = {}
  self.keyDetector.widget = nil

  OptionMenu()
  MenuBack()
end

function OptionsMenu:changeKey(widget)
  self.keyDetector.widget = widget
end

function OptionsMenu:keypressed( key )
  if (self.keyDetector.widget ~= nil) then
    self.assets:playSFX("mSelect")
    self.gui.elements["optionMenu"].isVisible = true
    self.gui:delayFocus("optionMenu", 0.1)
    self.keyDetector.widget:receiveKey( key )
    self.keyDetector.isActive = false
    self.keyDetector.widget = nil
  end
end

return OptionsMenu
