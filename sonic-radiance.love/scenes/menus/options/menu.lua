local Parent = require "game.modules.gui.boxedmenu"
local MainMenu = Parent:extend()

local TextMenuWidget = require "birb.modules.gui.textmenu.widgets.basic"
local ResolutionWidget = TextMenuWidget:extend()
local SwitchWidget = TextMenuWidget:extend()
local AudioWidget = TextMenuWidget:extend()
local DifficultyWidget = TextMenuWidget:extend()
local KeysWidget = TextMenuWidget:extend()

local defTransitions = require "birb.modules.transitions"
local ConfirmDialog = require "game.modules.confirmdialog"

local const = require "scenes.menus.options.const"

function MainMenu:new()
    local screenw, screenh = core.screen:getDimensions()
    MainMenu.super.new(self, "optionMenu", screenw/2, const.MENU_Y, const.MENU_W, const.MENU_ITEM_NUMBER, true)
    self.ox = const.MENU_W/2
    self:addVideoMenu()
    self:addAudioMenu()
    self:addDifficultyMenu()
    self:addInputMenu()
    self:switch("main")
    self:addItem("Delete save", "left", function() self:deleteSave() end, "select", nil, {1, 0.3, 0.3})
    self:addItem("Exit", "left", function() self:exit() end, "back")
    self:setCancelWidget()
    self:getFocus()
end

function MainMenu:addVideoMenu()
    self:addSubmenu("video", "Videos", "main", true)
    ResolutionWidget()
    SwitchWidget("fullscreen", "Fullscreen")
    SwitchWidget("border", "Borders")
    SwitchWidget("vsync", "Vsync")
end

function MainMenu:addAudioMenu()
    self:addSubmenu("audio", "Audio", "main", true)
    AudioWidget("music", "Music")
    AudioWidget("sfx", "SFX")
end

function MainMenu:addDifficultyMenu()
    self:addSubmenu("difficulty", "Difficulty", "main", true)
    DifficultyWidget("hazardMakesKo", "Hazards can make KO")
    DifficultyWidget("playerKoChar", "Play KO characters on maps")
    DifficultyWidget("easierBattles", "Easier battles")
    DifficultyWidget("checkPointRegen", "Checkpoints heal you")
    DifficultyWidget("levelUpHeal", "Gaining a level heal")
    DifficultyWidget("allDamage", "Hazards damage everybody")
end

function MainMenu:addInputMenu()
    self:addSubmenu("input", "Input", "main", true)
    local keyList = require "datas.keys"
    for j, key in ipairs(keyList) do
      KeysWidget(key)
    end
end

function MainMenu:exit()
    core.screen:startTransition(defTransitions.default, defTransitions.default,
        function()  scenes.menus.main() end, 0, 0)
end

function MainMenu:deleteSave()
    local confirm = ConfirmDialog(core.scenemanager.currentScene,
    "Do you want to delete your save ? \nYou won't be able to recover your data.",
    function()
        core.scenemanager:setStoredScene("mainmenu")
        game:deleteCurrentSave()
        core.screen:startTransition(defTransitions.default, defTransitions.circle,
          function() scenes.menus.title(true) end,
          424/2, 240/2)
    end)
    confirm:setCancelChoice(2)
end


-- WIDGETS

function ResolutionWidget:new()
    ResolutionWidget.super.new(self, "optionMenu", core.lang:translate("options", "resolution"), "left")
    self:addLabel(self:getSecondLabel(), "right")
end

function ResolutionWidget:getSecondLabel()
    return "x" .. core.options.data.video.resolution
end

function ResolutionWidget:action()
    core.options.data.video.resolution = ((core.options.data.video.resolution) % 3) + 1
    self:replaceLabel(2, self:getSecondLabel())
    core.screen:applySettings()
    self:invalidateCanvas()
    core.options:write()
end

-- SWITCH

function SwitchWidget:new(keyname, label)
    self.keyname = keyname
    SwitchWidget.super.new(self, "optionMenu", label, "left")
    self:addLabel(self:getSecondLabel(), "right")
end

function SwitchWidget:modifyKey()
    core.options.data.video[self.keyname] = (core.options.data.video[self.keyname] == false)
    core.screen:applySettings()
end

function SwitchWidget:getKey()
    return core.options.data.video[self.keyname]
end

function SwitchWidget:getSecondLabel()
    return core.lang:translate("commons", utils.math.either(self:getKey(), "true", "false"))
end

function SwitchWidget:action()
    self:modifyKey()
    self:replaceLabel(2, self:getSecondLabel())
    core.options:write()
    self:invalidateCanvas()
end

-- AUDIO

function AudioWidget:new(audiotype, label)
    self.audiotype = audiotype
    SwitchWidget.super.new(self, "optionMenu", label, "left")
    self:addLabel(self:getSecondLabel(), "right")
    self.order = 0
end

function AudioWidget:getSecondLabel()
    return utils.math.numberToString(self:getVolume(), 3) .. "%"
end

function AudioWidget:getVolume()
    return core.options.data.audio[self.audiotype]
end

function AudioWidget:setVolume(vol)
    core.options.data.audio[self.audiotype] = utils.math.either(vol >= 0, vol, 100)
end

function AudioWidget:action()
    local value = self:getVolume()
    self:setVolume(value - 20)
    self:replaceLabel(2, self:getSecondLabel())
    self:invalidateCanvas()
    --self.scene.assets.music:setVolume(core.options.data.audio.music / 100)
    core.options:write()
end

-- DIFFICULTY

function DifficultyWidget:new(keyname, label)
    self.keyname = keyname
    SwitchWidget.super.new(self, "optionMenu", label, "left")
    self:addLabel(self:getSecondLabel(), "right")
end

function DifficultyWidget:modifyKey()
    game.difficulty:toggle(self.keyname)
end

function DifficultyWidget:getKey()
    return game.difficulty:get(self.keyname)
end

function DifficultyWidget:getSecondLabel()
    return core.lang:translate("commons", utils.math.either(self:getKey(), "true", "false"))
end

function DifficultyWidget:action()
    self:modifyKey()
    self:replaceLabel(2, self:getSecondLabel())
    game:write()
    self:invalidateCanvas()
end

-- KEYS

function KeysWidget:new(key)
    self.source = 1
    self.key    = key

    SwitchWidget.super.new(self, "optionMenu", self.key, "left")
    self:addLabel(self:getSecondLabel(), "right")
    self.type = "navigate"
end

function KeysWidget:getSecondLabel()
    return core.input.data[self.source].keys[self.key]
end

function KeysWidget:action()
    self.menu:playSFX("navigate")
    self.scene:changeKey(self)
    self.menu.isVisible = false
    self.menu:looseFocus()
end

function KeysWidget:receiveKey( key )
    core.options:setInputKey(self.source, self.key, key)
    self:replaceLabel(2, self:getSecondLabel())
    self:invalidateCanvas()
end

return MainMenu
