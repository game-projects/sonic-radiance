local Battler = require("scenes.battlesystem.actors.battler")
local Ennemy = Battler:extend()

function Ennemy:new(world, x, y, owner)
  Ennemy.super.new(self, world, x, y, 0, owner)
  self.isEnnemy = true

  self.actionPerTurn = 2
  self:initSprite()
  if (self.owner:haveProtecType("aerial")) then
    self.z = 10
    self.start.z = 10
  end
  self.sprHeight = self.owner.abstract.data.hudHeight + 14
end

function Ennemy:setCheapEffect(cheapEffect)
  if (cheapEffect) then
    self.sprite.sx = 2
    self.sprite.sy = 2
  end
end

function Ennemy:draw()
  self:drawSprite(0, -self.z)
  local x, y = self.world.map:gridToPixel(self.x, self.y, true)

  self.owner:drawOversprite(x - 12, y - ((self.sprHeight - 8) * self.sprite.sy) - self.z)

  if (self.isSelected) then
    self.assets.images["cursorpeak"]:draw(x - 7, (y - 24 - self.sprHeight) - self.z)
  end

  self:drawOutput()
end

function Ennemy:die()
  self.assets.sfx["badnicsBoom"]:play()
  self.world.obj.GFX(self.world, self.x, self.y, self.z, "boom1", self, false)
  self:destroy()
end

function Ennemy:getStats()
  return self.data.stats
end

-- ASSETS FUNCTIONS
-- Load and play assets needed by the character

function Ennemy:getSpritePath()
  return "datas/gamedata/ennemies/" .. self.owner.category .. "/" .. self.owner.name .. "/sprites"
end


return Ennemy
