local Battler = require("scenes.battlesystem.actors.battler")
local Hero = Battler:extend()

-- INIT FUNCTIONS
-- Initialize the hero

function Hero:new(world, x, y, owner, charnumber)
  Hero.super.new(self, world, x, y, 0, owner)
  self:initSprite()
  self.isKo = false
  self.sprHeight = 32
end

-- UPDATE FUNCTION
-- Update the hero

function Hero:update(dt)
  Hero.super.update(self, dt)
  self:updateAnimation(dt)
end

-- HURT/DEATH FUNCTIONS

function Hero:getHurt()
  self:changeAnimation("hurt")
end

function Hero:die()
  if (not self.isKo) then
    self:changeAnimation("getKo")
    self.isKo = true
  end
end

function Hero:revive()
  if (self.isKo) then
    self:changeAnimation("idle")
    self.isKo = false
    self.isDefending = false
  end
end

function Hero:setAsKo()
  self:changeAnimation("ko")
  self.isKo = true
end

-- ASSETS FUNCTIONS
-- Load and play assets needed by the character

function Hero:getSpritePath()
  return "datas/gamedata/characters/" .. self.owner.name .. "/sprites"
end

function Hero:updateAnimation(dt)
  if (self.z > 0 and self.jump.useDefaultAnimation) then
    if self.zspeed > 0 then
      self:changeAnimation("jump")
    else
      self:changeAnimation("fall")
    end
  end

  self:setCustomSpeed(self.speed * 160 * dt)
end

function Hero:getNewAnimation(animation)
  if (animation == "hurt") then
    self:changeAnimation("idle")
  end
end

function Hero:flee()
  if (not self.isKo) then
    self:changeAnimation("walk")
    self:goTo(-80, self.y, 3)
  end
end

-- DRAW FUNCTIONS
-- Draw everything related to the hero

function Hero:draw()
  self:drawSprite(0, -self.z)

  if (self.isSelected) then
    local x, y = self.world.map:gridToPixel(self.x, self.y, true)
    self.assets.images["cursorpeak"]:draw(x - 7, y - 24 - 32)
  end

  self:drawOutput()
end

return Hero
