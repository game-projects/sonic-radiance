local entities = {}

local baseURI = "scenes.battlesystem.actors."

entities.Hero       = require(baseURI .. "hero")
entities.Ennemy     = require(baseURI .. "ennemy")
entities.Battler    = require(baseURI .. "battler")
entities.GFX        = require(baseURI .. "gfx")

return entities
