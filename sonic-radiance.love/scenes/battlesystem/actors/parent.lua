local Parent = Object:extend() -- On créer la classe des entitées, c'est la classe de base

local maputils = require "scenes.battlesystem.utils"

local TweenManager = require "birb.classes.time"

-- INIT FUNCTION
-- Initilize the actor

function Parent:new(world, x, y, z)
  self.depth = 0
  self.x     = x
  self.y     = y
  self.z     = z or 0
  self.direction = 1
  --self.id    = self.world.creationID

  self.world  = world
  self.assets = self.world.assets
  self.scene  = self.world.scene
  self.map    = self.world.map

  self.maputils = maputils

  self.isHero   = false
  self.isActor  = false
  self.isEnnemy = false
  self.isDestroyed = false

  self.tweens = TweenManager(self)

  self:resetTags()

  self:setSprite()
  self:register()
end

function Parent:setIndexName(indexName)
  self.indexName = indexName
  self.world.index[self.indexName] = self
end

function Parent:register()
  self.world:registerActor(self)
end

function Parent:destroy()
  self.world:destroyActor(self)
  self.isDestroyed = true
  if (self.indexName ~= nil) then
    self.world.index[self.indexName] = nil
  end
end

function Parent:update(dt)
  self:updateSprite(dt)
  self.tweens:update(dt)
end

-- GET FUNCTIONS
-- Get informations

function Parent:getCoordinate()
  return self.x, self.y, self.z
end

-- SPRITE FUNCTIONS
-- Handle the character sprite

function Parent:setSprite(spritename, ox, oy, active)
  self.sprite = {}
  self.sprite.name    = spritename or nil
  self.sprite.ox      = ox or 0
  self.sprite.oy      = oy or 0
  self.sprite.sx      = 1
  self.sprite.sy      = 1
  self.sprite.exist   = (spritename ~= nil)
  self.sprite.clone   = nil
  self.sprite.active  = active or false

  self:resetFrameSignal()
end

function Parent:cloneSprite()
  if self.sprite.name ~= nil then
    self.sprite.clone = self.assets.sprites[self.sprite.name]:clone()
    self.sprite.clone:setCallback(self)
  end
end

function Parent:changeAnimation(animation, restart)
  self:resetFrameSignal()
  if (self.sprite.clone == nil) then
    self.assets.sprites[self.sprite.name]:changeAnimation(animation, restart)
  else
    self.sprite.clone:changeAnimation(animation, restart)
  end
end

function Parent:setAnimSpeed(speed)
  if (self.sprite.clone == nil) then
    self.assets.sprites[self.sprite.name]:setSpeedFactor(speed)
  else
    self.sprite.clone:setSpeedFactor(speed)
  end
end

function Parent:animationEnded(animation)
  if (self.currentlyBlocking ~= nil and self.blockedBy=="animation") then
    self:unblockChoregraphy()
  end
  self:unlockTag("animation")
  self:getNewAnimation(animation)
end

function Parent:getNewAnimation(animation)

end

function Parent:setCustomSpeed(customSpeed)
  if (self.sprite.clone == nil) then
    self.assets.sprites[self.sprite.name]:setCustomSpeed(customSpeed)
  else
    self.sprite.clone:setCustomSpeed(customSpeed)
  end
end

function Parent:updateSprite(dt)
  if (self.sprite.clone ~= nil) then
    self.sprite.clone:update(dt)
  end
end

function Parent:setSpriteScallingX(sx)
  local sx = sx or 1

  self.sprite.sx = sx
end

function Parent:setSpriteScallingY(sy)
  local sy = sy or 1

  self.sprite.sy = sy
end

function Parent:getCurrentAnimation()
  if (self.sprite.clone == nil) then
    return self.assets.sprites[self.sprite.name]:getCurrentAnimation()
  else
    return self.sprite.clone:getCurrentAnimation()
  end
end


function Parent:getSpriteScalling()
  return self.sprite.sx, self.sprite.sy
end

function Parent:getFrame()
  if (self.sprite.name ~= nil) then
    if (self.sprite.clone ~= nil) then
      return self.sprite.clone:getFrame()
    else
      return self.assets.sprites[self.sprite.name]:getFrame()
    end
  end
end

function Parent:getRelativeFrame()
  if (self.sprite.name ~= nil) then
    if (self.sprite.clone ~= nil) then
      return self.sprite.clone:getRelativeFrame()
    else
      return self.assets.sprites[self.sprite.name]:getRelativeFrame()
    end
  end
end

function Parent:getAnimationDuration()
  if (self.sprite.name ~= nil) then
    if (self.sprite.clone ~= nil) then
      return self.sprite.clone:getAnimationDuration()
    else
      return self.assets.sprites[self.sprite.name]:getAnimationDuration()
    end
  end
end

function Parent:drawSprite(tx, ty)
  utils.graphics.resetColor()

  local x, y = self.world.map:gridToPixel(self.x, self.y, true)

  local tx = tx or 0
  local ty = ty or 0

  if (self.sprite.active) then
    local sx = self.direction * self.sprite.sx
    local sy = self.sprite.sy
    if (self.sprite.clone ~= nil) then
      self.sprite.clone:draw(x + tx, y + ty, 0, sx, sy, self.sprite.ox, self.sprite.oy)
    else
      self.assets.sprites[self.sprite.name]:draw(x + tx, y + ty, 0, sx, sy, self.sprite.ox, self.sprite.oy)
    end
  end
end

-- FRAME SIGNAL
-- Get signals from specific frames of the animation

function Parent:resetFrameSignal()
  self.frameSignals = {}
end

function Parent:receiveFrameSignal(signal)
  table.insert(self.frameSignals, signal)
end

function Parent:haveFrameSignal(signal)
  return utils.table.contain(self.frameSignals, signal)
end

-- TAGS
-- Handle tags

function Parent:resetTags()
  self.tags = {}
  self.choregraphy = nil
end

function Parent:addTaggedAction(tag, choregraphy, taggedBy)
  if (not utils.string.isEmpty(tag)) then
    self.tags[tag] = taggedBy
    self.choregraphy = choregraphy
  end
end

function Parent:unlockTag(taggedBy)
  for tag, actionTag in pairs(self.tags) do
    if (self.choregraphy ~= nil) and (actionTag == taggedBy) then
      self.choregraphy:finishTagAction(tag)
      self.tags[tag] = nil
    end
  end
end

-- CHOREGRAPHY BLOCKING
-- Handle blocking/unblocking the choregraphy

function Parent:blockChoregraphy(isBlocking, currentlyBlocking, blockedBy)
  if (isBlocking) then
    self.currentlyBlocking = currentlyBlocking
    self.blockedBy = blockedBy
  end
end

function Parent:unblockChoregraphy()
  self.currentlyBlocking:finish()
  self.currentlyBlocking = nil
end

function Parent:timerResponse(signal)
  self:finishAction(signal)

  if (signal == "removeOutput") then
    self.showOutput = false
  end
end

function Parent:finishAction(signal)
  if ((self.currentlyBlocking ~= nil) and (signal == self.blockedBy)) then
    self:unblockChoregraphy()
  end
  self:unlockTag(signal)
end

-- DRAW FUNCTIONS
-- Handle draw functions

function Parent:draw()

end

function Parent:drawShadow()
  local x, y = self.world.map:gridToPixel(self.x, self.y, true)
  self.assets.images["actorsShadow"]:draw(x, y, 0, self.sprite.sx, self.sprite.sy, 12, 5)
  if (self.isSelected == true) then
    self.assets.sprites["cursorground"]:draw(x - 2, y - 1, 0, 1, 1, 12, 5)
  end
end

return Parent
