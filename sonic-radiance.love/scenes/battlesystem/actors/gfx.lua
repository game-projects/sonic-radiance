local Parent = require("scenes.battlesystem.actors.movable")
local GFX = Parent:extend()
local GFX_DIRECTORY = "assets/sprites/gfx/"

function GFX:new(world, x, y, z, spritename, creator, blockProcess, tag)
  GFX.super.new(self, world, x, y, z)

  if (creator.choregraphy ~= nil) then
    self.char = self:getCharacter(creator.choregraphy.fighter)
  end
  self:setAnimation(spritename)

  self.creator = creator
  self.blockProcess = blockProcess or false
  self.tag = tag or ""

  if (not utils.string.isEmpty(self.tag)) then
    self:setIndexName(self.tag)
  end

  self.direction = 1
end

function GFX:getCharacter(fighter)
  if (fighter.isHero) then
      return fighter.abstract.simplename
  end
  return nil
end

function GFX:setAnimation(spritename)
  local defaultPath = GFX_DIRECTORY .. spritename
  if (utils.string.isEmpty(self.char)) then
      self:loadAnimation(spritename, defaultPath)
  else
    local charGFXPath = "datas/gamedata/characters/" .. self.char .. "/gfx/" .. spritename
    if (utils.filesystem.exists(charGFXPath .. ".lua")) then
        self:loadAnimation(self.char .. spritename, charGFXPath)
    else
      self:loadAnimation(spritename, defaultPath)
    end
  end
end

function GFX:loadAnimation(spritename, path)
  if self.world.assets.sprites[spritename] == nil then
    self.world.assets:addSprite(spritename, path)
  end
  local width, height = self.world.assets.sprites[spritename]:getDimensions()
  self:setSprite(spritename, width/2, height, true)
  self:cloneSprite()
end

function GFX:animationEnded(animation)
  core.debug:print("gfx", 'Current animation "' .. animation .. '" have ended, destroying gfx')
  if (self.blockProcess) and (self.creator ~= nil) and (self.creator.getSignal ~= nil) then
    self.creator:getSignal("gfxEnded")
  end
  if ((self.creator ~= nil) and (self.creator.choregraphy ~= nil) and (not utils.string.isEmpty(self.tag))) then
    self.creator.choregraphy:finishTagAction(self.tag)
  end
  self:destroy()
end

function GFX:draw()
  self:drawSprite(0, -self.z)
end

function GFX:drawShadow()

end

return GFX
