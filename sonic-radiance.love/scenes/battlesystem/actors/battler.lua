local Parent = require("scenes.battlesystem.actors.movable")
local Battler = Parent:extend()

local outputColor = {
  good = {0, 1, 0},
  bad = {1, 0, 0},
  pp = {0.3, 0.8, 1}
}

function Battler:new(world, x, y, z, owner)
  Battler.super.new(self, world, x, y, z)

  self.isBattler = true
  self.isActive = false
  self.debugActiveTimer = 0

  self.output = {}
  self.output.string = "0"
  self.output.isBad = true
  self.output.type = ""
  self.showOutput = false
  self.outputY = 0

  self.isSelected = false
  self.owner = owner

  self.isDefending = false
end

function Battler:destroy()
  Battler.super.destroy(self)
end

function Battler:getOutputY()
  return self.sprHeight or 32
end

function Battler:avoidedAttack()
  self:newOutput("", "MISS")
end

function Battler:setDamageNumber(number, isPP)
  local type = "good"
  if (isPP == true) then type = "pp" end
  if (number < 0) then type = "bad" end
  local string = math.abs(math.floor(number))

  self:newOutput(type, string)
end

function Battler:newOutput(type, string)
  self.output.type = type or ""
  self.output.string = string or "error"
  self.outputY = self:getOutputY() - 8
  self.showOutput = true

  self.tweens:newTween(0, 0.4, {outputY = self:getOutputY()}, "outQuad")
  self.tweens:newTimer(0.5, "removeOutput")
end

function Battler:getOuputColor(type)
  return outputColor[type] or {1,1,1}
end

function Battler:setActive()
  core.debug:print("cbs/actor","actor " .. self.id .. " is active")
  self.isActive = true
  self.debugActiveTimer = 0
end

function Battler:update(dt)
  Battler.super.update(self, dt)
  if (self.isActive) then
    self.debugActiveTimer = self.debugActiveTimer + dt
    if self.debugActiveTimer >= 0.5 then
      core.debug:print("cbs/battler", "counter ended, switching active battler")
      self.isActive = false
      self.world:switchActiveBattler()
    end
  end
end

-- Movement
-- Some specific movement function

function Battler:land()
  self:changeAnimation("idle")
end

-- CHOREGRAPHY FUNCTIONS
-- All functions related to the choregraphy system

function Battler:choregraphyEnded()
  self:resetMovement()
end

-- DAMAGE FUNCTIONS

function Battler:getHurt()

end

function Battler:die()
  self:destroy()
end

-- DRAW FUNCTIONS
function Battler:drawOutput()
  if (self.showOutput) then
    local x, y = self.world.map:gridToPixel(self.x, self.y, true)
    local color = self:getOuputColor(self.output.type)
    love.graphics.setColor(color[1], color[2], color[3], 1)

    self.assets.fonts["hudnbrs_small"]:print(self.output.string, x, y - self.outputY - self.z, "center")
    utils.graphics.resetColor()
  end
end

function Battler:draw()
  local x, y = self.world.map:gridToPixel(self.x, self.y, true)
  love.graphics.setColor(1, 0, 0, 1)
  love.graphics.rectangle("fill", x - 8, y - 32, 16, 32)
  love.graphics.setColor(1, 1, 1, 1)
end

function Battler:initSprite()
  if (self.assets.sprites[self.owner.name] == nil) then
    self.assets:addSprite(self.owner.name, self:getSpritePath())
  end
  self.assets.sprites[self.owner.name]:setCustomSpeed(16)
  self:setSprite(self.owner.name, 32, 48, true)
  self:cloneSprite()
  self:changeAnimation("idle")
end

function Battler:validateAction()

end

return Battler
