local Rewards = Object:extend()

local rankFactor = {1, 1.25, 1.5, 1.75, 2}

function Rewards:new(turns)
    self.turns = turns
    self.qte = 0
    self.qteSuccess = 0
end

function Rewards:apply()
    local _, rings = self:getRewards(true)
    game.loot.rings = game.loot.rings + rings
end

function Rewards:getRank()
    local ennemyNbr, turns, _, _, ko = self.turns:getDatas()
    local qteSuccess, haveDoneQte = self:getQteSuccessRate()
    local rank = 3

    if (not haveDoneQte) then
        rank = 3
    elseif (qteSuccess >= 0.75) then
        rank = 4
    elseif (qteSuccess >= 0.5) then
        rank = 3
    elseif (qteSuccess >= 0.25) then
        rank = 2
    else
        rank = 1
    end

    -- TODO: modifier l'effet de nombre de tour pour les boss
    if (turns/ennemyNbr > 3) then
        rank = rank - 1
    end

    if (ko == 0) then
        rank = rank + 1
    end

    return math.max(1, math.min(5, rank))
end

function Rewards:getExp(character)
    local exp = self:getRewards(true)
    return character.abstract.exp + exp
end

function Rewards:getRewards(real)
    local exp, ring = 0, 0
    for i, ennemy in ipairs(self.turns.ennemies.list) do
        exp = exp + ennemy.abstract.data.giveExp
        ring = ring + ennemy.abstract.data.giveRings
    end

    if (real) then
        exp = exp * rankFactor[self:getRank()]
        ring = ring * rankFactor[self:getRank()]
    end

    return exp, ring
end

function Rewards:getQteSuccessRate()
    if (self.qte == 0) then
        return 0, false
    end
    return self.qteSuccess/self.qte, true
end

function Rewards:addQTE(success)
    self.qte = self.qte + 1
    if (success) then
        self.qteSuccess = self.qteSuccess + 1
    end
end

return Rewards
