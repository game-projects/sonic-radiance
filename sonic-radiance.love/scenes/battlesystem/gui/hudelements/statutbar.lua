local GuiElement = require "birb.modules.gui.elements.parent"
local StatusBar = GuiElement:extend()
local TweenManager = require "birb.classes.time"

local Emblem = require "game.modules.gui.emblem"
local ComplexHPBar = require "game.modules.gui.complexhpbar"

local DIST_STATUSBAR = 106
local Y = 200
local STATUSBAR_W = 90

function StatusBar:new(fighter, i)
    self:initAbstract(fighter)
    StatusBar.super.new(self, self.abstract.name .. "StatutBar", (i-0.5)*DIST_STATUSBAR-(STATUSBAR_W/2), Y, STATUSBAR_W, 64)
    self:createParts(self.scene)
end

function StatusBar:initAbstract(fighter)
    self.abstract = fighter.abstract
    self.hp = self.abstract.hp
    self.pp = self.abstract.pp
    self.stats = self.abstract:getStats()
end

function StatusBar:createParts(scene)
    self.emblem = Emblem(self.abstract, scene)

    self.hpbar = ComplexHPBar(58)
    self.ppbar = ComplexHPBar(58)
    self.hpbar:setColorForeground(248 / 255, 160 / 255, 0, 1)
    self.hpbar:setColorBackground(112 / 255, 0, 0)
    self.ppbar:setColorForeground(0, 248 / 255, 248 / 255, 1)
    self.ppbar:setColorBackground(0, 54 / 255, 229 / 255)
end

function StatusBar:updateHP()
    self.tweens:newTween(0, 0.3, {hp = self.abstract.hp}, "linear")
end

function StatusBar:updatePP()
    self.tweens:newTween(0, 0.3, {pp = self.abstract.pp}, "linear")
end

function StatusBar:draw()
    self.emblem:drawBackground(self.x, self.y)
    self:drawStatusArea(self.x + 10, self.y - 8)
    self.emblem:drawForeground(self.x, self.y)
end

function StatusBar:drawStatusArea(x, y)
    self.assets.images["statusbar"]:draw(x, y)

    local hpmax = self.stats:get(self.stats.HPMAX)
    local ppmax = self.stats:get(self.stats.PPMAX)

    self.assets.fonts["hudnbrs_small"]:set()
    self.hpbar:drawWithLabels(x + 6, y + 9, self.hp, hpmax)
    self.ppbar:drawWithLabels(x + 18, y + 21, self.pp, ppmax)

    local lvl = self.abstract.level

    if lvl < 100 then
        lvl = "0" .. lvl
    end
    love.graphics.print(lvl, x + 42, y + 1)
end

return StatusBar
