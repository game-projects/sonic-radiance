local Parent = require "game.modules.gui.fancymenu"
local BattleMenu = Parent:extend()

local defTransitions = require "birb.modules.transitions"
local radTransitions = require "game.modules.transitions"

local MENU_X, MENU_Y = 88, 72
local MENU_W = 180-32
local MENU_ITEM_NUMBER = 6

function BattleMenu:new()
    BattleMenu.super.new(self, "battleMenu", MENU_X, MENU_Y, MENU_W, MENU_ITEM_NUMBER, true)
    self.isVisible = false
end

function BattleMenu:rebuild(character)
    self:clear()
    self:addItem("Attack", "left", function() self:doAction(character, "attack") end)
    self:generateSubmenu("skills", "Skill", "main", character.abstract.skills, function(data) return self:createSkillWidget(character, data) end, true)
    self:createItemMenus(character)
    self:switch("main")
    self:addItem("Defend", "left", function() self:doAction(character, "defend") end)
    self:addItem("Flee", "left", function() self:doAction(character, "flee") end)
    self:getFocus()
    self.canvas.needRedraw = true
end

function BattleMenu:createItemMenus(character)
    self:addSubmenu("items", "Items", "main", true)
    for i,pocket in ipairs(game.loot.inventory) do
      if (pocket.inBattle) then
        self:generateSubmenu(pocket.name, pocket.fullname, "items", pocket.list,
            function(itemInBag)
                return self:createItemWidget(character, pocket.name, itemInBag)
            end, true)
      end
    end
end

function BattleMenu:createSkillWidget(character, skill)
    local data = core.datas:get("skills", skill.name)
    local isOk = (data.cost <= character.abstract.pp)

    local cost = {utils.math.numberToString(data.cost or 0, 2), "right"}
    local color = utils.math.either(isOk, {1, 1, 1}, {1, 0.3, 0.3})
    local sfx = utils.math.either(isOk, "select", "error")
    local func = function() self:doSkill(character, skill.name, isOk) end

    --text, position, func, type, additionnalItems, color
    return character.abstract:getSkillName(skill.name), "left", func, sfx, {cost}, color
end

function BattleMenu:createItemWidget(character, pocketName, itemInBag)
    local data = core.datas:get("items", itemInBag.name)
    local nbr = game.loot:getItemNumber(pocketName, itemInBag.name)
    local nbrLabel = {"x" .. utils.math.numberToString(nbr, 2), "right"}
    local func = function() self:useItem(character, pocketName, itemInBag.name) end

    --text, position, func, type, additionnalItems, color
    return data.fullname, "left", func, "select", {nbrLabel}, {1, 1, 1}
end


function BattleMenu:doAction(character, action)
    self:looseFocus()
    self.isVisible = false
    character:doBasicAction(action)
end

function BattleMenu:useItem(character, pocketName, item)
    self:looseFocus()
    self.isVisible = false
    character:useItem(pocketName, item)
end

function BattleMenu:doSkill(character, skill, isOk)
    if (isOk) then
        self:looseFocus()
        character:useSkill(skill)
        self.isVisible = false
    else
        self.scene:showMessage("You haven't enough PP!")
    end
end

return BattleMenu
