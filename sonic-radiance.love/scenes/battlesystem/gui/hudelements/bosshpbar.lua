local GuiElement = require "birb.modules.gui.elements.parent"
local BossHpBar = GuiElement:extend()

local ComplexHPBar = require "game.modules.gui.complexhpbar"

local POS_X, POS_Y = 280, 28

function BossHpBar:new(hp)
    self.hp = hp
    self.baseHP = hp
    self.hpbar = ComplexHPBar(120)
    self.hpbar:setColorForeground(248/255, 160/255, 0, 1)
    self.hpbar:setColorBackground(112/255, 0, 0)

    self.bossTexture = love.graphics.newImage("assets/gui/strings/boss.png")
    local w, h = self.bossTexture:getDimensions()

    BossHpBar.super.new(self, "BossHPBar", POS_X, POS_Y, w, h)
end

function BossHpBar:setHP(newHP)
  self:newTween(0, 0.1, {hp = newHP}, 'inCubic')
end

function BossHpBar:draw()
    self.hpbar:draw(self.x, self.y, self.hp / self.baseHP)
    love.graphics.draw(self.bossTexture, self.x + 98, self.y + 10)
end

return BossHpBar