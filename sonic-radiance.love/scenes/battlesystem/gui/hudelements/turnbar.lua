local Parent = require "birb.modules.gui.elements.parent"
local TurnBar = Parent:extend()

local gui = require "game.modules.gui"

function TurnBar:new()
    TurnBar.super.new(self, "turnbar", 76, 5, 1, 1)
    self.turns = self.scene.turns
    self.cursor = self.turns.turns.current
end

function TurnBar:draw()
    for i, action in ipairs(self.turns.actionList) do
        if action.fighter:canFight() then
            action.fighter:drawIcon(self.x + (i-1)*(20), self.y)
        else
            gui.drawEmptyIcon(self.x + (i-1)*(20), self.y)
        end
    end
    local cursorx = (self.cursor-1) * 20

    if #self.turns.actionList > 0 then
        self.assets.images["menucursor"]:draw(self.x + cursorx, self.y, math.rad(90), 1, 1, 4, 16)
    end
end

return TurnBar