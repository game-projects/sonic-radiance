local Screen = require "birb.modules.gui.screen"
local VictoryScreen = Screen:extend()

local TextElement = require "birb.modules.gui.elements.text"
local ChoiceElement = require "game.modules.gui.choiceElem"
local ConfirmDialog = require "game.modules.confirmdialog"
local TextureElement = require "birb.modules.gui.elements.drawable"
local LootElement = require "scenes.battlesystem.gui.victory.loot"
local ItemsElement = require "scenes.battlesystem.gui.victory.items"
local CharExperience = require "scenes.battlesystem.gui.victory.exp"
local TileElement = require "birb.modules.gui.elements.tile"

local defTransitions = require "birb.modules.transitions"
local gui         = require "game.modules.gui"

local HEIGHT = 32
local STARTX, STARTY = 32, HEIGHT + 44
local ENDX, ENDY = 424 - 32, 240 - 24
local SIZE_FEATS = 128+28
local START_ITEMS = STARTX + SIZE_FEATS
local START_EXP = START_ITEMS + 128

local show = {
    {"gameText", "movement", 0.9, 0.4, 424/2, HEIGHT, "inExpo"},
    --{"rankBox", "tween", 1.4, 0.4, {opacity = 1}, "inExpo"},
    {"loot", "tween", 1.4, 0.4, {opacity = 1}, "inExpo"},
    {"items", "tween", 1.4, 0.4, {opacity = 1}, "inExpo"},
  }

function VictoryScreen:new()
    self.feats = 0
    self.rankBox    = gui.newTextBox("assets/gui/dialogbox.png", 48, 32)
    self.itemBox    = gui.newTextBox("assets/gui/dialogbox.png", 96, 48)

    VictoryScreen.super.new(self, "titleScreen")
    self:addTransform("show", show)
    self.scene:showOverlay(true)
    self:show()
end

function VictoryScreen:createFeats(list)
    local ennemyNbr, turns, dmgSent, dmgTaken, ko = self.turns:getDatas()
    self:addFeat(list,"Ennemies", ennemyNbr)
    self:addFeat(list,"Turns", turns)
    self:addFeat(list,"Dmg sent", dmgSent)
    if (dmgTaken == 0) then
        self:addFeat(list,"No damage !", "")
    else
        self:addFeat(list,"Dmg taken", dmgTaken)
        self:addFeat(list,"KO", ko)
    end
    local qteRate, haveDoneQte = self.rewards:getQteSuccessRate()
    if (not haveDoneQte) then
        self:addFeat(list,"No QTE done")
    else
        self:addFeat(list,"QTE success", math.floor(qteRate * 100) .. "%")
    end
    self:addRank(list)
end

function VictoryScreen:addFeat(list, text, label)
    self.feats = self.feats + 1
    local featID = "feat" .. self.feats

    label = label or ""

    local elem = ChoiceElement(featID, text, "", label, STARTX - 16, STARTY + (16*(self.feats-1)), SIZE_FEATS)
    elem.opacity = 0
    elem:newTween(1 + (0.2*self.feats), 0.15, {opacity = 1, x = STARTX}, 'inQuad')

    table.insert(list,  {elem, 0, 1})
end

function VictoryScreen:addRank(list)
    local rank = TileElement("rank", "ranks", self.rewards:getRank(), 424/2, ENDY - 28,0,4,4,13,13, 0)
    rank:newTween(1.9, 0.4, {sx=1, sy=1, opacity=1}, 'inExpo')

    table.insert(list, {rank, 0, 1})
end

function VictoryScreen:nextExp(nbr)
    local list = self.scene.turns.player:getList()
    local nextChar = list[nbr]
    if nextChar ~= nil then
        self.elements[nextChar.name .. "Exp"]:startExpGain(nbr)
        return false
    else
        self.scene:setPrompt("Finish")
        return true
    end
end

function VictoryScreen:createElements()
    self.turns = self.scene.turns
    self.rewards = self.turns.rewards

    local list = {
        {TextElement("gameText", "SA2font", "BATTLE COMPLETED", 424/2, -24, "center"), 0, 1},
        --{TextureElement("rankBox", self.rankBox, 424/2, ENDY - 4, 0, 1,1, 64/2,48, 0), 0, 1},
        {LootElement(START_ITEMS + 2, STARTY - 2), 0, 1},
        {ItemsElement(START_ITEMS + 2, STARTY + 40, {"test", "test", "test"}), 0, 1}
    }
    self:createFeats(list)

    for i, character in ipairs(self.scene.turns.player:getList()) do
        table.insert(list, {CharExperience(START_EXP - 4, STARTY + (i-1)*24 - 12, character, i), 0, 1})
    end

    return list
end

function VictoryScreen:returnToTitle()
  core.screen:startTransition(defTransitions.default, defTransitions.circle, function() scenes.menus.title(true) end, 424/2, 240/2)
end

function VictoryScreen:loadLastSave()
  self.scene.tweens:newTween(0, 0.3, {borderPosition=0}, "inOutQuad")
  core.screen:startTransition(defTransitions.default, defTransitions.default, function() game:reload() scenes.overworld() end, 424/2, 240/2)
end


return VictoryScreen