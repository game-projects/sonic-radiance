local CanvasElement = require "birb.modules.gui.elements.canvas"
local ItemsElement = CanvasElement:extend()

local gui = require "game.modules.gui"

function ItemsElement:new(x, y, list)
    self.background = gui.newTextBox("assets/gui/dialogbox.png", 128, 40+16)
    local w, h = self.background:getDimensions()
    ItemsElement.super.new(self, "items", x, y, w, h)
    self.opacity = 0
    self.list = list
end

function ItemsElement:drawTexture()
    love.graphics.draw(self.background, 0, 0)
    for index, value in ipairs(self.list) do
        if (index <= 4) then
            self.assets.fonts["small"]:draw(value, 8, 4 + (16*(index-1)), -1, "left")
        end
    end
end

return ItemsElement