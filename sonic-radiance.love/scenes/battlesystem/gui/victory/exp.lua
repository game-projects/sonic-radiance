local GuiElement = require "birb.modules.gui.elements.parent"
local CharacterExp = GuiElement:extend()

local gui = require "game.modules.gui"
local charutils = require "game.utils.characters"

function CharacterExp:new(x, y, character, number)
    self.character = character
    self.started = false
    self.done = false
    CharacterExp.super.new(self, character.name .. "Exp", x, y, 1, 1)
    self.opacity = 0
    self.number = number
    self:newTween(1.4, 0.4, {opacity = 1}, "inExpo")
    if self.number == 1 then
        self:startInSeconds(2.5)
    end
    self.targetExp = self.scene.turns.rewards:getExp(character)
    self.exp = character.abstract.exp
end

function CharacterExp:update(dt)
    if (not self.done and (self.started)) then
        self:addCharacterExp(dt)
    end
end

function CharacterExp:startInSeconds(seconds)
    self:newFunc(seconds, "startMe", function() self:startExpGain(1) end)
end

function CharacterExp:startExpGain(number)
    self.started = true
end

function CharacterExp:addCharacterExp(dt)
    local level = self.character.abstract.level
    local xpAddRatio = charutils.getLevelExpRange(level) * 0.5
    local newExp = self.exp + (xpAddRatio * dt)

    if (newExp < self.targetExp) then
        self.exp = newExp
        local nextLevelExp = charutils.getExpValue(level + 1)
        if (self.exp >= nextLevelExp) then
            self.character.abstract.abstract:levelUp()
        end
    else
        self.exp = self.targetExp
        self.character.abstract.exp = self.targetExp
        self.done = true
        self.isLast = self.screen:nextExp(self.number + 1)
        if (self.isLast) then
            self:getFocus()
        end
    end
end

function CharacterExp:keypressed(key)
    if (key == "A" and self.done and self.isLast) then
        self.scene:returnToOverworld(false)
        self.scene:setPrompt("")
    end
end

function CharacterExp:draw()
    love.graphics.setColor(1, 1, 1, self.opacity)
    self.assets.images["expbar"]:draw(self.x, self.y)
    self.character:drawIcon(self.x + 1, self.y + 6)
    love.graphics.setColor(0, 0.8, 0.1, self.opacity)
    local level = self.character.abstract.level
    local exp = self.exp
    local remainingExp = charutils.getRemainingExp(exp, level)
    local expRatio = charutils.getRelativeExpValue(exp, level) / charutils.getLevelExpRange(level)
    gui.drawBar(self.x + 22, self.y + 11, math.floor(56 * expRatio), 7)
    love.graphics.setColor(1, 1, 1, self.opacity)
    self.assets.fonts["hudnbrs_small"]:print(math.floor(remainingExp), self.x + 71, self.y + 11, "right")
    self.assets.fonts["hudnbrs_small"]:print(level, self.x + 72, self.y + 1, "right")
end

return CharacterExp