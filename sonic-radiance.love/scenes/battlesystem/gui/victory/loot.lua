local CanvasElement = require "birb.modules.gui.elements.canvas"
local LootElement = CanvasElement:extend()

local gui = require "game.modules.gui"

function LootElement:new(x, y)
    self.background = gui.newTextBox("assets/gui/dialogbox.png", 128, 40)
    local w, h = self.background:getDimensions()
    LootElement.super.new(self, "loot", x, y, w, h)
    self.canvas.isAnimated = true
    self.opacity = 0
end

function LootElement:drawTexture()
    love.graphics.draw(self.background, 0, 0)
    self.assets.fonts["small"]:draw("Rings: ", 8, 4, -1, "left")
    self.assets.fonts["small"]:draw("0", self.w - 8, 4, -1, "right")
    self.assets.fonts["small"]:draw("Exp: ", 8, 20, -1, "left")
    self.assets.fonts["small"]:draw("0", self.w - 8, 20, -1, "right")
end

return LootElement