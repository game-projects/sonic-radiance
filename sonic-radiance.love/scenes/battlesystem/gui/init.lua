local GuiScreen = require "birb.modules.gui.screen"
local CbsScreen = GuiScreen:extend()

local StatutBar = require "scenes.battlesystem.gui.hudelements.statutbar"
local TurnBar = require "scenes.battlesystem.gui.hudelements.turnbar"
local BattleMenu = require "scenes.battlesystem.gui.hudelements.menu"

local Composite = require "birb.modules.gui.elements.composite"
local Counter = require "birb.modules.gui.elements.counter"
local Asset = require "birb.modules.gui.elements.assets"

local show = {
    {"turns", "movement", 0.5, 0.6, 424/2, 80, "inOutQuart"},
}

function CbsScreen:new()
    CbsScreen.super.new(self, "hud")
    self:show()
end

function CbsScreen:buildMenu(character)
    self.elements["battleMenu"]:rebuild(character)
end

function CbsScreen:createElements()
    local turns = self.scene.turns
    local list = {
        {Composite("turns", 10, 10, {
            {Asset("turnImg", "images", "hudturn", -1, -1), 0, 0},
            {Counter("turnCnt", "hudnbrs", turns.turns, "number", 2, -1, -1), 33, 1},
            {TurnBar(), 62, -3},
        }), 0},
        {BattleMenu(), 0}
    }
    for i, fighter in ipairs(turns.player:getList()) do
        table.insert(list, {StatutBar(fighter, i), 0})
    end

    return list
end

return CbsScreen