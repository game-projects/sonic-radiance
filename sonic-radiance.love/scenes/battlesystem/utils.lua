local maputils = {}
local STATS = require "datas.consts.stats"

maputils.CONST = {}

maputils.CONST.STARTX = -8
maputils.CONST.STARTY = 90

function maputils.sortBattlers(a, b)
  local aspeed = a.fighter:getStat(STATS.SPEED) / (3 ^ (a.number-1))
  local bspeed = b.fighter:getStat(STATS.SPEED) / (3 ^ (b.number-1))

  if (aspeed == bspeed) then
    if (a.fighter.isHero == b.fighter.isHero) then
      return (a.fighter.id < b.fighter.id)
    else
      return a.fighter.isHero
    end
  else
    return (aspeed > bspeed)
  end
end

return maputils
