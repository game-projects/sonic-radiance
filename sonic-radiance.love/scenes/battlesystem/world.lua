local World = Object:extend()

local Map       = require "game.modules.drawing.parallaxBackground"

local HEIGHT = 5
local BORDER_BOTTOM = 2

-- INIT FUNCTIONS
-- Initialize the battle world

function World:new(scene, battlefile)
  self.scene    = scene
  self.assets   = scene.assets

  self.obj      = require "scenes.battlesystem.actors"

  self.actors   = {}
  self.globalID = 0

  self.map    = Map(scene, HEIGHT, BORDER_BOTTOM, "city")
  self.index  = {}

  self.isBattleActive = false
end

function World:registerActor(actor)
  self.globalID = self.globalID + 1

  table.insert(self.actors, actor)
  actor.id = self.globalID
end

function World:destroyActor(actorToDestroy)
  for i, actor in ipairs(self.actors) do
    if actor == actorToDestroy then
      table.remove(self.actors, i)
    end
  end
end

-- INFO FUNCTIONS
-- Get info from the world

function World:caseIsEmpty(x, y, notThisActor)
  local actor = self:getActorInCase(x, y, notThisActor)

  return (actor == nil)
end

function World:getActorInCase(x, y, notThisActor)
  for i, actor in ipairs(self.actors) do
    if (actor.x == x) and (actor.y == y) and (actor ~= notThisActor) then
      core.debug:print("cbs/world", "one actor found in case <" .. x .. ";" .. y .. ">")
      return actor
    end
  end

  return nil
end

function World:getActorByName(name)
  return self.index[name]
end

-- UPDATE FUNCTION
-- Update all actors

function World:update(dt)
  for i, actor in ipairs(self.actors) do
    actor:update(dt)
  end
end

function World:sendSignalToCurrentBattler(signal, subSignal)
  self.scene.turns:sendSignalToCurrentBattler(signal, subSignal)
end

-- DRAW FUNCTION
-- Draw the world

function World:draw()
  self.map:draw()
  self:drawShadows()
  self:drawActors()
end

function World:drawActors()
  table.sort(self.actors, function(a, b) return (a.y < b.y) end)
  for i, actor in ipairs(self.actors) do
    actor:draw()
  end
end

function World:drawShadows()
  for i, actor in ipairs(self.actors) do
    actor:drawShadow()
  end
end

return World
