local TurnController = Object:extend()

local Player = require "scenes.battlesystem.fighters.player"
local Ennemy = require "scenes.battlesystem.fighters.ennemies"

local Rewards = require "scenes.battlesystem.rewards"

local maputils  = require "scenes.battlesystem.utils"

function TurnController:new(scene, battleData)
  self.scene = scene
  self.world = scene.world

  self.isActive = false

  self.currentlyPlaying = ""

  self.turns      = {}
  self.turns.current        = 1
  self.turns.number         = 0
  self.turns.isFinished     = true
  self.turns.changeBattler  = true
  self.actionList = {}

  self.currentFighter = nil

  self.canFleeBattle = true

  self.gui = self.scene.gui

  self.player = Player(self)
  self.ennemies = Ennemy(self, battleData)

  self.rewards = Rewards(self)

  -- Change the seed at the start of each battle
  math.randomseed( os.time() )
  self:applyDeath()
end

function TurnController:getChanceTooFlee(value)
  if (self.canFleeBattle) then
    local speedComparison = math.min(value / self.ennemies:getHighestSpeed(), 1.5)
    return 25 + (50 * speedComparison)
  end
  return 0
end

function TurnController:fleeBattle()
  self.player:fleeBattle()
  self.scene:fleeBattle()
end

function TurnController:startBattle()
  self.isActive = true
end

function TurnController:finishBattle()
  self.isActive = false
  self.actionlist = {}
  self.scene:finishBattle()
  self.rewards:apply()
end

function TurnController:looseBattle()
  self.isActive = false
  self.actionlist = {}
  self.scene:looseBattle()
end

function TurnController:update(dt)
  self.player:update(dt)
  self.ennemies:update(dt)
  if (self.isActive) then
    if (self.currentFighter ~= nil) then
      self.currentFighter:update(dt)
    else
      if (self.player:countAlive() > 0) then
        self:nextAction()
      else
        self:looseBattle()
      end
    end
  end
end

function TurnController:getRank()
  return self.rewards:getRank()
end

function TurnController:getRewards()
  return self.rewards:getRewards(false)
end

function TurnController:getDatas()
  local ennemyNbr = #self.ennemies.list
  local turns     = self.turns.current
  local dmgSent   = self.ennemies.damages
  local dmgTaken  = self.player.damages
  local ko        = self.player.ko
  return ennemyNbr, turns, dmgSent, dmgTaken, ko
end

function TurnController:nextAction()
  if (self.turns.isFinished) or (self.turns.current >= #self.actionList) then
    self:startNewTurn()
  else
    self.turns.current = self.turns.current + 1
    core.debug:print("cbs/turns", "switching to next action")
  end

  self:startAction()
end

function TurnController:startNewTurn()
  core.debug:print("cbs/turns", "New Turn Starting")
  self.turns.current = 1
  self.turns.isFinished = false
  self.turns.number = self.turns.number + 1
  self:calculateTurn()
end

function TurnController:calculateTurn()
  self.actionList = {}
  self.player:putActions(self.actionList)
  self.ennemies:putActions(self.actionList)
  table.sort(self.actionList, maputils.sortBattlers)
end

function TurnController:removeAllActionsFromFighter(fighterToRemove)
  for i, action in ipairs(self.actionlist) do
    if action.fighter == fighterToRemove then
      table.remove(self.actionlist, i)
    end
  end
end

function TurnController:applyDeath()
  local ennemiesAlive = self.ennemies:applyDeath()
  if (ennemiesAlive == 0) then
    self:finishBattle()
  end
  self.player:applyDeath()
end


function TurnController:startAction()
  core.debug:print("cbs/turns", "Starting action " .. self.turns.current)
  local nextAction = self.actionList[self.turns.current]
  local nextFighter = nextAction.fighter
  if (not nextFighter:canFight()) then
    -- On skipe le personnage s'il a été detruit
    self:nextAction()
  else
    self.currentFighter = nextFighter
    core.debug:print("cbs/turns", "Activating " .. self.currentFighter.name)
    self.currentFighter:setActive()
    self.gui:newTween("turnbar", 0, 0.2, {cursor = self.turns.current}, 'inCubic')
  end
end

function TurnController:endAction()
  self.currentFighter = nil
end

function TurnController:sendSignalToCurrentBattler(signal, subSignal)
  self.actionList[self.turns.current].actor:receiveSignal(signal, subSignal)
end

return TurnController
