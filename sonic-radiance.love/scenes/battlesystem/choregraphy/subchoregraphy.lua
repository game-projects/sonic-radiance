local SubChoregraphy = Object:extend()

local QteMixin = require "scenes.battlesystem.choregraphy.mixins.qtes"
local StepsMixin = require "scenes.battlesystem.choregraphy.mixins.steps"
local TagsMixin = require "scenes.battlesystem.choregraphy.mixins.tags"
local CountersMixin = require "scenes.battlesystem.choregraphy.mixins.counters"
local WrappersMixin = require "scenes.battlesystem.choregraphy.mixins.wrappers"

SubChoregraphy:implement(QteMixin)
SubChoregraphy:implement(StepsMixin)
SubChoregraphy:implement(TagsMixin)
SubChoregraphy:implement(CountersMixin)
SubChoregraphy:implement(WrappersMixin)

function SubChoregraphy:new(parent, target, choregraphy)
  self.parent = parent
  self:initWrappers(parent.action, target)

  self:initSteps(choregraphy)
  self:initQte()
  self:initTagActions()
  self:initCounters()

  self:register()
end

function SubChoregraphy:register()
  self.parent:registerSubChoregraphy(self)
end

function SubChoregraphy:update(dt)
  self:updateQte(dt)
  self:updateSteps(dt)
end

function SubChoregraphy:endChoregraphy()
  self.parent:removeSubChoregraphy(self)
end

function SubChoregraphy:draw()
  self:drawQte()
end

return SubChoregraphy
