local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local WaitForStep = StepParent:extend()

function WaitForStep:new(controller, args)
  WaitForStep.super.new(self, controller, args)
end

function WaitForStep:start()
end

function WaitForStep:update(dt)
  --print(self.arguments.waitFor)
  if (self.choregraphy:checkCondition(self.arguments.waitFor)) then
    self:finish()
  end
end

return WaitForStep
