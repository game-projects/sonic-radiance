local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local GoTo3DStep = StepParent:extend()

function GoTo3DStep:new(controller, args)
  GoTo3DStep.super.new(self, controller, args, true)
end

function GoTo3DStep:start()
  local x, y, z = self:getStepCoordinate()
  local actor = self:getActor(self.arguments.who)

  actor:goTo3D(x, y, z, self.arguments.duration)
  actor:addTaggedAction(self.tag, self.choregraphy, "goTo")

  if (self.arguments.blockProcess == false) then
    self:finish()
  else
    actor:blockChoregraphy(self.arguments.blockProcess, self, "goTo")
  end
end

function GoTo3DStep:update(dt)

end

function GoTo3DStep:getSignal(signal)

end

return GoTo3DStep;
