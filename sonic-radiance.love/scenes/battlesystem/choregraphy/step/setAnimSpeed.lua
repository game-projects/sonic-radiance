local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local SetAnimSpeedStep = StepParent:extend()

function SetAnimSpeedStep:new(controller, args)
  SetAnimSpeedStep.super.new(self, controller, args)
end

function SetAnimSpeedStep:start()
  local actor = self:getActor(self.arguments.who)

  actor:setAnimSpeed(self.arguments.speed)
  self:finish()
end

function SetAnimSpeedStep:update(dt)

end

function SetAnimSpeedStep:getSignal(signal)

end

return SetAnimSpeedStep;
