local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local AnimationSetterStep = StepParent:extend()

function AnimationSetterStep:new(controller, args)
  AnimationSetterStep.super.new(self, controller, args, true)
end

function AnimationSetterStep:start()
  local actor = self:getActor(self.arguments.who)

  actor:changeAnimation(self.arguments.animation)

  actor:addTaggedAction(self.tag, self.choregraphy, "animation")
  if (self.arguments.blockProcess == false) then
    self:finish()
  else
    actor:blockChoregraphy(self.arguments.blockProcess, self, "animation")
  end
end

function AnimationSetterStep:update(dt)

end

function AnimationSetterStep:getSignal(signal)

end

return AnimationSetterStep;
