local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local StartSubchoregraphiesStep = StepParent:extend()

function StartSubchoregraphiesStep:new(controller, args)
  StartSubchoregraphiesStep.super.new(self, controller, args)
end

function StartSubchoregraphiesStep:start()
  self.choregraphy:startSubChoregraphies(self.arguments.waitTime)
  if (not self.arguments.blockProcess) then
    self:finish()
  end
end

function StartSubchoregraphiesStep:update(dt)
  if (not self.choregraphy:hasNextTarget()) then
    self:finish()
  end
end

return StartSubchoregraphiesStep;
