local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local SetCounterStep = StepParent:extend()

function SetCounterStep:new(system, args)
  SetCounterStep.super.new(self, system, args)
end

function SetCounterStep:start()
  self.choregraphy:setCounter(self.arguments.counterName, self.arguments.number, self.arguments.relative)
  self:finish()
end

return SetCounterStep
