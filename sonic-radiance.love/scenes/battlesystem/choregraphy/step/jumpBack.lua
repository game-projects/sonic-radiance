local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local JumpBackStep = StepParent:extend()

function JumpBackStep:new(controller, args)
  JumpBackStep.super.new(self, controller, args, true)
end

function JumpBackStep:start()
  local actor = self:getActor(self.arguments.who)

  actor:jumpBack(self.arguments.height, self.arguments.speed)

  actor:addTaggedAction(self.tag, self.choregraphy, "jump")
  if (self.arguments.blockProcess == false) then
    self:finish()
  else
    actor:blockChoregraphy(self.arguments.blockProcess, self, "jump")
  end
end

function JumpBackStep:update(dt)

end

function JumpBackStep:getSignal(signal)

end

return JumpBackStep;
