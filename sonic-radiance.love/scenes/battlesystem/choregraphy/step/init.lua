local actions = {}

local baseURI = "scenes.battlesystem.choregraphy.step."

actions["addGFX"] = require(baseURI .. "addGFX")
actions["addQTE"] = require(baseURI .. "addQTE")
actions["goTo"] = require(baseURI .. "goTo")
actions["goTo3D"] = require(baseURI .. "goTo3D")
actions["stopMov"] = require(baseURI .. "stopMov")
actions["jumpBack"] = require(baseURI .. "jumpBack")
actions["jump"] = require(baseURI .. "jump")
actions["playSFX"] = require(baseURI .. "playSFX")
actions["sendDamage"] = require(baseURI .. "sendDamage")
actions["sendStatus"] = require(baseURI .. "sendStatus")
actions["setAnimation"] = require(baseURI .. "setAnimation")
actions["setAnimSpeed"] = require(baseURI .. "setAnimSpeed")
actions["wait"] = require(baseURI .. "wait")
actions["waitFor"] = require(baseURI .. "waitFor")
actions["skipTo"] = require(baseURI .. "skipTo")
actions["waitActorFinished"] = require(baseURI .. "waitActorFinished")
actions["useItemEffect"] = require(baseURI .. "useItemEffect")
actions["setCounter"] = require(baseURI .. "setCounter")
actions["startSubChoregraphies"] = require(baseURI .. "startSubChoregraphies")

return actions
