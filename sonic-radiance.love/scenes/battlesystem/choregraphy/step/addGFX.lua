local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local StepGFX = StepParent:extend()

function StepGFX:new(controller, args)
  StepGFX.super.new(self, controller, args, true)
end

function StepGFX:start()
  local x, y, z = self:getStepCoordinate()

  self.choregraphy.world.obj.GFX(self.choregraphy.world, x, y, z, self.arguments.sprite, self, self.arguments.blockProcess, self.tag)

  if (not self.arguments.blockProcess) then
    self:finish()
  end
end

function StepGFX:update(dt)

end

function StepGFX:getSignal(signal)
  if (signal == "gfxEnded") then
    self:finish()
  end
end


return StepGFX
