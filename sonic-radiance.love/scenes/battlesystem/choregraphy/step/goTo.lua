local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local GoToStep = StepParent:extend()

function GoToStep:new(controller, args)
  GoToStep.super.new(self, controller, args, true)
end

function GoToStep:start()
  local x, y = self:getStepCoordinate()
  local actor = self:getActor(self.arguments.who)

  actor:goTo(x, y, self.arguments.duration)

  actor:addTaggedAction(self.tag, self.choregraphy, "goTo")
  if (self.arguments.blockProcess == false) then
    self:finish()
  else
    actor:blockChoregraphy(self.arguments.blockProcess, self, "goTo")
  end
end

function GoToStep:update(dt)

end

function GoToStep:getSignal(signal)

end

return GoToStep;
