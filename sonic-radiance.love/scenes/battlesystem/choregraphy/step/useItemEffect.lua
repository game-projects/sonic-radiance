local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local UseItemEffect = StepParent:extend()

function UseItemEffect:new(system, args)
  UseItemEffect.super.new(self, system, args)
end

function UseItemEffect:start()
  self.choregraphy:useItemEffect()
  self:finish()
end

return UseItemEffect
