local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local SendStatus = StepParent:extend()

function SendStatus:new(system, args)
  SendStatus.super.new(self, system, args)
end

function SendStatus:start()
  local status = self.arguments.status
  local force = self.arguments.force
  local duration = self.arguments.duration

  self.choregraphy:sendStatus(status, duration, force)
  self:finish()
end

return SendStatus
