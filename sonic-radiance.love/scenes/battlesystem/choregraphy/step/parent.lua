local StepParent = Object:extend()

function StepParent:new(choregraphySystem, arguments, canBeAsync)
  self.choregraphy = choregraphySystem
  self.arguments = arguments
  self.isStarted = false
  self.canBeAsync = (canBeAsync == true)
  self.tag = ""
end

function StepParent:addTag(tag)
  if (not utils.string.isEmpty(tag)) then
    if (self.canBeAsync) then
      core.debug:print("choregraphy/step", "Tag " .. tag .. " added to step.")
      self.tag = tag
    end
    self.choregraphy:startTagAction(tag)
  end
end

function StepParent:updateStep(dt)
  if (not self.isStarted) then
    self:start()
    self.isStarted = true
  else
    self:update(dt)
  end
end

function StepParent:getActor(name)
  return self.choregraphy:getActor(name)
end

function StepParent:getStepCoordinate()
  local argx, argy, argz = self.arguments.x, self.arguments.y, self.arguments.z or 0
  local x, y, z
  argx = argx * self.choregraphy.actor.start.direction
  local source = self.arguments.origin

  if (source == "start") then
    x, y, z = self.choregraphy.actor.start.x, self.choregraphy.actor.start.y, self.choregraphy.actor.start.z
  elseif (source == "center") then
    x, y, z = 6, 3, 0
  else
    local actor = self:getActor(source)
    if (actor == nil) then
      error("source " .. source .. " not found")
    end
    x, y, z = actor.x, actor.y, actor.z
  end

  return x + argx, y + argy, z + argz
end

function StepParent:finish()
  self.choregraphy:endStep()
  self.choregraphy:switchStep()
end

return StepParent
