local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local WaitActorFinishedStep = StepParent:extend()

function WaitActorFinishedStep:new(controller, args)
  WaitActorFinishedStep.super.new(self, controller, args)
end

function WaitActorFinishedStep:start()
  self.choregraphy.actor:blockChoregraphy(true, self, self.arguments.waitFor)
end

function WaitActorFinishedStep:update(dt)

end

function WaitActorFinishedStep:getSignal(signal)

end

return WaitActorFinishedStep;
