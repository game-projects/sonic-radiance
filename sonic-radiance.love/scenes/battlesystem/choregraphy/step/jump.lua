local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local JumpStep = StepParent:extend()

function JumpStep:new(controller, args)
  JumpStep.super.new(self, controller, args, true)
end

function JumpStep:start()
  local actor = self:getActor(self.arguments.who)

  actor:setJump(self.arguments.power, 0, self.arguments.useDefaultAnimation)

  actor:addTaggedAction(self.tag, self.choregraphy, "jump")
  if (self.arguments.blockProcess == false) then
    self:finish()
  else
    actor:blockChoregraphy(self.arguments.blockProcess, self, "jump")
  end
end

function JumpStep:update(dt)

end

function JumpStep:getSignal(signal)

end

return JumpStep;
