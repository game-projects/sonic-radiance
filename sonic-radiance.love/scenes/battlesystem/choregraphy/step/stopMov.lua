local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local StopMovStep = StepParent:extend()

function StopMovStep:new(controller, args)
  StopMovStep.super.new(self, controller, args, true)
end

function StopMovStep:start()
  local actor = self:getActor(self.arguments.who)

  actor:stopMoving()
  self:finish()
end

function StopMovStep:update(dt)

end

function StopMovStep:getSignal(signal)

end

return StopMovStep;
