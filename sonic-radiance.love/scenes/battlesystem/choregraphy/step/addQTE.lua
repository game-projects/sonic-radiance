local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local StepQTE = StepParent:extend()

function StepQTE:new(controller, args)
  StepQTE.super.new(self, controller, args, true)
end

function StepQTE:start()
  self.choregraphy:addQTE(self, self.arguments.origin, self.arguments.qteData, self.arguments.blockProcess, self.tag)
  if (not self.arguments.blockProcess) then
    self:finish()
  end
end

function StepQTE:update(dt)

end

function StepQTE:getSignal(signal)
  if (signal == "qteEnded") then
    self:finish()
  end
end


return StepQTE
