local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local PlaySFX = StepParent:extend()

function PlaySFX:new(system, args)
  PlaySFX.super.new(self, system, args)
end

function PlaySFX:start()
  self.choregraphy.assets.sfx[self.arguments.sfx]:play()
  self:finish()
end

return PlaySFX
