local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local SendDamage = StepParent:extend()

local maputils = require "scenes.battlesystem.utils"

function SendDamage:new(system, args)
  SendDamage.super.new(self, system, args)
end

function SendDamage:start()
  local power = self.arguments.power
  local isSpecial = self.arguments.isSpecial
  local type = self.arguments.type
  local element = self.arguments.element

  self.choregraphy:sendDamage(power, type, element, isSpecial)
  self:finish()
end

return SendDamage
