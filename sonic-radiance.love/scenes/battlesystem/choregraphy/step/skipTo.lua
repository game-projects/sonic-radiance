local StepParent = require "scenes.battlesystem.choregraphy.step.parent"
local SkipToStep = StepParent:extend()

function SkipToStep:new(system, args)
  SkipToStep.super.new(self, system, args)
end

function SkipToStep:start()
  self.choregraphy:skipToStepByTag(self.arguments.skipTo)
end

return SkipToStep
