local GuiElements = require "birb.modules.gui.elements.parent"
local QteParent = GuiElements:extend()

local Prompts = require "scenes.battlesystem.choregraphy.qte.prompts"

function QteParent:new(choregraphySystem, arguments)
  self.choregraphy = choregraphySystem
  self.arguments = arguments
  self.isBlocking = nil
  self.prompts = Prompts(self)
  self.timer = 0
  self.timerActive = false
  self.isSuccess = false
  self.tag = ""

  QteParent.super.new(self, "qte", -1, -1, 1, 1)

  self:start()
  self.isStarted = true
  self:getFocus()
end

function QteParent:setTag(tag)
  self.tag = tag
end

function QteParent:blockAction(action, isBlocked)
  if (isBlocked) then
    self.isBlocking = action
  end
end

function QteParent:endQte()
  self.choregraphy:endQte(self.isSuccess)
  if (self.isBlocking ~= nil) then
    self.isBlocking:getSignal("qteEnded")
  end
  if (not utils.string.isEmpty(self.tag)) then
    self.choregraphy:finishTagAction(self.tag)
  end
end

function QteParent:setOrigin(origin)
  local x, y, z = 424/2, 240/2, 0
  local argy = 16
  local battleCoord = false
  if (origin == "target") then
    local target = self.choregraphy.action.target
    x, y = target.actor:getCoordinate()
    z = target.actor.z
    battleCoord = true
  elseif (origin == "start") then
    x, y = self.choregraphy.actor.start.x, self.choregraphy.actor.start.y
    battleCoord = true
  elseif (origin == "actor") then
    x, y = self.choregraphy.actor:getCoordinate()
    z = self.choregraphy.actor.z
    battleCoord = true
  end
  if (not battleCoord) then
    self.x, self.y = x, y
  else
    x, y = self.choregraphy.world.map:gridToPixel(x, y, true)
    self.x, self.y = x, y - argy - z
  end
end

function QteParent:updateElement(dt)
  QteParent.super.updateElement(self, dt)
  self.prompts:update(dt)
end

function QteParent:keypressed(key)
  self:verifyPrompts(key)
end

function QteParent:timerResponse(timer)
  if (timer == "qteTimer") then
    core.debug:print("qte", "Timer finished")
    if (self.timerActive) then
      self:finish(self:isSuccessOnEnd())
    end
  elseif (timer == "startTimer") then
    core.debug:print("qte", "Timer started with " .. self.timer .. " seconds")
    self.tweens:newTimer(self.timer, "qteTimer")
    self.timerActive = true
    self.prompts.defaultTimerValue = 0
  elseif (timer == "endQte") then
    self.choregraphy:removeQte()
    self:destroy()
  end
end

function QteParent:finish(success)
  self.isSuccess = success
  self.timerActive = false
  self.tweens:newTimer(self.prompts.BEGINTIME, "endQte")
  self:endQte()
end

function QteParent:verifyPrompts(key)
  local promptResult = self.prompts:verifyPrompts(key)
  if (promptResult == 1) then
    self:promptSuccess()
  elseif (promptResult == -1) then
    self:fail()
  end
end

function QteParent:draw()
  self:drawOverButtons()
  self.prompts:draw(self.x, self.y)
  self:drawUnderButtons()
end

-- USABLE FUNCTIONS
-- Use these functions to access some of the API

function QteParent:setTimer(time)
  core.debug:print("qte", "Timer started with " .. time .. " prepared")
  self.timer = time
  self.tweens:newTimer(self.prompts.BEGINTIME, "startTimer")
end

function QteParent:delay(time)
  core.debug:print("qte", "Timer delayed by " .. time .. " seconds")
  self.tweens:delayTimer(time, "qteTimer", false)
end

function QteParent:getTimerInfo()
  return self.tweens:getTimerInfo("qteTimer")
end

function QteParent:success()
  self:finish(true)
end

function QteParent:fail()
  self:finish(false)
end

-- PUBLIC API
-- The functions that the children must overrides

function QteParent:start()

end

function QteParent:update(dt)
  --
end

function QteParent:promptSuccess()

end

function QteParent:isSuccessOnEnd()
  return false
end

function QteParent:drawOverButtons()

end

function QteParent:drawUnderButtons()

end

return QteParent
