local QtePrompts = Object:extend()
local Button = require "scenes.battlesystem.choregraphy.qte.prompts.button"

QtePrompts.BEGINTIME = 0.2

function QtePrompts:new(qte)
    self.qte = qte
    self.canPress = true
    self.removeWhenPressed = true
    self.assets = qte.choregraphy.assets
    self.scene = core.scenemanager.currentScene
    self.current = 1
    self.defaultTimerValue = 1

    self.list = {}
end

function QtePrompts:add(key, number)
    table.insert(self.list, Button(self, key, number, QtePrompts.BEGINTIME))
end

function QtePrompts:getTimer()
    local defaultValue = self.defaultTimerValue
    local _, _, timerValue = self.qte:getTimerInfo()
    return timerValue or defaultValue
end

function QtePrompts:update(dt)
    for i, button in ipairs(self.list) do
        button:update(dt, (i == self.current))
    end
end

function QtePrompts:verifyPrompts(key)
    local buttonValue, mustBeRemoved = 0, false
    if (self.list[self.current] ~= nil) then
        buttonValue, mustBeRemoved = self.list[self.current]:verifyPrompts(key, self.removeWhenPressed)
        if (not self.canPress and buttonValue == 1) then
            buttonValue = -1
        end
        if (mustBeRemoved) then
            self.list[self.current]:finish(true)
            self.current = self.current + 1
        end
    end
    for i, button in ipairs(self.list) do
        if (buttonValue == -1 and (i ~= self.current)) then
            button:finish(false)
        end
    end
    return buttonValue
end

function QtePrompts:isOver()
    return (self.current > #self.list)
end

function QtePrompts:draw(x, y)
    local SIZE = 20
    for i, button in ipairs(self.list) do
        button:draw(x + (SIZE*(i-1)) - ((SIZE/2)*(#self.list-1)), y, (i == self.current))
    end
end

return QtePrompts