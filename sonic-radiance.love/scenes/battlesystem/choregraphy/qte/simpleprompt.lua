local Parent = require "scenes.battlesystem.choregraphy.qte.parent"
local SimplePrompt = Parent:extend()

function SimplePrompt:start()
    self:setTimer(self.arguments.duration)
    for i, keyData in ipairs(self.arguments.key) do
        self.prompts:add(keyData[1], keyData[2])
    end
end

function SimplePrompt:promptSuccess()
    if (self.prompts:isOver()) then
        self:success()
    end
end

return SimplePrompt