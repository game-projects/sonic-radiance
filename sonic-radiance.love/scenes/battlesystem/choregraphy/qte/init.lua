local qtes = {}

local baseURI = "scenes.battlesystem.choregraphy.qte."

qtes["simplePrompt"] = require (baseURI .. "simpleprompt")

return qtes
