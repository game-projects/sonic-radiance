local SubChoregraphiesMixin = Object:extend()

local SubChoregraphy = require "scenes.battlesystem.choregraphy.subchoregraphy"

function SubChoregraphiesMixin:initSubchoregraphies(subChoregraphy)
    self.subChoregraphies = {} 
    self.subChoregraphies.isActive = (self.target == nil)
    self.subChoregraphies.steps = subChoregraphy
    self.subChoregraphies.currentTarget = 0
    self.subChoregraphies.waitTime = 0
    self.subChoregraphies.list = {}
end

function SubChoregraphiesMixin:startSubChoregraphies(waitTime)
    self.subChoregraphies.currentTarget = 0
    self.subChoregraphies.waitTime = waitTime
    self:startNextTarget()
end

function SubChoregraphiesMixin:updateSubChoregraphies(dt)
    for _, subchoregraphy in ipairs(self.subChoregraphies.list) do
        subchoregraphy:update(dt)
    end
end

function SubChoregraphiesMixin:startNextTarget()
    local target = self:getNextTarget()
    if (target ~= nil) then
        SubChoregraphy(self, target, self.subChoregraphies.steps)
        self.tweens:newTimer(self.subChoregraphies.waitTime, "startNextTarget")
    end
end

function SubChoregraphiesMixin:registerSubChoregraphy(subChoregraphy)
    table.insert(self.subChoregraphies.list, subChoregraphy)
    self:updateSubChoregraphyIds()
end

function SubChoregraphiesMixin:removeSubChoregraphy(subChoregraphy)
    self:updateSubChoregraphyIds()
    table.remove(self.subChoregraphies.list, subChoregraphy.id)
    self:updateSubChoregraphyIds()
end

function SubChoregraphiesMixin:updateSubChoregraphyIds()
    for id, subchoregraphy in ipairs(self.subChoregraphies.list) do
        subchoregraphy.id = id
    end
end

function SubChoregraphiesMixin:getNextTarget()
    if (self:hasNextTarget()) then
        self.subChoregraphies.currentTarget = self.subChoregraphies.currentTarget + 1
        return self.targetList[self.subChoregraphies.currentTarget]
    end
end

function SubChoregraphiesMixin:hasNextTarget()
    return (self.targetList[self.subChoregraphies.currentTarget + 1] ~= nil)
end

function SubChoregraphiesMixin:hasSubChoregraphiesActive()
    return (#self.subChoregraphies.list > 0)
end

function SubChoregraphiesMixin:drawSubChoregraphies()
    for _, subchoregraphy in ipairs(self.subChoregraphies.list) do
        subchoregraphy:draw()
    end
end


return SubChoregraphiesMixin