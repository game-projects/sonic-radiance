local CountersMixin = Object:extend()

function CountersMixin:initCounters()
    self.counters = {}
end

function CountersMixin:getCounter(counterName)
    return (self.counters[counterName] or 0)
end

function CountersMixin:setCounter(counterName, number, relative)
    if (relative == true) then
        number = number + self:getCounter(counterName)
    end
    self.counters[counterName] = number
end

return CountersMixin
