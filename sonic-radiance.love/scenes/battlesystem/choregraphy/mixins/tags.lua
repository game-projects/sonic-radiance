local TagsMixin = Object:extend()

local ACTION_STARTED = 1
local ACTION_FINISHED = 2

function TagsMixin:initTagActions()
    self.finishedTagActions = {}
end

function TagsMixin:findTaggedAction(tag)
    for stepId, step in ipairs(self.stepList) do
        if (step[1] == "taggedAction") and (step[2] == tag) then
            return stepId
        end
    end
    return 0
end

function TagsMixin:testTagAction(tag, statut)
    local tagStatut = self.finishedTagActions[tag] or 0
    return (statut <= tagStatut)
end

function TagsMixin:startTagAction(tag)
    self.finishedTagActions[tag] = ACTION_STARTED
end

function TagsMixin:finishTagAction(tag)
    core.debug:print("choregraphy/step", "Tag action " .. tag .. " finished.")
    self.finishedTagActions[tag] = ACTION_FINISHED
end

return TagsMixin
