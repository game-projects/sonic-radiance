local QteMixin = Object:extend()

local qteObjectList = require "scenes.battlesystem.choregraphy.qte"

function QteMixin:initQte()
    self.qte = {}
    self.qte.current = nil
    self.qte.wasSuccess = false
    self.qte.isActive = false
    self.qte.list = {}
end

function QteMixin:updateQte(dt)
    if (self.qte.current ~= nil) then
        self.qte.current:updateQte(dt, self.qte.isActive)
    end
end

function QteMixin:isQteSuccess(qteID)
    qteID = qteID or #self.qte.list
    return self.qte.list[qteID]
end

function QteMixin:addQTE(action, origin, qteBaseData, blockProcess, tag)
    local qteData = core.datas:parse("qtesteps", qteBaseData)
    if (qteObjectList[qteData.name] ~= nil) then
        core.debug:print("cbs/choregraphy", "Starting qte " .. qteData.name)
        self.qte.current = qteObjectList[qteData.name](self, qteData.arguments, 0, 0)
        self.qte.current:blockAction(action, blockProcess)
        self.qte.current:setOrigin(origin)
        self.qte.current:setTag(tag)
        self.qte.isActive = true
    end
end

function QteMixin:endQte(success)
    self.qte.isActive = false
    self.qte.wasSuccess = success

    table.insert(self.qte.list, success)
    self.rewards:addQTE(success)
end

function QteMixin:removeQte()
    self.qte.current = nil
end

function QteMixin:drawQte()
    if (self.qte.current ~= nil) then
        self.qte.current:draw()
    end
end

return QteMixin
