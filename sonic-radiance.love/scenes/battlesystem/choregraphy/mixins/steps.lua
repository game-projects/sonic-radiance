local StepsMixins = Object:extend()

local Predicate = require "birb.classes.predicate"
local Conditions = require "scenes.battlesystem.choregraphy.conditions"

local stepObjectList = require "scenes.battlesystem.choregraphy.step"

function StepsMixins:initSteps(choregraphy)
    self.currentStepId = 0
    self.currentStep = nil
    self.stepList = choregraphy
end

function StepsMixins:haveNextStep()
    return ((self.currentStepId + 1) <= #self.stepList)
end

-- UPDATE

function StepsMixins:updateSteps(dt)
    if (self.currentStep ~= nil) then
        self.currentStep:updateStep(dt)
    else
        self:switchStep()
    end
end

function StepsMixins:checkCondition(condition)
    local predicate = Predicate.createPredicate(condition, Conditions, self)
    return predicate:solve()
end

function StepsMixins:parseStep(step)
    local tagName = ""
    if (step[1] == "taggedAction") then
        tagName = step[2]
        step = step[3]
    end
    local stepData = core.datas:parse("choregraphystep", step)
    core.debug:print("cbs/choregraphy", "Starting step " .. stepData.name)
    return stepData, tagName
end

function StepsMixins:switchStep()
    if self:haveNextStep() then
        self.currentStepId = self.currentStepId + 1
        local stepData, tagName = self:parseStep(self.stepList[self.currentStepId])
        if (stepObjectList[stepData.name] ~= nil and self:checkCondition(stepData.condition)) then
            self.currentStep = stepObjectList[stepData.name](self, stepData.arguments)
            self.currentStep:addTag(tagName)
        end
    else
        self:endChoregraphy()
    end
end

-- SKIP OR END STEPS

function StepsMixins:skipToStepByTag(tag)
    self:skipToStep(self:findTaggedAction(tag))
end

function StepsMixins:skipToStep(id)
    if (self.stepList[id] ~= nil) then
        self.currentStepId = id - 1
        self:switchStep()
    end
end

function StepsMixins:endStep()
    self.currentStep = nil
end

return StepsMixins
