local InfosMixin = Object:extend()

function InfosMixin:initWrappers(action, target)
    self.action = action
    self.fighter = action.fighter
    self.actor = self.fighter.actor
    self.assets = self.fighter.actor.assets
    self.world = self.actor.world
    self.scene = self.world.scene
    self.turns = self.scene.turns
    self.rewards = self.turns.rewards

    self:initTargets(target or action.target)
end

function InfosMixin:initTargets(target)
    self.target = target
    self.haveSentDamage = false

    if (self.target == nil) then
        local _, targetEnnemies = self.action:needTarget()
        self.targetList = self.fighter:getTargets(targetEnnemies == false)
    end
end

function InfosMixin:getActor(name)
    if (name == "actor") then
        return self.actor
    elseif (name == "target") then
        return self:getTargetActor()
    else
        return self.fighter.world:getActorByName(name)
    end
end

function InfosMixin:getTargetActor()
    return self.target.actor
end

function InfosMixin:sendDamage(power, type, element, isSpecial)
    if (self.fighter.isAlive) then
        if (self.target ~= nil) then
            self.haveSentDamage = self.fighter:sendDamage(self.target, power, type, element, isSpecial)
        else
            self.haveSentDamage = self.fighter:sendDamageToAll(self.targetList, power, type, element, isSpecial)
        end
    else
        self.haveSentDamage = false
    end
end

function InfosMixin:sendStatus(status, duration, force)
    if (self.fighter.isAlive) then
        if (self.target ~= nil) then
            self.fighter:sendStatus(self.target, status, duration, force)
        else
            self.fighter:sendStatus(self.targetList, status, duration, force)
        end
    end
end

function InfosMixin:haveFrameSignal(signal)
    return self.actor:haveFrameSignal(signal)
end

function InfosMixin:useItemEffect()
    self.action:useItemEffect()
end

return InfosMixin
