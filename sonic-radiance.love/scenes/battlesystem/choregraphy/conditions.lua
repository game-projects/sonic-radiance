local Conditions = {}

function Conditions.sentDamage(cond, predicate, asker)
    return asker.haveSentDamage
end

function Conditions.qteSuccess(cond, predicate, asker)
    return asker:isQteSuccess(tonumber(cond[2]))
end

function Conditions.qteFailure(cond, predicate, asker)
    return (Conditions.qteSuccess(cond, predicate, asker) == false)
end

function Conditions.none(cond, predicate, asker)
    return true
end

function Conditions.actionFinished(cond, predicate, asker)
    return asker:testTagAction(cond[2], 2)
end

function Conditions.actionStarted(cond, predicate, asker)
    return asker:testTagAction(cond[2], 1)
end

function Conditions.counter(cond, predicate, asker)
    return predicate.utils.testVariables(asker:getCounter(cond[2]), cond[3], cond[4])
end

function Conditions.haveFrameSignal(cond, predicate, asker)
    return asker:haveFrameSignal(cond[2])
end

function Conditions.hasNextTarget(cond, predicate, asker)
    return asker:hasNextTarget()
end

function Conditions.hasSubChoregraphiesActive(cond, predicate, asker)
    return asker:hasSubChoregraphiesActive()
end

return Conditions