local ChoregraphySystem = Object:extend()

local QteMixin = require "scenes.battlesystem.choregraphy.mixins.qtes"
local StepsMixin = require "scenes.battlesystem.choregraphy.mixins.steps"
local TagsMixin = require "scenes.battlesystem.choregraphy.mixins.tags"
local CountersMixin = require "scenes.battlesystem.choregraphy.mixins.counters"
local WrappersMixin = require "scenes.battlesystem.choregraphy.mixins.wrappers"
local SubChoregraphiesMixin = require "scenes.battlesystem.choregraphy.mixins.subchoregraphies"

local TweenManager = require "birb.classes.time"

ChoregraphySystem:implement(QteMixin)
ChoregraphySystem:implement(StepsMixin)
ChoregraphySystem:implement(TagsMixin)
ChoregraphySystem:implement(CountersMixin)
ChoregraphySystem:implement(WrappersMixin)
ChoregraphySystem:implement(SubChoregraphiesMixin)

function ChoregraphySystem:new(action, choregraphy, subChoregraphy)
  self:initWrappers(action)

  self:initSteps(choregraphy)
  self:initQte()
  self:initTagActions()
  self:initCounters()
  self:initSubchoregraphies(subChoregraphy)

  self.tweens = TweenManager(self)

  self.actor:resetFrameSignal()
end

function ChoregraphySystem:update(dt)
  self:updateSteps(dt)
  self.tweens:update(dt)
  self:updateSubChoregraphies(dt)
end

function ChoregraphySystem:timerResponse(signal)
  if (signal == "startNextTarget") then
    self:startNextTarget()
  end
end

function ChoregraphySystem:endChoregraphy()
  self.actor:choregraphyEnded()
  self.action:choregraphyEnded()
  self.fighter.turnSystem:applyDeath()
end

return ChoregraphySystem
