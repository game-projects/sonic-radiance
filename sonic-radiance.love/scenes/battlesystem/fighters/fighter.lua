local FighterParent = Object:extend()

local battleutils = require "game.utils.battle"
local STATS = require "datas.consts.stats"

function FighterParent:new(owner, isHero, id)
  self.owner = owner
  self.turnSystem = owner.turnSystem
  self.world = owner.world
  self.isHero = isHero
  self.assets = self.turnSystem.scene.assets

  -- Note : l'ID est ici relatif à chaque quand, il n'est donc pas unique,
  -- ce qui est unique étant le combo id + isHero
  self.id = id

  self.abstract = self:getAbstract()
  self.actor = self:createActor()

  self.isActive = false
  self.isAlive = true

  self.isDefending = false

  self.action = nil
  self.statsBonus = {}
end

-- LIFE handling functions

function FighterParent:setHP(value, relative)
  local relativeNumber = value
  if (not relative) then
    relativeNumber = relative - self.abstract.hp
  end
  self.abstract:setHP(value, relative)
  self.actor:setDamageNumber(relativeNumber)
  self:updateHP()
end

function FighterParent:setPP(value, relative)
  self.abstract:setPP(value, relative)
  self:updatePP()
end

function FighterParent:applyDeath()
  if (self.abstract:isAlive() ~= self.isAlive ) then
    if (self.abstract:isAlive()) then
      self:revive()
    else
      self:die()
    end
  end
end

function FighterParent:revive()
  self.isAlive = true
  self.isDefending = false
  self.actor:revive()
end

function FighterParent:die()
  self.isAlive = false
  self.actor:die()
end

function FighterParent:updatePP()
  -- Fonction vide
end

function FighterParent:updateHP()
  -- Fonction vide
end

function FighterParent:haveProtecType(type)
  return self.abstract:haveProtecType(type)
end

function FighterParent:sendDamageToAll(listTarget, value, type, element, isSpecial)
  for _, target in ipairs(listTarget) do
    self:sendDamage(target, value, type, element, isSpecial)
  end
end

function FighterParent:sendStatusToAll(listTarget, status, duration, force)
  for _, target in ipairs(listTarget) do
    self:sendStatus(target, status, duration, force)
  end
end

function FighterParent:getTargets(ourSide)
  if (self.isHero == ourSide) then
    return self.turnSystem.player:getTargets(true)
  else
    return self.turnSystem.ennemies:getTargets(true)
  end
end

function FighterParent:sendDamage(target, value, type, element, isSpecial)
  local damage = battleutils.computeLaunchingDamages(value, self, isSpecial)

  core.debug:print("cbs/battler", "Sending " .. damage .." damage at " .. target.name)
  if (battleutils.rollDice(self:getStat(STATS.ACCURACY))) then
    return target:receiveDamage(damage, type, element, isSpecial, self)
  else
    target.actor:avoidedAttack()
    return false
  end
end

function FighterParent:sendStatus(target, status, duration, force)
  core.debug:print("cbs/battler", "Sending status " .. status .." at " .. target.name)
  if ((not force) or battleutils.rollDice(self:getStat(STATS.ACCURACY))) then
    target:receiveStatus(status, duration, force)
  end
end

function FighterParent:receiveStatus(status, duration, force)
  if (force or (not battleutils.rollDice(self:getStat(STATS.LUCK)))) then
    self.abstract:addStatut(status, duration)
  end
end

function FighterParent:receiveDamage(value, type, element, isSpecial, fromWho)
  local damages = battleutils.computeReceivingDamages(value, self, isSpecial, self.isDefending)
  damages = battleutils.applyProtectTypes(damages, type, self.abstract:getProtecTypes())
  damages = battleutils.applyResistences(damages, element, self.abstract:getResistences())
  damages = battleutils.applyWeaknesses(damages, element, self.abstract:getWeaknesses())

  if (battleutils.rollDice(self:getStat(STATS.EVASION))) then
    self.actor:avoidedAttack()
    return false
  else
    self:applyDamage(damages)
  end

  return true
end

function FighterParent:applyDamage(damage)
  core.debug:print("cbs/fighter", "Taken " .. damage .. " damage" )
  self:setHP(damage * -1, true)
  self.owner:registerDamage(damage)
  if (not self.isDefending) then
    self.actor:getHurt()
  end
  if (not self.abstract:isAlive()) then
    self.owner:registerKO()
  end
end

function FighterParent:getAbstract()
  return {}
end

function FighterParent:createActor()
  return {}
end

function FighterParent:update(dt)
  if (self.action ~= nil) then
    if (self.action.isStarted) then
      self.action:update(dt)
    end
  end
end

function FighterParent:updateAssets(dt)
  -- Vide pour l'instant
end

function FighterParent:setActive()
  self.isActive = true
  if (self.isDefending) then
    self.actor:changeAnimation("idle")
    self.actor.isDefending = false
  end
  self:startAction()
end

function FighterParent:applyRegen()
  local regenStat = self:getStat(STATS.HPREGEN)
  if (regenStat ~= 0) then
    self:setHP(regenStat*self:getStat(STATS.HPMAX)/100, true)
  end

  local regenStat = self:getStat(STATS.PPREGEN)
  if (regenStat ~= 0) then
    self:setPP(regenStat*self:getStat(STATS.PPMAX)/100, true)
  end
end

function FighterParent:setInactive()
  self.isActive = false
  self:applyRegen()
  self.turnSystem:endAction()
end

function FighterParent:getNbrActionPerTurn()
  return self.abstract.turns
end

function FighterParent:getStat(statname)
  local stat = (self.abstract.stats:get(statname) * self:getStatBonusValue(statname)) + self.abstract:getStatutsStat(statname)
  if (self.abstract:haveStatuts("cursed") and (statname == "evasion")) then
    return math.max(0, stat)
  end
  return stat
end

function FighterParent:getStatBonusValue(statname)
  if (utils.table.contain(STATS.NOBONUS, statname)) then
    return 1
  else
    return STATS.BONUS[self:getStatBonus(statname) + 5]
  end
end

function FighterParent:getStatBonus(statname)
  return self.statsBonus[statname] or 0
end

function FighterParent:setStatBonus(statname, value, relative)
  local statBonus = 0
  if (relative) then
    statBonus = self:getStatBonus(statname)
  end
  self.statsBonus[statname] = math.max(-4, math.min(statBonus + value, 4))
end

function FighterParent:getStats()
  return self.abstract:getStats()
end

function FighterParent:startAction()

end

function FighterParent:finishAction()
  self.action = nil
  self:setInactive()
end

function FighterParent:getUniqueIdentificator()
  local side = 1
  if (self.isHero == false) then
    side = -1
  end
  return self.id * side
end

function FighterParent:getNonUniqueIdentificator()
  return self.id
end

function FighterParent:canFight()
  return self.isAlive
end

-- DRAW FUNCTIONS

function FighterParent:drawIcon(x, y)
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.rectangle("fill", x, y, 16, 16)
end

return FighterParent
