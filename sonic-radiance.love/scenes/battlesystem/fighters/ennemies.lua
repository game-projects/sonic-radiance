local FighterControllerParent = require "scenes.battlesystem.fighters.parent"
local EnnemyController = FighterControllerParent:extend()

local Villain = require "scenes.battlesystem.fighters.ennemy"
local STATS = require "datas.consts.stats"

function EnnemyController:new(owner, battleData)
  self.super.new(self, owner)
  self:initVillains(battleData)
end

function EnnemyController:initVillains(battleData)
  for i,ennemyBaseData in ipairs(battleData.ennemies) do
    local ennData = core.datas:parse("ennemytype", ennemyBaseData)
    if (ennData.type == "normal") then
      self:addVillain(ennData)
    elseif (ennData.type == "boss") then
      self:addBoss(ennData)
    else
      core.debug:warning("unknown type " .. ennData.type)
    end
  end
end

function EnnemyController:addVillain(ennData)
  for i=1, ennData.number do
    self:add(Villain(self, ennData.category, ennData.name, self:count() + 1))
  end
end

function EnnemyController:getHighestSpeed()
  local highestSpeed = 0
  for i, villain in ipairs(self.list) do
    local currentSpeed = villain:getStat(STATS.SPEED)
    if (currentSpeed > highestSpeed) then
      highestSpeed = currentSpeed
    end
  end
  return highestSpeed
end

function EnnemyController:addBoss(ennData)
  local boss = Villain(self, ennData.category, ennData.name, self:count() + 1)
  boss:setBonus(ennData.pvFactor, ennData.statFactor)
  boss.isBoss = true
  boss:setCheapEffect(ennData.cheapEffect)
  self.turnSystem.canFleeBattle = false
  self:add(boss)
  boss:createHPBar()
end

return EnnemyController
