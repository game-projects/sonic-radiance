local FighterControllerParent = Object:extend()

function FighterControllerParent:new(turnSystem)
  self.turnSystem = turnSystem
  self.world = turnSystem.world
  self.list = {}
  self.damages = 0
  self.ko = 0
end

function FighterControllerParent:get(id)
  return self.list[id]
end

function FighterControllerParent:getList()
  return self.list
end

function FighterControllerParent:add(fighter)
  table.insert(self.list, fighter)
end

function FighterControllerParent:count()
  return #self.list
end

function FighterControllerParent:registerDamage(damage)
  self.damages = math.floor(self.damages + damage)
end

function FighterControllerParent:registerKO()
  self.ko = self.ko + 1
end

function FighterControllerParent:countAlive()
  local aliveCount = 0
  for i, fighter in ipairs(self.list) do
    if (fighter:canFight()) then
      aliveCount = aliveCount + 1
    end
  end
  return aliveCount
end

function FighterControllerParent:getTargets(onlyAlive)
  local targetList = {}
  for i, fighter in ipairs(self.list) do
    if (fighter:canFight() or (not onlyAlive)) then
      table.insert(targetList, fighter)
    end
  end
  return targetList
end

function FighterControllerParent:applyDeath()
  for i, fighter in ipairs(self.list) do
    fighter:applyDeath()
  end
  return self:countAlive()
end

function FighterControllerParent:setActive(activeActor)
  self.activeActor = activeActor
  activeActor:setActive()
end

function FighterControllerParent:update(dt)
  for i, fighter in ipairs(self.list) do
    fighter:updateAssets(dt)
  end
end

function FighterControllerParent:endAction()
  self.owner:nextAction()
end

function FighterControllerParent:putActions(actionList)
  for i, fighter in ipairs(self.list) do

    for i=1, fighter:getNbrActionPerTurn() do
      local action = {}
      action.fighter = fighter
      action.number  = i
      table.insert(actionList, action)
    end

  end
end

function FighterControllerParent:removeFighter(fighterToRemove)
  -- remove the actor from the battler liste
  for i, fighter in ipairs(self.list) do
    if fighter == fighterToRemove then
      table.remove(self.list, i)
    end
  end

  -- Also remove all actions related to the actor
  self.turnSystem:removeAllActionsFromFighter(fighterToRemove)
end



return FighterControllerParent
