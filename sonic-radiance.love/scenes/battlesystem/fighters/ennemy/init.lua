local FighterParent = require "scenes.battlesystem.fighters.fighter"
local VillainFighter = FighterParent:extend()

local SimpleHPBar   = require "game.modules.gui.simplehpbar"
local BossHPBar   = require "scenes.battlesystem.gui.hudelements.bosshpbar"
local EnnemyAction = require "scenes.battlesystem.fighters.ennemy.action"
local behaviourList = require "scenes.battlesystem.fighters.ennemy.behaviours"

local POSITIONS = {1, 3, 5}
local ENNEMY_LINE = 11;

function VillainFighter:new(owner, category, ennemy, id)
  self.name = ennemy
  self.category = category
  self.super.new(self, owner, false, id)

  self.hpbar = SimpleHPBar(self.abstract.hp)
  self.isBoss = false
end

function VillainFighter:setCheapEffect(cheapEffect)
  self.actor:setCheapEffect(cheapEffect)
end

function VillainFighter:updateAssets(dt)
  self.hpbar:update(dt)
end

function VillainFighter:getAbstract()
  return game.ennemies:getEnnemyData(self.category, self.name)
end

function VillainFighter:createActor()
  local x, y = ENNEMY_LINE, POSITIONS[self.id]
  return self.world.obj.Ennemy(self.world, x, y, self)
end

function VillainFighter:startAction()
  core.debug:print("cbs/villainFighter", "choosing an action")
  self.action = nil
  self:selectAction()
end

function VillainFighter:selectAction()
  if (#self.abstract.skills < 1) then
    self:finishAction()
  else
    local skillId = math.floor(math.random(1, #self.abstract.skills))
    local skill = self.abstract.skills[skillId]
    self.action = EnnemyAction(self, skill)
    self:verifyTargets()
  end
end

function VillainFighter:verifyTargets()
  local needTarget, targetEnnemies = self.action:needTarget()

  if (needTarget) then
    if (targetEnnemies) then
      --fighter, skilldata, targetList
      local Behaviour = behaviourList[self.abstract.data.behaviour]
      local behav = Behaviour(self, self.action:getData(), self.owner.turnSystem.player:getTargets(true))
      self.action:setTarget(behav:getTarget())
      self.action:start()
    else
      --self.selection = SelectionSystem(self, self.owner, false)
    end
  else
    self.action:start()
  end

end


function VillainFighter:endAction()

end

function VillainFighter:setBonus(pvFactor, statFactor)
  self.abstract:setBonus(pvFactor, statFactor)
end

-- LIFE FUNCTIONS
function VillainFighter:setHP(value, relative)
  VillainFighter.super.setHP(self, value, relative)
  self.hpbar:setHP(self.abstract.hp)
  if (self.bossHpBar ~= nil) then
    self.bossHpBar:setHP(self.abstract.hp)
  end
end

-- DRAW FUNCTIONS
function VillainFighter:drawIcon(x, y)
  self.assets.images["badnicsIcon"]:draw(x-1, y-2)
  self.assets.fonts["hudnbrs_small"]:print(self.id, x+10, y+8)
end

function VillainFighter:drawOversprite(x, y)
  if (not self.isBoss) then
    self.hpbar:draw(x, y)
  else
    self.assets.images["crown"]:draw(x, y - 2)
  end
end

function VillainFighter:createHPBar()
  self.bossHpBar = BossHPBar(self.abstract.hp)
end

return VillainFighter
