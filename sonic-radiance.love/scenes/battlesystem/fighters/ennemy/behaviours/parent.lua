local BehaviourParent = Object:extend()

function BehaviourParent:new(fighter, skilldata, targetList, highestScore)
  self.fighter = fighter
  self.scoreList = {}
  self.targetList = targetList
  self.skilldata = skilldata
  self.highestScore = highestScore

  self:initScoreList()
  self:calculateAllScores()
end

function BehaviourParent:initScoreList()
  for i, target in ipairs(self.targetList) do
    self.scoreList[target.name] = 0
  end
end

function BehaviourParent:calculateAllScores()
  for i, target in ipairs(self.targetList) do
    self.scoreList[target.name] = self:calculateScore(target)
  end
end

function BehaviourParent:calculateScore(target)
  return 0
end

function BehaviourParent:getScore(target)
  return self.scoreList[target.name]
end

function BehaviourParent:isBestTarget(target, bestTargetScore, isHighest)
  if (bestTargetScore == nil) then
    return true
  else
    if (isHighest) then
      return (self:getScore(target) >= bestTargetScore)
    else
      return (self:getScore(target) <= bestTargetScore)
    end
  end
end

function BehaviourParent:getTarget()
  local bestTargetScore = nil
  local finalTarget = nil

  for i, target in ipairs(self.targetList) do
    if (self:isBestTarget(target, bestTargetScore, self.isHighest)) then
      finalTarget = target
      bestTargetScore = self:getScore(target)
    end
  end

  return finalTarget
end


return BehaviourParent
