local ParentBehaviour = require "scenes.battlesystem.fighters.ennemy.behaviours.parent"
local RandomBehaviour = ParentBehaviour:extend()

function RandomBehaviour:new(fighter, skilldata, targetList)
  RandomBehaviour.super.new(self, fighter, skilldata, targetList, true)
end

function RandomBehaviour:calculateScore(target)
  return math.random(1, 200)
end

return RandomBehaviour
