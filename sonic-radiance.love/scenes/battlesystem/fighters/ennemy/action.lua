local ActionParent = require "scenes.battlesystem.fighters.character.actions.parent"
local EnnemyAction = ActionParent:extend()

function EnnemyAction:new(fighter, skill)
  self.data = core.datas:get("badskills", skill)
  EnnemyAction.super.new(self, fighter)
end

function EnnemyAction:needTarget()
  return (self.data.targetNumber == 1), self.data.targetEnnemies
end

function EnnemyAction:startAction()
  core.debug:print("cbs/action", "Starting flee action")
  self:loadChoregraphyFromSkill(self.data)
end

function EnnemyAction:getData()
  return self.data
end

return EnnemyAction
