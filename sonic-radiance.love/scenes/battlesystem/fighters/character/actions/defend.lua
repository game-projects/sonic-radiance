local ActionParent = require "scenes.battlesystem.fighters.character.actions.parent"
local DefendAction = ActionParent:extend()

function DefendAction:new(fighter)
  DefendAction.super.new(self, fighter)
end

function DefendAction:needTarget()
  return false, false
end

function DefendAction:startAction()
  core.debug:print("cbs/action", "Starting defend action")
  self.fighter.isDefending = true
  self.fighter.actor:changeAnimation("defend")
  self.fighter.actor.isDefending = true
  self:finishAction()
end

return DefendAction
