local ActionParent = require "scenes.battlesystem.fighters.character.actions.parent"
local AttackAction = ActionParent:extend()

function AttackAction:new(fighter)
  AttackAction.super.new(self, fighter)
end

function AttackAction:needTarget()
  return true, true
end

function AttackAction:startAction()
  core.debug:print("cbs/action", "Starting attack action")
  self:loadChoregraphy("attack")
end

return AttackAction
