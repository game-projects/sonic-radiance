local actions = {}

actions.attack = require "scenes.battlesystem.fighters.character.actions.attack"
actions.skill = require "scenes.battlesystem.fighters.character.actions.skill"
actions.item = require "scenes.battlesystem.fighters.character.actions.item"
actions.defend = require "scenes.battlesystem.fighters.character.actions.defend"
actions.flee = require "scenes.battlesystem.fighters.character.actions.flee"

return actions
