local ActionParent = require "scenes.battlesystem.fighters.character.actions.parent"
local SkillAction = ActionParent:extend()

function SkillAction:new(fighter, skill)
  self.data = core.datas:get("skills", skill)
  SkillAction.super.new(self, fighter)
end

function SkillAction:needTarget()
  return (self.data.targetNumber == 1), self.data.targetEnnemies
end

function SkillAction:startAction()
  core.debug:print("cbs/action", "Starting flee action")
  self:loadChoregraphyFromSkill(self.data)
  self.fighter:setPP(self.data.cost * -1, true)
end

return SkillAction
