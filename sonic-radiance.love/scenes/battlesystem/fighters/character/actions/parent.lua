local ActionParent = Object:extend()

local ChoregraphySystem = require "scenes.battlesystem.choregraphy"

function ActionParent:new(fighter)
  self.fighter = fighter
  self.target = nil
  self.choregraphy = nil

  self.isStarted = false
end

function ActionParent:update(dt)
  if (self.choregraphy ~= nil) then
    self.choregraphy:update(dt)
  end
end

function ActionParent:loadChoregraphy(skillname)
  local skill = core.datas:get("skills", skillname)
  self:loadChoregraphyFromSkill(skill)
end

function ActionParent:loadChoregraphyFromSkill(skill)
  self.choregraphy = ChoregraphySystem(self, skill.choregraphy, skill.subChoregraphy)
end

function ActionParent:needTarget()
  -- needTarget, targetEnnemies
  return false, false
end

function ActionParent:setTarget(target)
  self.target = target
end

function ActionParent:start()
  self.isStarted = true
  self:startAction()
end

function ActionParent:choregraphyEnded()
  self.choregraphy = nil
  self:finishAction()
end

function ActionParent:startAction()

  self:finishAction()
end

function ActionParent:finishAction()
  self.fighter:finishAction()
end

return ActionParent
