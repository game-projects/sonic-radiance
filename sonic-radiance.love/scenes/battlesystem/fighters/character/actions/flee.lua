local ActionParent = require "scenes.battlesystem.fighters.character.actions.parent"
local FleeAction = ActionParent:extend()
local STATS = require "datas.consts.stats"

function FleeAction:new(fighter)
  FleeAction.super.new(self, fighter)
end

function FleeAction:needTarget()
  return false, false
end

function FleeAction:startAction()
  core.debug:print("cbs/action", "Starting flee action")
  local stats = self.fighter.abstract.stats

  if (self.fighter.abstract.name == "shadow") then
    self.fighter.turnSystem.scene:showMessage("You won't flee the battle")
    self:finishAction()
    return
  end

  local chanceToFlee = self.fighter.turnSystem:getChanceTooFlee(self.fighter:getStat(STATS.SPEED))
  if (math.random(100) < chanceToFlee) then
    self.fighter.turnSystem.scene:showMessage("You flee the battle")
    self.fighter.turnSystem:fleeBattle()
  else
    self.fighter.turnSystem.scene:showMessage("You failed to flee the battle")
    self:finishAction()
  end

end

return FleeAction
