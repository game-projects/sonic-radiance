local ActionParent = require "scenes.battlesystem.fighters.character.actions.parent"
local ItemAction = ActionParent:extend()
local EffectManager = require "game.loot.effectManager"

function ItemAction:new(fighter, category, item)
  ItemAction.super.new(self, fighter)
  self.category = category
  self.item = item
  self.itemdata = core.datas:get("items", item)
  self.effectManager = EffectManager()
  self.effectManager:getItemData(category, item)
end

function ItemAction:needTarget()
  return (not self.itemdata.affectEverybody), (self.category == "wisps")
end

function ItemAction:useItemEffect()
  self.effectManager:applyEffectsBattle(self.target)
end

function ItemAction:startAction()
  core.debug:print("cbs/action", "Starting item action")
  self:loadChoregraphy("useitem")
  game.loot:removeItem(self.category, self.item, 1)
end

return ItemAction
