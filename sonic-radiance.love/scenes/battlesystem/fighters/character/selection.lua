local GuiElement = require "birb.modules.gui.elements.parent"
local SelectionSystem = GuiElement:extend()

function SelectionSystem:new(owner, fighterSide, onlyAlive)
  SelectionSystem.super.new(self, "selection", 0, 0, 1, 1)
  self.fighterList = fighterSide:getTargets(onlyAlive)
  self.owner = owner
  self.assets = self.owner.assets
  self.selectedTarget = 1
  self:updateTarget()
  self:getFocus()
end

function SelectionSystem:keypressed(key)
  if (key == "A") then
    self.assets.sfx["mSelect"]:play()
    self:selectTarget()
  elseif (key == "B") then
    self.assets.sfx["mBack"]:play()
    self:goBack()
  elseif (key == "up") then
    self:purgeTarget()
    if (self.selectedTarget == 1) then
      self.selectedTarget = #self.fighterList
    else
      self.selectedTarget = self.selectedTarget - 1
    end
    self.assets.sfx["mBeep"]:play()
    self:updateTarget()
  elseif (key == "down") then
    self:purgeTarget()
    if (self.selectedTarget == #self.fighterList) then
      self.selectedTarget = 1
    else
      self.selectedTarget = self.selectedTarget + 1
    end
    self.assets.sfx["mBeep"]:play()
    self:updateTarget()
  end
end

function SelectionSystem:purgeTarget()
  local target = self.fighterList[self.selectedTarget]
  target.actor.isSelected = false
end

function SelectionSystem:updateTarget()
  local target = self.fighterList[self.selectedTarget]
  target.actor.isSelected = true
end

function SelectionSystem:selectTarget()
  self:looseFocus()
  self:destroy()
  self:purgeTarget()
  self.owner:receiveTarget(self.fighterList[self.selectedTarget])
end

function SelectionSystem:goBack()
  self:looseFocus()
  self:destroy()
  self:purgeTarget()
  self.owner:goBackToMenu()
end

return SelectionSystem
