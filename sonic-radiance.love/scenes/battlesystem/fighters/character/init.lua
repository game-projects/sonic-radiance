local FighterParent = require "scenes.battlesystem.fighters.fighter"
local HeroFighter = FighterParent:extend()

local SelectionSystem = require "scenes.battlesystem.fighters.character.selection"
local actionList = require "scenes.battlesystem.fighters.character.actions"

local HEROES_LINE = 2;

function HeroFighter:new(owner, character, id)
  self.name = character
  self.super.new(self, owner, true, id)

  self:initVoices()

  self.exp = self.abstract.exp
end

function HeroFighter:update(dt)
  HeroFighter.super.update(self, dt)
end

function HeroFighter:getAbstract()
  return game.characters.list[self.name]
end

function HeroFighter:createActor()
  local x, y = HEROES_LINE, ((self.id-1)*(4/(#game.characters.team-1))+1)
  local hero = self.world.obj.Hero(self.world, x, y, self)
  if (self.abstract.hp <= 0) then
    hero:setAsKo()
  end
  return hero
end

function HeroFighter:startAction()
  core.debug:print("cbs/heroFighter", "launching the action menu")
  self.action = nil
  self:talk("turnstart")
  self.turnSystem.scene.gui.screens["hud"]:buildMenu( self )
end

function HeroFighter:endAction()

end

-- Basic actions
function HeroFighter:doNothing()
  self:setInactive()
end

function HeroFighter:doBasicAction(action)
  self.action = actionList[action](self)
  self:verifyTargets()
end

function HeroFighter:useItem(category, item)
  self.action = actionList["item"](self, category, item)
  self:verifyTargets()
end

function HeroFighter:useSkill(skill)
  self.action = actionList["skill"](self, skill)
  self:verifyTargets()
end

function HeroFighter:verifyTargets()
  local needTarget, targetEnnemies = self.action:needTarget()

  if (needTarget) then
    if (targetEnnemies) then
      SelectionSystem(self, self.owner.turnSystem.ennemies, true)
    else
      SelectionSystem(self, self.owner, false)
    end
  else
    self.action:start()
  end

end

function HeroFighter:attack()
  self:doBasicAction("attack")
end

function HeroFighter:receiveTarget(target)
  if (self.action ~= nil) then
    self.action:setTarget(target)
    self.action:start()
  end
end

function HeroFighter:goBackToMenu()
  self.turnSystem.scene.gui:setFocus("battleMenu")
end

-- LIFE functions
function HeroFighter:updatePP()
  local elem = self.turnSystem.scene.gui:getElement(self.abstract.name .. "StatutBar")
  elem:updatePP()
end

function HeroFighter:updateHP()
  local elem = self.turnSystem.scene.gui:getElement(self.abstract.name .. "StatutBar")
  elem:updateHP()
end

-- VOICE SYSTEM

function HeroFighter:initVoices()
  self:addVoiceEffect("move")
  self:addVoiceEffect("turnstart")
end

function HeroFighter:addVoiceEffect(name)
  local completename = self.name .. "_" .. name
  local path = "datas/gamedata/characters/" .. self.name .. "/voices/" .. name .. ".wav"
  self.assets:newSFX(completename, path)
end

function HeroFighter:talk(name)
  local completename = self.name .. "_" .. name
  self.assets.sfx[completename]:play()
end

-- DRAW FUNCTIONS
function HeroFighter:drawIcon(x, y)
  local iconID = self.abstract.data.icon
  self.assets.tileset["charicons"]:drawTile(iconID, x, y)
end

return HeroFighter
