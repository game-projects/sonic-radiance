local FighterControllerParent = require "scenes.battlesystem.fighters.parent"
local HeroFighterController = FighterControllerParent:extend()

local Character = require "scenes.battlesystem.fighters.character"

function HeroFighterController:new(owner)
  self.super.new(self, owner)
  self:initHeroes()
end

function HeroFighterController:initHeroes()
  for i, hero in ipairs(game.characters.team) do
    self:add(Character(self, hero, i))
  end
end

function HeroFighterController:fleeBattle()
  for i, hero in ipairs(self.list) do
    hero.actor:flee()
  end
end

return HeroFighterController
