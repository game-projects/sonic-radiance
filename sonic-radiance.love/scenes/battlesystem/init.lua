local Scene = require "game.scenes"

local BattleSystem = Scene:extend()

local World         = require "scenes.battlesystem.world"
local Turns         = require "scenes.battlesystem.turns"

local VictoryScreen = require "scenes.battlesystem.gui.victory"
local GameOverScreen = require "game.modules.gui.gameover"

local CbsScreen = require "scenes.battlesystem.gui"

local TweenManager = require "birb.classes.time"

function BattleSystem:new(battleData)
  BattleSystem.super.new(self, false, false)
  self.assets:batchImport("assets.battle")

  self:playMusic(battleData.music)
  self:initManagers(battleData)
  self:startBattle()

  CbsScreen()

  self.screen = nil
  self.tweens = TweenManager(self)
end

function BattleSystem:playMusic(music)
  self.assets:setMusic("assets/music/" .. music .. ".mp3")
  self.assets:playMusic()
end

function BattleSystem:initManagers(battleData)
  self.datas = {}
  self.world        = World(self)
  self.turns        = Turns(self, battleData)
end

function BattleSystem:startBattle()
  self.turns:startBattle()
end

function BattleSystem:finishBattle()
  self.assets:setMusic("assets/music/victory.mp3")
  self.assets:playMusic()
  VictoryScreen(self)
end

function BattleSystem:fleeBattle()
  self.tweens:newTimer(2, "flee")
end

function BattleSystem:timerResponse(name)
  if (name == "flee") then
    self:returnToOverworld(true)
  end
end

function BattleSystem:returnToOverworld(isFleeing)
  self.assets:silence()
  game.cbs:endBattle(isFleeing)
end

function BattleSystem:looseBattle()
  GameOverScreen(self)
end

function BattleSystem:haveMenus()
  return self.gui.elements["battleMenu"].isVisible
end

function BattleSystem:update(dt)
  self.tweens:update(dt)
  self.turns:update(dt)
  self.world:update(dt)
end

function BattleSystem:exit()
  self.world:destroy()
  self.battlearena = nil

  collectgarbage()
end

return BattleSystem
