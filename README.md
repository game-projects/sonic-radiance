# Sonic Radiance
Sonic Radiance is a Sonic (J)RPG, inspired by the GBA/DS era mostly, with some elements from other Sonic games, especially from Adventure/Heroes. It aim to convey the goods of the GBA RPG (large-but-not-too-large area with secrets to find, simple yet effective storytelling), and to adapt the Sonic experience in an turn-based RPG form.

It'll adapt some of the "core" element of Adventure/Modern Sonic games (ranks, speed, action), inside of its RPG System. It'll be done by having a more dynamic battle system inspired by Chronicles, with QTE and multiple action per turn, with the action selection at the moment your character act. The overworld will add some Zelda-esque elements, in order to adapt the capacities of the Sonic characters.

## Synopsis

Moonstone Island is a small temperate island, that have been for its whole history subject of settlers. The mysterious Sidh People came first, nearly 700 years ago, while the current administration of Monado City came nearly 80 years ago. Today, the first one. A lot of old myth and new urban legends exist about the isle. Visited by the Moon herself, having been the place to energetic experiments... There is a lot too see on this touristic island !

A few days ago, it's a panicked call that Sonic, Tails and Amy got from the Island governor. A lot of badnics have appeared on the entire Island ! Suspecting a new mysterious Eggman plot, our heroes are ready to take action.

Will they be alone in this adventure ? What are the secrets of Moonstone Island ? Who is the Moon Daughter ? Why is the Gosth Central forbidden ? Sonic alone won't be able to solve all these mysteries !

## Licence

Game code is under MIT licence. All the assets are :copyright: it's original owners, be it SEGA or the fancreators that did them.
