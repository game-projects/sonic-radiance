# Sonic Radiance Credits

Sonic Radiance wouldn't be possible without the work of everybody that either contributed to it directly or indirectly (by posting and letting people user their creation).

You can find all of them in this

## Code

- Kazhnuz

## Game Design et Story

- All people from the Sonic Radiance discord : Kazhnuz, Izaky, Di-Luezzia, Meloe.

## Engine used

- [Love2D](https://love2d.org/)

- [gamecore](https://git.chlore.net/game-projects/gamecore)

## Visuals

Some elements have been modified from the sources used. If you use some elements directly from Radiance, don't forget to search who was the original creator of the asset used. Crediting work is important folks !

### Character Set

- Character set are from Zach Zin Furr

### Tileset

- [Revamped Tiles](https://www.deviantart.com/magiscarf/art/Revamped-Tiles-829482346) by Magiscarf
- [Alienor Tileset v1](https://www.deviantart.com/anarlaurendil/art/Alienor-Tileset-v1-Pokemon-Sacred-Phoenix-834634422) by Amras Anárion, Magiscarf, Chaoticcherrycake, Pixame, Carchagui, Kyle-Dove, Chimcharsfireworkd, princess-phoenix, aveontrainer, Alistair and WesleyFG.

### Backgrounds

- [Sonic Battle](https://www.spriters-resource.com/game_boy_advance/sonicbattle/) levels ripped by Skylight

- "Linear level" mode tiles from [Shadow Shoot](https://www.spriters-resource.com/mobile/shadowshoot/) ripped by Kazhnuz and Mr. C.

- Carnelian Bridge Background based on [Advance Starlight Zone](https://www.spriters-resource.com/custom_edited/sonicthehedgehogcustoms/sheet/18270/) by Skylight

- Tails Lab Background based on [City Escape Advance](https://www.spriters-resource.com/custom_edited/sonicthehedgehogcustoms/sheet/18187/) by Bold the Hedgehog, Blaze, Dan Sidney and Flare.

###

### HUD elements used


- [Sonic Heroes HUD](https://www.spriters-resource.com/custom_edited/sonicthehedgehogcustoms/sheet/104761/) by [FieryExplosion](https://www.spriters-resource.com/submitter/FieryExplosion/)
- [Sonic Adventure 2](https://www.spriters-resource.com/custom_edited/sonicthehedgehogcustoms/sheet/104763/) by [FieryExplosion](https://www.spriters-resource.com/submitter/FieryExplosion/)
- [Sonic Unleashed Mini QTE Prompt](https://www.spriters-resource.com/fullview/150622/) by [FieryExplosion](https://www.spriters-resource.com/submitter/FieryExplosion/)
